package com.lucylhill.projectrune.objects.battle;

public enum BattleEventFlag {
    MARK_PLAYER_TURN,
    MARK_ENEMY_TURN,
    END_BATTLE,
    END_BATTLE_LOST
}
