package com.lucylhill.projectrune.objects.battle.status;

import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StatusEffectList {
    List<StatusEffect> effects = new ArrayList<>();
    List<String> resistances = new ArrayList<>();
    public List<StatusEffect> getEffects() {
        return effects;
    }

    public void handleEndOfTurn() {
        for(int i = 0; i < effects.size(); i++) {
            StatusEffect effect = effects.get(i);
            effect.setTurns(effect.getTurns() - 1);
            if(effect.getTurns() <= 0) {
                effects.remove(effect);
            }
        }
    }

    public void addEffect(StatusEffect effect) {
        int idx = -1;
        if(resistances.contains(effect.getName())) {
            return;
        }
        for(int i = 0; i < effects.size(); i++) {
            if(effects.get(i).getName().equals(effect.getName()) && effects.get(i).getStat() == effect.getStat() && !effect.isStackable()) {
                idx = i;
                break;
            }
        }
        if(idx == -1) {
            effects.add(effect);
        } else {
            effects.set(idx, effect);
        }
    }

    public void removeEffect(String name) {
        int pointer = 0;
        if(effects.size() == 0) {
            return;
        }
        if(name.equals("*")) {
            while(effects.size() > pointer && !effects.get(pointer).isPositive) {
                pointer++;
            }
            effects.remove(pointer);
        }
        effects.removeIf(e -> (e.getName().equals(name) || e.getStat() != null) && e.getStat().name().equals(name));
    }

    public boolean hasFlag(AilmentFlag flag) {
        return effects.stream().anyMatch(e -> e.getAilmentFlag() == flag);
    }

    public boolean hasAilment(String string) {
        return effects.stream().anyMatch(e -> e.getName().equals(string));
    }

    public int getStackOfEffect(String string) {
        return (int) effects.stream().filter(e -> e.getName().equals(string)).count();
    }

    public List<StatusEffect> getAddActionAilments() {
        return effects.stream().filter(e -> e.getStat() == Stat.ADD_ACTION).collect(Collectors.toList());
    }

    public void removeNegativeAilments() {
        effects.removeIf(e -> !e.isPositive);
    }

    public void removeNegativeDebuffs() {
        effects.removeIf(e -> (e.getStat() == Stat.ATTACK ||
                e.getStat() == Stat.DEFENSE ||
                e.getStat() == Stat.CHARISMA ||
                e.getStat() == Stat.INTELLIGENCE ||
                e.getStat() == Stat.AGILITY) && e.getChange() < 0 );
    }

    public void clearStatChanges() {
        effects.removeIf(e -> (e.getStat() == Stat.ATTACK ||
                e.getStat() == Stat.DEFENSE ||
                e.getStat() == Stat.CHARISMA ||
                e.getStat() == Stat.INTELLIGENCE ||
                e.getStat() == Stat.AGILITY));
    }


    public double getStat(Stat stat) {
        return this.getEffects().stream()
                .filter(e -> e.getStat() == stat)
                .map(StatusEffect::getChange)
                .reduce(0.0, Double::sum);
    }

    public double getStat(Stat stat, Aspects aspect) {
        return this.getEffects().stream()
                .filter(e -> e.getStat() == stat && e.getInfo().equals(aspect.name()))
                .map(StatusEffect::getChange)
                .reduce(0.0, Double::sum);
    }

    public List<String> getResistances() {
        return resistances;
    }

    public void setResistances(List<String> resistances) {
        this.resistances = resistances;
    }

    public void removeResistance(String resistance) {
        this.resistances.removeIf(i -> i.equals(resistance));
    }
}
