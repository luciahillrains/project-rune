package com.lucylhill.projectrune.objects.battle.actions;

import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.misc.Operator;

public class ActionTrigger {
    Stat whenStat;
    Aspects whenAspect;
    Operator operator;
    String aspectInfo;
    int statInfo;
    boolean statAndAspect;

    public Stat getWhenStat() {
        return whenStat;
    }

    public void setWhenStat(Stat whenStat) {
        this.whenStat = whenStat;
    }

    public Aspects getWhenAspect() {
        return whenAspect;
    }

    public void setWhenAspect(Aspects whenAspect) {
        this.whenAspect = whenAspect;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public int getStatInfo() {
        return statInfo;
    }

    public void setStatInfo(int info) {
        this.statInfo = info;
    }

    public boolean isStatAndAspect() {
        return statAndAspect;
    }

    public void setStatAndAspect(boolean statAndAspect) {
        this.statAndAspect = statAndAspect;
    }

    public String getAspectInfo() {
        return aspectInfo;
    }

    public void setAspectInfo(String aspectInfo) {
        this.aspectInfo = aspectInfo;
    }
}
