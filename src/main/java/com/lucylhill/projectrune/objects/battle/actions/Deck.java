package com.lucylhill.projectrune.objects.battle.actions;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.battle.status.StatusEffectList;
import com.lucylhill.projectrune.objects.characters.CharacterInfo;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.classes.ClassInfo;
import com.lucylhill.projectrune.objects.items.Equipment;
import com.lucylhill.projectrune.objects.items.Item;
import com.lucylhill.projectrune.services.DebugLogger;
import com.lucylhill.projectrune.services.InternalState;

import java.util.*;
import java.util.stream.Collectors;

public class Deck {
    List<Action> deck;
    List<Action> discard;
    List<Action> secondDeck =  new ArrayList<>();
    static Map<String, Integer> quantities;
    Action reserve;

    DebugLogger logger = new DebugLogger(this.getClass());


    public static int HAND_SIZE = 5;
    public Deck(boolean fullDeck) {
        deck = new ArrayList<>();
        discard = new ArrayList<>();
        boolean setQuantities = false;
        if(quantities == null) {
            quantities = new HashMap<>();
            setQuantities = true;
        }
        List<Action> classActions = new ArrayList<>();
        for(CharacterInfo ch : InternalState.i().partyList().getParty()) {
            for(String acId: ch.getClassInfo().getActions()) {
                Optional<Action> action = RuneConfiguration.i().actions().getActionFromList(acId);
                if(action.isPresent()) {
                    if(action.get().getLevel() <= InternalState.i().level().getLevel()) {
                        Action a = action.get();
                        if(a.getClassId() == null) {
                            a.setClassId(ch.getClassInfo().getId());
                        }
                        if(ch.getActionAlignment() != null) {
                            a.setAlignment(ch.getActionAlignment());
                        }
                        classActions.add(a);
                        if(setQuantities || !quantities.containsKey(a.getId())) {
                            quantities.put(a.getId(), a.getRarity());
                        }
                    }
                }
            }
            for(Action ac : ch.getRelicActions()) {
                if(ac.getClassId() == null) {
                    ac.setClassId("Entity");
                }
                ac.setCharacterName(ch.getName());
                classActions.add(ac);
                if(setQuantities || !quantities.containsKey(ac.getId())) {
                    quantities.put(ac.getId(), ac.getRarity());
                }
            }
            for(Action a : classActions) {
                if(ch.getClassInfo().getActions().contains(a.getId()) || (a.getCharacterName() == null || a.getCharacterName().equals(""))) {
                    a.setCharacterName(ch.getName());
                }
                if(fullDeck && quantities.size() > 0) {
                    for(int i = 0; i < quantities.getOrDefault(a.getId(), 1); i++) {
                        deck.add(a);
                    }
                } else {
                    deck.add(a);
                }

            }
            for(Item i : InternalState.i().inventory().getItems()) {
                if(!i.isKeyItem() && !i.isComponent() && !(i instanceof Equipment)) {
                    Action a = createItemAction(i);
                    deck.add(a);
                }
            }
            deck.addAll(secondDeck);
        }
    }

    public void debugDeck() {
        if(!InternalState.i().configuration().getDebugConfig().getTestAction().isEmpty()) {
            Optional<Action> action = RuneConfiguration.i().actions().getActionFromList(InternalState.i().configuration().getDebugConfig().getTestAction());
            if(action.isPresent()) {
                deck.clear();
                Action a = action.get();
                for(ClassInfo c : RuneConfiguration.i().classList().getClasses()) {
                    if(c.getActions().contains(a.getId()) && a.getClassId() == null) {
                        a.setClassId(c.getName());
                    }
                }
                a.setCharacterName(InternalState.i().partyList().getParty().get(0).getName());
                for(int i = 0; i < 50; i++) {
                    deck.add(action.get());
                }
            } else {
                logger.log("DEBUG ERR: TEST ACTION NOT FOUND !!!");
            }
        }
    }

    public List<Action> getHand() {
        if(deck.size() < HAND_SIZE) {
            deck.addAll(discard);
            discard = new ArrayList<>();
        }
        List<Action> hand = new ArrayList<>();
        Random random = new Random();
        for(int i =0 ; i < HAND_SIZE; i++) {
            Action toAdd = drawCard();
            hand.add(toAdd);
        }
        return hand;
    }

    public void removeCard(String actionId) {
        List<Action> subset = this.deck.stream().filter(a -> a.getId().equals(actionId)).collect(Collectors.toList());
        this.deck.remove(subset.get(0));
    }

    public Action drawCard() {
        List<Action> tempDeck = new ArrayList<>();
        for(CharacterInfo c : InternalState.i().partyList().getParty()) {
            StatusEffectList list = c.getStatusEffects();
            List<StatusEffect> addCardEffects = list.getAddActionAilments();
            for(StatusEffect e : addCardEffects) {
                double amount = e.getChange();
                Optional<Action> action = RuneConfiguration.i().actions().getActionFromList(e.getName());
                if(action.isPresent()) {
                    for(int i = 0; i < amount; i++) {
                        tempDeck.add(action.get());
                    }
                }
            }
        }
        Random random = new Random();
        if(deck.size() == 0) {
            deck.addAll(discard);
            discard = new ArrayList<>();
        }

        Action toAdd;
        int pos = random.nextInt(deck.size() + tempDeck.size());
        if(pos < deck.size()) {
            toAdd = deck.remove(pos);
        } else {
            toAdd = tempDeck.get(pos);
        }
        return toAdd;
    }

    public static void buildQuantities() {
        for(CharacterInfo ch : InternalState.i().partyList().getParty()) {
            for (String acId : ch.getClassInfo().getActions()) {
                Optional<Action> action = RuneConfiguration.i().actions().getActionFromList(acId);
                if (action.isPresent()) {
                    if (action.get().getLevel() <= InternalState.i().level().getLevel()) {
                        Action a = action.get();
                        quantities.put(a.getId(), a.getRarity());
                    }
                }
            }
            for (Action ac : ch.getRelicActions()) {
                if (ac.getClassId() == null) {
                    ac.setClassId("Entity");
                }
                quantities.put(ac.getId(), ac.getRarity());
            }

        }
    }

    public void discardHand(List<Action> hand) {
        discard.addAll(hand);
    }

    public List<Action> getDeck() {
        return deck;
    }

    public void setDeck(List<Action> deck) {
        this.deck = deck;
    }

    public List<Action> getDiscard() {
        return discard;
    }

    public void setDiscard(List<Action> discard) {
        this.discard = discard;
    }

    public Action getReserve() {
        return reserve;
    }

    public void setReserve(Action reserve) {
        this.reserve = reserve;
    }

    public static Map<String, Integer> getQuantities() {
        if(quantities == null) {
            buildQuantities();
        }
        return quantities;
    }

    public static void setQuantities(Map<String, Integer> quantities) {
        Deck.quantities = quantities;
    }

    public List<Action> getSecondDeck() {
        return secondDeck;
    }

    public void setSecondDeck(List<Action> secondDeck) {
        this.secondDeck = secondDeck;
    }

    public Action createItemAction(Item item) {
        Action a = new Action();
        a.setName(item.getName());
        a.setDescription(item.getDescription());
        a.setId(item.getId());
        a.setClassId("Item");
        a.setCharacterName(InternalState.i().partyList().getParty().get(0).getName());
        a.setAlignment(Aspects.NONE);
        a.setTargetType(item.getTargetType());
        a.setItemAction(true);
        return a;
    }
}
