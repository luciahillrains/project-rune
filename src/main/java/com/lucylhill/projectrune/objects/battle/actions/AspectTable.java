package com.lucylhill.projectrune.objects.battle.actions;

import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.classes.ClassAspects;

import java.util.Map;

public class AspectTable {
    Map<ClassAspects, Map<ClassAspects, String>> classAspectTable;
    Map<Aspects, Map<Aspects, String>> aspectTable;

    public Map<ClassAspects, Map<ClassAspects, String>> getClassAspectTable() {
        return classAspectTable;
    }

    public void setClassAspectTable(Map<ClassAspects, Map<ClassAspects, String>> classAspectTable) {
        this.classAspectTable = classAspectTable;
    }

    public Map<Aspects, Map<Aspects, String>> getAspectTable() {
        return aspectTable;
    }

    public void setAspectTable(Map<Aspects, Map<Aspects, String>> aspectTable) {
        this.aspectTable = aspectTable;
    }
}
