package com.lucylhill.projectrune.objects.battle.actions;

public class DeckManagementResult {
    boolean success;
    int originalQuantity;
    int newQuantity;
    int numberOfDuplicates;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getOriginalQuantity() {
        return originalQuantity;
    }

    public void setOriginalQuantity(int originalQuantity) {
        this.originalQuantity = originalQuantity;
    }

    public int getNewQuantity() {
        return newQuantity;
    }

    public void setNewQuantity(int newQuantity) {
        this.newQuantity = newQuantity;
    }

    public int getNumberOfDuplicates() {
        return numberOfDuplicates;
    }

    public void setNumberOfDuplicates(int numberOfDuplicates) {
        this.numberOfDuplicates = numberOfDuplicates;
    }
}
