package com.lucylhill.projectrune.objects.battle.actions;

public class EnemyAction {
    String actionId;
    int weight;
    ActionTrigger trigger;

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public ActionTrigger getTrigger() {
        return trigger;
    }

    public void setTrigger(ActionTrigger trigger) {
        this.trigger = trigger;
    }
}
