package com.lucylhill.projectrune.objects.battle.actions;

import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.classes.ClassAspects;

import java.util.ArrayList;
import java.util.List;

public class ActionResult {
    double hpChange;
    boolean isEnemy;
    boolean actionMissed;
    boolean mortalBlow;
    boolean drawCard;
    List<StatusEffect> addedStatusEffect = new ArrayList<>();
    String text;
    int targetPosition;

    public double getHpChange() {
        return hpChange;
    }

    public void setHpChange(double hpChange) {
        this.hpChange = hpChange;
    }

    public boolean isEnemy() {
        return isEnemy;
    }

    public void setEnemy(boolean enemy) {
        isEnemy = enemy;
    }

    public boolean isActionMissed() {
        return actionMissed;
    }

    public void setActionMissed(boolean actionMissed) {
        this.actionMissed = actionMissed;
    }

    public boolean isMortalBlow() {
        return mortalBlow;
    }

    public void setMortalBlow(boolean mortalBlow) {
        this.mortalBlow = mortalBlow;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<StatusEffect> getAddedStatusEffect() {
        return addedStatusEffect;
    }

    public void setAddedStatusEffect(List<StatusEffect> addedStatusEffect) {
        this.addedStatusEffect = addedStatusEffect;
    }

    public boolean isDrawCard() {
        return drawCard;
    }

    public void setDrawCard(boolean drawCard) {
        this.drawCard = drawCard;
    }

    @Override
    public String toString() {
        return "ActionResult{" +
                "hpChange=" + hpChange +
                ", actionMissed=" + actionMissed +
                ", text=" + text +
                " statusEffects=" + getAddedStatusEffect().toString()+ '}';
    }

    public int getTargetPosition() {
        return targetPosition;
    }

    public void setTargetPosition(int targetPosition) {
        this.targetPosition = targetPosition;
    }
}
