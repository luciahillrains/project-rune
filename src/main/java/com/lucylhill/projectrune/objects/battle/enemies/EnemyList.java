package com.lucylhill.projectrune.objects.battle.enemies;

import java.util.List;
import java.util.stream.Collectors;

public class EnemyList {
    List<Enemy> enemies;

    public List<Enemy> getEnemies() {
        return enemies;
    }

    public void setEnemies(List<Enemy> enemies) {
        this.enemies = enemies;
    }

    public List<Enemy> getEnemiesForDungeon(String dungeonId) {
        return enemies.stream().filter(e -> e.getDungeonId().equals(dungeonId)).collect(Collectors.toList());
    }

    public Enemy getEnemyById(String id) {
        return enemies.stream().filter(e -> e.getId().equals(id)).collect(Collectors.toList()).get(0);
    }
}
