package com.lucylhill.projectrune.objects.battle.actions;

public enum ActionTargetType {
    SINGLE,
    ALL,
    RANDOM1,
    RANDOM2,
    RANDOM3,
    EVERYONE,
    PARTY_SINGLE,
    PARTY_ALL,
    PARTY_RANDOM,
    PARTY_DEAD,
    SELF,
    NONE
}
