package com.lucylhill.projectrune.objects.battle.status;

public enum AilmentFlag {
    AUTO,
    AUTO_HIT_ALL,
    DISABLE,
    ABSOLUTE_SHIELD,
    ENRAGE
}
