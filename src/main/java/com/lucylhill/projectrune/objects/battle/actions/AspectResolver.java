package com.lucylhill.projectrune.objects.battle.actions;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.battle.enemies.BattleEnemy;
import com.lucylhill.projectrune.objects.battle.enemies.Enemy;
import com.lucylhill.projectrune.objects.characters.CharacterInfo;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.classes.ClassAspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AspectResolver {
    AspectTable aspectTable;

    public AspectResolver() {
        aspectTable = RuneConfiguration.i().aspectTable();
    }

    public AspectVulnerability resolveAspect(EntityInfo target, EntityInfo source, Aspects skillAspect) {
        int count = 0;
        String t = aspectTable.getClassAspectTable().get(target.getClassAlignment()).get(source.getClassAlignment().toString());
        count = count + (t.equals("O") ? -1 : t.equals("X") ? 1 : 0);
        String x = aspectTable.getAspectTable().get(target.getAlignment()).get(skillAspect.toString());
        count = count + (x.equals("O") ? -1 : t.equals("X") ? 1 : 0);
        switch(count) {
            case -2: return AspectVulnerability.VERY_STRONG;
            case -1: return AspectVulnerability.STRONG;
            case 0: return AspectVulnerability.NEUTRAL;
            case 1: return AspectVulnerability.WEAK;
            case 2: return AspectVulnerability.VERY_WEAK;
        }
        return AspectVulnerability.NEUTRAL;
    }


}
