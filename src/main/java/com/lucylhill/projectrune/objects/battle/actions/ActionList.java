package com.lucylhill.projectrune.objects.battle.actions;

import java.util.List;
import java.util.Optional;

public class ActionList {
    List<Action> actions;

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public Optional<Action> getActionFromList(String actionId) {
        Optional<Action> action = Optional.empty();
        for(Action a : actions) {
            if(a.getId().equals(actionId)) {
                action = Optional.of(a);
                break;
            }
        }
        return action;
    }
}
