package com.lucylhill.projectrune.objects.battle.actions;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.battle.BattleState;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.objects.items.Item;
import com.lucylhill.projectrune.services.DebugLogger;
import javafx.scene.control.Alert;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;
import java.util.Random;

public class ActionEffectResolver {
    AspectResolver aspectResolver = new AspectResolver();
    Random random = new Random();
    int stealingModifier = 1;
    DebugLogger logger = new DebugLogger(this.getClass());

    public int getStealingModifier() {
        return stealingModifier;
    }

    public void setStealingModifier(int stealingModifier) {
        this.stealingModifier = stealingModifier;
    }

    public ActionResult resolveAction(Action action, EntityInfo target, EntityInfo source) {
        String classId = action.getClassId();
        if(classId == null) {
            classId = "Enemy";
        }
        ActionResult result = null;
        try {
            Class classEffect = Class.forName("com.lucylhill.projectrune.objects.classes.classSkills."+classId);
            Method method = classEffect.getDeclaredMethod(action.getId()+"Effect", EntityInfo.class, EntityInfo.class);
            result = (ActionResult) method.invoke(classEffect.getConstructor().newInstance(), target, source);
        } catch(NoSuchMethodException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("E001: No such method found for "+action.getId());
            alert.show();
            logger.log("NO SUCH METHOD "+action.getId()+"Effect");
            e.printStackTrace();
        } catch (InvocationTargetException | IllegalAccessException | ClassNotFoundException | InstantiationException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("E002: Misc error for "+action.getId());
            alert.show();
            e.printStackTrace();
        }
        if(result != null && target.getStatusEffects().hasAilment("Adrenaline Rush")) {
            if(result.getHpChange() < 0) {
                target.setHp((int)(target.getHp() + ((result.getHpChange()) * -1)));
                result.setHpChange(0.0);
            }
        }
        if(result != null && !result.isActionMissed() && source.getName().equals("Chaoxing")) {
            int hpGain = (int)(target.getMaxHp() * .03);
            target.setHp(hpGain);
            result.setHpChange(hpGain);
        }
        BattleState.i().getPreviousActions().add(action);
        target.setLastSkillHit(action);
        return result;
    }

    public int calculateDamage(int initialDamage, EntityInfo target, EntityInfo source, Aspects aspect) {
        int damage = initialDamage;
        damage = (int)(damage * source.getStat(Stat.ELEMENTAL_OFFENSES, aspect));
        damage = (int)(damage * target.getStat(Stat.ELEMENTAL_DEFENSES, aspect));
        AspectVulnerability vuln = aspectResolver.resolveAspect(target, source, aspect);
        damage = (int)(damage * getAspectVulnerabilities(vuln));
        if(source.getAlignment() == aspect) {
            damage = (int)(damage * 1.2);
        }
        return damage;
    }

    public int calculateDamage(int initialDamage, EntityInfo target, EntityInfo source, Aspects aspect, Aspects aspect2) {
        int damage = calculateDamage(initialDamage, target, source, aspect);
        if(source.getOffenses().containsKey(aspect2)) {
            damage = (int)(damage * source.getOffenses().get(aspect2));
        }
        if(target.getDefenses().containsKey(aspect2)) {
            damage = (int)(damage * source.getDefenses().get(aspect2));
        }
        if(source.getAlignment() == aspect2) {
            damage = (int)(damage * 1.2);
        }

        return damage;
    }

    public double getAspectVulnerabilities(AspectVulnerability vuln) {
        double effect = 1.0;
        switch(vuln) {
           case WEAK -> effect =  1.4;
            case VERY_WEAK -> effect = 1.8;
           case STRONG -> effect = 0.8;
            case VERY_STRONG -> effect = 0.5;
        }
      return effect;
    }

    public boolean didAttackHit(int hitChance) {
        int hit = random.nextInt(100);
        return hit < hitChance;
    }

    public boolean didAttackHit(int hitChance, EntityInfo target, EntityInfo source) {
        int hit = random.nextInt(120);
        hitChance = (int)(hitChance * source.getHitChanceModifier());
        hit = hit + (target.getAgility()/2);
        return hit <= hitChance;
    }

    public ActionResult basicAttack(int initialDamage, EntityInfo target, EntityInfo source, Aspects aspect, int hitChance) {
        ActionResult result = new ActionResult();
        int hit = (int)(hitChance * source.getHitChanceModifier());
        if(didAttackHit(hit, target, source)) {
            int damage = calculateDamage(initialDamage, target, source, aspect);
            if(damage < 0) {
                damage = 1;
            }
            target.setHp(target.getHp() - damage);
            result.setHpChange(damage * -1);
        } else {
            result.setActionMissed(true);
            result.setText(source.getName() + " attacked but missed!");
        }
        return result;
    }

    public ActionResult basicAttack(int initialDamage, EntityInfo target, EntityInfo source, Aspects aspect, Aspects aspect2, int hitChance) {
        ActionResult result = new ActionResult();
        if(didAttackHit(hitChance, target, source)) {
            int damage = calculateDamage(initialDamage, target, source, aspect, aspect2);
            target.setHp(target.getHp() - damage);
            result.setHpChange(damage * -1);
        } else {
            result.setActionMissed(true);
        }
        return result;
    }

    public boolean combo(String actionId, String currentId) {
        boolean validCombo = false;
        for(Action a : BattleState.i().getPreviousActions()) {
            if(a.getId().equals(actionId)) {
                validCombo = true;
            }
            if(validCombo && a.getId().equals(currentId)) {
                validCombo = false;
            }
        }
        return validCombo;
    }

    public int physicalDamage(EntityInfo target, EntityInfo source, int baseDamage) {
        return physicalPiercingDamage(source, baseDamage) - target.getDefense();
    }

    public int physicalPiercingDamage(EntityInfo source, int baseDamage) {
        return (baseDamage * source.getAttack());
    }

    public int magicalDamage(EntityInfo target, EntityInfo source, int baseDamage) {
        return magicalPiercingDamage(source, baseDamage) - target.getDefense();
    }

    public int magicalPiercingDamage(EntityInfo source, int baseDamage) {
        return baseDamage * (source.getIntelligence() + source.getAttack() /2);
    }

    public int magicalPhysicalDamage(EntityInfo target, EntityInfo source, int baseDamage) {
        return magicalPhysicalPiercingDamage(source, baseDamage) - target.getDefense();
    }

    public int magicalPhysicalPiercingDamage(EntityInfo source, int baseDamage) {
        return baseDamage * (source.getAttack() + source.getIntelligence()/2);
    }

    public Item getItemFromId(String itemId) {
        String[] split = itemId.split(":");
        Optional<Item> item = Optional.empty();
        if(split[0].equals("Item")) {
            item =  RuneConfiguration.i().items().getItemById(split[1]);
        }
        if(split[0].equals("Equipment")) {
            item = RuneConfiguration.i().equipment().getItemById(split[1]);
        }
        if(split[0].equals("Relic")) {
            item = RuneConfiguration.i().relics().getItemById(split[1]);
        }
        return item.orElse(null);
    }

    public Item steal(EntityInfo target, EntityInfo source) {
        int base = stealingModifier * (100 - source.getAgility());
        int commonBoundary = source.getAgility() + 50;
        int rareBoundary = source.getAgility() + 10;
        int hit = random.nextInt(base + commonBoundary + rareBoundary);
        if(hit < base) {
            return null;
        } else if(hit < commonBoundary) {
            stealingModifier++;
           return getItemFromId(target.getCommonItemId());
        } else {
            stealingModifier++;
            return getItemFromId(target.getRareItemId());
        }
    }

}
