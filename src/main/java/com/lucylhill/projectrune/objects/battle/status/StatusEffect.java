package com.lucylhill.projectrune.objects.battle.status;

import com.lucylhill.projectrune.objects.characters.Stat;

public class StatusEffect {
    String name;
    Stat stat;
    double change;
    int turns;
    AilmentFlag ailmentFlag;
    String icon = "";
    String info;
    boolean isStackable = false;
    boolean global = false;
    boolean isNamedAilment = false;
    boolean isPositive = false;

    public StatusEffect() { }

    public StatusEffect(String name, Stat stat, double change, int turns) {
        this.name = name;
        this.stat = stat;
        this.change = change;
        this.turns = turns;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Stat getStat() {
        return stat;
    }

    public void setStat(Stat stat) {
        this.stat = stat;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public int getTurns() {
        return turns;
    }

    public void setTurns(int turns) {
        this.turns = turns;
    }

    public boolean getIsNamedAilment() {
        return isNamedAilment;
    }

    public void setIsNamedAilment(boolean namedAilment) {
        isNamedAilment = namedAilment;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public AilmentFlag getAilmentFlag() {
        return ailmentFlag;
    }

    public void setAilmentFlag(AilmentFlag ailmentFlag) {
        this.ailmentFlag = ailmentFlag;
    }

    public boolean isGlobal() {
        return global;
    }

    public void setGlobal(boolean global) {
        this.global = global;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public boolean isStackable() {
        return isStackable;
    }

    public void setStackable(boolean stackable) {
        isStackable = stackable;
    }

    public boolean isPositive() {
        return isPositive;
    }

    public void setPositive(boolean positive) {
        isPositive = positive;
    }

    @Override
    public String toString() {
        return "StatusEffect{" +
                "name='" + name + '\'' +
                ", stat=" + stat +
                ", change=" + change +
                ", isNamedAilment=" + isNamedAilment +
                '}';
    }
}
