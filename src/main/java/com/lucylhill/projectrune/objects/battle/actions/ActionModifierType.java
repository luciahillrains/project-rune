package com.lucylhill.projectrune.objects.battle.actions;

public enum ActionModifierType {
    MUST_USE,
    AUTO_USE,
    REDRAW,
    DRAW,
    DISCARD,
    EQUIPPED,
    STEAL,
    MULTISTRIKE,
    AUTO,
    MODIFIER_TURN_LIMIT,
    COMBO_ATTACK,
    TURN_LIMIT,
    CURE_AILMENT,
    IF,
    THEN,
    SPECIAL,
    REPEAT,
    ENABLED_ONLY,
    DISABLE_ALIGNMENT,
    PARTY_MEMBER_DIED,
    FLAG
}
