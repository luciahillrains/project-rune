package com.lucylhill.projectrune.objects.battle.enemies;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.battle.actions.Action;
import com.lucylhill.projectrune.objects.battle.actions.ActionTrigger;
import com.lucylhill.projectrune.objects.battle.actions.EnemyAction;
import com.lucylhill.projectrune.objects.battle.status.AilmentFlag;
import com.lucylhill.projectrune.objects.battle.status.StatusEffectList;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.objects.misc.Operator;
import com.lucylhill.projectrune.services.InternalState;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BattleEnemy extends EntityInfo {
    Enemy enemy;

    public BattleEnemy(Enemy enemy) {
        this.enemy = enemy;
        super.setMaxHp(enemy.getMaxHp());
        super.setHp(enemy.getMaxHp());
        super.setAttack(enemy.getAttack());
        super.setDefense(enemy.getDefense());
        super.setName(enemy.getName());
        super.setClassAlignment(enemy.getClassAlignment());
        super.setAlignment(enemy.getAlignment());
        super.setCommonItemId(enemy.getCommonItemId());
        super.setRareItemId(enemy.getRareItemId());
        super.setMorphItemId(enemy.getMorphItemId());
    }

    public Enemy getEnemy() {
        return enemy;
    }

    public void setEnemy(Enemy enemy) {
        this.enemy = enemy;
    }

    public int getCurrentWeightAmount() {
        int weight = 0;
        for(EnemyAction ea : this.getValidEnemyActions()) {
            weight = weight + ea.getWeight();
        }
        return weight;
    }

    public List<EnemyAction> getValidEnemyActions() {
        List<EnemyAction> newList = new ArrayList<>();
        List<Aspects> disabledAspects = InternalState.i().getListOfAspectsDisabled();
        for(EnemyAction ea : this.enemy.getActions()) {
            Optional<Action> a = RuneConfiguration.i().actions().getActionFromList(ea.getActionId());
            if(a.isPresent()) {
                if(disabledAspects.contains(a.get().getAlignment())) {
                    continue;
                }
            }
            if(ea.getTrigger() != null) {
                if(isTriggered(ea.getTrigger())){
                    newList.add(ea);
                }
            } else {
                newList.add(ea);
            }
        }
        if(newList.size() == 0) {
            EnemyAction wait = new EnemyAction();
            wait.setActionId("waitAndSee");
            newList.add(wait);
        }
        return newList;
    }

    public boolean isTriggered(ActionTrigger trigger) {
        boolean aspectTriggered = false;
        boolean statTriggered = false;
        if(trigger.getWhenAspect() != null) {
            aspectTriggered = trigger.getWhenAspect() == InternalState.i().getLastUsedAspect();
        }
        if(trigger.getWhenStat() != null) {
            int field = 0;
            if(trigger.getWhenStat() == Stat.HP) {
                field = this.getHp();
            }
            if(trigger.getWhenStat() == Stat.ATTACK) {
                field = this.enemy.getAttack();
            }
            if(trigger.getWhenStat() == Stat.DEFENSE) {
                field = this.enemy.getDefense();
            }
            int target = trigger.getStatInfo();
            if(trigger.getOperator() == Operator.LESS_THAN) {
                statTriggered = field < target;
            }
            if(trigger.getOperator() == Operator.LESS_THAN_EQUALS) {
                statTriggered = field <= target;
            }
            if(trigger.getOperator() == Operator.EQUALS) {
                statTriggered = field == target;
            }
            if(trigger.getOperator() == Operator.GREATER_THAN) {
                statTriggered = field > target;
            }
            if(trigger.getOperator() == Operator.GREATER_THAN_EQUALS) {
                statTriggered = field >= target;
            }
            if(trigger.getOperator() != Operator.NOT_EQUALS) {
                statTriggered = field != target;
            }
        }
        if(trigger.isStatAndAspect()) {
            return aspectTriggered && statTriggered;
        }

        return (aspectTriggered && trigger.getWhenAspect() != null) || (statTriggered && trigger.getWhenStat() != null);

    }

    public String getActionForWeight(int weight) {
        String ret = "";
        int checkWeight = 0;
        for(EnemyAction e : this.getValidEnemyActions()) {
            if(weight < checkWeight + e.getWeight()) {
                ret = e.getActionId();
                break;
            }
            checkWeight = checkWeight + e.getWeight();
        }
        return ret;
    }

    @Override
    public String toString() {
        return "BattleEnemy{" +
                "nane=" + enemy.getName() +
                "hp=" + getHp() +
                '}';
    }
}
