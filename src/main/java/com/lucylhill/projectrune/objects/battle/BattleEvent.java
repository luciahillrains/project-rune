package com.lucylhill.projectrune.objects.battle;

import com.lucylhill.projectrune.objects.battle.actions.ActionResult;

public class BattleEvent {
    ActionResult result;
    String text;
    BattleEventFlag flag;

    public BattleEvent(String text) {
        this.text = text;
    }

    public BattleEvent(ActionResult result) {
        this.result = result;
    }

    public ActionResult getResult() {
        return result;
    }

    public void setResult(ActionResult result) {
        this.result = result;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public BattleEventFlag getFlag() {
        return flag;
    }

    public void setFlag(BattleEventFlag flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "BattleEvent{" +
                "result=" + result +
                ", text='" + text + '\'' +
                '}';
    }
}
