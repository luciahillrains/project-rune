package com.lucylhill.projectrune.objects.battle.status;

import java.util.ArrayList;
import java.util.List;

public class AilmentList {
    List<StatusEffect> ailments = new ArrayList<>();

    public StatusEffect getStatusEffect(String name) {
        StatusEffect effect = null;
        for(StatusEffect e : ailments) {
            if(e.getName().equals(name)) {
                effect = e;
                break;
            }
        }
        return effect;
    }

    public List<StatusEffect> getAilments() {
        return ailments;
    }

    public void setAilments(List<StatusEffect> ailments) {
        this.ailments = ailments;
    }
}
