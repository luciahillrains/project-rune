package com.lucylhill.projectrune.objects.battle.enemies;

import com.lucylhill.projectrune.objects.battle.actions.ActionTrigger;
import com.lucylhill.projectrune.objects.battle.actions.EnemyAction;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.classes.ClassAspects;
import com.lucylhill.projectrune.services.InternalState;

import java.util.ArrayList;
import java.util.List;

public class Enemy {
    private String name;
    private String id;
    private String dungeonId;
    private String picture;
    private int maxHp;
    private int attack;
    private int defense;
    private List<EnemyAction> actions;
    private ClassAspects classAlignment;
    private Aspects alignment;
    private boolean heavy = false;
    private boolean finalBoss = false;
    String commonItemId = "";
    String rareItemId = "";
    String morphItemId = "";
    int exp = 0;
    int gold = 0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDungeonId() {
        return dungeonId;
    }

    public void setDungeonId(String dungeonId) {
        this.dungeonId = dungeonId;
    }

    public int getMaxHp() {
        return maxHp;
    }

    public void setMaxHp(int maxHp) {
        this.maxHp = maxHp;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public List<EnemyAction> getActions() {
        return actions;
    }

    public void setActions(List<EnemyAction> actions) {
        this.actions = actions;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public ClassAspects getClassAlignment() {
        return classAlignment;
    }

    public void setClassAlignment(ClassAspects classAlignment) {
        this.classAlignment = classAlignment;
    }

    public Aspects getAlignment() {
        return alignment;
    }

    public void setAlignment(Aspects alignment) {
        this.alignment = alignment;
    }

    public boolean isHeavy() {
        return heavy;
    }

    public void setHeavy(boolean heavy) {
        this.heavy = heavy;
    }

    public String getCommonItemId() {
        return commonItemId;
    }

    public void setCommonItemId(String commonItemId) {
        this.commonItemId = commonItemId;
    }

    public String getRareItemId() {
        return rareItemId;
    }

    public void setRareItemId(String rareItemId) {
        this.rareItemId = rareItemId;
    }

    public String getMorphItemId() {
        return morphItemId;
    }

    public void setMorphItemId(String morphItemId) {
        this.morphItemId = morphItemId;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public boolean isFinalBoss() {
        return finalBoss;
    }

    public void setFinalBoss(boolean finalBoss) {
        this.finalBoss = finalBoss;
    }
}
