package com.lucylhill.projectrune.objects.battle.actions;

import com.lucylhill.projectrune.objects.classes.Aspects;

import java.util.ArrayList;
import java.util.List;

public class Action implements Cloneable {
    String name;
    String id;
    String description;
    ActionTargetType targetType;
    Aspects alignment;
    int apCost;
    String comboOf;
    int level;
    int rarity;
    String characterName;
    String classId;
    boolean mustPlay;
    boolean isItemAction = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Aspects getAlignment() {
        return alignment;
    }

    public void setAlignment(Aspects alignment) {
        this.alignment = alignment;
    }

    public int getApCost() {
        return apCost;
    }

    public void setApCost(int apCost) {
        this.apCost = apCost;
    }

    public String getComboOf() {
        return comboOf;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getRarity() {
        return rarity;
    }

    public void setRarity(int rarity) {
        this.rarity = rarity;
    }

    public ActionTargetType getTargetType() {
        return targetType;
    }

    public void setTargetType(ActionTargetType targetType) {
        this.targetType = targetType;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public boolean isMustPlay() {
        return mustPlay;
    }

    public void setMustPlay(boolean mustPlay) {
        this.mustPlay = mustPlay;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public boolean isItemAction() {
        return isItemAction;
    }

    public void setItemAction(boolean itemAction) {
        isItemAction = itemAction;
    }

    @Override
    public String toString() {
        return "Action:" + name + " ("+characterName+")";
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
