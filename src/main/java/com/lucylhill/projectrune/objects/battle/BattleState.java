package com.lucylhill.projectrune.objects.battle;

import com.lucylhill.projectrune.objects.battle.actions.Action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BattleState {
    static BattleState i;
    List<Action> previousActions = new ArrayList<>();
    Map<String, String> enmityMap = new HashMap<>();
    int targetId = -1;
    boolean isEnemy;

    public static BattleState i() {
        if(i == null) {
            i = new BattleState();
        }
        return i;
    }

    public static BattleState getI() {
        return i;
    }

    public static void setI(BattleState i) {
        BattleState.i = i;
    }

    public List<Action> getPreviousActions() {
        return previousActions;
    }

    public void setPreviousActions(List<Action> previousActions) {
        this.previousActions = previousActions;
    }

    public int getTargetId() {
        return targetId;
    }

    public void setTargetId(int targetId) {
        this.targetId = targetId;
    }

    public boolean isEnemy() {
        return isEnemy;
    }

    public void setEnemy(boolean enemy) {
        isEnemy = enemy;
    }

    public Map<String, String> getEnmityMap() {
        return enmityMap;
    }

    public void setEnmityMap(Map<String, String> enmityMap) {
        this.enmityMap = enmityMap;
    }
}
