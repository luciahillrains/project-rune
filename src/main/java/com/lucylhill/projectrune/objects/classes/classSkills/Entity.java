package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;

public class Entity {

    ActionEffectResolver resolver = new ActionEffectResolver();
    public ActionResult BreakdownEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        int baseDamage = 10;
        Aspects aspect = Aspects.NONE;
        if(target.isMechanical()) {
            result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect,  100);
            result.setText(source.getName() + " forced "+target.getName() + " to break down!");
        } else {
            result.setText(source.getName() + " stumbles!");
        }
        return result;
    }
    
    public ActionResult RubyGazeEffect(EntityInfo target, EntityInfo source) {
        ActionResult result;
        int baseDamage = 4;
        Aspects aspect = Aspects.FIRE;
        result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, 90);
        result.setText(source.getName() + " used Ruby Gaze!");    
        return result;
    }

    public ActionResult SapphireGazeEffect(EntityInfo target, EntityInfo source) {
        ActionResult result;
        int baseDamage = 4;
        Aspects aspect = Aspects.ICE;
        result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, 90);
        result.setText(source.getName() + " used Sapphire Gaze!");
        return result;
    }

    public ActionResult LapisGazeEffect(EntityInfo target, EntityInfo source) {
        ActionResult result;
        int baseDamage = 4;
        Aspects aspect = Aspects.WATER;
        result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, 90);
        result.setText(source.getName() + " used Lapis Gaze!");
        return result;
    }

    public ActionResult EmeraldGazeEffect(EntityInfo target, EntityInfo source) {
        ActionResult result;
        int baseDamage = 4;
        Aspects aspect = Aspects.AIR;
        result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, 90);
        result.setText(source.getName() + " used Emerald Gaze!");
        return result;
    }

    public ActionResult TopazGazeEffect(EntityInfo target, EntityInfo source) {
        ActionResult result;
        int baseDamage = 4;
        Aspects aspect = Aspects.EARTH;
        result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, 90);
        result.setText(source.getName() + " used Topaz Gaze!");
        return result;
    }

    public ActionResult OnyxGazeEffect(EntityInfo target, EntityInfo source) {
        ActionResult result;
        int baseDamage = 4;
        Aspects aspect = Aspects.DARK;
        result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, 90);
        result.setText(source.getName() + " used Onyx Gaze!");
        return result;
    }

    public ActionResult AgateGazeEffect(EntityInfo target, EntityInfo source) {
        ActionResult result;
        int baseDamage = 4;
        Aspects aspect = Aspects.HOLY;
        result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, 90);
        result.setText(source.getName() + " used Agate Gaze!");
        return result;
    }

    public ActionResult QuickRecoverEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        int hpChange = (int)(target.getMaxHp() * 0.05);
        target.setHp(target.getHp() + hpChange);
        result.setHpChange(hpChange);
        result.setText(source.getName() + " used Quick Recover!");
        return result;
    }
    
    
    

}
