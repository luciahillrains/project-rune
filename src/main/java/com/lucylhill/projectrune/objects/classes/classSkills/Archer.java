package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.objects.items.EquipmentType;
import com.lucylhill.projectrune.objects.passives.PassiveEffectResolver;

import java.util.Random;

public class Archer {
    PassiveEffectResolver passiveEffectResolver = new PassiveEffectResolver();
    ActionEffectResolver resolver = new ActionEffectResolver();
    Random random = new Random();

    public boolean BowMasteryEffect(EntityInfo source) {
        return true;
    }

    public boolean BetterArrowsEffect(EntityInfo source) {
        if(passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.BOW)) {
            source.addAdditionalAttack(3);
        }
        return true;
    }

    public boolean DoubleShotEffect(EntityInfo source) {
        return true;
    }

    public ActionResult NormalShotEffect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.AIR;
        ActionResult result = new ActionResult();
        if(!passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.BOW)) {
            result.setText(source.getName() + " doesn't have a bow to use this action with!");
            return result;
        }
        result.setText(source.getName() + " shot an arrow at "+target.getName()+" and missed!");
        if(random.nextInt(100) <= 80) {
            int hit = random.nextInt(100);
            int baseDamage;
            if(hit <= 30) {
                baseDamage = 2;
            }
            else if ( hit < 80) {
                baseDamage = 3;
            }
            else if ( hit < 95) {
                baseDamage = 5;
            } else  {
                baseDamage = 8;
            }
            if(random.nextInt(100) <= 10) {
                baseDamage = baseDamage + (int)(baseDamage * 1.5);
            }
            ActionResult subresult = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, 100);
            result.setHpChange(subresult.getHpChange());
            result.setText(source.getName() + " shot an arrow at "+target.getName()+" and hit!");
            if(source.getPassives().hasPassive("DoubleShot")) {
                if(random.nextInt(100) <= 50) {
                    hit = random.nextInt(100);
                    int baseDamage2;
                    if(hit <= 30) {
                        baseDamage2 = 2;
                    }
                    else if (hit < 80) {
                        baseDamage2 = 3;
                    }
                    else if (hit < 95) {
                        baseDamage2 = 5;
                    } else  {
                        baseDamage2 = 8;
                    }
                    if(random.nextInt(100) <= 10) {
                        baseDamage2 = baseDamage2 + (int)(baseDamage2 * 1.5);
                    }
                    ActionResult supersubResult = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage2), target, source, aspect, 100);
                    result.setHpChange(subresult.getHpChange() + supersubResult.getHpChange());
                    result.setText(source.getName() + " shot an arrow at "+target.getName()+" and it hit twice!");

                }
            }

        } else {
            result.setActionMissed(true);
        }
        return result;
    }

    public ActionResult FireyShotEffect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.FIRE;
        ActionResult result = new ActionResult();
        if(!passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.BOW)) {
            result.setText(source.getName() + " doesn't have a bow to use this action with!");
            return result;
        }
        int baseDamage = 3;
        result.setText(source.getName() + " shot a firey arrow  at "+target.getName()+" and missed!");
        if(random.nextInt(100) <= 80) {
            if(random.nextInt(100) <= 10) {
                baseDamage = baseDamage + (int) (baseDamage * 1.5);
            }
            result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, 100);
            result.setText(source.getName() + " shot a firey arrow at "+target.getName()+"!");
            if(random.nextInt(100) <= 30) {
                StatusEffect effect = new StatusEffect("Firey Shot", Stat.HP, 3 * source.getAttack(), 3 );
                target.getStatusEffects().addEffect(effect);
                result.getAddedStatusEffect().add(effect);
            }
            if(random.nextInt(100) <= 60) {
                baseDamage = 3;
                if(random.nextInt(100) <= 10) {
                    baseDamage = baseDamage + (int) (baseDamage * 1.5);
                }
                ActionResult subresult = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, 100);
                result.setHpChange(result.getHpChange() + subresult.getHpChange());
                if(random.nextInt(100) <= 30) {
                    StatusEffect effect = new StatusEffect("Another Firey Shot", Stat.HP, 3 * source.getAttack(), 3 );
                    target.getStatusEffects().addEffect(effect);
                    result.getAddedStatusEffect().add(effect);
                }
                result.setText(source.getName() + " shot a firey arrow at "+target.getName()+" and it hit twice!");
            }
        } else {
            result.setActionMissed(true);
        }
        return result;
    }

    public ActionResult FanShotEffect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.AIR;
        ActionResult result = new ActionResult();
        if(!passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.BOW)) {
            result.setText(source.getName() + " doesn't have a bow to use this action with!");
            return result;
        }
        if (random.nextInt(100) <= 80) {
            int baseDamage = 2;
            int hit = random.nextInt(100);
            if (hit >= 50 && hit < 90) {
                baseDamage = 3;
            } else if (hit >= 90) {
                baseDamage = 5;
            }
            result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, 100);
        } else {
            result.setActionMissed(true);
        }
        result.setText(source.getName() + " shot an array of arrows!");
        return result;
    }

    public ActionResult VenomShotEffect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.FIRE;
        ActionResult result = new ActionResult();
        if(!passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.BOW)) {
            result.setText(source.getName() + " doesn't have a bow to use this action with!");
            return result;
        }
        int baseDamage = 4;
        result.setText(source.getName() + " shot a poison arrow  at "+target.getName()+" and missed!");
        if(random.nextInt(100) <= 80) {
            if(random.nextInt(100) <= 10) {
                baseDamage = baseDamage + (int) (baseDamage * 1.5);
            }
            result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, 100);
            result.setText(source.getName() + " shot a poison arrow at "+target.getName()+"!");
            if(random.nextInt(100) <= 30) {
                StatusEffect effect = RuneConfiguration.i().ailmentList().getStatusEffect("Poison");
                target.getStatusEffects().addEffect(effect);
                result.getAddedStatusEffect().add(effect);
            }
            if(random.nextInt(100) <= 60) {
                baseDamage = 3;
                if(random.nextInt(100) <= 10) {
                    baseDamage = baseDamage + (int) (baseDamage * 1.5);
                }
                ActionResult subresult = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, 100);
                result.setHpChange(result.getHpChange() + subresult.getHpChange());
                if(random.nextInt(100) <= 30) {
                    StatusEffect effect = RuneConfiguration.i().ailmentList().getStatusEffect("Poison");
                    effect.setName("Double Poison");
                    target.getStatusEffects().addEffect(effect);
                    result.getAddedStatusEffect().add(effect);
                }
                result.setText(source.getName() + " shot a poison arrow at "+target.getName()+" and it hit twice!");
            }
        } else {
            result.setActionMissed(true);
        }
        return result;
    }

    public ActionResult QuickShotEffect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.AIR;
        ActionResult result = new ActionResult();
        if(!passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.BOW)) {
            result.setText(source.getName() + " doesn't have a bow to use this action with!");
            return result;
        }
        result.setText(source.getName() + " shot an arrow at "+target.getName()+" and missed!");
        if(random.nextInt(100) <= 80) {
            int hit = random.nextInt(100);
            int baseDamage = 2;
            if (hit > 30 && hit < 80) {
                baseDamage = 3;
            }
            else if (hit >= 80) {
                baseDamage = 4;
            }
            if(random.nextInt(100) <= 10) {
                baseDamage = baseDamage + (int)(baseDamage * 1.5);
            }
            ActionResult subresult = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, 100);
            result.setHpChange(subresult.getHpChange());
            result.setText(source.getName() + " shot an arrow at "+target.getName()+" and hit!");
            if(source.getPassives().hasPassive("DoubleShot")) {
                if(random.nextInt(100) <= 50) {
                    hit = random.nextInt(100);
                    int baseDamage2 = 2;
                    if (hit > 30 && hit < 80) {
                        baseDamage2 = 3;
                    }
                    else if (hit >= 80) {
                        baseDamage2 = 4;
                    }
                    if(random.nextInt(100) <= 10) {
                        baseDamage2 = baseDamage2 + (int)(baseDamage2 * 1.5);
                    }
                    ActionResult supersubResult = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage2), target, source, aspect, 100);
                    result.setHpChange(subresult.getHpChange() + supersubResult.getHpChange());
                    result.setText(source.getName() + " shot an arrow at "+target.getName()+" and it hit twice!");
                }
            }

        } else {
            result.setActionMissed(true);
        }
        return result;
    }

    public ActionResult ExplosiveShotEffect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.FIRE;
        ActionResult result = new ActionResult();
        if(!passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.BOW)) {
            result.setText(source.getName() + " doesn't have a bow to use this action with!");
            return result;
        }
        int baseDamage = 6;
        result.setText(source.getName() + " shot a firey arrow  at "+target.getName()+" and missed!");
        if(random.nextInt(100) <= 80) {
            if(random.nextInt(100) <= 10) {
                baseDamage = baseDamage + (int) (baseDamage * 1.5);
            }
            result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, 100);
            result.setText(source.getName() + " shot a firey arrow at "+target.getName()+"!");
            if(random.nextInt(100) <= 30) {
                StatusEffect effect = new StatusEffect("Explosive Shot", Stat.HP, 4 * source.getAttack(), 3 );
                target.getStatusEffects().addEffect(effect);
                result.getAddedStatusEffect().add(effect);
            }
        } else {
            result.setActionMissed(true);
        }
        return result;
    }
}
