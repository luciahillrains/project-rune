package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.objects.battle.BattleState;
import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.objects.items.EquipmentType;
import com.lucylhill.projectrune.objects.items.Item;
import com.lucylhill.projectrune.objects.passives.PassiveEffectResolver;
import com.lucylhill.projectrune.services.DebugLogger;
import com.lucylhill.projectrune.services.InternalState;

public class Pirate {
    ActionEffectResolver resolver = new ActionEffectResolver();
    PassiveEffectResolver passiveEffectResolver = new PassiveEffectResolver();
    DebugLogger logger = new DebugLogger(this.getClass());
    public boolean HiddenSidearmEffect(EntityInfo source) {
        source.addAdditionalAttack(1);
        return true;
    }
    public boolean EnhancedPlunderEffect(EntityInfo source) {
        return true;
    }
    public boolean BelligerentEffect(EntityInfo source) {
        return true;
    }

    public ActionResult PirateStrikeEffect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.WATER;
        int baseDamage = 4;
        if(passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.SWORD)) {
            baseDamage = 6;
        }
        ActionResult result = resolver.basicAttack(((baseDamage * source.getAttack()) - (target.getDefense()/2)), target, source, aspect, 95);
        result.setText(source.getName() + " attacks!");
        if(!result.isActionMissed()) {
            if(source.getPassives().hasPassive("Belligerent")) {
                BattleState.i().getEnmityMap().put(target.getName() + target.hashCode(), source.getName());
            }
        }
        return result;
    }

    public ActionResult PlunderEffect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.WATER;
        int baseDamage = 8;
        int plunderChance = 15;
        if(source.getPassives().hasPassive("EnhancedPlunder")) {
            plunderChance = plunderChance + (InternalState.i().level().getLevel() / 3);
            if(plunderChance > 30) {
                plunderChance = 30;
            }
            baseDamage = 10;
        }
        ActionResult result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, plunderChance);
        if(result.isActionMissed()) {
            result.setText(source.getName() + " wasn't able to plunder!");
            return result;
        } else {
            String txt = source.getName() + " attacked for the plunder!";
                Item item = resolver.steal(target, source);
                if(item != null) {
                    txt = txt + " Stole a "+item.getName()+"!";
                    InternalState.i().inventory().getItems().add(item);
                } else {
                    txt = txt + " Missed the plunder!";

                }

            result.setText(txt);
            if(source.getPassives().hasPassive("Belligerent")) {
                logger.log("Belligerant trait found");
                BattleState.i().getEnmityMap().put(target.getName() + target.hashCode(), source.getName());
            }

        }
        return result;
    }

    public ActionResult PirateStabEffect(EntityInfo target, EntityInfo source) {
        ActionResult result;
        if(passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.SWORD)) {
            int baseDamage = 6;
            Aspects aspect = Aspects.PHYSICAL;
            result = resolver.basicAttack(resolver.physicalPiercingDamage(source, baseDamage), target, source, aspect, 80);
            if(result.isActionMissed()) {
                result.setText(source.getName() + " missed with their stab!");
            } else {
                result.setText(source.getName() + " made the "+target.getName()+" bleed!");
            }
            StatusEffect effect = new StatusEffect("Bleed", Stat.DAMAGE, source.getAttack(), 3);
            target.getStatusEffects().addEffect(effect);
            result.getAddedStatusEffect().add(effect);
            if(source.getPassives().hasPassive("Belligerent")) {
                BattleState.i().getEnmityMap().put(target.getName() + target.hashCode(), source.getName());
            }
        } else {
            result = new ActionResult();
            result.setText(source.getName() + " doesn't have the weapon to do this attack with!");
        }

        return result;
    }

    public ActionResult AhoyEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        String action = source.getName() + " called out to "+target.getName();
        if(resolver.didAttackHit(80)) {
            action = action + " and they responded!";
            BattleState.i().getEnmityMap().put(target.getName() + target.hashCode(), source.getName());
        } else {
            action = action + " and they ignored the call!";
        }
        result.setText(action);
        return result;
    }

    public ActionResult PirateComboEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        int numOfHits = 3;
        int strikedHit = 0;
        Aspects alignment = Aspects.PHYSICAL;
        int baseDamage = 4;
        for(int i = 0; i < numOfHits; i++) {
            ActionResult subResult = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, alignment, 70);
            if(subResult.getHpChange() < 0) {
                strikedHit++;
                result.setHpChange(subResult.getHpChange() + result.getHpChange());
            }
        }
        result.setText(source.getName() + " attacked the enemy and hit "+strikedHit+ " time(s)!");
        if(source.getPassives().hasPassive("Belligerent")) {
            BattleState.i().getEnmityMap().put(target.getName() + target.hashCode(), source.getName());
        }
        return result;
    }

    public ActionResult ToThePlankEffect(EntityInfo target, EntityInfo source) {
        int baseDamage = 20;
        int hitchance = 50;
        Aspects alignment = Aspects.DARK;
        ActionResult result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, alignment, hitchance);
        String action = source.getName() + " forced "+target.getName()+" onto the plank and ";
        if(result.isActionMissed()) {
            action = action + "the attack missed!";
        } else {
            action = action + source.getName() + " jumped off!";
        }
        result.setText(action);
        if(source.getPassives().hasPassive("Belligerent")) {
            BattleState.i().getEnmityMap().put(target.getName() + target.hashCode(), source.getName());
        }
        return result;
    }

}
