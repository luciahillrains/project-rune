package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;

import java.util.Random;

public class Monk {
    ActionEffectResolver resolver = new ActionEffectResolver();
    Random random = new Random();

    public boolean BareFistSupremacyEffect(EntityInfo source) {
        if(source.getEquipment().getWeapon() == null) {
            source.addAdditionalAttack(10);
        }
        return true;
    }

    public boolean PhysicalFistEffect(EntityInfo source) {
        return true;
    }

    public boolean SpiritualDefenseEffect(EntityInfo source) {
        source.addAdditionalDefense(10);
        return true;
    }

    public ActionResult ImbueEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        result.setText(source.getName() + " got ready for their next attack!");
        StatusEffect imbued = new StatusEffect("Imbue", Stat.NONE, 0, 3);
        result.getAddedStatusEffect().add(imbued);
        target.getStatusEffects().addEffect(imbued);
        return result;
    }

    public ActionResult LightningKickEffect(EntityInfo target, EntityInfo source) {
        int baseDamageImbued = 8;
        int baseDamage = 2;
        Aspects aspectImbued = Aspects.AIR;
        Aspects aspect = Aspects.PHYSICAL;
        ActionResult result;
        if(source.getStatusEffects().hasAilment("Imbue")) {
            if(source.getPassives().hasPassive("PhysicalFist")) {
                result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamageImbued), target, source, aspectImbued, aspect, 95);
            } else {
                result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamageImbued), target, source, aspectImbued, 95);
            }
        } else {
            result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, 95);
        }
        result.setText(source.getName() + " used the Lightning Kick!");
        return result;
    }

    public ActionResult TerraCottaPunchEffect(EntityInfo target, EntityInfo source) {
        int baseDamageImbued = 8;
        int baseDamage = 2;
        Aspects aspectImbued = Aspects.EARTH;
        Aspects aspect = Aspects.PHYSICAL;
        ActionResult result;
        if(source.getStatusEffects().hasAilment("Imbue")) {
            if(source.getPassives().hasPassive("PhysicalFist")) {
                result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamageImbued), target, source, aspectImbued, aspect, 95);
            } else {
                result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamageImbued), target, source, aspectImbued, 95);
            }
            if(!result.isActionMissed()) {
                StatusEffect effect = new StatusEffect("Terra Cotta", Stat.DEFENSE, target.getDefense() / 2, 3);
                target.getStatusEffects().addEffect(effect);
                result.getAddedStatusEffect().add(effect);
            }
        } else {
            result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, 95);
        }
        result.setText(source.getName() + " used Terra Cotta Punch!");
        return result;
    }

    public ActionResult FireheartEffect(EntityInfo target, EntityInfo source) {
        int baseDamageImbued = 5;
        int baseDamage = 1;
        Aspects aspectImbued = Aspects.FIRE;
        Aspects aspect = Aspects.PHYSICAL;
        ActionResult result;
        if(source.getStatusEffects().hasAilment("Imbue")) {
            if(source.getPassives().hasPassive("PhysicalFist")) {
                result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamageImbued), target, source, aspectImbued, aspect, 95);
            } else {
                result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamageImbued), target, source, aspectImbued, 95);
            }
        } else {
            result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, 95);
        }
        result.setText(source.getName() + " used Fireheart!");
        return result;
    }

    public ActionResult MeditationEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        result.setText(source.getName() + " meditated quietly.");
        StatusEffect imbued = new StatusEffect("Imbue", Stat.NONE, 0, 3);
        StatusEffect meditation1 = new StatusEffect("Meditation: Attack", Stat.ATTACK, 10, 3);
        StatusEffect meditation2 = new StatusEffect("Meditation: Defense", Stat.DEFENSE, 10, 3);
        result.getAddedStatusEffect().add(imbued);
        result.getAddedStatusEffect().add(meditation1);
        result.getAddedStatusEffect().add(meditation2);
        target.getStatusEffects().addEffect(imbued);
        target.getStatusEffects().addEffect(meditation1);
        target.getStatusEffects().addEffect(meditation2);
        return result;
    }

    public ActionResult MeditativeWallEffect(EntityInfo target, EntityInfo source) {
        if(target == source) {
            return MeditationEffect(target, source);
        } else {
            ActionResult result = new ActionResult();
            StatusEffect effect = new StatusEffect("Meditation Barrier", Stat.DEFENSE, 1 + source.getHp()/10, source.getAttack() % 10 + 1);
            source.getStatusEffects().addEffect(effect);
            result.getAddedStatusEffect().add(effect);
            result.setText(source.getName() + " puts up a meditative wall!");
            return result;
        }
    }

}
