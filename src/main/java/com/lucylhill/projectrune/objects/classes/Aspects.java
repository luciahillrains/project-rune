package com.lucylhill.projectrune.objects.classes;

import java.util.Random;

public enum Aspects {
    HOLY("Holy"),
    DARK("Dark"),
    PHYSICAL("Physical"),
    FIRE("Fire"),
    ICE("Ice"),
    WATER("Water"),
    AIR("Air"),
    EARTH("Earth"),
    OMNI("Omni"),
    NONE("None"),
    RANDOM("Random");

    private final String name;
    Aspects(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }

    public static  Aspects getRandomAspect() {
        Aspects[] containingList = {HOLY, DARK, PHYSICAL, FIRE, ICE, WATER, AIR, EARTH, OMNI, NONE};
        Random random = new Random();
        return containingList[random.nextInt(containingList.length)];
    }
}
