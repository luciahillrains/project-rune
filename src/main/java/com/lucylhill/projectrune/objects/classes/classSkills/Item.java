package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.battle.actions.Action;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.services.InternalState;
import com.lucylhill.projectrune.services.ItemService;

import java.io.ObjectInputFilter;
import java.util.List;
import java.util.stream.Collectors;

public class Item {
    ItemService itemService = new ItemService();
    public void healHp(int hpGain, EntityInfo target, EntityInfo source) {
        hpGain = (int)(hpGain * (source.getHealingItemModifier()));
        target.setHp(target.getHp() + hpGain);
    }

    public ActionResult createActionResult(EntityInfo source, String itemName, int hpChange) {
        ActionResult result = new ActionResult();
        hpChange = (int)(hpChange * (source.getHealingItemModifier()));
        String playerName = source.getName();
        result.setText(playerName + " used a "+itemName);
        result.setHpChange(hpChange);
        return result;
    }

    public ActionResult HealthTonicEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("HealthTonic");
        int hpGain = 30;
        healHp(hpGain, target, source);
        return createActionResult(source, "Health Tonic", hpGain);
    }

    public ActionResult HealthPotionEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("HealthPotion");
        int hpGain = 100;
        healHp(hpGain, target, source);
        return createActionResult(source, "Health Potion", hpGain);
    }

    public ActionResult RefreshingDrinkEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("RefreshingDrink");
        List<Action> characterDiscarded = InternalState.i().deck().getDiscard().stream()
                .filter(a -> a.getCharacterName().equals(target.getName()))
                .collect(Collectors.toList());
        InternalState.i().deck().getDeck().addAll(characterDiscarded);
        ActionResult result = new ActionResult();
        result.setText(source.getName() + " used a refreshing drink on "+target.getName()+ " and now they are refreshed!");
        return result;
    }

    public ActionResult ReenableDrinkEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("ReenableDrink");
        source.getStatusEffects().removeEffect("Disable");
        return createActionResult(source, "Re-enabling Drink", 0);
    }

    public ActionResult BandageEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("Bandage");
        int hpChange = (int) (target.getMaxHp() * .1);
        if (InternalState.i().partyList().getParty().stream().anyMatch(c -> c.getPassives().hasPassive("BandageMastery"))) {
            hpChange = (int) (target.getMaxHp() * .25);
        }
        healHp(hpChange, target, source);
        return createActionResult(source, "Bandage", hpChange);
    }

    public ActionResult FirstAidKitEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("FirstAidKit");
        int hpChange =(int)(target.getMaxHp() * .25);
        if(InternalState.i().partyList().getParty().stream().anyMatch( c -> c.getPassives().hasPassive("BandageMastery"))) {
            hpChange = (int)(target.getMaxHp() * .5);
        }
        healHp(hpChange, target, source);
        return createActionResult(source, "First Aid Kit", hpChange);
    }

    public ActionResult TimeOutElixirEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("TimeOutElixir");
        target.setHp(target.getMaxHp());
        target.getStatusEffects().removeNegativeAilments();
        StatusEffect effect = RuneConfiguration.i().ailmentList().getStatusEffect("Disable");
        target.getStatusEffects().addEffect(effect);
        ActionResult result = createActionResult(source, "Time Out Elixir", target.getMaxHp());
        result.getAddedStatusEffect().add(effect);
        return result;
    }

    public ActionResult HealthElixirEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("HealthElixir");
        target.setHp(target.getMaxHp());
        return createActionResult(source, "Health Elixir", target.getMaxHp());
    }

    public ActionResult FreshMintEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("FreshMint");
        target.getStatusEffects().removeEffect("Poison");
        target.getStatusEffects().removeEffect("Confusion");
        return createActionResult(source, "Fresh Mint", 0);
    }

    public ActionResult TabletEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("Tablet");
        target.getStatusEffects().removeEffect("Paralysis");
        target.getStatusEffects().removeEffect("Blind");
        return createActionResult(source, "Tablet", 0);
    }

    public ActionResult HealthTinctureEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("HealthTincture");
        int hpGain = 500;
        healHp(hpGain, target, source);
        return createActionResult(source, "Health Tincture", hpGain);
    }

    public ActionResult SuperTinctureEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("SuperTincture");
        int hpGain = 500;
        healHp(hpGain, target, source);
        target.getStatusEffects().removeNegativeAilments();
        target.getStatusEffects().removeNegativeDebuffs();
        return createActionResult(source, "Super Tincture", hpGain);
    }

    public ActionResult SuperElixirEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("SuperElixir");
        int hpGain = target.getMaxHp();
        healHp(hpGain, target, source);
        target.getStatusEffects().removeNegativeAilments();
        target.getStatusEffects().removeNegativeDebuffs();
        return createActionResult(source, "Super Elixir", hpGain);
    }

    public ActionResult StrengthTonicEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("StrengthTonic");
        target.setAttack(target.getAttack() + 1);
        return createActionResult(source, "Strength Tonic", 0);
    }

    public ActionResult ArmorTonicEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("ArmorTonic");
        target.setDefense(target.getDefense() + 1);
        return createActionResult(source, "Armor Tonic", 0);
    }

    public ActionResult BrainTonicEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("BrainTonic");
        target.setIntelligence(target.getIntelligence() + 1);
        return createActionResult(source, "Brain Tonic", 0);
    }

    public ActionResult HareTonicEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("HareTonic");
        target.setAgility(target.getAgility() + 3);
        return createActionResult(source, "Hare Tonic", 0);
    }

    public ActionResult HappyTonicEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("HappyTonic");
        target.setCharisma(target.getCharisma() + 3);
        return createActionResult(source, "Happy Tonic", 0);
    }

    public ActionResult LapisEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("Lapis");
        target.setHp(target.getMaxHp());
        return createActionResult(source, "Diamond Crystal", target.getMaxHp());
    }

    public ActionResult SuperLapisEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("SuperLapis");
        return createActionResult(source, "Diamond Cluster", target.getMaxHp());
    }

    public ActionResult HalfLapisEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("HalfLapis");
        target.getStatusEffects().removeNegativeDebuffs();
        target.getStatusEffects().removeNegativeAilments();
        return createActionResult(source, "Diamond Shard", 0);
    }

    public ActionResult SteroidEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("Steroid");
        target.setHp((int)(target.getMaxHp() * .1));
        return createActionResult(source, "Life Potion", (int)(target.getMaxHp() * .1));
    }

    public ActionResult NeutralizingGemEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("NeutralizingGem");
        target.getStatusEffects().removeNegativeDebuffs();
        return createActionResult(source, "Neutralizing Gem", 0);
    }

    public ActionResult NeutralizingShardEffect(EntityInfo target, EntityInfo source) {
        itemService.discardItem("NeutralizingShard");
        target.getStatusEffects().removeNegativeDebuffs();
        return createActionResult(source, "Neutralizing Shard", 0);
    }

    public ActionResult MoonshineEffect(EntityInfo target, EntityInfo source) {
        double modifier = .16;
        boolean bartending = InternalState.i().partyList().partyHasTrait("Bartending");
        if(bartending) {
            modifier = modifier + .1;
        }
        int hpChange = (int)((target.getMaxHp() * modifier) * target.getAlcoholHealingModifier());
        target.setHp(target.getHp() + hpChange);
        StatusEffect effect = RuneConfiguration.i().ailmentList().getStatusEffect("Bezerk");
        ActionResult result = createActionResult(source, "Moonshine", hpChange);
        if(effect != null) {
            if(!bartending) {
                target.getStatusEffects().addEffect(effect);
                result.getAddedStatusEffect().add(effect);
            }
        }
        return result;
    }

    public ActionResult IllWaterEffect(EntityInfo target, EntityInfo source) {
        double modifier = .08;
        boolean bartending = InternalState.i().partyList().partyHasTrait("Bartending");
        if(bartending) {
            modifier = modifier + .1;
        }
        int hpChange = (int)((target.getMaxHp() * modifier) * target.getAlcoholHealingModifier());
        target.setHp(target.getHp() + hpChange);
        StatusEffect effect = RuneConfiguration.i().ailmentList().getStatusEffect("Poison");
        ActionResult result = createActionResult(source, "Ill Water", hpChange);
        if(effect != null) {
            if(bartending) {
                target.getStatusEffects().addEffect(effect);
                result.getAddedStatusEffect().add(effect);
            }
        }
        return result;
    }

    public ActionResult PloughmansMealEffect(EntityInfo target, EntityInfo source) {
        int hpChange = (int)((target.getMaxHp() * .3) * target.getMeatHealingModifier());
        target.setHp(target.getHp() + hpChange);
        return createActionResult(source, "Ploughman's Meal", hpChange);
    }

    public ActionResult PicnicLunchEffect(EntityInfo target, EntityInfo source) {
        int hpChange = (int) (target.getMaxHp() * .2);
        target.setHp(target.getHp() + hpChange);
        return createActionResult(source, "Ploughman's Meal", hpChange);
    }

    public ActionResult SpringWaterffect(EntityInfo target, EntityInfo source) {
        int hpChange = (int) (target.getMaxHp() * .06);
        target.setHp(target.getHp() + hpChange);
        return createActionResult(source, "Spring Water", hpChange);
    }
}
