package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.objects.items.EquipmentType;
import com.lucylhill.projectrune.objects.passives.PassiveEffectResolver;

import java.util.List;

public class Witch {
    ActionEffectResolver resolver = new ActionEffectResolver();
    PassiveEffectResolver passiveEffectResolver = new PassiveEffectResolver();
    public boolean MysteriousPowerEffect(EntityInfo source) {
        source.addAdditionalAttack(1);
        source.addAdditionalDefense(1);
        source.addAdditionalIntelligence(3);
        return true;
    }

    public boolean WandMasteryWitchEffect(EntityInfo source) {
        if(passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.STAFF) ||
            passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.ROD)) {
            source.addAdditionalAttack(2);
            source.addAdditionalIntelligence(2);
        }
        return true;
    }

    public boolean EnhancedWitchsMixEffect(EntityInfo source) {
        return true;
    }

    public ActionResult WitchMixEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        int change = 2;
        int turns = 2;
        if(source.getPassives().hasPassive("EnhancedWitchsMix")) {
            change = 4;
            turns = 3;
        }
        StatusEffect effect = new StatusEffect("Witch's Mix: Defense", Stat.DEFENSE, change, turns);
        StatusEffect effect2 = new StatusEffect("Witch's Mix: Agility", Stat.AGILITY, change, turns);
        target.getStatusEffects().getEffects().add(effect);
        target.getStatusEffects().getEffects().add(effect2);
        result.setAddedStatusEffect(List.of(effect, effect2));
        result.setText(source.getName() + " created a brew and drank it!");
        return result;
    }

    public ActionResult EnergyVacuumEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        int hpChange = (int)(target.getMaxHp() * 0.05);
        if(hpChange < 1) {
            hpChange = 1;
        }
        StatusEffect effect = new StatusEffect("Energy Vacuum", Stat.HP, hpChange, 10);
        target.getStatusEffects().addEffect(effect);
        result.getAddedStatusEffect().add(effect);
        result.setText(source.getName() + " cast a spell for the party to absorb energy from the earth!");
        return result;
    }

    public ActionResult LifePotionEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        int hpChange = (int)(target.getMaxHp() * 0.1);
        hpChange = (int)(hpChange * target.getHealingModifier());
        target.setHp(hpChange);
        result.setText(source.getName() + " brought out a potion and brought "+target.getName()+" back to life!");
        return result;
    }

    public ActionResult EnergizeEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        int hpChange = (int)(target.getMaxHp() * 0.2);
        StatusEffect effect = new StatusEffect("Tenacity Potion", Stat.HP, hpChange, 5);
        target.getStatusEffects().addEffect(effect);
        result.getAddedStatusEffect().add(effect);
        result.setText(source.getName() + " forces "+target.getName()+" to drink a tenacity potion!");
        return result;
    }

    public ActionResult MagicTouchEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        result.setText(source.getName() + " touched "+target.getName()+"!");
        target.getStatusEffects().removeNegativeAilments();
        return result;
    }

    public ActionResult ShadowflareEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        result.setText(source.getName() + " casted Shadowbomb!");
        int baseDamage =resolver.magicalDamage(target, source, 6);
        int damagePerTurn = resolver.magicalDamage(target, source, 1);
        result = resolver.basicAttack(baseDamage, target, source, Aspects.DARK, 100);
        StatusEffect effect = new StatusEffect("Shadobombed", Stat.HP, damagePerTurn * -1, 7);
        result.getAddedStatusEffect().add(effect);
        target.getStatusEffects().addEffect(effect);
        return result;
    }

}
