package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.battle.actions.Action;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.objects.items.EquipmentResolverState;

import java.util.Optional;

public class Equipment {

    public void BrittleSwordEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(2);
        } else {
            entity.addAttack(-2);
        }

    }

    public void WoodenSwordEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(1);
        } else {
            entity.addAttack(-1);
        }
    }

    public void MopEffect(EntityInfo entity, EquipmentResolverState state) {

    }

    public void CopperSwordEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(3);
        } else {
            entity.addAttack(-3);
        }
    }

    public void BronzeSwordEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(4);
        } else {
            entity.addAttack(-4);
        }
    }

    public void IronSwordEffect(EntityInfo entity, EquipmentResolverState state) {
        int aUp = 5;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(aUp);
        } else {
            entity.addAttack(aUp *-1);
        }
    }

    public void IronGreatswordSwordEffect(EntityInfo entity, EquipmentResolverState state) {
        int aUp = 6;
        int agUp = -2;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(aUp);
            entity.addAgility(agUp);
        } else {
            entity.addAttack(aUp *-1);
            entity.addAgility(agUp * -1);
        }
    }

    public void SteelSwordEffect(EntityInfo entity, EquipmentResolverState state) {
        int aUp = 6;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(aUp);
        } else {
            entity.addAttack(aUp *-1);
        }
    }

    public void DoubleEdgeSwordEffect(EntityInfo entity, EquipmentResolverState state) {
        int aUp = 8;
        int defUp = -4;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(aUp);
            entity.addDefense(defUp);
        } else {
            entity.addAttack(aUp *-1);
            entity.addDefense(defUp * -1);
        }
    }

    public void SilverSwordEffect(EntityInfo entity, EquipmentResolverState state) {
        int aUp = 7;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(aUp);
        } else {
            entity.addAttack(aUp *-1);
        }
    }

    public void RainbowSwordEffect(EntityInfo entity, EquipmentResolverState state) {
        int aUp = 5;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(aUp);
            entity.addBaseElementalOffense(Aspects.FIRE, 1.1);
            entity.addBaseElementalOffense(Aspects.ICE, 1.1);
            entity.addBaseElementalOffense(Aspects.WATER, 1.1);
            entity.addBaseElementalOffense(Aspects.AIR, 1.1);
            entity.addBaseElementalOffense(Aspects.EARTH, 1.1);
            entity.addBaseElementalOffense(Aspects.HOLY, 1.1);
            entity.addBaseElementalOffense(Aspects.DARK, 1.1);
        } else {
            entity.addAttack(aUp *-1);
            entity.addBaseElementalOffense(Aspects.FIRE, 0.1);
            entity.addBaseElementalOffense(Aspects.ICE, 0.1);
            entity.addBaseElementalOffense(Aspects.WATER, 0.1);
            entity.addBaseElementalOffense(Aspects.AIR, 0.1);
            entity.addBaseElementalOffense(Aspects.EARTH, 0.1);
            entity.addBaseElementalOffense(Aspects.HOLY, 0.1);
            entity.addBaseElementalOffense(Aspects.DARK, 0.1);
        }
    }

    public void MythrilSwordEffect(EntityInfo entity, EquipmentResolverState state) {
        int aUp = 8;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(aUp);
        } else {
            entity.addAttack(aUp *-1);
        }
    }

    public void CursedSwordEffect(EntityInfo entity, EquipmentResolverState state) {
        int aUp = 20;
        int def = -10;
        int agi = -10;
        int chr = -10;
        int it = -10;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(aUp);
            entity.addDefense(def);
            entity.addAgility(agi);
            entity.addCharisma(chr);
            entity.addIntelligence(it);
        } else {
            entity.addAttack(aUp * -1);
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
            entity.addCharisma(chr * -1);
            entity.addIntelligence(it * -1);
        }
    }

    public void MythrilGreatswordEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 12;
        int agi = -8;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addAgility(agi);
        } else {
            entity.addAttack(atk *-1);
            entity.addAgility(agi * -1);
        }
    }

    public void CrystalSwordEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 9;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
        } else {
            entity.addAttack(atk *-1);
        }
    }

    public void SunmetalSwordEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 10;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
        } else {
            entity.addAttack(atk *-1);
        }
    }

    public void UltimateSwordEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 11;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
        } else {
            entity.addAttack(atk *-1);
        }
    }

    public void CrystalGreatswordEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 15;
        int agi = -8;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addAgility(agi);
        } else {
            entity.addAttack(atk *-1);
            entity.addAgility(agi * -1);
        }
    }

    public void HatchetEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
        } else {
            entity.addAttack(atk *-1);
        }
    }

    public void WoodenAxeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 2;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
        } else {
            entity.addAttack(atk *-1);
        }
    }

    public void GlitchAxeEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            entity.addBaseElementalOffense(Aspects.FIRE, 1.1);
            entity.addBaseElementalOffense(Aspects.ICE, 1.1);
            entity.addBaseElementalOffense(Aspects.WATER, 1.1);
            entity.addBaseElementalOffense(Aspects.AIR, 1.1);
            entity.addBaseElementalOffense(Aspects.EARTH, 1.1);
            entity.addBaseElementalOffense(Aspects.HOLY, 1.1);
            entity.addBaseElementalOffense(Aspects.DARK, 1.1);
        } else {
            entity.addBaseElementalOffense(Aspects.FIRE, 0.1);
            entity.addBaseElementalOffense(Aspects.ICE, 0.1);
            entity.addBaseElementalOffense(Aspects.WATER, 0.1);
            entity.addBaseElementalOffense(Aspects.AIR, 0.1);
            entity.addBaseElementalOffense(Aspects.EARTH, 0.1);
            entity.addBaseElementalOffense(Aspects.HOLY, 0.1);
            entity.addBaseElementalOffense(Aspects.DARK, 0.1);
        }
    }

    public void CopperAxeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 3;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
        } else {
            entity.addAttack(atk *-1);
        }
    }

    public void BronzeAxeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 4;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
        } else {
            entity.addAttack(atk *-1);
        }
    }

    public void IronAxeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 5;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
        } else {
            entity.addAttack(atk *-1);
        }
    }

    public void GreataxeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 8;
        int agi = -3;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addAgility(agi);
        } else {
            entity.addAttack(atk *-1);
            entity.addAgility(agi * -1);
        }
    }

    public void SteelAxeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 6;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
        } else {
            entity.addAttack(atk *-1);
        }
    }

    public void SilverAxeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 9;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
        } else {
            entity.addAttack(atk *-1);
        }
    }

    public void HeavyGreataxeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 12;
        int agi = -10;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addAgility(agi);
        } else {
            entity.addAttack(atk *-1);
            entity.addAgility(agi * -1);
        }
    }

    public void MythrilAxeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 10;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
        } else {
            entity.addAttack(atk *-1);
        }
    }

    public void CursedAxeEffect(EntityInfo entity, EquipmentResolverState state) {
        int aUp = 15;
        int def = -18;
        int agi = -12;
        int chr = -5;
        int it = -20;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(aUp);
            entity.addDefense(def);
            entity.addAgility(agi);
            entity.addCharisma(chr);
            entity.addIntelligence(it);
        } else {
            entity.addAttack(aUp * -1);
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
            entity.addCharisma(chr * -1);
            entity.addIntelligence(it * -1);
        }
    }

    public void SunmetalAxeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 12;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
        } else {
            entity.addAttack(atk *-1);
        }
    }

    public void WarhammerEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 14;
        int agi = -2;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addAgility(agi);
        } else {
            entity.addAttack(atk *-1);
            entity.addAgility(agi * -1);
        }
    }

    public void WoodenKnifeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 1;
        int agi = 1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addAgility(agi);
        } else {
            entity.addAttack(atk *-1);
            entity.addAgility(agi * -1);
        }
    }

    public void CopperKnifeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 2;
        int agi = 1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addAgility(agi);
        } else {
            entity.addAttack(atk *-1);
            entity.addAgility(agi * -1);
        }
    }

    public void BronzeKnifeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 3;
        int agi = 2;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addAgility(agi);
        } else {
            entity.addAttack(atk *-1);
            entity.addAgility(agi * -1);
        }
    }

    public void IronKnifeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 3;
        int agi = 4;
        if (state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addAgility(agi);
        } else {
            entity.addAttack(atk * -1);
            entity.addAgility(agi * -1);
        }
    }

    public void DirkEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 4;
        int agi = 3;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addAgility(agi);
        } else {
            entity.addAttack(atk *-1);
            entity.addAgility(agi * -1);
        }
    }

    public void ThrowingKnifeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 8;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - 0.2);
        } else {
            entity.addAttack(atk *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + 0.2);
        }
    }

    public void SteelKnifeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 4;
        int agi = 4;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addAgility(agi);
        } else {
            entity.addAttack(atk *-1);
            entity.addAgility(agi * -1);
        }
    }

    public void SilverKnifeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 4;
        int agi = 6;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addAgility(agi);
        } else {
            entity.addAttack(atk *-1);
            entity.addAgility(agi * -1);
        }
    }

    public void GoldKnifeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 5;
        int agi = 4;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addAgility(agi);
        } else {
            entity.addAttack(atk *-1);
            entity.addAgility(agi * -1);
        }
    }

    public void MythrilKnifeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 5;
        int agi = 8;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addAgility(agi);
        } else {
            entity.addAttack(atk *-1);
            entity.addAgility(agi * -1);
        }
    }

    public void CursedKnifeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 30;
        int agi = 30;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addAgility(agi);
        } else {
            entity.addAttack(atk *-1);
            entity.addAgility(agi * -1);
        }
    }

    public void SunmetalKnifeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 6;
        int agi = 10;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addAgility(agi);
        } else {
            entity.addAttack(atk *-1);
            entity.addAgility(agi * -1);
        }
    }

    public void UltimateKnifeEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 7;
        int agi = 11;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addAgility(agi);
        } else {
            entity.addAttack(atk *-1);
            entity.addAgility(agi * -1);
        }
    }

    public void BrittleBowEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
        } else {
            entity.addAttack(atk *-1);
        }
    }

    public void MapleBowEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 2;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + 0.01);
        } else {
            entity.addAttack(atk *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - 0.01);
        }
    }

    public void YewBowEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 2;
        int ite = 1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + 0.01);
        } else {
            entity.addAttack(atk *-1);
            entity.addIntelligence(ite *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - 0.01);
        }
    }

    public void OakBowEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 4;
        int ite = 1;
        double hit = 0.02;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAttack(atk *-1);
            entity.addIntelligence(ite *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
        }
    }

    public void IronBowEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 6;
        int agi = -6;
        int ite = 1;
        double hit = 0.02;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addAgility(agi);
            entity.addIntelligence(ite);
        } else {
            entity.addAttack(atk *-1);
            entity.addAgility(agi * -1);
            entity.addIntelligence(ite *-1);
        }
    }

    public void MahoganyBowEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 5;
        int ite = 1;
        double hit = 0.02;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAttack(atk *-1);
            entity.addIntelligence(ite *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
        }
    }

    public void CompoundBowEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 6;
        double hit = 0.08;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAttack(atk *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
        }
    }

    public void DarkMapleBowEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 6;
        int ite = 2;
        double hit = 0.03;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAttack(atk *-1);
            entity.addIntelligence(ite *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
        }
    }

    public void ElmBowEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 7;
        int ite = 3;
        double hit = 0.03;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAttack(atk *-1);
            entity.addIntelligence(ite *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
        }
    }

    public void GaulianLongbowffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 10;
        int ite = 2;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
        } else {
            entity.addAttack(atk *-1);
            entity.addIntelligence(ite *-1);
        }
    }

    public void BlackOakBowEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 8;
        int ite = 4;
        double hit = 0.04;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAttack(atk *-1);
            entity.addIntelligence(ite *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
        }
    }

    public void CrossBowEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 10;
        int ite = 3;
        double hit = 0.01;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAttack(atk *-1);
            entity.addIntelligence(ite *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
        }
    }

    public void CursedBowEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 30;
        double hit = -0.3;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAttack(atk *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
        }
    }

    public void BrittleMusketEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 2;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
        } else {
            entity.addAttack(atk *-1);
        }
    }

    public void CopperMusketEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 3;
        double hit = 0.01;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAttack(atk *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
        }
    }

    public void BronzeMusketEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 4;
        double hit = 0.02;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAttack(atk *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
        }
    }

    public void IronMusketEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 5;
        double hit = 0.02;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAttack(atk *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
        }
    }

    public void IronCarbineEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 4;
        double hit = 0.05;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAttack(atk *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
        }
    }

    public void SteelMusketEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 6;
        double hit = 0.02;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAttack(atk *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
        }
    }

    public void SteelCarbineEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 5;
        double hit = 0.05;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAttack(atk *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
        }
    }

    public void AlloyMusketEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 7;
        double hit = 0.02;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAttack(atk *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
        }
    }

    public void RedstoneMusketEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 3;
        double hit = 0.02;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
            entity.addBaseElementalOffense(Aspects.FIRE, 1.2);
        } else {
            entity.addAttack(atk *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
            entity.addBaseElementalOffense(Aspects.FIRE, .2);
        }
    }

    public void MythrilMusketEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 8;
        double hit = 0.03;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAttack(atk *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
        }
    }

    public void MythrilCarbineEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 5;
        double hit = 0.08;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAttack(atk *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
        }
    }

    public void SunmetalMusketEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 9;
        double hit = 0.04;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAttack(atk *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
        }
    }

    public void SunmetalCarbineEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 6;
        double hit = 0.08;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAttack(atk *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
        }
    }

    public void ProtoRifleEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 8;
        double hit = 0.1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAttack(atk *-1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);
        }
    }

    public void OneStringedHarpEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(8);
                entity.addIntelligence(8);
            }
        } else {
            entity.addAttack(atk * -1);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(-8);
                entity.addIntelligence(-8);
            }
        }
    }

    public void TwoStringedHarpEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 2;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(8);
                entity.addIntelligence(8);
            }
        } else {
            entity.addAttack(atk * -1);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(-8);
                entity.addIntelligence(-8);
            }
        }
    }

    public void ThreeStringedHarpEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 2;
        int ite = 1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(8);
                entity.addIntelligence(8);
            }
        } else {
            entity.addAttack(atk * -1);
            entity.addIntelligence(ite * -1);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(-8);
                entity.addIntelligence(-8);
            }
        }
    }

    public void OakHarpEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 3;
        int ite = 2;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(8);
                entity.addIntelligence(8);
            }
        } else {
            entity.addAttack(atk * -1);
            entity.addIntelligence(ite * -1);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(-8);
                entity.addIntelligence(-8);
            }
        }
    }

    public void FluteEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 1;
        int ite = 3;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(8);
                entity.addIntelligence(8);
            }
        } else {
            entity.addAttack(atk * -1);
            entity.addIntelligence(ite * -1);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(-8);
                entity.addIntelligence(-8);
            }
        }
    }

    public void MahoganyHarpEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 3;
        int ite = 3;
        int chr = 1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
            entity.addCharisma(chr);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(8);
                entity.addIntelligence(8);
            }
        } else {
            entity.addAttack(atk * -1);
            entity.addIntelligence(ite * -1);
            entity.addCharisma(chr * -1);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(-8);
                entity.addIntelligence(-8);
            }
        }
    }

    public void DarkMapleHarpEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 4;
        int ite = 4;
        int chr = 3;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
            entity.addCharisma(chr);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(8);
                entity.addIntelligence(8);
            }
        } else {
            entity.addAttack(atk * -1);
            entity.addIntelligence(ite * -1);
            entity.addCharisma(chr * -1);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(-8);
                entity.addIntelligence(-8);
            }
        }
    }

    public void ElmHarpEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 4;
        int ite = 5;
        int chr = 5;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
            entity.addCharisma(chr);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(8);
                entity.addIntelligence(8);
            }
        } else {
            entity.addAttack(atk * -1);
            entity.addIntelligence(ite * -1);
            entity.addCharisma(chr * -1);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(-8);
                entity.addIntelligence(-8);
            }
        }
    }

    public void BattleLuteEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 6;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(8);
                entity.addIntelligence(8);
            }
        } else {
            entity.addAttack(atk * -1);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(-8);
                entity.addIntelligence(-8);
            }
        }
    }

    public void BlackOakHarpEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 5;
        int ite = 5;
        int chr = 6;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
            entity.addCharisma(chr);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(8);
                entity.addIntelligence(8);
            }
        } else {
            entity.addAttack(atk * -1);
            entity.addIntelligence(ite * -1);
            entity.addCharisma(chr * -1);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(-8);
                entity.addIntelligence(-8);
            }
        }
    }

    public void ApolloHarpEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 4;
        int ite = 6;
        int chr = 10;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
            entity.addCharisma(chr);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(8);
                entity.addIntelligence(8);
            }
        } else {
            entity.addAttack(atk * -1);
            entity.addIntelligence(ite * -1);
            entity.addCharisma(chr * -1);
            if(entity.getName().equals("Meda")) {
                entity.addAttack(-8);
                entity.addIntelligence(-8);
            }
        }
    }

    public void FakeStaffEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
        } else {
            entity.addAttack(atk * -1);
        }
    }

    public void FakeRodEffect(EntityInfo entity, EquipmentResolverState state) {
        int ite = 1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addIntelligence(ite);
        } else {
            entity.addIntelligence(ite * -1);
        }
    }

    public void WoodenStaffEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 2;
        int ite = 1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
        } else {
            entity.addAttack(atk * -1);
            entity.addIntelligence(ite * - 1);
        }
    }

    public void WoodenRodEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 1;
        int ite = 2;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
        } else {
            entity.addAgility(atk * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void MapleStaffEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 2;
        int ite = 2;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
        } else {
            entity.addAgility(atk * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void MapleRodEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 1;
        int ite = 3;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
        } else {
            entity.addAgility(atk * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void YewStaffEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 3;
        int ite = 2;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
        } else {
            entity.addAgility(atk * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void BroomEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 1;
        int ite = 2;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
            entity.addBaseElementalOffense(Aspects.DARK, 1.1);
        } else {
            entity.addAgility(atk * -1);
            entity.addIntelligence(ite * -1);
            entity.addBaseElementalOffense(Aspects.DARK, .1);
        }
    }

    public void YewRodEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 2;
        int ite = 4;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
        } else {
            entity.addAgility(atk * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void ShamansStickEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 4;
        int ite = 4;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
        } else {
            entity.addAgility(atk * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void MahoganyStaffEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 3;
        int ite = 4;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
        } else {
            entity.addAgility(atk * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void MahoganyRodEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 3;
        int ite = 5;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
        } else {
            entity.addAgility(atk * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void DarkMapleStaffEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 5;
        int ite = 4;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
        } else {
            entity.addAgility(atk * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void DarkMapleRodEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 3;
        int ite = 6;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
        } else {
            entity.addAgility(atk * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void RainbowCarbonRodEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            entity.addBaseElementalOffense(Aspects.FIRE, 1.1);
            entity.addBaseElementalOffense(Aspects.ICE, 1.1);
            entity.addBaseElementalOffense(Aspects.WATER, 1.1);
            entity.addBaseElementalOffense(Aspects.AIR, 1.1);
            entity.addBaseElementalOffense(Aspects.EARTH, 1.1);
            entity.addBaseElementalOffense(Aspects.HOLY, 1.1);
            entity.addBaseElementalOffense(Aspects.DARK, 1.1);
        } else {
            entity.addBaseElementalOffense(Aspects.FIRE, 0.1);
            entity.addBaseElementalOffense(Aspects.ICE, 0.1);
            entity.addBaseElementalOffense(Aspects.WATER, 0.1);
            entity.addBaseElementalOffense(Aspects.AIR, 0.1);
            entity.addBaseElementalOffense(Aspects.EARTH, 0.1);
            entity.addBaseElementalOffense(Aspects.HOLY, 0.1);
            entity.addBaseElementalOffense(Aspects.DARK, 0.1);
        }
    }

    public void ElmStaffEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 5;
        int ite = 5;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
        } else {
            entity.addAgility(atk * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void MythrilStaffEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 7;
        int ite = 4;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
        } else {
            entity.addAgility(atk * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void ElmRodEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 3;
        int ite = 7;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
        } else {
            entity.addAgility(atk * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void BlackOakStaffEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 6;
        int ite = 6;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
        } else {
            entity.addAgility(atk * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void BlackOakRodEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 4;
        int ite = 8;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
        } else {
            entity.addAgility(atk * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void UltimateStaffEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 7;
        int ite = 6;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
        } else {
            entity.addAgility(atk * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void UltimateRodEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 5;
        int ite = 10;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
        } else {
            entity.addAgility(atk * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void CursedStaffEffect(EntityInfo entity, EquipmentResolverState state) {
        int atk = 3;
        int ite = 10;
        int chr = -30;
        int agi = -30;
        double hit = 0.05;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAttack(atk);
            entity.addIntelligence(ite);
            entity.addCharisma(chr);
            entity.addAgility(agi);
            entity.setHitChanceModifier(entity.getHitChanceModifier() + hit);
        } else {
            entity.addAgility(atk * -1);
            entity.addIntelligence(ite * -1);
            entity.addCharisma(chr * -1);
            entity.addAgility(agi * -1);
            entity.setHitChanceModifier(entity.getHitChanceModifier() - hit);

        }
    }

    public void ClothArmorEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
        } else {
            entity.addDefense(def * -1);
        }
    }

    public void CopperArmorEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 3;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
        } else {
            entity.addDefense(def * -1);
        }
    }

    public void BronzeArmorEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 4;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
        } else {
            entity.addDefense(def * -1);
        }
    }

    public void LightweightArmorEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 3;
        int agi = 4;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
        }
    }

    public void IronArmorEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 5;
        int agi = -1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
        }
    }

    public void SteelArmorEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 6;
        int agi = -1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
        }
    }

    public void SilverArmorEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 5;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
        } else {
            entity.addDefense(def * -1);
        }
    }

    public void GoldArmorEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 8;
        int agi = -3;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
        }
    }

    public void AlloyArmorEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 7;
        int agi = -1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
        }
    }

    public void HolyArmorEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 6;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addBaseElementalDefenses(Aspects.HOLY, 1.1);
        } else {
            entity.addDefense(def * -1);
            entity.addBaseElementalDefenses(Aspects.HOLY, .1);
        }
    }

    public void MythrilArmorEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 8;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
        } else {
            entity.addDefense(def * -1);
        }
    }

    public void HeavyMythrilArmorEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 9;
        int agi = -2;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
        }
    }

    public void RainbowArmorEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 6;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addBaseElementalOffense(Aspects.FIRE, 1.05);
            entity.addBaseElementalOffense(Aspects.ICE, 1.05);
            entity.addBaseElementalOffense(Aspects.WATER, 1.05);
            entity.addBaseElementalOffense(Aspects.AIR, 1.05);
            entity.addBaseElementalOffense(Aspects.EARTH, 1.05);
            entity.addBaseElementalOffense(Aspects.HOLY, 1.05);
            entity.addBaseElementalOffense(Aspects.DARK, 1.05);
        } else {
            entity.addDefense(def * -1);
            entity.addBaseElementalOffense(Aspects.FIRE, 0.05);
            entity.addBaseElementalOffense(Aspects.ICE, 0.05);
            entity.addBaseElementalOffense(Aspects.WATER, 0.05);
            entity.addBaseElementalOffense(Aspects.AIR, 0.05);
            entity.addBaseElementalOffense(Aspects.EARTH, 0.05);
            entity.addBaseElementalOffense(Aspects.HOLY, 0.05);
            entity.addBaseElementalOffense(Aspects.DARK, 0.05);
        }
    }

    public void CrystalArmorEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 10;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
        } else {
            entity.addDefense(def * -1);
        }
    }

    public void SunmetalArmorEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 12;
        int agi = -3;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
        }
    }

    public void UltimateArmorEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 14;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
        } else {
            entity.addDefense(def * -1);
        }
    }

    public void ClothCloakEffect(EntityInfo entity, EquipmentResolverState state) {
        int agi = 2;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAgility(agi);
        } else {
            entity.addAgility(agi * -1);
        }
    }

    public void CottonCloakEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 1;
        int agi = 2;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
        }
    }

    public void GlitchyCloakEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 5;
        int agi = 3;
        int ite = 3;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
            entity.addIntelligence(ite);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void VelveteenCloakEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 2;
        int agi = 1;
        int ite = 1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
            entity.addIntelligence(ite);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void LinenCloakEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 3;
        int agi = 1;
        int ite = 1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
            entity.addIntelligence(ite);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void LeatherCloakEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 4;
        int agi = 1;
        int ite = 2;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
            entity.addIntelligence(ite);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void ThiefsCloakEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 4;
        int agi = 4;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
        }
    }

    public void SilkCloakEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 4;
        int agi = 3;
        int ite = 4;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
            entity.addIntelligence(ite);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void WoolCloakEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 5;
        int agi = 2;
        int ite = 2;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
            entity.addIntelligence(ite);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void CoarseWoolCloakEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 6;
        int agi = 2;
        int ite = 2;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
            entity.addIntelligence(ite);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void RainbowCloakEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 3;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addBaseElementalOffense(Aspects.FIRE, 1.1);
            entity.addBaseElementalOffense(Aspects.ICE, 1.1);
            entity.addBaseElementalOffense(Aspects.WATER, 1.1);
            entity.addBaseElementalOffense(Aspects.AIR, 1.1);
            entity.addBaseElementalOffense(Aspects.EARTH, 1.1);
            entity.addBaseElementalOffense(Aspects.HOLY, 1.1);
            entity.addBaseElementalOffense(Aspects.DARK, 1.1);
        } else {
            entity.addDefense(def * -1);
            entity.addBaseElementalOffense(Aspects.FIRE, 0.1);
            entity.addBaseElementalOffense(Aspects.ICE, 0.1);
            entity.addBaseElementalOffense(Aspects.WATER, 0.1);
            entity.addBaseElementalOffense(Aspects.AIR, 0.1);
            entity.addBaseElementalOffense(Aspects.EARTH, 0.1);
            entity.addBaseElementalOffense(Aspects.HOLY, 0.1);
            entity.addBaseElementalOffense(Aspects.DARK, 0.1);
        }
    }
    public void ToughLinenCloakEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 6;
        int agi = 3;
        int ite = 3;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
            entity.addIntelligence(ite);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void HardLeatherCloakEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 8;
        int agi = 3;
        int ite = 1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
            entity.addIntelligence(ite);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void UltimateCloakEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 9;
        int agi = 4;
        int ite = 5;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
            entity.addIntelligence(ite);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void TatteredClothesEffect(EntityInfo entity, EquipmentResolverState state) {
        int agi = 5;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addAgility(agi);
        } else {
            entity.addAgility(agi * -1);
        }
    }

    public void RagamuffinClothesEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 1;
        int agi = 5;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
        }
    }

    public void FarsicGarbEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 3;
        int agi = 2;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
            entity.addBaseElementalDefenses(Aspects.HOLY, 1.03);
            entity.addBaseElementalDefenses(Aspects.DARK, 1.03);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
            entity.addBaseElementalDefenses(Aspects.HOLY, 0.03);
            entity.addBaseElementalDefenses(Aspects.DARK, 0.03);
        }
    }

    public void SteelKnightGearEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 6;
        int agi = -3;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
        }
    }

    public void JestersGarbEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 5;
        int agi = 3;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
        }
    }

    public void BougieGarbEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 1;
        int ite = 2;
        int chr = 10;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addIntelligence(ite);
            entity.addCharisma(chr);
        } else {
            entity.addDefense(def * -1);
            entity.addIntelligence(ite * -1);
            entity.addCharisma(chr * -1);
        }
    }

    public void ClericsRobeEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 5;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addBaseElementalDefenses(Aspects.HOLY, 1.1);
            entity.addBaseElementalDefenses(Aspects.DARK, 1.1);
        } else {
            entity.addDefense(def * -1);
            entity.addBaseElementalDefenses(Aspects.HOLY, 0.1);
            entity.addBaseElementalDefenses(Aspects.DARK, 0.1);
        }
    }

    public void NewWorldGarbEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 5;
        int ite = 3;
        int atk = 1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addIntelligence(ite);
            entity.addAttack(atk);
        } else {
            entity.addDefense(def * -1);
            entity.addIntelligence(ite * -1);
            entity.addAttack(atk);
        }
    }

    public void TrappersGarbEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 3;
        int agi = 5;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
        }
    }

    public void KastelgradGarbEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 6;
        int agi = 5;
        int ite = 1;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
            entity.addIntelligence(ite);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
            entity.addIntelligence(ite * -1);
        }
    }

    public void BrightGarbEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 4;
        int chr = 15;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addCharisma(chr);
        } else {
            entity.addDefense(def * -1);
            entity.addCharisma(chr * -1);
        }
    }

    public void HeavyKnightGarbEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 12;
        int agi = -10;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addAgility(agi);
        } else {
            entity.addDefense(def * -1);
            entity.addAgility(agi * -1);
        }
    }

    public void RainbowGarbEffect(EntityInfo entity, EquipmentResolverState state) {
        int def = 5;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addDefense(def);
            entity.addBaseElementalOffense(Aspects.FIRE, 1.1);
            entity.addBaseElementalOffense(Aspects.ICE, 1.1);
            entity.addBaseElementalOffense(Aspects.WATER, 1.1);
            entity.addBaseElementalOffense(Aspects.AIR, 1.1);
            entity.addBaseElementalOffense(Aspects.EARTH, 1.1);
            entity.addBaseElementalOffense(Aspects.HOLY, 1.1);
            entity.addBaseElementalOffense(Aspects.DARK, 1.1);
        } else {
            entity.addDefense(def * -1);
            entity.addBaseElementalOffense(Aspects.FIRE, 0.1);
            entity.addBaseElementalOffense(Aspects.ICE, 0.1);
            entity.addBaseElementalOffense(Aspects.WATER, 0.1);
            entity.addBaseElementalOffense(Aspects.AIR, 0.1);
            entity.addBaseElementalOffense(Aspects.EARTH, 0.1);
            entity.addBaseElementalOffense(Aspects.HOLY, 0.1);
            entity.addBaseElementalOffense(Aspects.DARK, 0.1);
        }
    }

    public void RubyNecklaceEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            entity.addBaseElementalOffense(Aspects.FIRE, 1.03);
            entity.addBaseElementalDefenses(Aspects.FIRE, 1.03);
        } else {
            entity.addBaseElementalOffense(Aspects.FIRE, 0.03);
            entity.addBaseElementalDefenses(Aspects.FIRE, 0.03);
        }
    }
    
    public void SapphireNecklaceEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            entity.addBaseElementalOffense(Aspects.ICE, 1.03);
            entity.addBaseElementalDefenses(Aspects.ICE, 1.03);
        } else {
            entity.addBaseElementalOffense(Aspects.ICE, 0.03);
            entity.addBaseElementalDefenses(Aspects.ICE, 0.03);
        }
    }

    public void LapisNecklaceEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            entity.addBaseElementalOffense(Aspects.WATER, 1.03);
            entity.addBaseElementalDefenses(Aspects.WATER, 1.03);
        } else {
            entity.addBaseElementalOffense(Aspects.WATER, 0.03);
            entity.addBaseElementalDefenses(Aspects.WATER, 0.03);
        }
    }

    public void TopazNecklaceEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            entity.addBaseElementalOffense(Aspects.EARTH, 1.03);
            entity.addBaseElementalDefenses(Aspects.EARTH, 1.03);
        } else {
            entity.addBaseElementalOffense(Aspects.EARTH, 0.03);
            entity.addBaseElementalDefenses(Aspects.EARTH, 0.03);
        }
    }

    public void EmeraldNecklaceEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            entity.addBaseElementalOffense(Aspects.AIR, 1.03);
            entity.addBaseElementalDefenses(Aspects.AIR, 1.03);
        } else {
            entity.addBaseElementalOffense(Aspects.AIR, 0.03);
            entity.addBaseElementalDefenses(Aspects.AIR, 0.03);
        }
    }

    public void OnyxNecklaceEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            entity.addBaseElementalOffense(Aspects.DARK, 1.03);
            entity.addBaseElementalDefenses(Aspects.DARK, 1.03);
        } else {
            entity.addBaseElementalOffense(Aspects.DARK, 0.03);
            entity.addBaseElementalDefenses(Aspects.DARK, 0.03);
        }
    }

    public void AgateNecklaceEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            entity.addBaseElementalOffense(Aspects.HOLY, 1.03);
            entity.addBaseElementalDefenses(Aspects.HOLY, 1.03);
        } else {
            entity.addBaseElementalOffense(Aspects.HOLY, 0.03);
            entity.addBaseElementalDefenses(Aspects.HOLY, 0.03);
        }
    }

    public void DiamondNecklaceEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            entity.addBaseElementalOffense(Aspects.FIRE, 1.01);
            entity.addBaseElementalOffense(Aspects.ICE, 1.01);
            entity.addBaseElementalOffense(Aspects.WATER, 1.01);
            entity.addBaseElementalOffense(Aspects.AIR, 1.01);
            entity.addBaseElementalOffense(Aspects.EARTH, 1.01);
            entity.addBaseElementalOffense(Aspects.HOLY, 1.01);
            entity.addBaseElementalOffense(Aspects.DARK, 1.01);
            entity.addBaseElementalDefenses(Aspects.FIRE, 1.01);
            entity.addBaseElementalDefenses(Aspects.ICE, 1.01);
            entity.addBaseElementalDefenses(Aspects.WATER, 1.01);
            entity.addBaseElementalDefenses(Aspects.AIR, 1.01);
            entity.addBaseElementalDefenses(Aspects.EARTH, 1.01);
            entity.addBaseElementalDefenses(Aspects.HOLY, 1.01);
            entity.addBaseElementalDefenses(Aspects.DARK, 1.01);
        } else {
            entity.addBaseElementalOffense(Aspects.FIRE, 0.01);
            entity.addBaseElementalOffense(Aspects.ICE, 0.01);
            entity.addBaseElementalOffense(Aspects.WATER, 0.01);
            entity.addBaseElementalOffense(Aspects.AIR, 0.01);
            entity.addBaseElementalOffense(Aspects.EARTH, 0.01);
            entity.addBaseElementalOffense(Aspects.HOLY, 0.01);
            entity.addBaseElementalOffense(Aspects.DARK, 0.01);
            entity.addBaseElementalDefenses(Aspects.FIRE, 0.01);
            entity.addBaseElementalDefenses(Aspects.ICE, 0.01);
            entity.addBaseElementalDefenses(Aspects.WATER, 0.01);
            entity.addBaseElementalDefenses(Aspects.AIR, 0.01);
            entity.addBaseElementalDefenses(Aspects.EARTH, 0.01);
            entity.addBaseElementalDefenses(Aspects.HOLY, 0.01);
            entity.addBaseElementalDefenses(Aspects.DARK, 0.01);
        }
    }

    public void CrucifixNecklaceEffect(EntityInfo entity, EquipmentResolverState state) {
        int chr = -20;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addCharisma(chr);
            entity.addBaseElementalDefenses(Aspects.HOLY, 1.5);
        } else {
            entity.addCharisma(chr * -1);
            entity.addBaseElementalDefenses(Aspects.HOLY, 0.5);
        }
    }

    public void PentagramNecklaceEffect(EntityInfo entity, EquipmentResolverState state) {
        int chr = -20;
        if(state == EquipmentResolverState.EQUIP) {
            entity.addCharisma(chr);
            entity.addBaseElementalDefenses(Aspects.DARK, 1.5);
        } else {
            entity.addCharisma(chr * -1);
            entity.addBaseElementalDefenses(Aspects.DARK, 0.5);
        }
    }

    public void MechanicalCogEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            Optional<Action> action = RuneConfiguration.i().actions().getActionFromList("Breakdown");
            action.ifPresent(value -> entity.getRelicActions().add(value));
        } else {
            entity.getRelicActions().removeIf(a -> a.getId().equals("Breakdown"));
        }
    }

    public void ChaosMarkEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            entity.getStatusEffects().getResistances().add("Confusion");
            entity.getStatusEffects().getResistances().add("Bezerk");
        } else {
            entity.getStatusEffects().getResistances().remove("Confusion");
            entity.getStatusEffects().getResistances().remove("Bezerk");
        }
    }

    public void BookOfTheWorldEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            entity.addBaseElementalOffense(Aspects.HOLY, 1.15);
        } else {
            entity.addBaseElementalOffense(Aspects.HOLY, 0.15);
        }
    }

    public void BookOfTheOneTrueGodEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            entity.addBaseElementalOffense(Aspects.DARK, 1.15);
        } else {
            entity.addBaseElementalOffense(Aspects.DARK, 0.15);
        }
    }
    
    public void RubyBroachEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            Optional<Action> action = RuneConfiguration.i().actions().getActionFromList("RubyGaze");
            action.ifPresent(value -> entity.getRelicActions().add(value));
        } else {
            entity.getRelicActions().removeIf(a -> a.getId().equals("RubyGaze"));
        }
    }

    public void SapphireBroachEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            Optional<Action> action = RuneConfiguration.i().actions().getActionFromList("SapphireGaze");
            action.ifPresent(value -> entity.getRelicActions().add(value));
        } else {
            entity.getRelicActions().removeIf(a -> a.getId().equals("SapphireGaze"));
        }
    }

    public void LapisBroachEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            Optional<Action> action = RuneConfiguration.i().actions().getActionFromList("LapisGaze");
            action.ifPresent(value -> entity.getRelicActions().add(value));
        } else {
            entity.getRelicActions().removeIf(a -> a.getId().equals("LapisGaze"));
        }
    }

    public void EmeraldBroachEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            Optional<Action> action = RuneConfiguration.i().actions().getActionFromList("EmeraldGaze");
            action.ifPresent(value -> entity.getRelicActions().add(value));
        } else {
            entity.getRelicActions().removeIf(a -> a.getId().equals("EmeraldGaze"));
        }
    }

    public void TopazBroachEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            Optional<Action> action = RuneConfiguration.i().actions().getActionFromList("TopazGaze");
            action.ifPresent(value -> entity.getRelicActions().add(value));
        } else {
            entity.getRelicActions().removeIf(a -> a.getId().equals("TopazGaze"));
        }
    }

    public void OnyxBroachEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            Optional<Action> action = RuneConfiguration.i().actions().getActionFromList("OnyxGaze");
            action.ifPresent(value -> entity.getRelicActions().add(value));
        } else {
            entity.getRelicActions().removeIf(a -> a.getId().equals("OnyxGaze"));
        }
    }

    public void AgateBroachEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            Optional<Action> action = RuneConfiguration.i().actions().getActionFromList("AgateGaze");
            action.ifPresent(value -> entity.getRelicActions().add(value));
        } else {
            entity.getRelicActions().removeIf(a -> a.getId().equals("AgateGaze"));
        }
    }

    public void MedicalBraceletEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            Optional<Action> action = RuneConfiguration.i().actions().getActionFromList("QuickRecovery");
            action.ifPresent(value -> entity.getRelicActions().add(value));
        } else {
            entity.getRelicActions().removeIf(a -> a.getId().equals("QuickRecovery"));
        }
    }

    public void FaerieMarkEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            entity.getStatusEffects().getResistances().add("Poison");
            entity.getStatusEffects().getResistances().add("Blindness");
            entity.getStatusEffects().getResistances().add("Paralysis");
        } else {
            entity.getStatusEffects().getResistances().remove("Poison");
            entity.getStatusEffects().getResistances().remove("Blindness");
            entity.getStatusEffects().getResistances().remove("Paralysis");
        }
    }

    public void VaccineMarkEffect(EntityInfo entity, EquipmentResolverState state) {
        if(state == EquipmentResolverState.EQUIP) {
            entity.getStatusEffects().getResistances().add("Poison");
        } else {
            entity.getStatusEffects().getResistances().remove("Poison");
        }
    }
}
