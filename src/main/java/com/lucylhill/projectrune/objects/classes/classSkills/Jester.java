package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.battle.actions.Action;
import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.services.DebugLogger;
import com.lucylhill.projectrune.services.InternalState;

import java.util.Random;

public class Jester {
    ActionEffectResolver resolver = new ActionEffectResolver();
    DebugLogger logger = new DebugLogger(this.getClass());
    Random random = new Random();
    Aspects[] aspectsList = {
            Aspects.PHYSICAL,
            Aspects.AIR,
            Aspects.FIRE,
            Aspects.HOLY,
            Aspects.DARK,
            Aspects.EARTH,
            Aspects.ICE,
            Aspects.NONE,
            Aspects.OMNI,
            Aspects.WATER
    };
    public boolean DodgeRollEffect(EntityInfo source) {
        source.addAdditionalAgility(20);
        source.setAlignment(Aspects.PHYSICAL);
        return true;
    }

    public boolean JuggleOffenseEffect(EntityInfo source) {
        return true;
    }

    public boolean JuggleAlignmentEffect(EntityInfo source) {
        Action lastSkill = source.getLastSkillHit();
        if(lastSkill != null && lastSkill.getAlignment() != null) {
            source.setAlignment(lastSkill.getAlignment());
            source.setLastSkillHit(null);
        }
        return true;
    }

    public ActionResult KnifeJuggleEffect(EntityInfo target, EntityInfo source) {
        int baseDamage = 3;
        Aspects alignment = Aspects.PHYSICAL;
        ActionResult result;
        if(source.getPassives().hasPassive("JuggleOffense")) {
            Aspects alignment2 = aspectsList[random.nextInt(aspectsList.length)];
            result = resolver.basicAttack(baseDamage * ((source.getAttack() / 2) + (source.getIntelligence()/2)),target, source, alignment, alignment2, 80 );
        } else {
            result = resolver.basicAttack(baseDamage * ((source.getAttack() /2 ) + (source.getIntelligence()/2)), target, source, alignment, 80);
        }
        result.setText(source.getName() + " threw knives at the enemy party!");
        return result;
    }

    public ActionResult CardTrickEffect(EntityInfo target, EntityInfo source) {
        int baseDamage = 4;
        Aspects alignment = Aspects.PHYSICAL;
        ActionResult result;
        if(source.getPassives().hasPassive("JuggleOffense")) {
            Aspects alignment2 = aspectsList[random.nextInt(aspectsList.length)];
            result = resolver.basicAttack(baseDamage * ((source.getAttack() / 2) + (source.getIntelligence()/2)),target, source, alignment, alignment2, 90 );
        } else {
            result = resolver.basicAttack(baseDamage * ((source.getAttack() /2 ) + (source.getIntelligence()/2)), target, source, alignment, 90);
        }
        String text = source.getName() + " did a card trick!";
        if(!result.isActionMissed()) {
            text = text + " And stupified the "+target.getName()+"!";
            if(resolver.didAttackHit(60)) {
                StatusEffect paralysis = RuneConfiguration.i().ailmentList().getStatusEffect("Paralysis");
                target.getStatusEffects().addEffect(paralysis);
                result.getAddedStatusEffect().add(paralysis);
            }
            if(resolver.didAttackHit(60)) {
                StatusEffect confusion = RuneConfiguration.i().ailmentList().getStatusEffect("Confusion");
                target.getStatusEffects().addEffect(confusion);
                result.getAddedStatusEffect().add(confusion);
            }
        }
        result.setText(text);
        return result;
    }

    public ActionResult JoyBuzzerEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        String text = source.getName() + " opened his hand out in a display of friendship!";
        if(resolver.didAttackHit(60)) {
            text = text + " Psyche!  "+target.getName()+" was paralyzed!";
            StatusEffect paralysis = RuneConfiguration.i().ailmentList().getStatusEffect("Paralysis");
            target.getStatusEffects().addEffect(paralysis);
            result.getAddedStatusEffect().add(paralysis);
            result.setText(text);
        } else {
            text = text + " But "+target.getName() + " did not fall for it!";
            result.setText(text);
        }
        return result;
    }

    public ActionResult ClownDespairEffect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.NONE;
        int baseDamage = 6;
        ActionResult result;
        String text = source.getName() + " used Clown's Despair!";
        if(source.getPassives().hasPassive("JuggleOffense")) {
            Aspects alignment2 = aspectsList[random.nextInt(aspectsList.length)];
            result = resolver.basicAttack(baseDamage * ((source.getAttack() / 2) + (source.getIntelligence()/2)),target, source, aspect, alignment2, 90 );
        } else {
            result = resolver.basicAttack(baseDamage * ((source.getAttack() /2 ) + (source.getIntelligence()/2)), target, source, aspect, 90);
        }
        if(result.isActionMissed()) {
           text = text + " "+target.getName()+" didn't care!";
        } else {
            text = text + " "+target.getName()+" fell for the story!";
            target.setAlignment(Aspects.NONE);
        }
        result.setText(text);
        return result;
    }

    public ActionResult ConfusingStoryEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        String text = source.getName() + " waved a confusing and conflicting story!";
        if(resolver.didAttackHit(60)) {
            text = text + " " + target.getName() + " was confused and discombobulated!";
            StatusEffect paralysis = RuneConfiguration.i().ailmentList().getStatusEffect("Confusion");
            target.getStatusEffects().addEffect(paralysis);
            result.getAddedStatusEffect().add(paralysis);
            target.setAlignment(aspectsList[random.nextInt(aspectsList.length)]);
            result.setText(text);
        } else {
            text = text + " " + target.getName() + " wasn't impressed!";
            result.setText(text);
        }
        return result;
    }

    public ActionResult OpenTheVoidEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        String text = source.getName() + " opened the doors to the void!";
        int chance = 1 + (InternalState.i().level().getLevel() / 10) + ((source.getMaxHp() - source.getHp())/10) + random.nextInt(0, 5);
        if(resolver.didAttackHit(chance) && target.isNormal()) {
            text = text + " " + target.getName() + " is critical!";
            target.setHp(target.getHp() / 10);
            result.setHpChange(target.getMaxHp() - target.getHp());
            result.setText(text);
        } else {
            text = text + " " + target.getName() + " wasn't affected!";
            result.setText(text);
            result.setActionMissed(true);
        }
        return result;
    }
}
