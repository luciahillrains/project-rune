package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.objects.battle.BattleState;
import com.lucylhill.projectrune.objects.battle.actions.Action;
import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.services.InternalState;

import java.util.List;

public class Bard {
    ActionEffectResolver resolver = new ActionEffectResolver();
    public boolean MusicAtCampEffect(EntityInfo source) {
        return true;
    }

    public boolean EnhancedSongsEffect(EntityInfo source) {
        return true;
    }

    public boolean EnhancedSongs2Effect(EntityInfo source) {
        return true;
    }

    public ActionResult EncoreEffect(EntityInfo target, EntityInfo source) {
        Action lastAction = BattleState.i().getPreviousActions().get(BattleState.i().getPreviousActions().size() - 1);
        ActionResult result = new ActionResult();
        if(lastAction == null || lastAction.getId().equals("Encore") ) {
            result.setText(source.getName() + " played an encore...but there was no one demanding it!");
        } else {
            result =  resolver.resolveAction(lastAction, target, source);
            result.setText(source.getName() + " played an encore! "+result.getText());
        }
        return result;
    }

    public ActionResult HeroMarchEffect(EntityInfo target, EntityInfo source) {
        StatusEffect effect = new StatusEffect("Hero's March: Attack", Stat.ATTACK, 2, 3);
        effect.setPositive(true);
        StatusEffect effect2 = new StatusEffect("Hero's March: Defense", Stat.DEFENSE, 1, 3);
        effect2.setPositive(true);
        ActionResult actionResult = new ActionResult();
        actionResult.setText(source.getName() + " played the Hero's March for the party!");
        actionResult.getAddedStatusEffect().addAll(List.of(effect, effect2));
        target.getStatusEffects().addEffect(effect);
        target.getStatusEffects().addEffect(effect2);
        if(source.getPassives().hasPassive("EnhancedSongs")) {
            StatusEffect effect3 = new StatusEffect("Hero's March: Intelligence", Stat.INTELLIGENCE, 2, 3);
            effect2.setPositive(true);
            StatusEffect effect4 = new StatusEffect("Hero's March: Agility", Stat.AGILITY, 10, 3);
            effect2.setPositive(true);
            actionResult.getAddedStatusEffect().addAll(List.of(effect3, effect4));
            target.getStatusEffects().addEffect(effect3);
            target.getStatusEffects().addEffect(effect4);
        }
        if(source.getPassives().hasPassive("EnhancedSongs2")) {
            if(resolver.didAttackHit(76)) {
                int hpToRecover = (int)(source.getMaxHp() * 0.20);
                hpToRecover = (int)(hpToRecover * source.getHealingModifier());
                actionResult.setHpChange(hpToRecover);
                target.setHp(target.getHp() + hpToRecover);
            }
        }
        return actionResult;
    }

    public ActionResult GaurdianWaltzEffect(EntityInfo target, EntityInfo source) {
        StatusEffect effect = new StatusEffect("Guardian's Waltz: Attack", Stat.ATTACK, 1, 3);
        effect.setPositive(true);
        StatusEffect effect2 = new StatusEffect("Guardian's Waltz: Defense", Stat.DEFENSE, 2, 3);
        effect2.setPositive(true);
        ActionResult actionResult = new ActionResult();
        actionResult.setText(source.getName() + " played the Guardian's Waltz for the party!");
        actionResult.getAddedStatusEffect().addAll(List.of(effect, effect2));
        target.getStatusEffects().addEffect(effect);
        target.getStatusEffects().addEffect(effect2);
        if(source.getPassives().hasPassive("EnhancedSongs")) {
            StatusEffect effect3 = new StatusEffect("Guardian's Waltz: Intelligence", Stat.INTELLIGENCE, 2, 3);
            effect2.setPositive(true);
            StatusEffect effect4 = new StatusEffect("Guardian's Waltz: Agility", Stat.AGILITY, 10, 3);
            effect2.setPositive(true);
            actionResult.getAddedStatusEffect().addAll(List.of(effect3, effect4));
            target.getStatusEffects().addEffect(effect3);
            target.getStatusEffects().addEffect(effect4);
        }
        if(source.getPassives().hasPassive("EnhancedSongs2")) {
            if(resolver.didAttackHit(76)) {
                int hpToRecover = (int)(source.getMaxHp() * 0.20);
                hpToRecover = (int)(hpToRecover * target.getHealingModifier());
                target.setHp(target.getHp() + hpToRecover);
            }
        }
        return actionResult;
    }

    public ActionResult DiscordantNoiseEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        result.setText(source.getName() + "played a bunch of discordant noise!");
        if(resolver.didAttackHit(60)) {
            StatusEffect effect = new StatusEffect("Discordant Noise: Attack", Stat.ATTACK, -5, 3);
            StatusEffect effect2 = new StatusEffect("Discordant Noise: Defense", Stat.DEFENSE, -5, 3);
            StatusEffect effect3 = new StatusEffect("Discordant Noise: Intelligence", Stat.INTELLIGENCE, -5, 3);
            target.getStatusEffects().addEffect(effect);
            target.getStatusEffects().addEffect(effect2);
            target.getStatusEffects().addEffect(effect3);
            result.setAddedStatusEffect(List.of(effect, effect2, effect3));
        }
        return result;
    }

    public ActionResult OdeFallenEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        String text = source.getName() + " played a funeral march!";
        int amountOfIncap = (int) InternalState.i().partyList().getParty()
                .stream().filter(p -> p.getHp() <= 0)
                .count();
        if(amountOfIncap > 0) {
            text = text + " Damage was dealt!";
            Aspects aspect = Aspects.AIR;
            int baseDamage = 8;
            int hitRate = 100;
            result = resolver.basicAttack(resolver.magicalPhysicalDamage(target, source, baseDamage), target, source, aspect, hitRate);
            text = text + " Damage was dealt!";
        } else {
            text = text + " No one cared!";
        }
        result.setText(text);
        return result;
    }

    public ActionResult DedicationEffect(EntityInfo target, EntityInfo source) {
        StatusEffect effect = new StatusEffect("Dedication: Attack", Stat.ATTACK, 5, 4);
        effect.setPositive(true);
        StatusEffect effect2 = new StatusEffect("Dedication: Defense", Stat.DEFENSE, 5, 4);
        effect2.setPositive(true);
        StatusEffect effect3 = new StatusEffect("Dedication: Intelligence", Stat.INTELLIGENCE, 5, 4);
        effect2.setPositive(true);
        ActionResult actionResult = new ActionResult();
        actionResult.setText(source.getName() + " dedicates a song to "+target.getName()+"!");
        actionResult.getAddedStatusEffect().addAll(List.of(effect, effect2, effect3));
        target.getStatusEffects().addEffect(effect);
        target.getStatusEffects().addEffect(effect2);
        target.getStatusEffects().addEffect(effect3);
        int hpToRecover = (int)(source.getMaxHp() * 0.25);
        hpToRecover = (int)(hpToRecover * target.getHealingModifier());
        target.setHp(target.getHp() + hpToRecover);

        return actionResult;
    }
}
