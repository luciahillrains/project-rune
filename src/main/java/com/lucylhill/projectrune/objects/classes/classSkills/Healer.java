package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.services.DebugLogger;
import com.lucylhill.projectrune.services.InternalState;

public class Healer {
    ActionEffectResolver resolver = new ActionEffectResolver();
    DebugLogger logger = new DebugLogger(this.getClass());

    public boolean FormalEducationEffect(EntityInfo source) {
        source.addAdditionalIntelligence(1);
        return true;
    }

    public boolean EnhancedHealEffect(EntityInfo source) {
        return true;
    }

    public boolean EnhancedHeal2Effect(EntityInfo source) {
        return true;
    }

    public ActionResult HealEffect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.WATER;
        int hpChange = 2;
        if(source.getPassives().hasPassive("EnhancedHeal")) {
            hpChange = 5;
        }
        if(source.getPassives().hasPassive("EnhancedHeal2")) {
            hpChange = 5 + (InternalState.i().level().getLevel() / 10);
        }
        if(source.getOffenses().containsKey(aspect)) {
            hpChange = (int)(hpChange * source.getOffenses().get(aspect));
        }
        hpChange = (int)(hpChange * target.getHealingModifier());
        target.setHp(target.getHp() + hpChange);
        ActionResult result = new ActionResult();
        result.setText(source.getName() + " casted Heal on "+target.getName()+"!");
        result.setHpChange(hpChange);
        return result;

    }

    public ActionResult LifeEffect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.WATER;
        int hpChange = 3;
        if(target.getHp() > 0) {
            logger.log("DID YOU CAST LIFE ON SOMEONE LIVING!?");
        }
        target.setHp(hpChange);
        ActionResult result = new ActionResult();
        result.setHpChange(hpChange);
        result.setText(source.getName() + " casted Life on "+target.getName()+" and reawakened them!");
        return result;
    }

    public ActionResult Heal2Effect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.WATER;
        int hpChange = 6;
        if(source.getPassives().hasPassive("EnhancedHeal")) {
            hpChange = 9;
        }
        if(source.getPassives().hasPassive("EnhancedHeal2")) {
            hpChange = 9 + (InternalState.i().level().getLevel() / 10);
        }
        if(source.getOffenses().containsKey(aspect)) {
            hpChange = (int)(hpChange * source.getOffenses().get(aspect));
        }
        hpChange = (int)(hpChange * target.getHealingModifier());
        target.setHp(target.getHp() + hpChange);
        ActionResult result = new ActionResult();
        result.setText(source.getName() + " casted Heal II on "+target.getName()+"!");
        result.setHpChange(hpChange);
        return result;
    }

    public ActionResult LifeboostEffect(EntityInfo target, EntityInfo source) {
        int maxHpChange = (int)(target.getMaxHp() * 0.2);
        maxHpChange = (int)(maxHpChange * target.getHealingModifier());
        target.setHp(target.getHp() + maxHpChange);
        StatusEffect effect = new StatusEffect("Lifeboost", Stat.MAX_HP, maxHpChange, 3);
        effect.setPositive(true);
        target.getStatusEffects().addEffect(effect);
        ActionResult result = new ActionResult();
        result.getAddedStatusEffect().add(effect);
        result.setHpChange(maxHpChange);
        result.setText(source.getName() + " casted Lifeboost and boosted "+target.getName()+"'s life!");
        return result;
    }

    public ActionResult Heal3Effect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.WATER;
        int hpChange = 3;
        if(source.getPassives().hasPassive("EnhancedHeal2")) {
            hpChange = 3 + (InternalState.i().level().getLevel() / 10);
        }
        if(source.getOffenses().containsKey(aspect)) {
            hpChange = (int)(hpChange * source.getOffenses().get(aspect));
        }
        hpChange = (int)(hpChange * target.getHealingModifier());
        target.setHp(target.getHp() + hpChange);
        ActionResult result = new ActionResult();
        result.setText(source.getName() + " casted Heal III!");
        result.setHpChange(hpChange);
        return result;
    }

    public ActionResult LeechesEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        result.setText(source.getName() + " threw leeches onto "+target.getName()+"!");
        target.getStatusEffects().removeNegativeAilments();
        return result;
    }

}
