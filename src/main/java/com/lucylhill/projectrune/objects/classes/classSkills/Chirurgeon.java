package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.objects.battle.actions.Action;
import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.services.DebugLogger;

import java.util.Random;

public class Chirurgeon {
    ActionEffectResolver resolver = new ActionEffectResolver();
    DebugLogger logger = new DebugLogger(this.getClass());
    Random random = new Random();

    public boolean BandageMasteryEffect(EntityInfo source) {
        return true;
    }

    public boolean FieldAidMasteryEffect(EntityInfo source) {
        return true;
    }

    public boolean AidMasteryEffect(EntityInfo source) {
        return true;
    }

    public ActionResult AidEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        int hpChange = (int)(target.getMaxHp() * .1);
        target.setHp(target.getHp() + hpChange);
        result.setHpChange(hpChange);
        return result;
    }

    public ActionResult RecoveryEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        target.getStatusEffects().removeNegativeAilments();
        result.setText(source.getName() + " gave aid to "+target.getName()+"!");
        return result;
    }

    public ActionResult FieldAid1Effect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        double percentageAmount = .25;
        if(source.getPassives().hasPassive("FieldAidMastery")) {
            percentageAmount = .5;
        }
        int hpChange = (int)(target.getMaxHp() * percentageAmount);
        target.setHp(target.getHp() + hpChange);
        result.setHpChange(hpChange);
        result.setText(source.getName() + " gave aid to "+target.getName()+"!");
        return result;
    }

    public ActionResult FieldAid2Effect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        double percentageAmount = .25;
        if(source.getPassives().hasPassive("FieldAidMastery")) {
            percentageAmount = .5;
        }
        int hpChange = (int)(target.getMaxHp() * percentageAmount);
        target.setHp(target.getHp() + hpChange);
        result.setHpChange(hpChange);
        result.setText(source.getName() + " gave aid to "+target.getName()+"!");
        return result;
    }

    public ActionResult FieldAid3Effect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        double percentageAmount = .25;
        if(source.getPassives().hasPassive("FieldAidMastery")) {
            percentageAmount = .5;
        }
        int hpChange = (int)(target.getMaxHp() * percentageAmount);
        target.setHp(target.getHp() + hpChange);
        target.getStatusEffects().removeEffect("*");
        result.setHpChange(hpChange);
        result.setText(source.getName() + " gave aid to "+target.getName()+"!");
        return result;
    }

    public ActionResult TraumaTechniquesEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        target.setHp(target.getMaxHp());
        result.setHpChange(target.getMaxHp());
        result.setText(source.getName() + " gave aid to "+target.getName()+"!");
        return result;
    }

    public ActionResult SpecialCareEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        target.setHp(target.getMaxHp());
        target.getStatusEffects().removeNegativeAilments();
        result.setHpChange(target.getMaxHp());
        result.setText(source.getName() + " gave succor to "+target.getName()+"!");
        return result;
    }

}
