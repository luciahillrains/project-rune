package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.objects.items.EquipmentType;
import com.lucylhill.projectrune.objects.passives.PassiveEffectResolver;

public class Magician {
    ActionEffectResolver resolver = new ActionEffectResolver();
    PassiveEffectResolver passiveEffectResolver = new PassiveEffectResolver();

    public boolean WandMasteryEffect(EntityInfo source) {
        if(passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.STAFF) ||
            passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.ROD)) {
            source.addAdditionalIntelligence(2);
        }
        return true;
    }

    public boolean PhysicalShieldEffect(EntityInfo source) {
        source.addAdditionalDefense(1);
        return true;
    }

    public boolean MagicWellEffect(EntityInfo source) {
        return true;
    }

    public ActionResult WindEffect(EntityInfo target, EntityInfo source) {
        Aspects alignment = Aspects.AIR;
        int potency = 3;
        int hitChance = 100;
        int numOfStacks = source.getStatusEffects().getStackOfEffect("Magic Well");
        int baseDamage = resolver.magicalDamage(target, source, potency);
        if(numOfStacks > 0) {
            baseDamage = (int)(baseDamage * (numOfStacks * 0.2));
        }
        baseDamage = baseDamage - target.getDefense();
        ActionResult result = resolver.basicAttack(baseDamage, target, source, alignment, hitChance);
        if(source.getPassives().hasPassive("MagicWell") && numOfStacks < 5) {
            StatusEffect effect = new StatusEffect("Magic Well", Stat.NONE, 0.0, -1 );
            effect.setStackable(true);
            effect.setIcon("🤯");
            effect.setPositive(true);
            source.getStatusEffects().addEffect(effect);
        }
        result.setText(source.getName() + " casted Wind!");
        return result;
    }

    public ActionResult ThunderEffect(EntityInfo target, EntityInfo source) {
        Aspects alignment = Aspects.AIR;
        int potency = 8;
        int hitChance = 50;
        int numOfStacks = source.getStatusEffects().getStackOfEffect("Magic Well");
        int baseDamage = resolver.magicalDamage(target, source, potency);
        if(numOfStacks > 0) {
            baseDamage = (int)(baseDamage * (numOfStacks * 0.2));
            hitChance = hitChance + (numOfStacks * 10);
        }
        baseDamage = baseDamage - target.getDefense();
        ActionResult result = resolver.basicAttack(baseDamage, target, source, alignment, hitChance);
        if(source.getPassives().hasPassive("MagicWell") && numOfStacks < 5) {
            StatusEffect effect = new StatusEffect("Magic Well", Stat.NONE, 0.0, -1 );
            effect.setStackable(true);
            effect.setIcon("🤯");
            effect.setPositive(true);
            source.getStatusEffects().addEffect(effect);
        }
        result.setText(source.getName() + " casted Thunder and "+((result.isActionMissed())?"it missed!":"it hit!"));
        return result;
    }

    public ActionResult HighWindEffect(EntityInfo target, EntityInfo source) {
        Aspects alignment = Aspects.AIR;
        int potency = 2;
        int hitChance = 100;
        int numOfStacks = source.getStatusEffects().getStackOfEffect("Magic Well");
        int baseDamage = resolver.magicalDamage(target, source, potency);
        if(numOfStacks > 0) {
            baseDamage = (int)(baseDamage * (numOfStacks * 0.2));
        }
        ActionResult result = resolver.basicAttack(baseDamage, target, source, alignment, hitChance);
        if(source.getPassives().hasPassive("MagicWell") && numOfStacks < 5) {
            StatusEffect effect = new StatusEffect("Magic Well", Stat.NONE, 0.0, -1 );
            effect.setStackable(true);
            effect.setIcon("🤯");
            effect.setPositive(true);
            source.getStatusEffects().addEffect(effect);
        }
        result.setText(source.getName() + " casted High Wind!");
        return result;
    }

    public ActionResult CloudFormationEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        result.setText(source.getName() + " casted Cloud Formation!  Clouds started to form!");
        target.addElementalOffense(Aspects.AIR, .2);
        return result;
    }

    public ActionResult AirCurrentsEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        result.setText(source.getName() + " casted Air Currents!  Air blankets the party!");
        target.getDefenses().put(Aspects.AIR, 0.0);
        return result;
    }

    public ActionResult MaelstromEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        Aspects alignment = Aspects.AIR;
        int potency = 6;
        int hitChance = 50;
        int numOfStacks = source.getStatusEffects().getStackOfEffect("Magic Well");
        int baseDamage = resolver.magicalDamage(target, source, potency);
        int amountofHits = 0;
        if(numOfStacks > 0) {
            baseDamage = (int)(baseDamage * (numOfStacks * 0.2));
            hitChance = hitChance + (numOfStacks * 10);
        }
        baseDamage = baseDamage - target.getDefense();
        for(int i = 0; i < 2; i++) {
            ActionResult subResult = resolver.basicAttack(baseDamage, target, source, alignment, hitChance);
            if(!subResult.isActionMissed()) {
                amountofHits++;
                result.setHpChange(result.getHpChange() + subResult.getHpChange());
            }
        }
        if(source.getPassives().hasPassive("MagicWell") && numOfStacks < 5) {
            StatusEffect effect = new StatusEffect("Magic Well", Stat.NONE, 0.0, -1 );
            effect.setStackable(true);
            effect.setIcon("🤯");
            effect.setPositive(true);
            source.getStatusEffects().addEffect(effect);
        }
        result.setText(source.getName() + " casted Maelstrom and lightning struck "+amountofHits+ " time(s)!");
        return result;
    }
}
