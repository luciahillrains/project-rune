package com.lucylhill.projectrune.objects.classes;

import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.passives.PassiveData;

import java.util.List;

public class ClassInfo {
    String name;
    String id;
    String flag;
    ClassAspects classAspect;
    Aspects aspect;
    String description;
    ClassType jobEligibility;
    int levelUnlock = 0;
    List<String> actions;
    List<PassiveData> passives;
    List<Stat> statPriority;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        if(this.id == null || this.id.equals("")) {
            this.id = name;

        }
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public ClassAspects getClassAspect() {
        return classAspect;
    }

    public void setClassAspect(ClassAspects classAspect) {
        this.classAspect = classAspect;
    }

    public Aspects getAspect() {
        return aspect;
    }

    public void setAspect(Aspects aspect) {
        this.aspect = aspect;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ClassType getJobEligibility() {
        return jobEligibility;
    }

    public void setJobEligibility(ClassType jobEligibility) {
        this.jobEligibility = jobEligibility;
    }

    public String getUIDescription() {
        return "(" + classAspect.toString() + "/" + aspect.toString() + ") " + description;
    }

    public int getLevelUnlock() {
        return levelUnlock;
    }

    public void setLevelUnlock(int levelUnlock) {
        this.levelUnlock = levelUnlock;
    }

    public List<String> getActions() {
        return actions;
    }

    public void setActions(List<String> actions) {
        this.actions = actions;
    }

    public List<PassiveData> getPassives() {
        return passives;
    }

    public void setPassives(List<PassiveData> passives) {
        this.passives = passives;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Stat> getStatPriority() {
        return statPriority;
    }

    public void setStatPriority(List<Stat> statPriority) {
        this.statPriority = statPriority;
    }
}
