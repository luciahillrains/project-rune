package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;

public class Enemy {
    ActionEffectResolver resolver = new ActionEffectResolver();

    public ActionResult waitAndSeeEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        result.setText(source.getName() + " bided their time!");
        return result;
    }

    public ActionResult weakAttackEffect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.PHYSICAL;
        ActionResult result = resolver.basicAttack(resolver.physicalDamage(target, source, 2), target, source, aspect, 90);
        result.setText(source.getName() + " attacked!");
        return result;
    }
    public ActionResult strongAttackEffect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.PHYSICAL;
        ActionResult result =  resolver.basicAttack(resolver.physicalDamage(target, source, 3), target, source, aspect, 85);
        result.setText(source.getName() + " wildly attacked!");
        return result;
    }
}
