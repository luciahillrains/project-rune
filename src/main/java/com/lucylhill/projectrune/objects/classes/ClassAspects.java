package com.lucylhill.projectrune.objects.classes;

public enum ClassAspects {
    LIGHT("Holy"),
    DARK("Vile"),
    NEUTRAL("Neutral");

    private final String name;

    ClassAspects(String name) {
        this.name = name;
    }

    public String toString() {
        return this.name;
    }
}
