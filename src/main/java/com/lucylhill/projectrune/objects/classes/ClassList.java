package com.lucylhill.projectrune.objects.classes;

import com.lucylhill.projectrune.services.InternalState;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

public class ClassList {
    List<ClassInfo> classes;

    public List<ClassInfo> getClasses() {
        return classes;
    }

    public void setClasses(List<ClassInfo> classes) {
        this.classes = classes;
    }

    public List<ClassInfo> getBasicClasses() {
        return classes.stream().filter( cls -> cls.getJobEligibility() == ClassType.BASIC).collect(Collectors.toList());
    }

    public List<ClassInfo> getLevelClassesUnderALevel() {
        return classes.stream().filter(cls -> cls.getJobEligibility() == ClassType.LEVEL && cls.getLevelUnlock() <= InternalState.i().level().getLevel()).collect(Collectors.toList());
    }

    public List<ClassInfo> getEnabledClasses() {
        List<ClassInfo> basicList = getBasicClasses();
        basicList.addAll(getLevelClassesUnderALevel());
        return basicList;
    }

    public Optional<ClassInfo> getClassByName(String name) {
        List<ClassInfo> possibleClasses =  classes.stream().filter(cls -> cls.getName().equals(name)).collect(Collectors.toList());
        if(possibleClasses.size() == 0) {
            return Optional.empty();
        }
        return Optional.of(possibleClasses.get(0));
    }

    //this will need to be changed in the future to include classes you've unlocked only.
    public ClassInfo getRandomClass() {
        Random random = new Random();
        List<ClassInfo> enabledClasses = getEnabledClasses();
        return enabledClasses.get(random.nextInt(enabledClasses.size()));
    }
}
