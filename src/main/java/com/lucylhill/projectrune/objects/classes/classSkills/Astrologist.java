package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.services.DebugLogger;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Astrologist {
    DebugLogger logger = new DebugLogger(this.getClass());
    ActionEffectResolver resolver = new ActionEffectResolver();

    public boolean NightAndDayEffect(EntityInfo source) {
        source.addElementalDefense(Aspects.DARK, .2);
        source.addElementalDefense(Aspects.HOLY, .2);
        source.addElementalOffense(Aspects.DARK, 1.2);
        source.addElementalDefense(Aspects.HOLY, 1.2);
        return true;
    }

    public boolean LightOfTheStarsEffect(EntityInfo source) {
        source.addElementalDefense(Aspects.HOLY, .2);
        return true;
    }

    public boolean VoidOfSpaceEffect(EntityInfo source) {
        source.addElementalOffense(Aspects.DARK, .2);
        return true;
    }

    public ActionResult ShootingStarEffect(EntityInfo target, EntityInfo source) {
        int baseDamage = 6;
        int calculation = resolver.magicalDamage(target, source, baseDamage);
        Aspects aspect = Aspects.HOLY;
        ActionResult result = resolver.basicAttack(calculation, target, source, aspect, 90);
        result.setText(source.getName() + " used Shooting Star!");
        return result;
    }

    public ActionResult SupernovaEffect(EntityInfo target, EntityInfo source) {
        int baseDamage = 4;
        int calculation = resolver.magicalDamage(target, source, baseDamage);
        Aspects aspect = Aspects.DARK;
        ActionResult result = resolver.basicAttack(calculation, target, source, aspect, 90);
        result.setText(source.getName() + " used Supernova!");
        return result;
    }

    public ActionResult HoroscopeEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        result.setDrawCard(true);
        result.setText(source.getName() + " drew up a horoscope!");
        return result;
    }

    public ActionResult SpaceEchoEffect(EntityInfo target, EntityInfo source) {
        int baseDamage = 3;
        String text = source.getName() + " used Space Echo!";
        if(source.getStatusEffects().getEffects().stream().filter(e -> !e.isPositive()).count() > 1) {
            baseDamage = 5;
            List<StatusEffect> statusEffectsToAdd = source.getStatusEffects().getEffects().stream().filter(e -> !e.isPositive() && e.getIsNamedAilment()).collect(Collectors.toList());
            for(StatusEffect e : statusEffectsToAdd) {
                target.getStatusEffects().addEffect(e);
            }
            text = text + " " + target.getName() + " is now infected!";
        }
        int calculation = resolver.magicalPhysicalDamage(target, source, baseDamage);
        Aspects aspect = Aspects.DARK;
        ActionResult result = resolver.basicAttack(calculation, target, source, aspect, 90);
        result.setText(text);
        return result;
    }

    public ActionResult Horoscope2Effect(EntityInfo target, EntityInfo source) {
        Random random = new Random();
        ActionResult result = new ActionResult();
        String text = source.getName() + " drew a horoscope!";
        int hit = random.nextInt(100);
        if(hit < 30) {
            text = text + " "+target.getName() + " has a bad future!";
            //bad result
            int hpChange = (int)(target.getHp() * .25);
            target.setHp(target.getHp() - hpChange);
            result.setHpChange(hpChange);
        }
        else if(hit < 70) {
            text = text + " " + target.getName() + " has a netural future!";
            //neutral result
            target.getStatusEffects().removeNegativeDebuffs();
        }
        else {
            text = text + " " + target.getName() + " has a good future!";
            //good result
            target.setHp(target.getMaxHp());
            result.setHpChange(target.getMaxHp());
        }
        result.setText(text);
        return result;
    }

    public ActionResult BigBangEffect(EntityInfo target, EntityInfo source) {
        int baseDamage = 10;
        Aspects aspect1 = Aspects.DARK;
        Aspects aspect2 = Aspects.HOLY;
        int calculation = resolver.magicalDamage(target, source, baseDamage);
        ActionResult result = resolver.basicAttack(calculation, target, source, aspect1, aspect2, 100);
        result.setText(source.getName() + " recreated the big bang!");
        return result;
    }
}
