package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.objects.items.EquipmentType;
import com.lucylhill.projectrune.objects.passives.PassiveEffectResolver;

public class Musketeer {
    ActionEffectResolver resolver = new ActionEffectResolver();
    PassiveEffectResolver passiveEffectResolver = new PassiveEffectResolver();
    public boolean MusketballEffect(EntityInfo source) {
        StatusEffect effect = new StatusEffect("Musketball", Stat.NONE, 0, -1);
        source.getStatusEffects().addEffect(effect);
        return true;
    }

    public boolean MusketSupremacyEffect(EntityInfo source) {
        if(passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.MUSKET)) {
            source.addAdditionalAttack(3);
            source.setAdditionalAttack(source.getAdditionalAttack() + 3);
        }
        return true;
    }

    public boolean EnhancedScoutEffect(EntityInfo source) {
        return true;
    }

    public ActionResult BayonetteStrikeEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        int baseDamage = 2;
        int hitRate = 100;
        Aspects aspect = Aspects.PHYSICAL;
        if(passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.MUSKET)) {
            result = resolver.basicAttack(resolver.physicalPiercingDamage(source, baseDamage), target, source, aspect, hitRate);
            result.setText(source.getName() + " attacked "+target.getName()+" with a bayonette!");
        } else {
            result.setText(source.getName() + " does not have the right weapon to do this attack!");
        }
        return result;
    }

    public ActionResult FireShotEffect(EntityInfo target, EntityInfo source) {
        int baseDamage = 4;
        Aspects aspect = Aspects.FIRE;
        int hitChance = 80;
        if(source.getStatusEffects().hasAilment("Musketball")) {
            baseDamage = 7;
            source.getStatusEffects().removeEffect("Musketball");
        }
        if(source.getStatusEffects().hasAilment("Scouting")) {
            hitChance = hitChance + 10;
            if(source.getPassives().hasPassive("EnhancedScout")) {
                baseDamage = baseDamage + 2;
            }
        }
        ActionResult result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, hitChance);
        result.setText(source.getName() + " fired a shot at "+target.getName());
        return result;
    }

    public ActionResult ViewpointEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        StatusEffect effect = new StatusEffect("Scouting", Stat.NONE, 0, 3);
        result.getAddedStatusEffect().add(effect);
        target.getStatusEffects().addEffect(effect);
        result.setText(source.getName() + " started scouting!");
        return result;
    }

    public ActionResult ExplosiveRoundEffect(EntityInfo target, EntityInfo source) {
        int baseDamage = 3;
        Aspects aspect = Aspects.FIRE;
        int hitChance = 80;
        if (source.getStatusEffects().hasAilment("Musketball")) {
            baseDamage = 6;
            source.getStatusEffects().removeEffect("Musketball");
        }
        if (source.getStatusEffects().hasAilment("Scouting")) {
            hitChance = hitChance + 10;
            if (source.getPassives().hasPassive("EnhancedScout")) {
                baseDamage = baseDamage + 2;
            }
        }
        ActionResult result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, hitChance);
        result.setText(source.getName() + " fired an explosive round!");
        return result;
    }

    public ActionResult ReloadEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        result.setText(source.getName() + " reloads!");
        StatusEffect effect = new StatusEffect("Musketball", Stat.NONE, 0, -1);
        result.getAddedStatusEffect().add(effect);
        target.getStatusEffects().addEffect(effect);
        return result;
    }

    public ActionResult ScattershotEffect(EntityInfo target, EntityInfo source) {
        int baseDamage = 8;
        Aspects aspect = Aspects.FIRE;
        int hitChance = 80;
        if (source.getStatusEffects().hasAilment("Musketball")) {
            baseDamage = 10;
            source.getStatusEffects().removeEffect("Musketball");
        }
        if (source.getStatusEffects().hasAilment("Scouting")) {
            hitChance = hitChance + 10;
            if (source.getPassives().hasPassive("EnhancedScout")) {
                baseDamage = baseDamage + 2;
            }
        }
        ActionResult result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, hitChance);
        result.setText(source.getName() + " shoots a round point blank!");
        return result;
    }
}
