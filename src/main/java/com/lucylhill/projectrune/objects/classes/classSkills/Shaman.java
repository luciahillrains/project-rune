package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.objects.items.EquipmentType;
import com.lucylhill.projectrune.objects.passives.PassiveEffectResolver;
import com.lucylhill.projectrune.services.InternalState;

import java.util.Random;

public class Shaman {
    PassiveEffectResolver passiveEffectResolver = new PassiveEffectResolver();
    ActionEffectResolver resolver = new ActionEffectResolver();
    Random random = new Random();

    public boolean StaffSupremacyEffect(EntityInfo source) {
        if(passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.STAFF)) {
            source.addAdditionalIntelligence(3);
            source.addAdditionalDefense(3);
            source.addAdditionalAttack(1);
        }
        return true;
    }

    public boolean PoisonMastery(EntityInfo source) {
        return true;
    }

    public boolean MindswellEffect(EntityInfo source) {
        source.addAdditionalIntelligence(5);
        return true;
    }
    public ActionResult PoisonEffect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.EARTH;
        ActionResult result = new ActionResult();
        if (source.getPassives().hasPassive("PoisonMastery")) {
            int baseDamage = 4;
            int damage = resolver.magicalDamage(target, source, baseDamage);
            result = resolver.basicAttack(damage, source, target, aspect, 100);
        }
        if (random.nextInt(100) < 80) {
            StatusEffect effect = RuneConfiguration.i().ailmentList().getStatusEffect("Poison");
            target.getStatusEffects().addEffect(effect);
            result.getAddedStatusEffect().add(effect);
            result.setText(source.getName() + " casted Poison and poisoned " + target.getName());
        } else {
            result.setText(source.getName() + " casted Poison but no one was poisoned!");
        }
        return result;
    }

    public ActionResult RuinateEffect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.EARTH;
        ActionResult result = new ActionResult();
        if (source.getPassives().hasPassive("PoisonMastery")) {
            int baseDamage = 4;
            int damage = resolver.magicalDamage(target, source, baseDamage);
            result = resolver.basicAttack(damage, source, target, aspect, 100);
        }
        int randomHit = random.nextInt(100);
        result.setText(source.getName() + " casted Ruinate and nothing happened!");
        if(randomHit < 80) {
            StatusEffect effect;
            if(randomHit > 0 && randomHit < 20) {
                effect = RuneConfiguration.i().ailmentList().getStatusEffect("Paralysis");
            }
            else if (randomHit >= 20 && randomHit < 40 && target.isNormal()) {
                effect = RuneConfiguration.i().ailmentList().getStatusEffect("Confusion");
            }
            else if (randomHit >= 40 && randomHit < 60) {
                effect = RuneConfiguration.i().ailmentList().getStatusEffect("Blind");
            } else {
                effect = RuneConfiguration.i().ailmentList().getStatusEffect("Poison");
            }
            result.getAddedStatusEffect().add(effect);
            target.getStatusEffects().addEffect(effect);
            result.setText(source.getName() + " casted Ruinate and "+ target.getName() + " was ruined!");
        }
        return result;
    }

    public ActionResult NaturalShieldEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        StatusEffect effect = new StatusEffect("Natural Shield", Stat.DEFENSE, 5, 3);
        effect.setPositive(true);
        result.getAddedStatusEffect().add(effect);
        target.getStatusEffects().addEffect(effect);
        result.setText(source.getName() + " forms an shield around "+target.getName());
        return result;
    }

    public ActionResult Ruinate2Effect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.DARK;
        int baseDamage = 4;
        int damage = resolver.magicalDamage(target, source, baseDamage);
        ActionResult result = resolver.basicAttack(damage, source, target, aspect, 100);
        int randomHit = random.nextInt(100);
        result.setText(source.getName() + " casted Ruinate and nothing happened!");
        if(randomHit < 30) {
            result.getAddedStatusEffect().add(RuneConfiguration.i().ailmentList().getStatusEffect("Poison"));
            target.getStatusEffects().addEffect(RuneConfiguration.i().ailmentList().getStatusEffect("Poison"));
            result.getAddedStatusEffect().add(RuneConfiguration.i().ailmentList().getStatusEffect("Blind"));
            target.getStatusEffects().addEffect(RuneConfiguration.i().ailmentList().getStatusEffect("Blind"));
            result.getAddedStatusEffect().add(RuneConfiguration.i().ailmentList().getStatusEffect("Paralysis"));
            target.getStatusEffects().addEffect(RuneConfiguration.i().ailmentList().getStatusEffect("Paralysis"));
            if(target.isNormal()) {
                result.getAddedStatusEffect().add(RuneConfiguration.i().ailmentList().getStatusEffect("Confusion"));
                target.getStatusEffects().addEffect(RuneConfiguration.i().ailmentList().getStatusEffect("Confusion"));
            }
        }
        result.setText(source.getName() + " casted Ruined Doom!");
        return result;
    }

    public ActionResult VileplumeEffect(EntityInfo target, EntityInfo source) {
        StatusEffect statusEffect = new StatusEffect();
        statusEffect.setName("Vileplume");
        statusEffect.setStat(Stat.ACTION_ALIGNMENT);
        statusEffect.setInfo("DARK");
        statusEffect.setTurns(2);
        InternalState.i().getGlobalStatusEffects().addEffect(statusEffect);
        ActionResult result = new ActionResult();
        result.setText(source.getName() + " spreads vile essence all over!");
        result.getAddedStatusEffect().add(statusEffect);
        return result;
    }

    public ActionResult EnVitraEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        if(target.getHp() == 0) {
            target.setHp(1);
            target.getStatusEffects().addEffect(RuneConfiguration.i().ailmentList().getStatusEffect("Zombie"));
            result.getAddedStatusEffect().add(RuneConfiguration.i().ailmentList().getStatusEffect(""));
        }
        else if(target.getStatusEffects().hasAilment("Zombie")) {
            target.getStatusEffects().removeEffect("Zombie");
        }
        result.setText(source.getName() + " casted En Vitra!  The dead are re-animating!");
        return result;
    }
}
