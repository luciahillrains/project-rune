package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.objects.items.Item;
import com.lucylhill.projectrune.services.InternalState;

import java.util.Random;

public class Rogue {
    ActionEffectResolver resolver = new ActionEffectResolver();

    public boolean QuickfootEffect(EntityInfo source) {
        source.addAdditionalAgility(10);
        return true;
    }

    public boolean EnhancedStealEffect(EntityInfo source) {
        return true;
    }

    public boolean TreasureSenseEffect(EntityInfo source) {
        return true;
    }

    public ActionResult QuickAttackEffect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.AIR;
        int baseDamage = 3;
        int hitRate = 100;
        ActionResult result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, hitRate);
        result.setText(source.getName() + " used Quick Attack!");
        return result;
    }

    public ActionResult StealEffect(EntityInfo target, EntityInfo source) {
        int hitRate = 90;
        Random random = new Random();
        ActionResult result = new ActionResult();
        if(source.getPassives().hasPassive("EnhancedSteal")) {
            if(resolver.getStealingModifier() > 1) {
                resolver.setStealingModifier(resolver.getStealingModifier() - 1);
            }
        }
        result.setText(source.getName() + " tried to steal an item but failed!");
        if(random.nextInt(100) < hitRate) {
            Item item = resolver.steal(target, source);
            if(item != null) {
                result.setText(source.getName() + " stole a "+item.getName()+" in the confusion of battle!");
                InternalState.i().inventory().getItems().add(item);
            }
        }
        return result;
    }

    public ActionResult ShadowLurkEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        StatusEffect effect = new StatusEffect("Lurking", Stat.AGILITY, 30, 3);
        effect.setPositive(true);
        target.getStatusEffects().addEffect(effect);
        result.setText(source.getName() + " faded into the shadows!");
        result.getAddedStatusEffect().add(effect);
        return result;
    }

    public ActionResult MugEffect(EntityInfo target, EntityInfo source) {
        int stealHitRate = 25;
        int hitRate = 90;
        int baseDamage= 3;
        Aspects aspect = Aspects.FIRE;
        Random random = new Random();
        ActionResult result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, hitRate);
        String txt = source.getName() + " mugged "+target.getName()+"!";
        if(source.getPassives().hasPassive("EnhancedSteal")) {
            if(resolver.getStealingModifier() > 1) {
                resolver.setStealingModifier(resolver.getStealingModifier() - 1);
            }
        }
        if(!result.isActionMissed()) {
            Item item = resolver.steal(target, source);
            if(item != null) {
                txt = txt + " Stole a "+item.getName()+"!";
                InternalState.i().inventory().getItems().add(item);
            }

            result.setText(txt);
        } else {
            result.setText(source.getName() + " tried to steal an item but failed!");
        }
        return result;
    }

    public ActionResult FlashOfLightEffect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.FIRE;
        int baseDamage = 2;
        int hitRate = 100;
        ActionResult result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, hitRate);
        result.setText(source.getName() + " used Flash of Light!");
        return result;
    }

    public ActionResult AssassinateEffect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.DARK;
        int baseDamage = 1;
        String text = source.getName() + " attempted to assassinate!";
        if((double)target.getHp() / target.getMaxHp() < 0.1) {
            baseDamage = 10;
            text = text + " The assassination was successful!";
        } else {
            text = text + " The assassination failed!";
        }
        int hitRate = 100;
        ActionResult result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, hitRate);
        result.setText(text);
        return result;
    }
}
