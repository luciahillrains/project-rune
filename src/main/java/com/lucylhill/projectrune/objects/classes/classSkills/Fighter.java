package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;

import java.util.Random;

public class Fighter {
    ActionEffectResolver resolver = new ActionEffectResolver();
    public boolean BarefistedEffect(EntityInfo source) {
        source.addAdditionalAttack(3);
        if(source.getEquipment().getWeapon() != null) {
            int attack = source.getAdditionalAttack() - 10;
            if(attack < 0) {
                attack = 0;
            }
            source.setAdditionalAttack(attack);
        }
        return true;
    }

    public boolean IceMasteryEffect(EntityInfo source) {
        return true;
    }

    public boolean TrainingEffect(EntityInfo source) {
        return true;
    }

    public ActionResult PunchEffect(EntityInfo target, EntityInfo source) {
        int baseDamage = 3;
        int hitRate = 90;
        Aspects aspect = Aspects.PHYSICAL;
        ActionResult result;
        if(source.getPassives().hasPassive("IceMastery")) {
            result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, Aspects.ICE, hitRate);
        } else {
            result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, hitRate);
        }
        result.setText(source.getName() + " punches "+target.getName());
        return result;
    }

    public ActionResult KickEffect(EntityInfo target, EntityInfo source) {
        int baseDamage = 2;
        int hitRate = 90;
        Aspects aspect = Aspects.PHYSICAL;
        if(resolver.combo("Punch", "Kick")) {
            baseDamage = 5;
        }
        ActionResult result;
        if(source.getPassives().hasPassive("IceMastery")) {
            result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, Aspects.ICE, hitRate);
        } else {
            result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, hitRate);
        }
        result.setText(source.getName() + " kicks "+target.getName());
        return result;
    }

    public ActionResult IcePunchEffect(EntityInfo target, EntityInfo source) {
        int baseDamage = 3;
        int hitRate = 90;
        Random random = new Random();
        Aspects aspect = Aspects.ICE;
        ActionResult result;
        result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, hitRate);
        String text = source.getName() + " punches "+target.getName()+" with an icy fist!";
        if(resolver.combo("Punch", "IcePunch")) {
            if(random.nextInt(100) < 25) {
                StatusEffect effect = RuneConfiguration.i().ailmentList().getStatusEffect("Frozen");
                source.getStatusEffects().addEffect(effect);
                result.getAddedStatusEffect().add(effect);
                text = text + " " + target.getName() + " is frozen!";
            }
        }
        result.setText(text);
        return result;
    }

    public ActionResult UppercutEffect(EntityInfo target, EntityInfo source) {
        int baseDamage = 5;
        int hitRate = 90;
        Aspects aspect = Aspects.PHYSICAL;
        if(resolver.combo("Kick", "Uppercut")) {
            baseDamage = 7;
        }
        ActionResult result;
        if(source.getPassives().hasPassive("IceMastery")) {
            result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, Aspects.ICE, hitRate);
        } else {
            result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, hitRate);
        }
        result.setText(source.getName() + " uppercuts "+target.getName());
        return result;
    }

    public ActionResult FinisherEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        String text = source.getName() + " stumbles!";
        Random random = new Random();
        if(resolver.combo("IcePunch", "Finisher") || resolver.combo("Uppercut", "Finisher")) {
            text = source.getName() + " performs an Icy Finisher on "+target.getName()+"!";
            int baseDamage = 8;
            int hitRate = 90;
            Aspects aspect = Aspects.ICE;
            if(random.nextInt(100) < 50) {
                StatusEffect effect = RuneConfiguration.i().ailmentList().getStatusEffect("Frozen");
                source.getStatusEffects().addEffect(effect);
                result.getAddedStatusEffect().add(effect);
                text = text + target.getName() + " is Frozen!";
            }
            if(random.nextInt(100) < 50) {
                StatusEffect effect = RuneConfiguration.i().ailmentList().getStatusEffect("Frostbite");
                source.getStatusEffects().addEffect(effect);
                result.getAddedStatusEffect().add(effect);
                text = text + target.getName() + " is Frostbitten!";
            }
            result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, hitRate);
            result.setText(text);
        } else {
            result.setText(text);
        }
        return result;
    }

    public ActionResult InternalReleaseEffect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.ICE;
        int div = source.getMaxHp() - source.getHp();
        if(div == 0) {
            div = 12;
        }
        int baseDamage = 12 / div;
        ActionResult result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, 90);
        result.setText(target.getName() + " releases their energy!");
        return result;
    }
}
