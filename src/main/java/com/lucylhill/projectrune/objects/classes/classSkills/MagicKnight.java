package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.objects.battle.BattleState;
import com.lucylhill.projectrune.objects.battle.actions.Action;
import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.objects.items.EquipmentType;
import com.lucylhill.projectrune.objects.passives.PassiveEffectResolver;
import com.lucylhill.projectrune.services.DebugLogger;

import java.util.HashMap;
import java.util.Random;

public class MagicKnight {
    ActionEffectResolver resolver = new ActionEffectResolver();
    PassiveEffectResolver passiveEffectResolver = new PassiveEffectResolver();
    DebugLogger logger = new DebugLogger(this.getClass());
    Random random = new Random();
    Aspects[] secondaryAspects = { Aspects.AIR, Aspects.FIRE, Aspects.ICE, Aspects.WATER, Aspects.EARTH};

    public boolean ElementalDefenseEffect(EntityInfo source) {
        source.setDefenses(new HashMap<>());
        source.getDefenses().put(Aspects.FIRE, .95);
        source.getDefenses().put(Aspects.ICE, .95);
        source.getDefenses().put(Aspects.AIR, .95);
        source.getDefenses().put(Aspects.WATER, .95);
        source.getDefenses().put(Aspects.EARTH, .95);
        return true;
    }

    public boolean BarrierChangeEffect(EntityInfo source) {
        if(source.getLastSkillHit() != null) {
            Action a = source.getLastSkillHit();
            source.getDefenses().put(a.getAlignment(), source.getDefenses().get(a.getAlignment()) - .5);
            source.setLastSkillHit(null);
        }
        return true;
    }

    public boolean KnightSupremacyEffect(EntityInfo source) {
        if(passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.SWORD) ||
            passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.GREATSWORD) ||
            passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.AXE) ||
            passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.GREATAXE)) {
            source.addAdditionalAttack(5);
            source.addAdditionalDefense(5);
            source.addAdditionalIntelligence(10);
        }
        return true;
    }

    public ActionResult EnmityEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        result.setText(source.getName() + " is intercepting all attacks!");
        BattleState.i().getEnmityMap().put(target.getName() + target.hashCode(), source.getName());
        return result;
    }

    public ActionResult FireSliceEffect(EntityInfo target, EntityInfo source) {
        ActionResult result;
        int baseDamage = 2;
        Aspects aspect = Aspects.FIRE;
        result = resolver.basicAttack(resolver.magicalPhysicalDamage(target, source, baseDamage), target, source, aspect, 90);
        result.setText(source.getName() + " used Fire Slice!");
        return result;
    }

    public ActionResult IceSliceEffect(EntityInfo target, EntityInfo source) {
        ActionResult result;
        int baseDamage = 2;
        Aspects aspect = Aspects.ICE;
        result = resolver.basicAttack(resolver.magicalPhysicalDamage(target, source, baseDamage), target, source, aspect, 90);
        result.setText(source.getName() + " used Ice Slice!");
        return result;
    }

    public ActionResult EarthenFuryEffect(EntityInfo target, EntityInfo source) {
        ActionResult result;
        int baseDamage = 1;
        Aspects aspect = Aspects.EARTH;
        result = resolver.basicAttack(resolver.magicalPhysicalDamage(target, source, baseDamage), target, source, aspect, 90);
        result.setText(source.getName() + " used Earthen Fury!");
        return result;
    }

    public ActionResult AeroFuryEffect(EntityInfo target, EntityInfo source) {
        ActionResult result;
        int baseDamage = 1;
        Aspects aspect = Aspects.AIR;
        result = resolver.basicAttack(resolver.magicalPhysicalDamage(target, source, baseDamage), target, source, aspect, 90);
        result.setText(source.getName() + " used Aero Fury!");
        return result;
    }

    public ActionResult GrandCrossEffect(EntityInfo target, EntityInfo source) {
        ActionResult result;
        int baseDamage = 12;
        Aspects aspect = Aspects.OMNI;
        Aspects aspect2 = secondaryAspects[random.nextInt(secondaryAspects.length)];
        result = resolver.basicAttack(resolver.magicalPhysicalDamage(target, source, baseDamage), target, source, aspect, aspect2,85);
        result.setText(source.getName() + " used Grand Cross!");
        return result;
    }
}
