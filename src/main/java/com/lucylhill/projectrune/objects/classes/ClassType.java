package com.lucylhill.projectrune.objects.classes;

public enum ClassType {
    BASIC,
    LEVEL,
    BOOK
}
