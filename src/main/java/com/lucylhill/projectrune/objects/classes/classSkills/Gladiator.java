package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.battle.BattleState;
import com.lucylhill.projectrune.objects.battle.actions.Action;
import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.objects.items.EquipmentType;
import com.lucylhill.projectrune.objects.passives.PassiveEffectResolver;

import java.util.Random;

public class Gladiator {
    ActionEffectResolver resolver = new ActionEffectResolver();
    PassiveEffectResolver passiveEffectResolver = new PassiveEffectResolver();
    Random random = new Random();

    public boolean ShakeItOffEffect(EntityInfo source) {
        source.getStatusEffects().removeNegativeAilments();
        return true;
    }

    public boolean AdrenalineRushEffect(EntityInfo source) {
        if(source.getHp() < (source.getMaxHp() * 0.1)) {
            StatusEffect effect = RuneConfiguration.i().ailmentList().getStatusEffect("Adrenaline Rush");
            source.getStatusEffects().addEffect(effect);
        }
        return true;
    }

    public boolean EagleSpiritEffect(EntityInfo source) {
        source.addAdditionalAttack(3);
        source.addAdditionalDefense(3);
        return true;
    }

    public ActionResult DefendEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        result.setText(source.getName() + " defends!");
        StatusEffect effect = new StatusEffect("Defend", Stat.DEFENSE, 8, 2);
        result.getAddedStatusEffect().add(effect);
        target.getStatusEffects().addEffect(effect);
        return result;
    }

    public ActionResult HeavyStrikeEffect(EntityInfo target, EntityInfo source) {
        int baseDamage = 3;
        if(passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.SWORD) ||
                passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.GREATAXE) ||
                passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.GREATSWORD)) {
            baseDamage = 6;
        }
        ActionResult result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, Aspects.PHYSICAL, 95);
        result.setText(source.getName() + " uses Heavy Strike!");
        BattleState.i().getEnmityMap().put(target.getName() + target.hashCode(), source.getName());
        return result;
    }

    public ActionResult HeavySlashEffect(EntityInfo target, EntityInfo source) {
        if(passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.SWORD) ||
            passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.GREATAXE) ||
            passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.GREATSWORD)) {
            int baseDamage = 3;
            if(resolver.combo("HeavyStrike", "HeavySlash")) {
                baseDamage = 5;
            }
            ActionResult result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, Aspects.PHYSICAL, 95);
            result.setText(source.getName() + " uses Heavy Slash!");
            return result;
        } else {
            ActionResult result = new ActionResult();
            result.setText(source.getName() + " doesn't have a weapon to use this action!");
            return result;
        }

    }

    public ActionResult AdrenalineEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        target.setHp(target.getMaxHp());
        target.getStatusEffects().removeNegativeAilments();
        result.setText(target.getName() + " gets pumped!");
        return result;
    }

    public ActionResult SelfIncapacitationEffect(EntityInfo target, EntityInfo source) {
        target.getStatusEffects().addEffect(new StatusEffect("Phalanx", Stat.DEFENSE, 4, 4));
        ActionResult result = new ActionResult();
        result.setText(target.getName() + " blocks with their shield!");
        return result;
    }

    public ActionResult FatalityEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        result.setText(target.getName() + " stumbled!");
        if(source.getStatusEffects().hasAilment("Adrenaline Rush")
                || source.getStatusEffects().hasAilment("Phalanx")) {
            if(passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.SWORD) ||
                    passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.GREATAXE) ||
                    passiveEffectResolver.sourceHasWeaponType(source, EquipmentType.GREATSWORD)) {
                result = resolver.basicAttack(resolver.physicalDamage(target, source, 24), target, source, Aspects.PHYSICAL, 100);
                if(random.nextInt(100) <= 15){
                    result.setText(target.getName() + " used their last breath to use Fatal Rush!");
                    source.setHp(0);
                } else {
                    result.setText(target.getName() + " used Fatal Rush!");
                }
            }
        }
        return result;
    }

}
