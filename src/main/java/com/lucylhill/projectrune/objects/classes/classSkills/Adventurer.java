package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.services.DebugLogger;

import java.util.List;

public class Adventurer {
    DebugLogger logger = new DebugLogger(this.getClass());
    ActionEffectResolver resolver = new ActionEffectResolver();

    public boolean PerserveranceEffect(EntityInfo source) {
        source.addAdditionalDefense(2);
        return true;
    }

    public boolean HungryEffect(EntityInfo source) {
        return true;
    }

    public boolean WeaponsExpertEffect(EntityInfo source) {
        return true;
    }

    public ActionResult ActiveSliceEffect(EntityInfo target, EntityInfo source) {
        int baseDamage = 3;
        Aspects aspect = Aspects.PHYSICAL;
        int hitChance = 90;
        if(source.getPassives().hasPassive("WeaponsExpert") && source.getEquipment().getWeapon() != null) {
            //TODO: check for equipment
            baseDamage = 6;
        }
        ActionResult result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, hitChance);
        if(!result.isActionMissed()) {
            result.setText(source.getName() + " used Active Slice!");
        }
        return result;
    }

    public ActionResult AdventureAidEffect(EntityInfo target, EntityInfo source) {
        int healing = target.getMaxHp() / 4;
        healing = (int)(healing * target.getHealingModifier());
        target.setHp(target.getHp() + healing);
        ActionResult result = new ActionResult();
        result.setText(source.getName() + " gave aid to "+target.getName()+"!");
        result.setHpChange(healing);
        return result;
    }

    public ActionResult WilddriveEffect(EntityInfo target, EntityInfo source) {
        double baseDamage = 2.5;
        Aspects aspect = Aspects.FIRE;
        int hitchance = 90;
        if(source.getPassives().hasPassive("WeaponsExpert")) {
            baseDamage = 3;
        }
        ActionResult result = resolver.basicAttack((int)(baseDamage * source.getAttack()) - target.getDefense(), target, source, aspect, hitchance);
        if(!result.isActionMissed()) {
            result.setText(source.getName() + " used Wilddrive!");
        }
        return result;
    }

    public ActionResult PumpUpEffect(EntityInfo target, EntityInfo source) {
        StatusEffect effect1 = new StatusEffect("Pumped Up Attack!", Stat.ATTACK, 5, 3);
        StatusEffect effect2 = new StatusEffect("Pumped Up Defense!", Stat.DEFENSE, 5, 3);
        target.getStatusEffects().addEffect(effect1);
        target.getStatusEffects().addEffect(effect2);
        ActionResult result = new ActionResult();
        result.setText(source.getName() + " pumped up "+target.getName()+"!");
        result.setAddedStatusEffect(List.of(effect1, effect2));
        return result;
    }

    public ActionResult MeanWordsEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        if(resolver.didAttackHit(60, target, source)) {
            StatusEffect effect = RuneConfiguration.i().ailmentList().getStatusEffect("Enraged");
            target.getStatusEffects().addEffect(effect);
            result.setText(source.getName() + " called "+target.getName()+" a bloody mess up!  "+target.getName()+" is enraged!");
            result.setAddedStatusEffect(List.of(effect));
        } else {
            result.setText(source.getName() + " called "+target.getName()+" a bloody mess up!  "+target.getName()+" didn't react!");
        }

        return result;
    }

    public ActionResult TimeOutEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        int hpRecovery = target.getMaxHp();
        target.setHp(hpRecovery);
        result.setHpChange(hpRecovery);
        result.setText(source.getName() + " took a break!");

        StatusEffect effect1 = new StatusEffect("Coffee Break!", Stat.DEFENSE, -100, 2);
        StatusEffect effect2 = new StatusEffect("Coffee Break", Stat.ATTACK, -100, 2);
        result.getAddedStatusEffect().addAll(List.of(effect1, effect2));
        target.getStatusEffects().addEffect(effect1);
        target.getStatusEffects().addEffect(effect2);
        return result;
    }
}
