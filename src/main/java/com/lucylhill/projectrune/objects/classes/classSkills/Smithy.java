package com.lucylhill.projectrune.objects.classes.classSkills;

import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.objects.items.Item;
import com.lucylhill.projectrune.objects.passives.PassiveEffectResolver;
import com.lucylhill.projectrune.services.InternalState;

import java.util.Random;

public class Smithy {
    ActionEffectResolver resolver = new ActionEffectResolver();
    Random random = new Random();
    public boolean HammerArmsEffect(EntityInfo source) {
        source.addAdditionalAttack(2);
        return true;
    }

    public boolean HammerBodyEffect(EntityInfo source) {
        source.addAdditionalDefense(3);
        return true;
    }

    public boolean SynthEffect(EntityInfo source) {
        return true;
    }

    public ActionResult TemperEffect(EntityInfo target, EntityInfo source) {
        StatusEffect effect = new StatusEffect("Temper", Stat.ATTACK, 1, 99);
        effect.setPositive(true);
        effect.setStackable(true);
        ActionResult actionResult = new ActionResult();
        actionResult.setText(source.getName() + " tempered "+target.getName()+"'s weapons!");
        actionResult.getAddedStatusEffect().add(effect);
        target.getStatusEffects().addEffect(effect);
        return actionResult;
    }

    public ActionResult StrengthenEffect(EntityInfo target, EntityInfo source) {
        StatusEffect effect = new StatusEffect("Strengthen", Stat.DEFENSE, 1, 99);
        effect.setPositive(true);
        effect.setStackable(true);
        ActionResult actionResult = new ActionResult();
        actionResult.setText(source.getName() + " strengthened "+target.getName()+"'s armor!");
        actionResult.getAddedStatusEffect().add(effect);
        target.getStatusEffects().addEffect(effect);
        return actionResult;
    }

    public ActionResult HammerStrikeEffect(EntityInfo target, EntityInfo source) {
        Aspects aspect = Aspects.FIRE;
        int baseDamage = random.nextInt(1, 7);
        int hitRate = 100;
        ActionResult result = resolver.basicAttack(resolver.physicalDamage(target, source, baseDamage), target, source, aspect, hitRate);
        result.setText(source.getName() + " used Hammer Throw");
        return result;
    }

    public ActionResult TemperAndStrengthenEffect(EntityInfo target, EntityInfo source) {
        StatusEffect effect = new StatusEffect("Temper", Stat.ATTACK, 1, 99);
        effect.setPositive(true);
        effect.setStackable(true);
        ActionResult actionResult = new ActionResult();
        actionResult.getAddedStatusEffect().add(effect);
        target.getStatusEffects().addEffect(effect);
        StatusEffect effect2 = new StatusEffect("Strengthen", Stat.DEFENSE, 1, 99);
        effect2.setPositive(true);
        effect2.setStackable(true);
        actionResult.getAddedStatusEffect().add(effect2);
        target.getStatusEffects().addEffect(effect2);
        actionResult.setText(source.getName() + " tempered & strengthen "+target.getName()+"'s equipment!");
        return actionResult;
    }

    public ActionResult SharpenEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        result.setText(source.getName() + " tried to sharpen "+target.getName()+" but there was nothing to sharpen!");
        if(target.getEquipment().getWeapon() != null) {
            StatusEffect effect = new StatusEffect("Sharpen", Stat.ATTACK, 5, 3);
            effect.setPositive(true);
            target.getStatusEffects().addEffect(effect);
            result.getAddedStatusEffect().add(effect);
            result.setText(source.getName() + " sharpened "+target.getName()+"'s weapons!");
        }
        return result;
    }

    public ActionResult SynthesisEffect(EntityInfo target, EntityInfo source) {
        ActionResult result = new ActionResult();
        if(target.getHp() < (target.getMaxHp() * 0.05)) {
            target.setHp(0);
            String itemId = target.getMorphItemId();
            Item item = resolver.getItemFromId(itemId);
            if(item != null) {
                result.setText(source.getName() + " morphed "+target.getName()+" into a "+item.getName()+"!");
                InternalState.i().inventory().getItems().add(item);
            }
        } else {
            result.setText(source.getName() + " stumbled!");
        }
        return result;
    }
}
