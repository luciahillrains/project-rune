package com.lucylhill.projectrune.objects.items;

public enum EquipmentFlag {
    WEAPON,
    BODY,
    RELIC,
    RUNE
}
