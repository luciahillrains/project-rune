package com.lucylhill.projectrune.objects.items;

public class Equipment extends Item {
    EquipmentFlag flag;
    EquipmentType type;

    public EquipmentFlag getFlag() {
        return flag;
    }

    public void setFlag(EquipmentFlag flag) {
        this.flag = flag;
    }

    public EquipmentType getType() {
        return type;
    }

    public void setType(EquipmentType type) {
        this.type = type;
    }
}
