package com.lucylhill.projectrune.objects.items;

import com.lucylhill.projectrune.objects.battle.actions.ActionTargetType;

import java.util.Objects;

public class Item {
    String name;
    String id;
    String description;
    boolean isComponent;
    boolean onlyUsableInBattle;
    int cost;
    boolean keyItem;
    boolean isAlcohol;
    boolean isMeat;
    boolean isPlant;
    boolean rune = false;
    ActionTargetType targetType = ActionTargetType.PARTY_SINGLE;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isComponent() {
        return isComponent;
    }

    public void setIsComponent(boolean component) {
        isComponent = component;
    }

    public boolean isOnlyUsableInBattle() {
        return onlyUsableInBattle;
    }

    public void setOnlyUsableInBattle(boolean onlyUsableInBattle) {
        this.onlyUsableInBattle = onlyUsableInBattle;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public boolean isKeyItem() {
        return keyItem;
    }

    public void setKeyItem(boolean keyItem) {
        this.keyItem = keyItem;
    }

    public ActionTargetType getTargetType() {
        return targetType;
    }

    public void setTargetType(ActionTargetType targetType) {
        this.targetType = targetType;
    }

    public boolean isRune() {
        return rune;
    }

    public void setRune(boolean rune) {
        this.rune = rune;
    }

    @Override
    public String toString() {
        return name;
    }

    public boolean isAlcohol() {
        return isAlcohol;
    }

    public void setIsAlcohol(boolean alcohol) {
        isAlcohol = alcohol;
    }

    public boolean isMeat() {
        return isMeat;
    }

    public void setIsMeat(boolean meat) {
        isMeat = meat;
    }

    public boolean isPlant() {
        return isPlant;
    }

    public void setIsPlant(boolean plant) {
        isPlant = plant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return Objects.equals(id, item.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
