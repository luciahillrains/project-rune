package com.lucylhill.projectrune.objects.items;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ItemList {
    List<Item> items = new ArrayList<>();

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Optional<Item> getItemById(String id) {
        List<Item> subset = items.stream()
                .filter(i -> i.getId().equals(id))
                .collect(Collectors.toList());
        if(subset.size() >0) {
            return Optional.of(subset.get(0));
        }
        return Optional.empty();
    }
}
