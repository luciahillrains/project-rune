package com.lucylhill.projectrune.objects.items;

import com.lucylhill.projectrune.objects.entities.EntityInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Equipped {
    Equipment weapon;
    Equipment body;
    List<Equipment> relics = Arrays.asList(null, null, null);
    EntityInfo parent;
    EquipmentResolver resolver = new EquipmentResolver();
    public static final int MAX_NUM_OF_RELICS_EQUIPPED = 3;

    public Equipped(EntityInfo parent) {
        this.parent = parent;
    }

    public Equipment equipWeapon(Equipment equipment) {
        Equipment old = this.weapon;
        resolver.resolveEquipment(old, parent, EquipmentResolverState.UNEQUIP);
        this.weapon = equipment;
        resolver.resolveEquipment(equipment, parent, EquipmentResolverState.EQUIP);
        return old;
    }

    public Equipment equipBody(Equipment equipment) {
        Equipment old = this.body;
        resolver.resolveEquipment(old, parent, EquipmentResolverState.UNEQUIP);
        this.body = equipment;
        resolver.resolveEquipment(this.body, parent, EquipmentResolverState.EQUIP);
        return old;
    }

    public Equipment equipRelic(Equipment equipment, int slot) {
        Equipment old;
        old = relics.get(slot);
        resolver.resolveEquipment(old, parent, EquipmentResolverState.UNEQUIP);
        relics.set(slot, equipment);
        resolver.resolveEquipment(equipment, parent, EquipmentResolverState.EQUIP);
        return old;
    }

    public Equipment getWeapon() {
        return weapon;
    }

    public void setWeapon(Equipment weapon) {
        this.weapon = weapon;
    }

    public Equipment getBody() {
        return body;
    }

    public void setBody(Equipment body) {
        this.body = body;
    }

    public List<Equipment> getRelics() {
        return relics;
    }

    public void setRelics(List<Equipment> relics) {
        this.relics = relics;
    }

    public Equipment getEquipment(EquipmentFlag flag, int slot) {
        if (flag == EquipmentFlag.WEAPON) {
            return weapon;
        }
        else if (flag == EquipmentFlag.BODY) {
            return body;
        }
        else if (flag == EquipmentFlag.RELIC) {
            if (slot < relics.size()) {
                return relics.get(slot);
            } else {
                return null;
            }
        }
        return null;
    }
}
