package com.lucylhill.projectrune.objects.items;

import java.util.ArrayList;
import java.util.List;

public class ShopList {
    List<List<String>> blacksmith = new ArrayList<>();
    List<List<String>> weaver  = new ArrayList<>();
    List<List<String>> items  = new ArrayList<>();
    List<List<String>> rareItems  = new ArrayList<>();
    List<List<String>> tavern  = new ArrayList<>();

    public List<List<String>> getBlacksmith() {
        return blacksmith;
    }

    public void setBlacksmith(List<List<String>> blacksmith) {
        this.blacksmith = blacksmith;
    }

    public List<List<String>> getWeaver() {
        return weaver;
    }

    public void setWeaver(List<List<String>> weaver) {
        this.weaver = weaver;
    }

    public List<List<String>> getItems() {
        return items;
    }

    public void setItems(List<List<String>> items) {
        this.items = items;
    }

    public List<List<String>> getRareItems() {
        return rareItems;
    }

    public void setRareItems(List<List<String>> rareItems) {
        this.rareItems = rareItems;
    }

    public List<List<String>> getTavern() {
        return tavern;
    }

    public void setTavern(List<List<String>> tavern) {
        this.tavern = tavern;
    }
}
