package com.lucylhill.projectrune.objects.items;

public enum EquipmentResolverState {
    EQUIP,
    UNEQUIP
}
