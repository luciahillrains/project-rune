package com.lucylhill.projectrune.objects.items;

import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.services.DebugLogger;
import javafx.scene.control.Alert;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class EquipmentResolver {
    DebugLogger logger = new DebugLogger(this.getClass());

    public void resolveEquipment(Equipment e, EntityInfo character, EquipmentResolverState state) {
        if(e != null) {
            try {
                Class classEffect = Class.forName("com.lucylhill.projectrune.objects.classes.classSkills.Equipment");
                Method method = classEffect.getDeclaredMethod(e.getId()+"Effect", EntityInfo.class, EquipmentResolverState.class);
                method.invoke(classEffect.getConstructor().newInstance(), character, state);
            } catch(NoSuchMethodException ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("E001: No action found for "+e.getId());
                alert.show();
                logger.log("NO SUCH METHOD "+ e.getId()+"Effect");
                ex.printStackTrace();
            } catch (InvocationTargetException | IllegalAccessException | ClassNotFoundException | InstantiationException ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("E002: misc error for action "+e.getId());
                alert.show();
                ex.printStackTrace();
            }
        }

    }
}
