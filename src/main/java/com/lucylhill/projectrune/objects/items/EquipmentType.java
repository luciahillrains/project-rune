package com.lucylhill.projectrune.objects.items;

public enum EquipmentType {
    SWORD,
    GREATSWORD,
    AXE,
    GREATAXE,
    KNIFE,
    BOW,
    MUSKET,
    HARP,
    STAFF,
    ROD,
    ARMOR,
    CLOAK,
    GARB
}
