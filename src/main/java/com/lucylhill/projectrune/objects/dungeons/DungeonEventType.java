package com.lucylhill.projectrune.objects.dungeons;

public enum DungeonEventType {
    ITEM,
    BATTLE,
    HEALING,
    POSITIVE,
    NEGATIVE,
    STORY,
    CHECKPOINT,
    NO_EVENT,
    ORANGE
}
