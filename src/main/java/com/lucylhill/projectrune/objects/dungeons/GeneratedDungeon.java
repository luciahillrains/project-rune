package com.lucylhill.projectrune.objects.dungeons;

import com.lucylhill.projectrune.objects.battle.enemies.Enemy;

import java.util.*;

public class GeneratedDungeon {
    String name;
    int currentFloor = 1;
    DungeonMap map;
    DungeonEventList events;
    List<Enemy> enemies;
    String battleBackgroundType;
    String background;
    String music;

    public GeneratedDungeon() {

    }

    public GeneratedDungeon(Dungeon d) {
        map = new DungeonMap(d);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public void setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;
    }

    public DungeonMap getMap() {
        return map;
    }

    public void setMap(DungeonMap map) {
        this.map = map;
    }

    public DungeonEventList getEvents() {
        return events;
    }

    public void setEvents(DungeonEventList events) {
        this.events = events;
    }

    public List<Enemy> getEnemies() {
        return enemies;
    }

    public void setEnemies(List<Enemy> enemies) {
        this.enemies = enemies;
    }

    public String getBattleBackgroundType() {
        return battleBackgroundType;
    }

    public void setBattleBackgroundType(String battleBackgroundType) {
        this.battleBackgroundType = battleBackgroundType;
    }

    public Optional<Enemy> getEnemyById(String id) {
        Optional<Enemy> en = Optional.empty();
        for(Enemy e: enemies) {
            if(e.getId().equals(id)) {
                en = Optional.of(e);
                break;
            }
        }
        return en;
    }

    public GeneratedDungeon copy() {
        GeneratedDungeon copy = new GeneratedDungeon();
        copy.setName(""+this.name);
        DungeonMap newMap = new DungeonMap();
        newMap.setMap(new HashMap<>(this.getMap().getMap()));
        copy.setMap(newMap);
        copy.setEnemies(new ArrayList<>(this.getEnemies()));
        DungeonEventList newList = new DungeonEventList();
        newList.setDungeonEvents(new ArrayList<>(this.getEvents().getDungeonEvents()));
        copy.setEvents(newList);
        copy.setBattleBackgroundType(this.getBattleBackgroundType());
        copy.setCurrentFloor(this.currentFloor);
        copy.setBackground(this.background);
        return copy;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }
}
