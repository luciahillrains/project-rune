package com.lucylhill.projectrune.objects.dungeons;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lucylhill.projectrune.objects.characters.CharacterInfo;
import com.lucylhill.projectrune.services.DebugLogger;
import com.lucylhill.projectrune.services.InternalState;
import javafx.scene.control.Button;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Checkpoint {
    Coordinate coordinate = new Coordinate();
    boolean dirty = false;
    DebugLogger logger = new DebugLogger(this.getClass());

    public Checkpoint(Coordinate coordinate){
        logger.log("Creating a checkpoint!");
        this.coordinate.x = coordinate.x;
        this.coordinate.y = coordinate.y;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }



    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

}
