package com.lucylhill.projectrune.objects.dungeons;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DungeonList {
    List<Dungeon> dungeons;

    public List<Dungeon> getDungeons() {
        return dungeons;
    }

    public void setDungeons(List<Dungeon> dungeons) {
        this.dungeons = dungeons;
    }

    public Optional<Dungeon> getDungeonById(String id) {
        Optional<Dungeon> optional = Optional.empty();
        List<Dungeon> sublist = dungeons.stream().filter(d -> d.getId().equals(id)).collect(Collectors.toList());
        if(sublist.size() >= 1) {
            optional = Optional.of(sublist.get(0));
        }
        return optional;
    }
}
