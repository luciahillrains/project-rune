package com.lucylhill.projectrune.objects.dungeons;

public class DungeonSquare {
    DungeonEventType type;
    boolean startSquare;
    boolean visited;
    boolean locked;
    String dungeonEventId;

    public DungeonSquare(DungeonEventType type) {
        this.type = type;
    }

    public DungeonEventType getType() {
        return type;
    }

    public void setType(DungeonEventType type) {
        this.type = type;
    }

    public boolean isStartSquare() {
        return startSquare;
    }

    public void setStartSquare(boolean startSquare) {
        this.startSquare = startSquare;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setIsLocked(boolean locked) {
        this.locked = locked;
    }

    public String getDungeonEventId() {
        return dungeonEventId;
    }

    public void setDungeonEventId(String dungeonEventId) {
        this.dungeonEventId = dungeonEventId;
    }
}
