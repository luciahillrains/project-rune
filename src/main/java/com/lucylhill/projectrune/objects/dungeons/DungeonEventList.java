package com.lucylhill.projectrune.objects.dungeons;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class DungeonEventList {
    List<DungeonEvent> dungeonEvents;
    Random random = new Random();
    List<DungeonEvent> seenDungeonEvents = new ArrayList<>();
    int storySelected = 0;
    public List<DungeonEvent> getDungeonEvents() {
        return dungeonEvents;
    }

    public void setDungeonEvents(List<DungeonEvent> dungeonEvents) {
        this.dungeonEvents = dungeonEvents;
    }

    public DungeonEvent getRandomDungeonEventOfType(DungeonEventType type) {
        List<DungeonEvent> subList = this.dungeonEvents.stream().filter(ev -> ev.getType() == type).collect(Collectors.toList());
        int totalWeight = subList.stream().map(DungeonEvent::getWeight).reduce(0, Integer::sum);
        if(totalWeight > 0) {
            int weight = random.nextInt(totalWeight);
            return getWeightedEvent(subList, weight);
        }
        if(type != DungeonEventType.STORY) {
            DungeonEvent event = subList.get(random.nextInt(subList.size()));
            if(seenDungeonEvents.contains(event)) {
                for(int i = 0; i < 5; i++) {
                    DungeonEvent newEvent = subList.get(random.nextInt(subList.size()));
                    if(!seenDungeonEvents.contains(newEvent)) {
                        event = newEvent;
                        break;
                    }
                }
            }
            seenDungeonEvents.add(event);
            return event;
        }
        DungeonEvent story = subList.get(storySelected);
        storySelected++;
        return story;
    }

    public DungeonEvent getChoiceEvent() {
        return this.dungeonEvents.stream().filter(ev -> ev.getChoices() != null && ev.getChoices().size() > 1).collect(Collectors.toList()).get(0);
    }

    public DungeonEvent getWeightedEvent(List<DungeonEvent> sublist, int weight) {
        int accumulator = 0;
        for(DungeonEvent event :sublist) {
            accumulator = accumulator + event.getWeight();
            if(weight < accumulator) {
                return event;
            }
        }
        return sublist.get(0);
    }
}
