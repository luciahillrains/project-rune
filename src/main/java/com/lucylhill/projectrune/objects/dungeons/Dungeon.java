package com.lucylhill.projectrune.objects.dungeons;

import java.util.List;

public class Dungeon {
    String name;
    String id;
    String music;
    int floorMin;
    int floorMax;
    int complexity;
    int checkpointNum;
    List<DungeonEvent> events;
    String description;
    String battleBackgroundType;
    DungeonGenerationCriteria generationCriteria;
    String background;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getFloorMin() {
        return floorMin;
    }

    public void setFloorMin(int floorMin) {
        this.floorMin = floorMin;
    }

    public int getFloorMax() {
        return floorMax;
    }

    public void setFloorMax(int floorMax) {
        this.floorMax = floorMax;
    }

    public int getComplexity() {
        return complexity;
    }

    public void setComplexity(int complexity) {
        this.complexity = complexity;
    }

    public List<DungeonEvent> getEvents() {
        return events;
    }

    public void setEvents(List<DungeonEvent> events) {
        this.events = events;
    }

    public int getCheckpointNum() {
        return checkpointNum;
    }

    public void setCheckpointNum(int checkpointNum) {
        this.checkpointNum = checkpointNum;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DungeonGenerationCriteria getGenerationCriteria() {
        return generationCriteria;
    }

    public String getBattleBackgroundType() {
        return battleBackgroundType;
    }

    public void setBattleBackgroundType(String battleBackgroundType) {
        this.battleBackgroundType = battleBackgroundType;
    }

    public void setGenerationCriteria(DungeonGenerationCriteria generationCriteria) {
        this.generationCriteria = generationCriteria;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }
}
