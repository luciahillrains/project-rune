package com.lucylhill.projectrune.objects.dungeons;

public class DungeonGenerationCriteria {
    int maxAdditionalRows;
    int pathMax;
    int storyNum;
    int maxNumOfPositives;
    int maxNumOfNegatives;
    int maxNumOfBattles;
    int maxNumOfCheckpoints;
    int maxNumOfItems;
    int maxNumOfHealing;
    int maxNumOfTiles;

    public int getMaxAdditionalRows() {
        return maxAdditionalRows;
    }

    public void setMaxAdditionalRows(int maxAdditionalRows) {
        this.maxAdditionalRows = maxAdditionalRows;
    }

    public int getPathMax() {
        return pathMax;
    }

    public void setPathMax(int pathMax) {
        this.pathMax = pathMax;
    }

    public int getStoryNum() {
        return storyNum;
    }

    public void setStoryNum(int storyNum) {
        this.storyNum = storyNum;
    }

    public int getMaxNumOfPositives() {
        return maxNumOfPositives;
    }

    public void setMaxNumOfPositives(int maxNumOfPositives) {
        this.maxNumOfPositives = maxNumOfPositives;
    }

    public int getMaxNumOfNegatives() {
        return maxNumOfNegatives;
    }

    public void setMaxNumOfNegatives(int maxNumOfNegatives) {
        this.maxNumOfNegatives = maxNumOfNegatives;
    }

    public int getMaxNumOfBattles() {
        return maxNumOfBattles;
    }

    public void setMaxNumOfBattles(int maxNumOfBattles) {
        this.maxNumOfBattles = maxNumOfBattles;
    }

    public int getMaxNumOfCheckpoints() {
        return maxNumOfCheckpoints;
    }

    public void setMaxNumOfCheckpoints(int maxNumOfCheckpoints) {
        this.maxNumOfCheckpoints = maxNumOfCheckpoints;
    }

    public int getMaxNumOfItems() {
        return maxNumOfItems;
    }

    public void setMaxNumOfItems(int maxNumOfItems) {
        this.maxNumOfItems = maxNumOfItems;
    }

    public int getMaxNumOfTiles() {
        return maxNumOfTiles;
    }

    public void setMaxNumOfTiles(int maxNumOfTiles) {
        this.maxNumOfTiles = maxNumOfTiles;
    }

    public int getMaxNumOfHealing() {
        return maxNumOfHealing;
    }

    public void setMaxNumOfHealing(int maxNumOfHealing) {
        this.maxNumOfHealing = maxNumOfHealing;
    }
}
