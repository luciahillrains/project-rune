package com.lucylhill.projectrune.objects.dungeons;

import com.lucylhill.projectrune.objects.misc.Choice;
import com.lucylhill.projectrune.objects.misc.ResultOpCode;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DungeonEvent {
    DungeonEventType type;
    List<ResultOpCode> resultCodes;
    String message = "";
    String picture = "";
    List<Choice> choices = new ArrayList<>();
    DungeonEvent chain;
    List<String> enemies;
    int weight;
    String commonItem;
    String rareItem;
    String itemId;
    String id;
    List<DungeonEventSnippet> snippets = new ArrayList<>();

    public DungeonEventType getType() {
        return type;
    }

    public void setType(DungeonEventType type) {
        this.type = type;
    }

    public List<ResultOpCode> getResultCodes() {
        return resultCodes;
    }

    public void setResultCodes(List<ResultOpCode> resultCodes) {
        this.resultCodes = resultCodes;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public List<Choice> getChoices() {
        return choices;
    }

    public void setChoices(List<Choice> choices) {
        this.choices = choices;
    }

    public DungeonEvent getChain() {
        return chain;
    }

    public void setChain(DungeonEvent chain) {
        this.chain = chain;
    }

    public List<String> getEnemies() {
        return enemies;
    }

    public void setEnemies(List<String> enemies) {
        this.enemies = enemies;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getCommonItem() {
        return commonItem;
    }

    public void setCommonItem(String commonItem) {
        this.commonItem = commonItem;
    }

    public String getRareItem() {
        return rareItem;
    }

    public void setRareItem(String rareItem) {
        this.rareItem = rareItem;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<DungeonEventSnippet> getSnippets() {
        return snippets;
    }

    public void setSnippets(List<DungeonEventSnippet> snippets) {
        this.snippets = snippets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DungeonEvent that = (DungeonEvent) o;
        return type == that.type && Objects.equals(message, that.message) && Objects.equals(commonItem, that.commonItem) && Objects.equals(rareItem, that.rareItem) && Objects.equals(id, that.id) && Objects.equals(snippets, that.snippets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, message, commonItem, rareItem, id, snippets);
    }
}
