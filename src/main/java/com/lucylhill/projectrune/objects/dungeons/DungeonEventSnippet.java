package com.lucylhill.projectrune.objects.dungeons;

import java.util.Map;

public class DungeonEventSnippet {
    String id;
    Map<String, String> responses;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, String> getResponses() {
        return responses;
    }

    public void setResponses(Map<String, String> responses) {
        this.responses = responses;
    }
}
