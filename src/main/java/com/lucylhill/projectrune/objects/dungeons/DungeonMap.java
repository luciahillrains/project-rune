package com.lucylhill.projectrune.objects.dungeons;

import com.lucylhill.projectrune.services.InternalState;

import java.util.*;

public class DungeonMap {
    Map<Integer, Map<Integer, DungeonSquare>> map;
    int numCheckpoints = 0;
    int numStory = 0;
    int numBattles = 0;
    int numPositives = 0;
    int numNegatives = 0;
    int numItems = 0;
    int numHealing = 0;
    Coordinate coordinate;
    public static final int MAP_COORD_MAX = 9;
    public DungeonMap() {

    }
    public DungeonMap(Dungeon d) {
        initializeMap();
        Random random = new Random();
        int numOfTiles = d.getComplexity() + random.nextInt(d.getGenerationCriteria().getMaxAdditionalRows());
        int xStartingPoint = 4;
        int range = 1;
        int rangeStart = xStartingPoint;
        coordinate = new Coordinate();
        for(int i = 0; i <numOfTiles + 1; i++) {
            coordinate.x = i;
            coordinate.y = xStartingPoint;
            if (i == 0) {
                map.get(i).put(coordinate.y, new DungeonSquare(DungeonEventType.ORANGE));
                coordinate.x = 1;
            }
            else {
                //TODO: change for more complex dungeons;
                coordinate.y = random.nextInt(rangeStart, rangeStart+range+1);
                int corridorLength = random.nextInt(1, d.getGenerationCriteria().getPathMax() + 1);
                range = corridorLength;
                int left = random.nextInt(corridorLength);
                int right = corridorLength - left;
                int oldY = coordinate.y;
                if(i == numOfTiles) {
                    DungeonSquare startSquare = new DungeonSquare(DungeonEventType.ORANGE);
                    startSquare.setStartSquare(true);
                    map.get(coordinate.x).put(coordinate.y, startSquare);
                    break;
                }
                map.get(coordinate.x).put(coordinate.y, new DungeonSquare(getRandomType(random, d)));
                for(int j = 1; j <= left + 1; j++) {
                    if(j == left) {
                        rangeStart = oldY-j;
                    }
                    coordinate.y = checkBounds(oldY - j);
                    map.get(coordinate.x).put(coordinate.y, new DungeonSquare(getRandomType(random, d)));
                }
                coordinate.y = oldY;
                for(int j = 1; j <= right + 1; j++) {
                    coordinate.y = checkBounds(oldY + j);
                    map.get(coordinate.x).put(coordinate.y, new DungeonSquare(getRandomType(random, d)));
                }
            }

        }
        finalizeMap(d);
    }

    public void initializeMap() {
        map = new HashMap<>();
        for(int i = 0; i < MAP_COORD_MAX + 1; i++) {
            map.put(i, new HashMap<>());
        }
    }

    public DungeonEventType getRandomType(Random random, Dungeon dungeon) {
        int val = random.nextInt(8);
        DungeonEventType type = DungeonEventType.values()[val];
        boolean exceedsBounds = false;
        switch (type) {
            case ITEM:
                exceedsBounds = getTypeExceedsBounds(dungeon.getGenerationCriteria().getMaxNumOfItems(), numItems);
                break;
            case CHECKPOINT:
                exceedsBounds = getTypeExceedsBounds(dungeon.getGenerationCriteria().getMaxNumOfCheckpoints(), numCheckpoints);
                break;
            case STORY:
                exceedsBounds = getTypeExceedsBounds(dungeon.getGenerationCriteria().getStoryNum(), numStory);
                break;
            case POSITIVE:
                exceedsBounds = getTypeExceedsBounds(dungeon.getGenerationCriteria().getMaxNumOfPositives(), numPositives);
                break;
            case NEGATIVE:
                exceedsBounds = getTypeExceedsBounds(dungeon.getGenerationCriteria().getMaxNumOfNegatives(), numNegatives);
                break;
            case BATTLE:
                exceedsBounds = getTypeExceedsBounds(dungeon.getGenerationCriteria().getMaxNumOfBattles(), numBattles);
                break;
            case HEALING:
                exceedsBounds = getTypeExceedsBounds(dungeon.getGenerationCriteria().getMaxNumOfHealing(), numHealing);
                break;
        }
        if (type == DungeonEventType.CHECKPOINT && (coordinate.y < 3 || coordinate.x < 3)) {
            exceedsBounds = true;
        }
        if (!exceedsBounds) {
            if (type == DungeonEventType.ITEM) {
                numItems++;
            }
            if (type == DungeonEventType.CHECKPOINT) {
                numCheckpoints++;
            }
            if (type == DungeonEventType.STORY) {
                numStory++;
            }
            if (type == DungeonEventType.POSITIVE) {
                numPositives++;
            }
            if (type == DungeonEventType.NEGATIVE) {
                numNegatives++;
            }
            if (type == DungeonEventType.BATTLE) {
                numBattles++;
            }
            if( type == DungeonEventType.BATTLE && InternalState.i().configuration().getDifficultyConfig().isNoBattles()) {
                return DungeonEventType.NO_EVENT;
            }
            if (type == DungeonEventType.HEALING) {
                numHealing++;
            }
            return type;
        } else {
            return getRandomType(random, dungeon);
        }

    }

    public boolean getTypeExceedsBounds(int bound, int current) {
        return bound <= current;
    }

    public int checkBounds(int c) {
        if(c < 0) {
            return 0;
        }
        if(c >= MAP_COORD_MAX +1) {
            return MAP_COORD_MAX;
        }
        return c;
    }

    public String getStringOfType(DungeonEventType t) {
        String stringType = "@";
        if(t == null) {
            return stringType;
        }
        switch(t) {
            case ITEM:
                stringType = "I";
                break;
            case BATTLE:
                stringType = "B";
                break;
            case HEALING:
                stringType = "H";
                break;
            case POSITIVE:
                stringType = "P";
                break;
            case NEGATIVE:
                stringType = "N";
                break;
            case STORY:
                stringType = "S";
                break;
            case CHECKPOINT:
                stringType = "C";
                break;
            case ORANGE:
                stringType = "O";
                break;
        }
                return stringType;
    }

    public Map<Integer, Map<Integer, DungeonSquare>> getMap() {
        return map;
    }

    public void setMap(Map<Integer, Map<Integer, DungeonSquare>> map) {
        this.map = map;
    }

    public void finalizeMap(Dungeon d) {
        if(!mapHasStory()) {
            for(int i = 0; i < MAP_COORD_MAX + 1; i++) {
                for(int j = 0; j < MAP_COORD_MAX + 1; j++) {
                    DungeonSquare square = map.get(i).get(j);
                    if(square != null && (square.getType() == DungeonEventType.BATTLE || square.getType() == DungeonEventType.NEGATIVE || square.getType() == DungeonEventType.POSITIVE)) {
                        square.setType(DungeonEventType.STORY);
                        return;
                    }
                }
            }
        }
        while(getNumberOfTilesInMap() > d.getGenerationCriteria().getMaxNumOfTiles()) {
            for(int i = 2; i < MAP_COORD_MAX - 1; i++) {
                for(int j = 2; j < MAP_COORD_MAX - 1; j++) {
                    DungeonSquare square = map.get(i).get(j);
                    if(square != null && square.getType() != DungeonEventType.STORY) {
                        map.get(i).remove(j);
                        return;
                    }
                }
            }
        }
    }

    public boolean mapHasStory() {
        for(int i = 0; i < MAP_COORD_MAX +1; i++) {
            Map<Integer, DungeonSquare> row = map.get(i);
            for(int j = 0; j < MAP_COORD_MAX + 1; j++) {
                DungeonSquare square = row.get(j);
                if(square != null && square.getType() == DungeonEventType.STORY) {
                    return true;
                }
            }
        }
        return false;
    }

    public int getNumberOfTilesInMap() {
        int counter = 0;
        for(int i = 0; i < MAP_COORD_MAX + 1; i++) {
            counter += map.get(i).keySet().size();
        }
        return counter;
    }

    @Override
    public String toString() {
        String mapStr = "";
        Map<Integer, DungeonSquare> row = new HashMap<>();
        for(int i = 0; i < MAP_COORD_MAX+1; i++) {
            row = map.get(i);
            for(int j = 0; j < MAP_COORD_MAX + 1; j++) {
                mapStr += getStringOfType(row.getOrDefault(j, new DungeonSquare(null)).getType());
            }
            mapStr += "\n";
        }
        return mapStr;
    }
}
