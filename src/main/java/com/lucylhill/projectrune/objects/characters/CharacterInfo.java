package com.lucylhill.projectrune.objects.characters;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.battle.status.AilmentFlag;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.battle.status.StatusEffectList;
import com.lucylhill.projectrune.objects.classes.ClassInfo;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.objects.passives.*;
import com.lucylhill.projectrune.objects.traits.Trait;
import com.lucylhill.projectrune.services.InternalState;

import java.util.List;
import java.util.Random;

public class CharacterInfo extends EntityInfo {
    CharacterType type;
    ClassInfo classInfo;
    Trait trait;
    String race;
    String gender;
    StatusEffectList statusEffects = new StatusEffectList();
    PassiveActionList passives = new PassiveActionList();

    public CharacterInfo() {
        //bounds will change based on level.
        setCharisma(0);
        setIntelligence(1);
        setAgility(0);
    }

    public CharacterType getType() {
        return type;
    }

    public CharacterInfo setType(CharacterType type) {
        this.type = type;
        return this;
    }


    public CharacterInfo setCharacterName(String name) {
        setName(name);
        return this;
    }

    public ClassInfo getClassInfo() {
        return classInfo;
    }

    public CharacterInfo setClassInfo(ClassInfo classInfo) {
        this.classInfo = classInfo;
        setAlignment(classInfo.getAspect());
        setClassAlignment(classInfo.getClassAspect());
        return this;
    }

    public Trait getTrait() {
        return trait;
    }

    public CharacterInfo setTrait(Trait trait) {
        this.trait = trait;
        return this;
    }

    public String getRace() {
        return race;
    }

    public CharacterInfo setRace(String race) {
        this.race = race;
        return this;
    }

    public String getShortRace() {
        if(race.equals("Not Disclosed")) {
            return "NA";
        }
        return race.substring(0,2);
    }

    public String getGender() {
        return gender;
    }

    public CharacterInfo setGender(String gender) {
        this.gender = gender;
        return this;
    }

    public String getShortGender() {
        if(gender.equals("Masculine")) {
            return '\u2642'+"";
        }
        if(gender.equals("Non-Binary")) {
            return '\u26A5'+"";
        }
        if(gender.equals("Feminine")) {
            return '\u2640'+"";
        }
        return "NA";
    }

    public String getRecruitedName() {
        return getName() + " " + getShortGender() + "/" + getShortRace();
    }


    public StatusEffectList getStatusEffects() {
        return statusEffects;
    }

    public void setStatusEffects(StatusEffectList statusEffects) {
        this.statusEffects = statusEffects;
    }

   public void addEffect(StatusEffect e) {
        if(!this.passives.hasPassive("AilmentImmune")
                && e.getIsNamedAilment()
                && e.getAilmentFlag() != AilmentFlag.ABSOLUTE_SHIELD) {
            statusEffects.addEffect(e);
        }
   }

    public PassiveActionList getPassives() {
        return passives;
    }

    public void setPassives(PassiveActionList passives) {
        this.passives = passives;
    }

    public void executePassives(PassiveFlag timing) {
        PassiveEffectResolver resolver = new PassiveEffectResolver();
        if(timing == PassiveFlag.BATTLE) {
            clearAdditionalStats();
        }
        for(PassiveData p : classInfo.getPassives()) {
            PassiveAction action = RuneConfiguration.i().passiveList().getPassiveWithId(p.getId());
            if(p.getTiming() == timing && action.getLevel() <= InternalState.i().level().getLevel()) {
                resolver.resolvePassive(p.getId(), classInfo.getName().replace(" ", ""),this);
            }
        }
    }
    @Override
    public String toString() {
        return this.getName();
    }
}
