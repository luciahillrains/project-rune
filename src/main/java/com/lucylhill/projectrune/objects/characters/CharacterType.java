package com.lucylhill.projectrune.objects.characters;

public enum CharacterType {
    LEADER,
    MEMBER
}
