package com.lucylhill.projectrune.objects.tavern;

import java.util.ArrayList;
import java.util.List;

public class ConversationResponse {
    String dialog;
    String response;
    List<ConversationResponse> choices = new ArrayList<>();

    public String getDialog() {
        return dialog;
    }

    public void setDialog(String dialog) {
        this.dialog = dialog;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public List<ConversationResponse> getChoices() {
        return choices;
    }

    public void setChoices(List<ConversationResponse> choices) {
        this.choices = choices;
    }
}
