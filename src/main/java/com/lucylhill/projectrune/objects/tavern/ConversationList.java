package com.lucylhill.projectrune.objects.tavern;

import com.lucylhill.projectrune.services.InternalState;

import java.util.List;
import java.util.stream.Collectors;

public class ConversationList {
    List<Conversation> conversations;

    public List<Conversation> getConversations() {
        return conversations;
    }

    public void setConversations(List<Conversation> conversations) {
        this.conversations = conversations;
    }


    public Conversation getConversationById(String id) {
        return conversations
                .stream()
                .filter(c -> c.getId().equals(id))
                .collect(Collectors.toList())
                .get(0);
    }

}
