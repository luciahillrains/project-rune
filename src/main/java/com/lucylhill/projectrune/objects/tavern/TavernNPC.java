package com.lucylhill.projectrune.objects.tavern;

import java.util.List;

public class TavernNPC {
    String name;
    String description;
    List<String> conversationIds;
    List<Integer> chaptersInvolved;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getConversationIds() {
        return conversationIds;
    }

    public void setConversationIds(List<String> conversationIds) {
        this.conversationIds = conversationIds;
    }

    public List<Integer> getChaptersInvolved() {
        return chaptersInvolved;
    }

    public void setChaptersInvolved(List<Integer> chaptersInvolved) {
        this.chaptersInvolved = chaptersInvolved;
    }

}
