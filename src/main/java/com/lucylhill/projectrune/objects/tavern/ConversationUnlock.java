package com.lucylhill.projectrune.objects.tavern;

public class ConversationUnlock {
    int levelMin;
    //chapter represents rune you've acheived: 0 = no rune, 6 = all runes, 1 = got the first rune, etc.
    int chapter;

    public int getLevelMin() {
        return levelMin;
    }

    public void setLevelMin(int levelMin) {
        this.levelMin = levelMin;
    }

    public int getChapter() {
        return chapter;
    }

    public void setChapter(int chapter) {
        this.chapter = chapter;
    }
}
