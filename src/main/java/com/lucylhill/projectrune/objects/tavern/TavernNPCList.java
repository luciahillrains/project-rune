package com.lucylhill.projectrune.objects.tavern;

import com.lucylhill.projectrune.services.InternalState;

import java.util.List;
import java.util.stream.Collectors;

public class TavernNPCList {
    List<TavernNPC> npcs;

    public List<TavernNPC> getNpcs() {
        return npcs;
    }

    public void setNpcs(List<TavernNPC> npcs) {
        this.npcs = npcs;
    }

    public List<TavernNPC> getNPCsForChapter() {
        int chapter = InternalState.i().level().getChapter();
        return npcs.stream()
                .filter(npc -> npc.getChaptersInvolved().contains(chapter))
                .collect(Collectors.toList());

    }
}
