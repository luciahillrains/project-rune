package com.lucylhill.projectrune.objects.tavern;

import java.util.ArrayList;
import java.util.List;

public class Conversation {
    String dialog;
    String id;
    List<ConversationResponse> choices = new ArrayList<>();
    ConversationUnlock unlock;

    public String getDialog() {
        return dialog;
    }

    public void setDialog(String dialog) {
        this.dialog = dialog;
    }

    public List<ConversationResponse> getChoices() {
        return choices;
    }

    public void setChoices(List<ConversationResponse> choices) {
        this.choices = choices;
    }

    public ConversationUnlock getUnlock() {
        return unlock;
    }

    public void setUnlock(ConversationUnlock unlock) {
        this.unlock = unlock;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

