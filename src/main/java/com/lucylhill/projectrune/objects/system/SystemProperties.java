package com.lucylhill.projectrune.objects.system;

import java.util.List;

public class SystemProperties {
    String buildInfo;
    int maxNumOfCharactersInParty;
    int randomNumOfTraits;
    List<String> races;
    List<String> genders;
    String hubWorldName;
    boolean debug;
    int initialLevel;
    String permadeathDescription;
    String permakickDescription;
    String turnLimitDescription;
    String noBattlesDescription;
    String resumeFromCheckpointDescription;
    boolean turnOffSplash;

    public String getBuildInfo() {
        return buildInfo;
    }

    public void setBuildInfo(String buildInfo) {
        this.buildInfo = buildInfo;
    }

    public int getMaxNumOfCharactersInParty() {
        return maxNumOfCharactersInParty;
    }

    public void setMaxNumOfCharactersInParty(int maxNumOfCharactersInParty) {
        this.maxNumOfCharactersInParty = maxNumOfCharactersInParty;
    }

    public List<String> getRaces() {
        return races;
    }

    public void setRaces(List<String> races) {
        this.races = races;
    }

    public List<String> getGenders() {
        return genders;
    }

    public void setGenders(List<String> genders) {
        this.genders = genders;
    }

    public String getHubWorldName() {
        return hubWorldName;
    }

    public void setHubWorldName(String hubWorldName) {
        this.hubWorldName = hubWorldName;
    }

    public int getRandomNumOfTraits() {
        return randomNumOfTraits;
    }

    public void setRandomNumOfTraits(int randomNumOfTraits) {
        this.randomNumOfTraits = randomNumOfTraits;
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public String getPermadeathDescription() {
        return permadeathDescription;
    }

    public void setPermadeathDescription(String permadeathDescription) {
        this.permadeathDescription = permadeathDescription;
    }

    public String getPermakickDescription() {
        return permakickDescription;
    }

    public void setPermakickDescription(String permakickDescription) {
        this.permakickDescription = permakickDescription;
    }

    public String getTurnLimitDescription() {
        return turnLimitDescription;
    }

    public void setTurnLimitDescription(String turnLimitDescription) {
        this.turnLimitDescription = turnLimitDescription;
    }

    public String getNoBattlesDescription() {
        return noBattlesDescription;
    }

    public void setNoBattlesDescription(String noBattlesDescription) {
        this.noBattlesDescription = noBattlesDescription;
    }

    public String getResumeFromCheckpointDescription() {
        return resumeFromCheckpointDescription;
    }

    public void setResumeFromCheckpointDescription(String resumeFromCheckpointDescription) {
        this.resumeFromCheckpointDescription = resumeFromCheckpointDescription;
    }

    public int getInitialLevel() {
        return initialLevel;
    }

    public void setInitialLevel(int initialLevel) {
        this.initialLevel = initialLevel;
    }

    public boolean isTurnOffSplash() {
        return turnOffSplash;
    }

    public void setTurnOffSplash(boolean turnOffSplash) {
        this.turnOffSplash = turnOffSplash;
    }
}
