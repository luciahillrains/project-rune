package com.lucylhill.projectrune.objects.entities;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.characters.CharacterInfo;
import com.lucylhill.projectrune.objects.characters.CharacterType;
import com.lucylhill.projectrune.objects.classes.ClassInfo;
import com.lucylhill.projectrune.objects.items.Equipment;
import com.lucylhill.projectrune.objects.items.Item;
import com.lucylhill.projectrune.objects.traits.Trait;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class PartyMemberList {
    List<PartyMember> partyMembers = new ArrayList<>();

    public PartyMember getPartyMember(String name) {
        return partyMembers.stream()
                .filter(p -> p.getName().equals(name)).collect(Collectors.toList()).get(0);
    }

    public CharacterInfo getCharacterInfo(String name) {
        PartyMember partyMember = getPartyMember(name);
        CharacterInfo character = new CharacterInfo();
        character.setName(partyMember.getName());
        Optional<Trait> trait = RuneConfiguration.i().traitList().getTraitByName(partyMember.getTraitName());
        trait.ifPresent(character::setTrait);
        Optional<ClassInfo> classInfo = RuneConfiguration.i().classList().getClassByName(partyMember.getStartingClass());
        classInfo.ifPresent(character::setClassInfo);

        Optional<Item> weapon = RuneConfiguration.i().equipment().getItemById(partyMember.getWeaponId());
        weapon.ifPresent(item -> character.getEquipment().equipWeapon((Equipment) item));

        Optional<Item> armor = RuneConfiguration.i().equipment().getItemById(partyMember.getArmorId());
        armor.ifPresent(item -> character.getEquipment().equipBody((Equipment) item));
        character.setAttack(partyMember.getAttack());
        character.setDefense(partyMember.getDefense());
        character.setIntelligence(partyMember.getIntelligence());
        character.setCharisma(partyMember.getCharisma());
        character.setAgility(partyMember.getAgility());
        character.setRace(partyMember.getRace());
        character.setGender(partyMember.getGender());
        character.setType(CharacterType.MEMBER);
        if(character.getClassInfo() == null) {
            return null;
        }
        return character;
    }

    public List<PartyMember> getPartyMembers() {
        return partyMembers;
    }

    public void setPartyMembers(List<PartyMember> partyMembers) {
        this.partyMembers = partyMembers;
    }
}
