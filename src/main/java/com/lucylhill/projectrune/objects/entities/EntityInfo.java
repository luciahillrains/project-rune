package com.lucylhill.projectrune.objects.entities;

import com.lucylhill.projectrune.objects.battle.actions.Action;
import com.lucylhill.projectrune.objects.battle.status.StatusEffectList;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.classes.ClassAspects;
import com.lucylhill.projectrune.objects.items.Equipped;
import com.lucylhill.projectrune.objects.passives.PassiveActionList;

import java.util.*;

public class EntityInfo {
    boolean boss;
    String name;
    private int maxHP = 10;
    int hp = 10;
    private int attack = 1;
    private int defense = 1;
    private int agility = 0;
    private int intelligence = 1;
    private int charisma = 0;
    private int additionalAttack = 0;
    private int additionalMaxHP = 0;
    private int additionalDefense = 0;
    private int additionalAgility = 0;
    private int additionalIntelligence = 0;
    private int additionalCharisma = 0;
    private double additionalHitChanceModifier = 0;
    StatusEffectList statusEffects = new StatusEffectList();
    PassiveActionList passives = new PassiveActionList();
    Map<Aspects, Double> offenses = new HashMap<>();
    Map<Aspects, Double> defenses = new HashMap<>();
    Map<Aspects, Double> additionalOffenses = new HashMap<>();
    Map<Aspects, Double> additionalDefenses = new HashMap<>();
    Aspects alignment = Aspects.NONE;
    ClassAspects classAlignment = ClassAspects.NEUTRAL;
    boolean heavy = false;
    double healingModifier = 1;
    double hitChanceModifier = 1;
    double meatHealingModifier = 1;
    double vegatableHealingModifier = 1;
    double alcoholHealingModifier = 1;
    double alchemyHealingModifier = 1;
    double relicIncreaseModifier = 1;
    double runeIncreaseModifier = 1;
    Aspects actionAlignment;
    Action lastSkillHit;
    Equipped equipment;
    boolean isMechanical;
    List<Action> relicActions = new ArrayList<>();
    String commonItemId = "";
    String rareItemId = "";
    String morphItemId = "";
    double healingItemModifier = 1.0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxHp() {
        return (int)getStat(Stat.MAX_HP);
    }

    public void setMaxHp(int maxHP) {
        this.maxHP = maxHP;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getAttack() {
        return (int) getStat(Stat.ATTACK);
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getDefense() {
        return (int) getStat(Stat.DEFENSE);
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getAgility() {
        return (int) getStat(Stat.AGILITY);
    }

    public void setAgility(int agility) {
        this.agility = agility;
    }

    public int getIntelligence() {
        return (int) getStat(Stat.INTELLIGENCE);
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public StatusEffectList getStatusEffects() {
        return statusEffects;
    }

    public void setStatusEffects(StatusEffectList statusEffects) {
        this.statusEffects = statusEffects;
    }

    public PassiveActionList getPassives() {
        return passives;
    }

    public void setPassives(PassiveActionList passives) {
        this.passives = passives;
    }


    public int getCharisma() {
        return (int) getStat(Stat.CHARISMA);
    }

    public void setCharisma(int charisma) {
        this.charisma = charisma;
    }

    public double getStat(Stat stat) {
        double total = this.statusEffects.getStat(stat);
        switch(stat) {
            case MAX_HP -> total = total + (maxHP + additionalMaxHP);
            case ATTACK -> total = total + (attack + additionalAttack);
            case DEFENSE -> total = total + (defense + additionalDefense);
            case AGILITY -> total = total + (agility + additionalAgility);
            case CHARISMA -> total = total + (charisma + additionalCharisma);
            case INTELLIGENCE ->  total = total + (intelligence + additionalIntelligence);
            case HIT_RATE -> total = total + (hitChanceModifier + additionalHitChanceModifier);
        }

        return total;
    }

    public double getStat(Stat stat, Aspects aspect) {
        double total = this.statusEffects.getStat(stat, aspect);
        switch(stat) {
            case ELEMENTAL_DEFENSES -> {
                double inner = defenses.computeIfAbsent(aspect, e -> 1.0);
                double add = additionalDefenses.computeIfAbsent(aspect, e-> 1.0);
                total = total + inner + add;
            }
            case ELEMENTAL_OFFENSES -> {
                double inner = offenses.computeIfAbsent(aspect, e-> 1.0);
                double add = additionalOffenses.computeIfAbsent(aspect, e-> 1.0);
                total = total + inner + add;
            }
        }
        return total;
    }

    public double getBase() {
        return 1.0;
    }

    public Map<Aspects, Double> getOffenses() {
        return offenses;
    }

    public Map<Aspects, Double> getDefenses() {
        return defenses;
    }

    public void setDefenses(Map<Aspects, Double> defenses) {
        this.defenses = defenses;
    }

    public Aspects getAlignment() {
        return alignment;
    }

    public void setAlignment(Aspects alignment) {
        this.alignment = alignment;
    }

    public ClassAspects getClassAlignment() {
        return classAlignment;
    }

    public void setClassAlignment(ClassAspects classAlignment) {
        this.classAlignment = classAlignment;
    }


    public int getAdditionalAttack() {
        return additionalAttack;
    }

    public void setAdditionalAttack(int additionalAttack) {
        this.additionalAttack = additionalAttack;
    }

    public int getAdditionalMaxHP() {
        return additionalMaxHP;
    }

    public void setAdditionalMaxHP(int additionalMaxHP) {
        this.additionalMaxHP = additionalMaxHP;
    }

    public int getAdditionalDefense() {
        return additionalDefense;
    }

    public void setAdditionalDefense(int additionalDefense) {
        this.additionalDefense = additionalDefense;
    }

    public int getAdditionalAgility() {
        return additionalAgility;
    }

    public void setAdditionalAgility(int additionalAgility) {
        this.additionalAgility = additionalAgility;
    }

    public int getAdditionalIntelligence() {
        return additionalIntelligence;
    }

    public void setAdditionalIntelligence(int additionalIntelligence) {
        this.additionalIntelligence = additionalIntelligence;
    }

    public int getAdditionalCharisma() {
        return additionalCharisma;
    }

    public void setAdditionalCharisma(int additionalCharisma) {
        this.additionalCharisma = additionalCharisma;
    }

    public boolean isNormal() {
        return !heavy;
    }

    public void setHeavy(boolean heavy) {
        this.heavy = heavy;
    }

    public void addAdditionalAttack(int attack) {
        this.additionalAttack = this.additionalAttack + attack;
    }

    public void addAdditionalDefense(int defense) {
        this.additionalDefense = this.additionalDefense + defense;
    }

    public void addAdditionalIntelligence(int increase) {
        this.additionalIntelligence = this.additionalIntelligence + increase;
    }

    public void addAdditionalAgility(int increase) {
        this.additionalAgility = this.additionalAgility + increase;
    }

    public void addAdditionalCharisma(int increase) {
        this.additionalCharisma = this.additionalCharisma + increase;
    }

    public void addAdditionalMaxHP(int increase) {
        this.additionalMaxHP = this.additionalMaxHP + increase;
    }

    public void addElementalOffense(Aspects aspect, double increase) {
        addElementalParameter(additionalOffenses, aspect, increase);
    }

    public void addElementalDefense(Aspects aspect, double increase) {
        addElementalParameter(additionalDefenses, aspect, increase);
    }

    public void addElementalParameter(Map<Aspects, Double> container, Aspects aspect, double increase) {
        double delta = 0.0;
        if(container.containsKey(aspect)) {
            if(increase >= 1.0) {
                delta = 1.0 - increase;
            }
            else {
                delta = delta * -1;
            }
            container.put(aspect, container.get(aspect) + delta);
        } else {
            container.put(aspect, increase);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EntityInfo that = (EntityInfo) o;
        return maxHP == that.maxHP && Objects.equals(name, that.name) && alignment == that.alignment && classAlignment == that.classAlignment;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, maxHP, alignment, classAlignment);
    }

    public boolean isBoss() {
        return boss;
    }

    public void setBoss(boolean boss) {
        this.boss = boss;
    }

    public void clearAdditionalStats() {
        this.additionalCharisma = 0;
        this.additionalIntelligence = 0;
        this.additionalAgility = 0;
        this.additionalDefense = 0;
        this.additionalMaxHP = 0;
        this.additionalAttack = 0;
        this.defenses = new HashMap<>();
        this.offenses = new HashMap<>();
    }

    public void setOffenses(Map<Aspects, Double> offenses) {
        this.offenses = offenses;
    }

    public double getHealingModifier() {
        return healingModifier;
    }

    public void setHealingModifier(double healingModifier) {
        this.healingModifier = healingModifier;
    }

    public double getHitChanceModifier() {
        return hitChanceModifier;
    }

    public void setHitChanceModifier(double hitChanceModifier) {
        this.hitChanceModifier = hitChanceModifier;
    }

    public double getMeatHealingModifier() {
        return meatHealingModifier;
    }

    public void setMeatHealingModifier(double meatHealingModifier) {
        this.meatHealingModifier = meatHealingModifier;
    }

    public double getVegatableHealingModifier() {
        return vegatableHealingModifier;
    }

    public void setVegatableHealingModifier(double vegatableHealingModifier) {
        this.vegatableHealingModifier = vegatableHealingModifier;
    }

    public double getAlcoholHealingModifier() {
        return alcoholHealingModifier;
    }

    public void setAlcoholHealingModifier(double alcoholHealingModifier) {
        this.alcoholHealingModifier = alcoholHealingModifier;
    }

    public double getAlchemyHealingModifier() {
        return alchemyHealingModifier;
    }

    public void setAlchemyHealingModifier(double alchemyHealingModifier) {
        this.alchemyHealingModifier = alchemyHealingModifier;
    }

    public double getRelicIncreaseModifier() {
        return relicIncreaseModifier;
    }

    public void setRelicIncreaseModifier(double relicIncreaseModifier) {
        this.relicIncreaseModifier = relicIncreaseModifier;
    }

    public double getRuneIncreaseModifier() {
        return runeIncreaseModifier;
    }

    public void setRuneIncreaseModifier(double runeIncreaseModifier) {
        this.runeIncreaseModifier = runeIncreaseModifier;
    }

    public Aspects getActionAlignment() {
        return actionAlignment;
    }

    public void setActionAlignment(Aspects actionAlignment) {
        this.actionAlignment = actionAlignment;
    }

    public Action getLastSkillHit() {
        return lastSkillHit;
    }

    public void setLastSkillHit(Action lastSkillHit) {
        this.lastSkillHit = lastSkillHit;
    }

    public Map<Aspects, Double> getAdditionalOffenses() {
        return additionalOffenses;
    }

    public void setAdditionalOffenses(Map<Aspects, Double> additionalOffenses) {
        this.additionalOffenses = additionalOffenses;
    }

    public Map<Aspects, Double> getAdditionalDefenses() {
        return additionalDefenses;
    }

    public void setAdditionalDefenses(Map<Aspects, Double> additionalDefenses) {
        this.additionalDefenses = additionalDefenses;
    }

    public void addAttack(int delta) {
        attack = attack + delta;
    }

    public void addDefense(int delta) {
        defense = defense + delta;
    }

    public void addIntelligence(int delta) {
        intelligence = intelligence + delta;
    }

    public void addAgility(int delta) {
        agility = agility + delta;
    }

    public void addCharisma(int delta) {
        charisma = charisma + delta;
    }

    public void addBaseElementalOffense(Aspects aspect, double delta) {
        addElementalParameter(offenses, aspect, delta);
    }

    public void addBaseElementalDefenses(Aspects aspect, double delta) {
        addElementalParameter(defenses, aspect, delta);
    }

    public Equipped getEquipment() {
        if(equipment == null) {
            equipment = new Equipped(this);
        }
        return equipment;
    }

    public void setEquipment(Equipped equipment) {
        this.equipment = equipment;
    }

    public boolean isMechanical() {
        return isMechanical;
    }

    public void setIsMechanical(boolean mechanical) {
        isMechanical = mechanical;
    }

    public List<Action> getRelicActions() {
        return relicActions;
    }

    public void setRelicActions(List<Action> relicActions) {
        this.relicActions = relicActions;
    }

    public String getCommonItemId() {
        return commonItemId;
    }

    public void setCommonItemId(String commonItemId) {
        this.commonItemId = commonItemId;
    }

    public String getRareItemId() {
        return rareItemId;
    }

    public void setRareItemId(String rareItemId) {
        this.rareItemId = rareItemId;
    }

    public String getMorphItemId() {
        return morphItemId;
    }

    public void setMorphItemId(String morphItemId) {
        this.morphItemId = morphItemId;
    }

    public double getHealingItemModifier() {
        return healingItemModifier;
    }

    public void setHealingItemModifier(double healingItemModifier) {
        this.healingItemModifier = healingItemModifier;
    }

    public double getAdditionalHitChanceModifier() {
        return additionalHitChanceModifier;
    }

    public void setAdditionalHitChanceModifier(double additionalHitChanceModifier) {
        this.additionalHitChanceModifier = additionalHitChanceModifier;
    }
}
