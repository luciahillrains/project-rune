package com.lucylhill.projectrune.objects.entities;

import java.util.ArrayList;
import java.util.List;

public class PartyMember {
    String name;
    String description;
    int chapterUnlocked;
    String traitName;
    String startingClass;
    List<String> interestedClasses = new ArrayList<>();
    String weaponId;
    String armorId;
    String race;
    String gender;
    int attack;
    int defense;
    int charisma;
    int agility;
    int intelligence;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getChapterUnlocked() {
        return chapterUnlocked;
    }

    public void setChapterUnlocked(int chapterUnlocked) {
        this.chapterUnlocked = chapterUnlocked;
    }

    public String getTraitName() {
        return traitName;
    }

    public void setTraitName(String traitName) {
        this.traitName = traitName;
    }

    public String getStartingClass() {
        return startingClass;
    }

    public void setStartingClass(String startingClass) {
        this.startingClass = startingClass;
    }

    public List<String> getInterestedClasses() {
        return interestedClasses;
    }

    public void setInterestedClasses(List<String> interestedClasses) {
        this.interestedClasses = interestedClasses;
    }

    public String getWeaponId() {
        return weaponId;
    }

    public void setWeaponId(String weaponId) {
        this.weaponId = weaponId;
    }

    public String getArmorId() {
        return armorId;
    }

    public void setArmorId(String armorId) {
        this.armorId = armorId;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getCharisma() {
        return charisma;
    }

    public void setCharisma(int charisma) {
        this.charisma = charisma;
    }

    public int getAgility() {
        return agility;
    }

    public void setAgility(int agility) {
        this.agility = agility;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
