package com.lucylhill.projectrune.objects.traits;

import com.lucylhill.projectrune.objects.characters.CharacterInfo;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.classes.ClassAspects;

import java.util.List;

public class TraitResolver {

    public void resolveTrait(CharacterInfo characterInfo) {
        Trait trait = characterInfo.getTrait();
        setStats(characterInfo, trait.getStat(), trait.getChange());
        for(int i = 0; i < trait.getAdditionalStats().size(); i++) {
            Stat s = trait.getAdditionalStats().get(i);
            if(trait.getAdditionalChanges() == null) {
                setStats(characterInfo, s, trait.getChange());
            } else {
                setStats(characterInfo, s, trait.getAdditionalChanges().get(i));
            }
        }
        for(int i = 0; i < trait.getAspects().size(); i++) {
            if(trait.getAdditionalChanges() == null) {
                setAlignmentStats(characterInfo, trait.getStat(), trait.getAspects().get(i),trait.getChange() );
            } else {
                setAlignmentStats(characterInfo, trait.getStat(), trait.getAspects().get(i), trait.getAdditionalChanges().get(i) );
            }
        }
        setClassAlignmentStats(characterInfo, trait.getStat(), trait.getAspects(), trait.getClassAlignment());
    }

    private CharacterInfo setStats(CharacterInfo characterInfo, Stat stat, double change) {
        if(stat == Stat.ATTACK) {
            characterInfo.setAttack(characterInfo.getAttack() + (int)change);
        }
        if(stat == Stat.DEFENSE) {
            characterInfo.setDefense(characterInfo.getDefense() +(int)change);
        }
        if(stat == Stat.INTELLIGENCE) {
            characterInfo.setIntelligence(characterInfo.getIntelligence() + (int)change);
        }
        if(stat == Stat.AGILITY) {
            characterInfo.setAgility(characterInfo.getAgility() + (int)change);
        }
        if(stat == Stat.CHARISMA) {
            characterInfo.setCharisma(characterInfo.getCharisma() + (int)change);
        }
        if(stat == Stat.HIT_RATE) {
            characterInfo.setHitChanceModifier(change);
        }
        if(stat == Stat.VEG_HEALING) {
            characterInfo.setVegatableHealingModifier(change);
        }
        if(stat == Stat.MEAT_HEALING) {
            characterInfo.setMeatHealingModifier(change);
        }
        if(stat == Stat.ALCOHOL_HEALING) {
            characterInfo.setAlcoholHealingModifier(change);
        }
        if(stat == Stat.HEALING) {
            characterInfo.setHealingModifier(change);
        }
        if(stat == Stat.ALCHEMY_HEALING) {
            characterInfo.setAlchemyHealingModifier(change);
        }
        if(stat == Stat.ITEM_HEALING) {
            characterInfo.setHealingItemModifier(change);
        }
        return characterInfo;
    }

    private CharacterInfo setAlignmentStats(CharacterInfo c, Stat s, Aspects aspects, double d) {
        if(s == Stat.ELEMENTAL_OFFENSES) {
            c.getOffenses().put(aspects, d);
        }
        if(s == Stat.ELEMENTAL_DEFENSES) {
            c.getDefenses().put(aspects, d);
        }
        if(s == Stat.ALIGNMENT) {
            c.setAlignment(aspects);
        }
        if(s == Stat.ACTION_ALIGNMENT) {
            c.setActionAlignment(aspects);
        }
        return c;
    }

    private CharacterInfo setClassAlignmentStats(CharacterInfo c, Stat s, List<Aspects> a, ClassAspects ca) {
        if(s == Stat.TOTAL_ALIGNMENT) {
            c.setClassAlignment(ca);
            c.setAlignment(a.get(0));
        }
        if(s == Stat.CLASS_ALIGNMENT) {
            c.setClassAlignment(ca);
        }
        return c;
    }
}
