package com.lucylhill.projectrune.objects.traits;

import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.classes.ClassAspects;

import java.util.ArrayList;
import java.util.List;

public class Trait {
    private String name;
    private Stat stat;
    private List<Stat> additionalStats = new ArrayList<>();
    private List<Aspects> aspects = new ArrayList<>();
    private double change;
    private List<Double> additionalChanges;
    private String description;
    private ClassAspects classAlignment;
    boolean locked;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Stat getStat() {
        return stat;
    }

    public void setStat(Stat stat) {
        this.stat = stat;
    }

    public List<Aspects> getAspects() {
        return aspects;
    }

    public void setAspects(List<Aspects> aspects) {
        this.aspects = aspects;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public List<Stat> getAdditionalStats() {
        return additionalStats;
    }

    public void setAdditionalStats(List<Stat> additionalStats) {
        this.additionalStats = additionalStats;
    }

    public List<Double> getAdditionalChanges() {
        return additionalChanges;
    }

    public void setAdditionalChanges(List<Double> additionalChanges) {
        this.additionalChanges = additionalChanges;
    }

    public ClassAspects getClassAlignment() {
        return classAlignment;
    }

    public void setClassAlignment(ClassAspects classAlignment) {
        this.classAlignment = classAlignment;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }
}
