package com.lucylhill.projectrune.objects.traits;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.traits.Trait;
import com.lucylhill.projectrune.services.InternalState;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;


public class TraitList {
    private List<Trait> traits;

    public List<Trait> getTraits() {
        return traits;
    }

    public void setTraits(List<Trait> traits) {
        this.traits = traits;
    }

    public List<Trait> getRandomTraits(int number) {
        List<Integer> randomNumbers = new ArrayList<>();
        List<Trait> randomTraits = new ArrayList<>();
        Random random = new Random();
        for(int i = 0; i < number; i++) {
            int rnum = -1;
            while(rnum == -1 || randomNumbers.contains(rnum) || traits.get(rnum).isLocked()) {
                rnum = random.nextInt(traits.size());
            }
            randomNumbers.add(rnum);
        }
        randomNumbers.forEach(num -> randomTraits.add(traits.get(num)));
        return randomTraits;
    }

    public Optional<Trait> getTraitByName(String name) {
        List<Trait> pos = traits.stream().filter(t -> t.getName().equals((name))).collect(Collectors.toList());
        return getTraitFromList(pos);
    }

    private Optional<Trait> getTraitFromList(List<Trait> possibleList) {
        if(possibleList.size() == 0) {
            return Optional.empty();
        }
        return Optional.of(possibleList.get(0));
    }
}
