package com.lucylhill.projectrune.objects.misc;

import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.classes.ClassAspects;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.paint.Color;

public class UITools {

    public static void setAlignmentText(Label label, ClassAspects aspects) {
        label.setText(aspects.name().charAt(0) + "");
        switch (aspects) {
            case DARK -> {
                label.setText("D");
                label.setTextFill(Color.PURPLE);
            }
            case LIGHT -> {
                label.setText("H");
                label.setTextFill(Color.DARKGRAY);
            }
            case NEUTRAL -> {
                label.setText("N");
                label.setTextFill(Color.GRAY);
            }
        }
        label.setTooltip(new Tooltip(aspects.name()));
    }

    public static void setAlignmentText(Label label, Aspects aspect) {
        label.setText(aspect.name().charAt(0) + "");
        switch (aspect) {
            case AIR -> {
                label.setText("/A");
                label.setTextFill(Color.GREEN);
            }
            case EARTH -> {
                label.setText("/E");
                label.setTextFill(Color.BROWN);
            }
            case FIRE -> {
                label.setText("/F");
                label.setTextFill(Color.RED);
            }
            case ICE -> {
                label.setText("/I");
                label.setTextFill(Color.LIGHTBLUE);
            }
            case WATER -> {
                label.setText("/W");
                label.setTextFill(Color.BLUE);
            }
            case PHYSICAL -> {
                label.setText("/P");
                label.setTextFill(Color.GRAY);
            }
            case HOLY -> {
                label.setText("/L");
                label.setTextFill(Color.DARKGRAY);
            }
            case DARK -> {
                label.setText("/V");
                label.setTextFill(Color.PURPLE);
            }
            case OMNI -> {
                label.setText("/O");
                label.setTextFill(Color.WHITE);
            }
            case NONE -> {
                label.setText("/N");
                label.setTextFill(Color.BLACK);
            }
        }
        label.setTooltip(new Tooltip(aspect.name()));
    }

}
