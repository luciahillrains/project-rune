package com.lucylhill.projectrune.objects.misc;

import java.util.List;

public class Choice {
    String choice;
    String resultChecked;
    String resultNotChecked;
    List<Check> checks;
    List<ResultOpCode> resultCodesChecked;
    List<ResultOpCode> resultCodes;

    public String getChoice() {
        return choice;
    }

    public void setChoice(String choice) {
        this.choice = choice;
    }

    public String getResultChecked() {
        return resultChecked;
    }

    public void setResultChecked(String resultChecked) {
        this.resultChecked = resultChecked;
    }

    public String getResultNotChecked() {
        return resultNotChecked;
    }

    public void setResultNotChecked(String resultNotChecked) {
        this.resultNotChecked = resultNotChecked;
    }

    public List<Check> getChecks() {
        return checks;
    }

    public void setChecks(List<Check> checks) {
        this.checks = checks;
    }

    public List<ResultOpCode> getResultCodesChecked() {
        return resultCodesChecked;
    }

    public void setResultCodesChecked(List<ResultOpCode> resultCodesChecked) {
        this.resultCodesChecked = resultCodesChecked;
    }

    public List<ResultOpCode> getResultCodes() {
        return resultCodes;
    }

    public void setResultCodes(List<ResultOpCode> resultCodes) {
        this.resultCodes = resultCodes;
    }
}
