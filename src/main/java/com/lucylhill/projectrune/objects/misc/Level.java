package com.lucylhill.projectrune.objects.misc;

import java.util.Random;

public class Level {
    int level;
    double expAccrued = 0;
    int randomThreshold;
    int chapter = 0;
    Random random = new Random();

    public Level() {
        level = 1;
        randomThreshold = generateRandomThreshold();
    }
    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void incrementLevel() {
        level++;
        randomThreshold = generateRandomThreshold();
        expAccrued = 0;
    }

    public int generateRandomThreshold() {
        return random.nextInt(1, (10*level));
    }

    public int getExpToNextLevel() {
        return ((level + 1) * 35) + randomThreshold;
    }

    public double getExpAccrued() {
        return expAccrued;
    }

    public void setExpAccrued(double expAccrued) {
        this.expAccrued = expAccrued;
    }

    public int getChapter() {
        return chapter;
    }

    public void incrementChapter() {
        chapter = chapter + 1;
    }

    public boolean incrementExperience(int delta) {
        expAccrued = expAccrued + delta;
        if(expAccrued >= getExpToNextLevel()) {
            incrementLevel();
            return true;
        }
        return false;
    }
}
