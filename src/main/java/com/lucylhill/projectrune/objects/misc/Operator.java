package com.lucylhill.projectrune.objects.misc;

public enum Operator {
    GREATER_THAN,
    GREATER_THAN_EQUALS,
    LESS_THAN,
    LESS_THAN_EQUALS,
    EQUALS,
    NOT_EQUALS,
    ADD,
    MINUS,
    MULTIPLY,
    DIVIDE,
    MOD
}
