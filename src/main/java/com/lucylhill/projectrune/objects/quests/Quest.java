package com.lucylhill.projectrune.objects.quests;

public class Quest {
    String name;
    String description;
    String picture;
    String interstitialId;
    QuestType type;
    boolean rewards = false;
    int goldReward = 0;
    String itemReward = "";
    int itemRewardQuantity = 0;
    String successMessage;
    String failMessage;
    String repeatMessage;
    String id;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getInterstitialId() {
        return interstitialId;
    }

    public void setInterstitialId(String interstitialId) {
        this.interstitialId = interstitialId;
    }

    public QuestType getType() {
        return type;
    }

    public void setType(QuestType type) {
        this.type = type;
    }

    public boolean isRewards() {
        return rewards;
    }

    public void setRewards(boolean rewards) {
        this.rewards = rewards;
    }

    public int getGoldReward() {
        return goldReward;
    }

    public void setGoldReward(int goldReward) {
        this.goldReward = goldReward;
    }

    public String getItemReward() {
        return itemReward;
    }

    public void setItemReward(String itemReward) {
        this.itemReward = itemReward;
    }

    public int getItemRewardQuantity() {
        return itemRewardQuantity;
    }

    public void setItemRewardQuantity(int itemRewardQuantity) {
        this.itemRewardQuantity = itemRewardQuantity;
    }

    public String getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    public String getFailMessage() {
        return failMessage;
    }

    public void setFailMessage(String failMessage) {
        this.failMessage = failMessage;
    }

    public String getRepeatMessage() {
        return repeatMessage;
    }

    public void setRepeatMessage(String repeatMessage) {
        this.repeatMessage = repeatMessage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
