package com.lucylhill.projectrune.objects.quests;

public enum QuestType {
    MAIN_QUEST,
    SIDE_QUEST
}
