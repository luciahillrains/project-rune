package com.lucylhill.projectrune.objects.quests;

import com.lucylhill.projectrune.objects.dungeons.DungeonEvent;

public class InterstitialEvent {
    String background;
    String name;
    String id;
    String music;
    DungeonEvent event;

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DungeonEvent getEvent() {
        return event;
    }

    public void setEvent(DungeonEvent event) {
        this.event = event;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }
}
