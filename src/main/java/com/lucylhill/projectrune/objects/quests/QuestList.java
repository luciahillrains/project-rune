package com.lucylhill.projectrune.objects.quests;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class QuestList {

    List<Quest> quests;

    public List<Quest> getQuests() {
        return quests;
    }

    public void setQuests(List<Quest> quests) {
        this.quests = quests;
    }

    public Optional<Quest> getQuestById(String id) {
        Optional<Quest> optional = Optional.empty();
        List<Quest> subset = quests.stream().filter(q -> q.getId().equals(id)).collect(Collectors.toList());
        if(subset.size() >= 1) {
            optional = Optional.of(subset.get(0));
        }
        return optional;
    }
}
