package com.lucylhill.projectrune.objects.quests;

import com.lucylhill.projectrune.objects.quests.InterstitialEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class InterstitialEventList {
    List<InterstitialEvent> events = new ArrayList<>();

    public List<InterstitialEvent> getEvents() {
        return events;
    }

    public void setEvents(List<InterstitialEvent> events) {
        this.events = events;
    }

    public Optional<InterstitialEvent> getEventById(String id) {
        Optional<InterstitialEvent> event = Optional.empty();
        List<InterstitialEvent> subset = events.stream().filter(e -> e.getId().equals(id)).collect(Collectors.toList());
        if(subset.size() >= 1) {
            event = Optional.of(subset.get(0));
        }
        return event;
    }
}
