package com.lucylhill.projectrune.objects.passives;

public enum PassiveFlag {
    BATTLE,
    TURN,
    NONE
}
