package com.lucylhill.projectrune.objects.passives;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.objects.items.EquipmentType;
import com.lucylhill.projectrune.services.DebugLogger;
import javafx.scene.control.Alert;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class PassiveEffectResolver {
    DebugLogger logger = new DebugLogger(this.getClass());
    public boolean resolvePassive(String passiveId, String classId, EntityInfo source) {
        boolean effect = false;
        try {
            Class classEffect = Class.forName("com.lucylhill.projectrune.objects.classes.classSkills."+classId);
            Method method = classEffect.getDeclaredMethod(passiveId+"Effect", EntityInfo.class);
            effect = (boolean) method.invoke(classEffect.getConstructor().newInstance(), source);
        }
        catch(NoSuchMethodException | IllegalAccessException | InvocationTargetException | ClassNotFoundException | InstantiationException e) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("E001: No method found for "+passiveId);
            a.show();
            logger.log(e.toString());
        }
        return effect;
    }

    public boolean sourceHasWeaponType(EntityInfo source, EquipmentType type) {
        return source.getEquipment().getWeapon() != null &&
                source.getEquipment().getWeapon().getType() == type;
    }

}
