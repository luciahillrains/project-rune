package com.lucylhill.projectrune.objects.passives;

public class PassiveData {
    String id;
    PassiveFlag timing;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PassiveFlag getTiming() {
        return timing;
    }

    public void setTiming(PassiveFlag timing) {
        this.timing = timing;
    }
}
