package com.lucylhill.projectrune.objects.passives;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PassiveActionList {
    List<PassiveAction> passives  = new ArrayList<>();
    public List<PassiveAction> getPassives() {
        return passives;
    }

    public void setPassives(List<PassiveAction> passives) {
        this.passives = passives;
    }

    public PassiveAction getPassiveWithId(String id) {
        return passives.stream()
                .filter(e -> e.getId().equals(id))
                .collect(Collectors.toList())
                .get(0);
    }


    public boolean hasPassive(String id) {
        return passives.stream().anyMatch(e -> e.getId().equals(id));
    }

}
