package com.lucylhill.projectrune.objects.passives;

public class PassiveAction {
    String name;
    String id;
    int level;
    String icon;
    String description;
    PassiveFlag flag;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PassiveFlag getFlag() {
        return flag;
    }

    public void setFlag(PassiveFlag flag) {
        this.flag = flag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
