package com.lucylhill.projectrune.objects.achievements;

import com.lucylhill.projectrune.configuration.RuneConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class AchievementList {
    List<Achievement> achievements = new ArrayList<>();

    public List<Achievement> getAchievements() {
        return achievements;
    }

    public void setAchievements(List<Achievement> achievements) {
        this.achievements = achievements;
    }

    public Optional<Achievement> getAchievementById(String id) {
        Optional<Achievement> achievement = Optional.empty();
        List<Achievement> subset = RuneConfiguration.i().achievements().getAchievements().stream().filter(a -> a.getId().equals(id)).collect(Collectors.toList());
        if(subset.size() > 0) {
            achievement = Optional.of(subset.get(0));
        }
        return achievement;
    }
}
