package com.lucylhill.projectrune.objects.achievements;

public class Records {
    int enemyGroupsDefeated;
    int timesDied;
    int soloRuns;
    int dungeonRuns;
    int runesCollected;
    int itemsCollected;
    int warmachinaSeen;
    int warmachinaDefeated;
    boolean rareMarkFound = false;

    public void incrementEnemiesDefeated() {
        enemyGroupsDefeated++;
    }

    public void incrementTimesDied() {
        timesDied++;
    }

    public void incrementedSoloRuns() {
        soloRuns++;
    }

    public void incrementRunesCollected() {
        runesCollected++;
    }

    public void incrementItemsCollected() {
        itemsCollected++;
    }

    public void incrementDungeonRuns() {
        dungeonRuns++;
    }

    public void incrementWarmachinaSeen() {
        warmachinaSeen++;
    }

    public void incrementWarmachinaDefeated() {
        warmachinaDefeated++;
    }

    public int getEnemyGroupsDefeated() {
        return enemyGroupsDefeated;
    }

    public void setEnemyGroupsDefeated(int enemyGroupsDefeated) {
        this.enemyGroupsDefeated = enemyGroupsDefeated;
    }

    public int getTimesDied() {
        return timesDied;
    }

    public void setTimesDied(int timesDied) {
        this.timesDied = timesDied;
    }

    public int getSoloRuns() {
        return soloRuns;
    }

    public void setSoloRuns(int soloRuns) {
        this.soloRuns = soloRuns;
    }

    public int getRunesCollected() {
        return runesCollected;
    }

    public void setRunesCollected(int runesCollected) {
        this.runesCollected = runesCollected;
    }

    public int getItemsCollected() {
        return itemsCollected;
    }

    public void setItemsCollected(int itemsCollected) {
        this.itemsCollected = itemsCollected;
    }

    public int getDungeonRuns() {
        return dungeonRuns;
    }

    public void setDungeonRuns(int dungeonRuns) {
        this.dungeonRuns = dungeonRuns;
    }

    public int getWarmachinaSeen() {
        return warmachinaSeen;
    }

    public void setWarmachinaSeen(int warmachinaSeen) {
        this.warmachinaSeen = warmachinaSeen;
    }

    public int getWarmachinaDefeated() {
        return warmachinaDefeated;
    }

    public void setWarmachinaDefeated(int warmachinaDefeated) {
        this.warmachinaDefeated = warmachinaDefeated;
    }

    public boolean isRareMarkFound() {
        return rareMarkFound;
    }

    public void setRareMarkFound(boolean rareMarkFound) {
        this.rareMarkFound = rareMarkFound;
    }
}
