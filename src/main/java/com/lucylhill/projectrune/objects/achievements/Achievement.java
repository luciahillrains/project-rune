package com.lucylhill.projectrune.objects.achievements;

import com.lucylhill.projectrune.services.InternalState;

import java.util.Objects;

public class Achievement {
    String name;
    String id;
    String description;
    AchievementContext context;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AchievementContext getContext() {
        return context;
    }

    public void setContext(AchievementContext context) {
        this.context = context;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        if(InternalState.i().achievements().getAchievements().contains(this)) {
            return this.getName()+"!!!";
        } else {
            return this.getName();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Achievement that = (Achievement) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
