package com.lucylhill.projectrune.objects.achievements;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.services.InternalState;
import javafx.application.Platform;
import javafx.scene.control.Alert;

public class AchievementService {

    public void awardAchievements() {
        for(Achievement a : RuneConfiguration.i().achievements().getAchievements()) {
            awardAchievement(a);
        }
    }
    public void awardAchievement(Achievement achievement) {
        boolean addAchievement = false;
        if(!InternalState.i().achievements().getAchievements().contains(achievement)) {
            if(achievement.getContext().getType() == AchievementType.WINS) {
                addAchievement = achievement.getContext().getGoal() > InternalState.i().records().getEnemyGroupsDefeated();
            }
            if(achievement.getContext().getType() == AchievementType.SOLO) {
                addAchievement = achievement.getContext().getGoal() > InternalState.i().records().getSoloRuns();
            }
            if(achievement.getContext().getType() == AchievementType.RUNES) {
                addAchievement = achievement.getContext().getGoal() > InternalState.i().records().getRunesCollected();
            }
            if(achievement.getContext().getType() == AchievementType.DEATH) {
                addAchievement = achievement.getContext().getGoal() > InternalState.i().records().getTimesDied();
            }
            if(achievement.getContext().getType() == AchievementType.MISC) {
                if(achievement.getId().equals("FINISH_LESS_THAN_30_LVL")) {
                    addAchievement = InternalState.i().level().getLevel() < 30 && InternalState.i().getCurrentDungeon().equals("towerOfBabel");
                }
                if(achievement.getId().equals("FINISH_SOLO")) {
                    addAchievement = InternalState.i().partyList().getParty().size() == 1&& InternalState.i().getCurrentDungeon().equals("towerOfBabel");
                }
                if(achievement.getId().equals("POSTGAME_SOLO")) {
                    addAchievement = InternalState.i().partyList().getParty().size() == 1&& InternalState.i().getCurrentDungeon().equals("towerOfTartarus");
                }
                if(achievement.getId().equals("COMPLETION")) {
                    addAchievement = InternalState.i().achievements().getAchievements().size() == RuneConfiguration.i().achievements().getAchievements().size() - 1;
                }
                if(achievement.getId().equals("WARMACHINA_ACHIEV")) {
                    addAchievement = InternalState.i().records().getWarmachinaDefeated() > 0;
                }
                if(achievement.getId().equals("RECRUIT_MISSINGPER")) {
                    addAchievement = InternalState.i().partyList().getReserves().stream().anyMatch(c -> c.getName().equals("MISSINGPER"));
                }
                if(achievement.getId().equals("RECRUIT_BARTENDER")) {
                    addAchievement = InternalState.i().partyList().getReserves().stream().anyMatch(c -> c.getName().equals("Bartender"));
                }
                if(achievement.getId().equals("TOTAL_RECRUIT")) {
                    addAchievement = InternalState.i().partyList().getReserves().size() > 15;
                }
                if(achievement.getId().equals("RARE_MARK_FIND")) {
                    addAchievement = InternalState.i().records().isRareMarkFound();
                }
                if(achievement.getId().equals("LEVEL_99")) {
                    addAchievement = InternalState.i().level().getLevel() == 99;
                }
            }
            if(addAchievement) {
                addAchievement(achievement);
            }
        }

    }

    public void addAchievement(Achievement a) {
        showMessage(a);
        InternalState.i().achievements().getAchievements().add(a);
    }

    public void showMessage(Achievement a) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("Congrats, you've finished the achievement '"+a.getName()+"'!");
            alert.show();
        });

    }
}
