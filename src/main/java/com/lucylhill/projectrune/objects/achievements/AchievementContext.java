package com.lucylhill.projectrune.objects.achievements;

public class AchievementContext {
    AchievementType type;
    int goal;

    public AchievementType getType() {
        return type;
    }

    public void setType(AchievementType type) {
        this.type = type;
    }

    public int getGoal() {
        return goal;
    }

    public void setGoal(int goal) {
        this.goal = goal;
    }
}
