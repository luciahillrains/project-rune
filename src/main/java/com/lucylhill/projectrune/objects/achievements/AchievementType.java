package com.lucylhill.projectrune.objects.achievements;

public enum AchievementType {
    ITEM,
    WINS,
    SOLO,
    RUNES,
    DEATH,
    MISC
}
