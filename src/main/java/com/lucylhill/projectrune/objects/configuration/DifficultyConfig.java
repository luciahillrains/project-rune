package com.lucylhill.projectrune.objects.configuration;

public class DifficultyConfig {

    boolean permadeath = true;
    boolean permakick = true;
    boolean noBattles = false;
    boolean resumeFromCheckpoint = false;
    boolean reputationMode = true;
    double partyStrength = 1.0;
    double enemyStrength = 1.0;
    double goldCostModifier = 1.0;
    int turnLimit = 365;

    public boolean isPermadeath() {
        return permadeath;
    }

    public void setPermadeath(boolean permadeath) {
        this.permadeath = permadeath;
    }

    public boolean isPermakick() {
        return permakick;
    }

    public void setPermakick(boolean permakick) {
        this.permakick = permakick;
    }

    public boolean isNoBattles() {
        return noBattles;
    }

    public void setNoBattles(boolean noBattles) {
        this.noBattles = noBattles;
    }

    public boolean isResumeFromCheckpoint() {
        return resumeFromCheckpoint;
    }

    public void setResumeFromCheckpoint(boolean resumeFromCheckpoint) {
        this.resumeFromCheckpoint = resumeFromCheckpoint;
    }

    public boolean isReputationMode() {
        return reputationMode;
    }

    public void setReputationMode(boolean reputationMode) {
        this.reputationMode = reputationMode;
    }

    public double getPartyStrength() {
        return partyStrength;
    }

    public void setPartyStrength(double partyStrength) {
        this.partyStrength = partyStrength;
    }

    public double getEnemyStrength() {
        return enemyStrength;
    }

    public void setEnemyStrength(double enemyStrength) {
        this.enemyStrength = enemyStrength;
    }

    public double getGoldCostModifier() {
        return goldCostModifier;
    }

    public void setGoldCostModifier(double goldCostModifier) {
        this.goldCostModifier = goldCostModifier;
    }

    public int getTurnLimit() {
        return turnLimit;
    }

    public void setTurnLimit(int turnLimit) {
        this.turnLimit = turnLimit;
    }
}
