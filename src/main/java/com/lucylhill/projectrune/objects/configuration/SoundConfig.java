package com.lucylhill.projectrune.objects.configuration;

public class SoundConfig {
    double volume = 100;
    boolean muteMusic = false;
    boolean muteSoundEffects = false;

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public boolean isMuteMusic() {
        return muteMusic;
    }

    public void setMuteMusic(boolean muteMusic) {
        this.muteMusic = muteMusic;
    }

    public boolean isMuteSoundEffects() {
        return muteSoundEffects;
    }

    public void setMuteSoundEffects(boolean muteSoundEffects) {
        this.muteSoundEffects = muteSoundEffects;
    }
}
