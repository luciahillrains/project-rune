package com.lucylhill.projectrune.objects.configuration;

public class DebugConfig {
    //all events are choice events
    boolean forceChoiceEvent;

    //quick start gives a party with all traits and classes.
    boolean quickStartUberParty;

    boolean uberStrongEnemies;

    //force a trait to always appear in character creation or quick start party.
    String forcedTrait;

    String testAction;

    String previousAction;

    boolean battleTestMode;

    public boolean isForceChoiceEvent() {
        return forceChoiceEvent;
    }

    public void setForceChoiceEvent(boolean forceChoiceEvent) {
        this.forceChoiceEvent = forceChoiceEvent;
    }

    public boolean isQuickStartUberParty() {
        return quickStartUberParty;
    }

    public void setQuickStartUberParty(boolean quickStartUberParty) {
        this.quickStartUberParty = quickStartUberParty;
    }

    public String getForcedTrait() {
        return forcedTrait;
    }

    public void setForcedTrait(String forcedTrait) {
        this.forcedTrait = forcedTrait;
    }

    public boolean isUberStrongEnemies() {
        return uberStrongEnemies;
    }

    public void setUberStrongEnemies(boolean uberStrongEnemies) {
        this.uberStrongEnemies = uberStrongEnemies;
    }

    public String getTestAction() {
        return testAction;
    }

    public void setTestAction(String testAction) {
        this.testAction = testAction;
    }

    public boolean isBattleTestMode() {
        return battleTestMode;
    }

    public void setBattleTestMode(boolean battleTestMode) {
        this.battleTestMode = battleTestMode;
    }

    public String getPreviousAction() {
        return previousAction;
    }

    public void setPreviousAction(String previousAction) {
        this.previousAction = previousAction;
    }
}
