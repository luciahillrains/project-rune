package com.lucylhill.projectrune.objects.configuration;

public class Configuration {
    DifficultyConfig difficultyConfig;
    DebugConfig debugConfig;
    SoundConfig soundConfig;

    public Configuration() {
        difficultyConfig = new DifficultyConfig();
        debugConfig = new DebugConfig();
        soundConfig = new SoundConfig();
    }

    public DifficultyConfig getDifficultyConfig() {
        return difficultyConfig;
    }

    public void setDifficultyConfig(DifficultyConfig difficultyConfig) {
        this.difficultyConfig = difficultyConfig;
    }

    public DebugConfig getDebugConfig() {
        return debugConfig;
    }

    public void setDebugConfig(DebugConfig debugConfig) {
        this.debugConfig = debugConfig;
    }

    public SoundConfig getSoundConfig() {
        return soundConfig;
    }

    public void setSoundConfig(SoundConfig soundConfig) {
        this.soundConfig = soundConfig;
    }
}
