package com.lucylhill.projectrune;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.configuration.Configuration;
import com.lucylhill.projectrune.services.InternalState;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputFilter;
import java.nio.file.Files;
import java.nio.file.Paths;

public class RuneApplication extends Application {
    Parent rootNode;

    public static final int WINDOW_WIDTH = 1920;
    public static final int WINDOW_HEIGHT= 1080;

    @Override
    public void init() throws Exception {
        FXMLLoader fxmlLoader;
        System.setProperty("prism.lcdtext", "false");
        if(RuneConfiguration.i().systemProperties().isTurnOffSplash()) {
            fxmlLoader = new FXMLLoader(RuneApplication.class.getResource("hello-view.fxml"));
        } else {
            fxmlLoader = new FXMLLoader(RuneApplication.class.getResource("splash-logo-view.fxml"));
        }
        rootNode = fxmlLoader.load();

    }
    @Override
    public void start(Stage stage) throws IOException {
        Scene scene = new Scene(rootNode, WINDOW_WIDTH, WINDOW_HEIGHT);
        scene.getStylesheets().add("style.css");
        stage.setTitle("Runeseeker "+ RuneConfiguration.i().systemProperties().getBuildInfo());
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }



}