package com.lucylhill.projectrune.configuration;

import com.lucylhill.projectrune.objects.achievements.AchievementList;
import com.lucylhill.projectrune.objects.battle.actions.ActionList;
import com.lucylhill.projectrune.objects.battle.actions.AspectTable;
import com.lucylhill.projectrune.objects.battle.enemies.EnemyList;
import com.lucylhill.projectrune.objects.battle.status.AilmentList;
import com.lucylhill.projectrune.objects.classes.ClassList;
import com.lucylhill.projectrune.objects.dungeons.DungeonList;
import com.lucylhill.projectrune.objects.quests.InterstitialEventList;
import com.lucylhill.projectrune.objects.entities.PartyMemberList;
import com.lucylhill.projectrune.objects.items.Item;
import com.lucylhill.projectrune.objects.items.ItemList;
import com.lucylhill.projectrune.objects.items.ShopList;
import com.lucylhill.projectrune.objects.passives.PassiveActionList;
import com.lucylhill.projectrune.objects.quests.Quest;
import com.lucylhill.projectrune.objects.quests.QuestList;
import com.lucylhill.projectrune.objects.system.SystemProperties;
import com.lucylhill.projectrune.objects.tavern.ConversationList;
import com.lucylhill.projectrune.objects.tavern.TavernNPCList;
import com.lucylhill.projectrune.objects.traits.TraitList;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RuneConfiguration {
    TraitList traitList;
    ClassList classList;
    DungeonList dungeonList;
    ActionList actionList;
    EnemyList enemyList;
    SystemProperties systemProperties;
    AspectTable aspectTable;
    AilmentList ailmentList;
    PassiveActionList passiveList;
    ConversationList conversationList;
    ItemList itemList;
    ItemList equipmentList;
    ItemList relicList;
    TavernNPCList npcList;
    ShopList shopList;
    AchievementList achievementList;
    InterstitialEventList interstitialEventList;
    QuestList questList;
    private PartyMemberList partyMembersList;

    static RuneConfiguration instance;

    public static RuneConfiguration i() {
        if(instance == null) {
            instance = new RuneConfiguration();
        }
        return instance;
    }

    public <T> T loadList(String resource, T list, Class<T> type) {
        if(list == null) {
            InputStream inputStream = RuneConfiguration.class.getResourceAsStream(resource);
            Yaml yaml = new Yaml(new Constructor(type));
            list = yaml.load(inputStream);
        }
        return list;
    }

    public Item getItemFromId(String itemId) {
        String[] split = itemId.split(":");
        Optional<Item> item = Optional.empty();
        if(split[0].equals("Item")) {
            item =  RuneConfiguration.i().items().getItemById(split[1]);
        }
        if(split[0].equals("Equipment")) {
            item = RuneConfiguration.i().equipment().getItemById(split[1]);
        }
        if(split[0].equals("Relic")) {
            item = RuneConfiguration.i().relics().getItemById(split[1]);
        }
        return item.orElse(null);
    }

    public List<Item> getItemsFromIds(List<String> ids) {
        List<Item> items = new ArrayList<>();
        for(String id : ids) {
            items.add(getItemFromId(id));
        }
        return items;
    }

    public TraitList traitList() {
        return loadList("/properties/traits.yml", traitList, TraitList.class);
    }

    public ClassList classList() {
        return loadList("/properties/classes.yml", classList, ClassList.class);
    }

    public DungeonList dungeonList() {
        return loadList("/properties/dungeons/dungeons.yml", dungeonList, DungeonList.class);
    }

    public SystemProperties systemProperties() {
        return loadList("/properties/system-properties.yml", systemProperties, SystemProperties.class);
    }

    public ActionList actions() {
        return loadList("/properties/actions.yml", actionList, ActionList.class);
    }

    public EnemyList enemies() {
        return loadList("/properties/enemies.yml", enemyList, EnemyList.class);
    }

    public AspectTable aspectTable() {
        return loadList("/properties/aspect-table.yml", aspectTable, AspectTable.class);
    }

    public AilmentList ailmentList() {
        return loadList("/properties/ailments.yml", ailmentList, AilmentList.class);
    }

    public PassiveActionList passiveList() {
        return loadList("/properties/passives.yml", passiveList, PassiveActionList.class);
    }

    public ConversationList conversationList() {
        return loadList("/properties/conversations.yml", conversationList, ConversationList.class);
    }

    public TavernNPCList npcs() {
        return loadList("/properties/tavernNPCs.yml", npcList, TavernNPCList.class);
    }

    public ItemList items() {
        return loadList("/properties/items.yml", itemList, ItemList.class);
    }

    public ItemList equipment() {
        return loadList("/properties/equipment.yml", equipmentList, ItemList.class);
    }

    public ItemList relics() {
        return loadList("/properties/relics.yml", relicList, ItemList.class);
    }

    public ShopList shops() { return loadList("/properties/shops.yml", shopList, ShopList.class);}

    public AchievementList achievements() { return loadList("/properties/achievements.yml", achievementList, AchievementList.class);}
    
    public PartyMemberList partyMembers() { return loadList("/properties/partyMembers.yml", partyMembersList, PartyMemberList.class);}

    public InterstitialEventList interstitials() { return loadList("/properties/interstitials.yml", interstitialEventList, InterstitialEventList.class); }

    public QuestList questList() { return loadList("/properties/quests.yml", questList, QuestList.class); }

}
