package com.lucylhill.projectrune.controllers.dungeon;

import com.lucylhill.projectrune.objects.characters.CharacterInfo;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.classes.ClassAspects;
import com.lucylhill.projectrune.objects.misc.Level;
import com.lucylhill.projectrune.objects.misc.UITools;
import com.lucylhill.projectrune.objects.passives.PassiveAction;
import com.lucylhill.projectrune.services.InternalState;
import javafx.beans.binding.BooleanExpression;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public class PartyViewPaneController implements Initializable {
    @FXML
    Label leaderNameLabel;

    @FXML
    Label leaderClassAlignmentLabel;

    @FXML
    Label leaderAlignmentLabel;


    @FXML
    Label leaderAtkLabel;

    @FXML
    Label leaderDefLabel;

    @FXML
    Label leaderCHRLabel;

    @FXML
    Label leaderINTLabel;

    @FXML
    Label leaderAGILabel;

    @FXML
    Label partyMember1NameLabel;

    @FXML
    Label partyMember1ClassAlignmentLabel;

    @FXML
    Label partyMember1AlignmentLabel;

    @FXML
    Label partyMember1ClassLabel;

    @FXML
    Label partyMember1AtkLabel;

    @FXML
    Label partyMember1DefLabel;

    @FXML
    Label partyMember1CHRLabel;

    @FXML
    Label partyMember1INTLabel;

    @FXML
    Label partyMember1AGILabel;

    @FXML
    Label partyMember2NameLabel;

    @FXML
    Label partyMember2ClassAlignmentLabel;

    @FXML
    Label partyMember2AlignmentLabel;

    @FXML
    Label partyMember2HpLabel;

    @FXML
    Label partyMember2AtkLabel;

    @FXML
    Label partyMember2DefLabel;

    @FXML
    Label partyMember2CHRLabel;

    @FXML
    Label partyMember2INTLabel;

    @FXML
    Label partyMember2AGILabel;

    @FXML
    Label partyMember3NameLabel;

    @FXML
    Label partyMember3ClassAlignmentLabel;

    @FXML
    Label partyMember3AlignmentLabel;

    @FXML
    Label partyMember3AtkLabel;

    @FXML
    Label partyMember3DefLabel;

    @FXML
    Label partyMember3CHRLabel;

    @FXML
    Label partyMember3INTLabel;

    @FXML
    Label partyMember3AGILabel;

    @FXML
    Label leaderHitLabel;

    @FXML
    Label partyMember1HitLabel;

    @FXML
    Label partyMember2HitLabel;

    @FXML
    Label partyMember3HitLabel;

    @FXML
    ProgressBar levelProgressBar;

    @FXML
    Label levelLabel;

    @FXML
    VBox parent;

    @FXML
    Label expLabel;

    @FXML
    Label goldLabel;




    private BooleanProperty isShowing = new SimpleBooleanProperty(false);
    private BooleanProperty isMapShowing = new SimpleBooleanProperty(false);
    List<Label> nameLabels;
    List<Label> classAlignmentLabels;
    List<Label> alignmentLabels;
    List<Label> attackLabels;
    List<Label> defenseLabels;
    List<Label> charismaLabels;
    List<Label> intelligenceLabels;
    List<Label> agilityLabels;
    List<Label> hitLabels;

    BooleanProperty isBarShowing = new SimpleBooleanProperty(true);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        parent.visibleProperty().bindBidirectional(isShowing);
        parent.managedProperty().bindBidirectional(isShowing);
        nameLabels = Arrays.asList(leaderNameLabel, partyMember1NameLabel, partyMember2NameLabel, partyMember3NameLabel);
        classAlignmentLabels = Arrays.asList(leaderClassAlignmentLabel, partyMember1ClassAlignmentLabel, partyMember2ClassAlignmentLabel, partyMember3ClassAlignmentLabel);
        alignmentLabels = Arrays.asList(leaderAlignmentLabel, partyMember1AlignmentLabel, partyMember2AlignmentLabel, partyMember3AlignmentLabel);
        attackLabels = Arrays.asList(leaderAtkLabel, partyMember1AtkLabel, partyMember2AtkLabel, partyMember3AtkLabel);
        defenseLabels = Arrays.asList(leaderDefLabel, partyMember1DefLabel, partyMember2DefLabel, partyMember3DefLabel);
        charismaLabels = Arrays.asList(leaderCHRLabel, partyMember1CHRLabel, partyMember2CHRLabel, partyMember3CHRLabel);
        intelligenceLabels = Arrays.asList(leaderINTLabel, partyMember1INTLabel, partyMember2INTLabel, partyMember3INTLabel);
        agilityLabels = Arrays.asList(leaderAGILabel, partyMember1AGILabel, partyMember2AGILabel, partyMember3AGILabel);
        hitLabels = Arrays.asList(leaderHitLabel, partyMember1HitLabel, partyMember2HitLabel, partyMember3HitLabel);
    }

    public void show(BooleanProperty isMapShowing, BooleanProperty isBarShowing) {
        isShowing.set(true);
        this.isMapShowing = isMapShowing;
        this.isBarShowing = isBarShowing;
        //isBarShowing.set(false);
        renderUI();
    }

    public void renderUI() {
        Level level = InternalState.i().level();
        List<CharacterInfo> party = InternalState.i().partyList().getParty();
        levelProgressBar.setProgress(level.getExpAccrued() / level.getExpToNextLevel());
        levelLabel.setText(level.getLevel()+"");
        for(int i = 0; i < party.size(); i++) {
            CharacterInfo c = party.get(i);
            nameLabels.get(i).setText(c.getRecruitedName());
            UITools.setAlignmentText(classAlignmentLabels.get(i), c.getClassInfo().getClassAspect());
            UITools.setAlignmentText(alignmentLabels.get(i), c.getClassInfo().getAspect());
            attackLabels.get(i).setText((int)c.getStat(Stat.ATTACK)+"");
            attackLabels.get(i).setTooltip(new Tooltip("Attack: Determines strength of physical and magical attacks."));
            defenseLabels.get(i).setText((int)c.getStat(Stat.DEFENSE)+"");
            defenseLabels.get(i).setTooltip(new Tooltip("Defense: Determines protection against attacks and resistance to status ailments."));
            charismaLabels.get(i).setText((int)c.getStat(Stat.CHARISMA) +"");
            charismaLabels.get(i).setTooltip(new Tooltip("Charisma: Determines how charming character is.  High charisma is important for passing dialog checks & bartering, and used in charisma-type attacks."));
            intelligenceLabels.get(i).setText((int)c.getStat(Stat.INTELLIGENCE) +"");
            intelligenceLabels.get(i).setTooltip(new Tooltip("Intelligence: Determines power of magical attacks, as well as how smart the character is in general."));
            agilityLabels.get(i).setText((int)c.getStat(Stat.AGILITY)+"");
            agilityLabels.get(i).setTooltip(new Tooltip("Agility: Determines escape rolls as well as used in some events and is used as strength in some attacks./"));
            hitLabels.get(i).setText((int)c.getStat(Stat.HIT_RATE)+"");
            hitLabels.get(i).setTooltip(new Tooltip("Hit Rate: Determines accuracy of skills."));
        }
        expLabel.setText(InternalState.i().level().getExpAccrued() + "/"+InternalState.i().level().getExpToNextLevel());
        goldLabel.setText(InternalState.i().gold() + " g");

    }



    @FXML
    public void goBack() {
        isShowing.set(false);
        isMapShowing.set(true);
        isBarShowing.set(true);
    }

    public void setPartyBarVisibilityProperty(BooleanProperty property) {
        isBarShowing = property;
    }


}
