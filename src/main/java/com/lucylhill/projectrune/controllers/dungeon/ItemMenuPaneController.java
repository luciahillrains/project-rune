package com.lucylhill.projectrune.controllers.dungeon;

import com.lucylhill.projectrune.objects.battle.BattleEvent;
import com.lucylhill.projectrune.objects.battle.BattleState;
import com.lucylhill.projectrune.objects.battle.actions.*;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.objects.items.Equipment;
import com.lucylhill.projectrune.objects.items.EquipmentFlag;
import com.lucylhill.projectrune.objects.items.Item;
import com.lucylhill.projectrune.services.BattleMessenger;
import com.lucylhill.projectrune.services.InternalState;
import com.lucylhill.projectrune.services.ItemService;
import com.lucylhill.projectrune.services.PartyList;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class ItemMenuPaneController implements Initializable {
    @FXML
    ListView<Item> inventoryItemList;

    @FXML
    Label itemNameLabel;

    @FXML
    Label itemTypeLabel;

    @FXML
    Label itemDescriptionLabel;

//    @FXML
//    Label leaderNameLabel;
//
//    @FXML
//    Label leaderHPLabel;
//
//    @FXML
//    Label partyMember1NameLabel;
//
//    @FXML
//    Label partyMember1HPLabel;
//
//    @FXML
//    Label partyMember2NameLabel;
//
//    @FXML
//    Label partyMember2HPLabel;
//
//    @FXML
//    Label partyMember3NameLabel;
//
//    @FXML
//    Label partyMember3HPLabel;
//
//    @FXML
//    VBox leaderBox;
//
//    @FXML
//    VBox partyMember1Box;
//
//    @FXML
//    VBox partyMember2Box;
//
//    @FXML
//    VBox partyMember3Box;

    @FXML
    VBox parent;

    @FXML
    VBox actionParent;

    @FXML
    Button useItemButton;

    @FXML
    Button discardItemButton;

    @FXML
    Label actionResultLabel;

    private BooleanProperty isShowing = new SimpleBooleanProperty(false);
    private BooleanProperty isMapShowing = new SimpleBooleanProperty(false);
    private List<VBox> partyBoxes;
    private List<Label> partyMemberNames;
    private List<Label> partyMemberHp;
    ItemService itemService = new ItemService();
    Item activeItem;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
//        partyBoxes = List.of(leaderBox, partyMember1Box, partyMember2Box, partyMember3Box);
//        partyMemberNames = List.of(leaderNameLabel, partyMember1NameLabel, partyMember2NameLabel, partyMember3NameLabel);
//        partyMemberHp = List.of(leaderHPLabel, partyMember1HPLabel, partyMember2HPLabel, partyMember3HPLabel);
        parent.visibleProperty().bindBidirectional(isShowing);
        parent.managedProperty().bindBidirectional(isShowing);
    }

    public void show(BooleanProperty isMapShowing) {
        isShowing.set(true);
        this.isMapShowing = isMapShowing;
        renderUI();
    }

    public void renderUI() {
        ObservableList<Item> items = FXCollections.observableArrayList(InternalState.i().inventory().getItems());
        inventoryItemList.setItems(items);
        inventoryItemList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            activeItem = newValue;
            if(activeItem != null) {
                itemNameLabel.setText(activeItem.getName());
                itemDescriptionLabel.setText(activeItem.getDescription());
                String typeLabel = "Type: ";
                if (activeItem.isKeyItem()) {
                    typeLabel = typeLabel + "Key";
                }
                else if (activeItem.isComponent()) {
                    typeLabel = typeLabel + "Component/Sellable";
                }
                else if (activeItem.isOnlyUsableInBattle()) {
                    typeLabel = typeLabel + "Battle";
                } else {
                    if (activeItem instanceof Equipment a) {
                        typeLabel = typeLabel + "Equipment";
                        if(a.getFlag() == EquipmentFlag.RELIC) {
                            typeLabel = typeLabel + "Relic";
                        }
                    } else {
                        typeLabel = typeLabel + "Field";
                    }
                }

                itemTypeLabel.setText(typeLabel);
                actionParent.setVisible(true);
                useItemButton.setDisable(!typeLabel.contains("Field"));
                discardItemButton.setDisable(activeItem.isKeyItem());
            } else {
                cleanUI();
            }

        });
    }
    public void cleanUI() {
        itemNameLabel.setText("");
        itemDescriptionLabel.setText("");
        itemTypeLabel.setText("");
        actionParent.setVisible(false);
        useItemButton.setDisable(false);
    }
    @FXML
    public void goBack() {
        isShowing.set(false);
        isMapShowing.set(true);
    }

    @FXML
    public void showAllItems() {
        ObservableList<Item> items = FXCollections.observableArrayList(InternalState.i().inventory().getItems());
        inventoryItemList.setItems(items);
    }

    @FXML
    public void showUseableItems() {
        List<Item> items = InternalState.i().inventory().getItems()
                .stream().filter(i -> !i.isKeyItem() &&
                        !i.isComponent() &&
                        !i.isOnlyUsableInBattle() &&
                        !(i instanceof Equipment))
                .collect(Collectors.toList());
        inventoryItemList.setItems(FXCollections.observableArrayList(items));
    }

    @FXML
    public void showBattleItems() {
        List<Item> items = InternalState.i().inventory().getItems()
                .stream().filter(i -> !i.isKeyItem() &&
                        !i.isComponent() &&
                        i.isOnlyUsableInBattle())
                .collect(Collectors.toList());
        inventoryItemList.setItems(FXCollections.observableArrayList(items));
    }

    @FXML
    public void showEquipment(){
        List<Item> items = InternalState.i().inventory().getItems()
                .stream().filter(i -> i instanceof Equipment)
                .collect(Collectors.toList());
        inventoryItemList.setItems(FXCollections.observableArrayList(items));
    }

    @FXML
    public void showRelics() {
        List<Item> items = InternalState.i().inventory().getItems()
                .stream().filter(i -> i instanceof Equipment && ((Equipment) i).getFlag() == EquipmentFlag.RELIC)
                .collect(Collectors.toList());
        inventoryItemList.setItems(FXCollections.observableArrayList(items));
    }

    @FXML
    public void showComponents() {
        List<Item> items = InternalState.i().inventory().getItems()
                .stream().filter(Item::isComponent)
                .collect(Collectors.toList());
        inventoryItemList.setItems(FXCollections.observableArrayList(items));
    }

    @FXML
    public void showKey() {
        List<Item> items = InternalState.i().inventory().getItems()
                .stream().filter(Item::isKeyItem)
                .collect(Collectors.toList());
        inventoryItemList.setItems(FXCollections.observableArrayList(items));
    }

    @FXML
    public void discardItem() {
        itemService.discardItem(activeItem.getId());
        inventoryItemList.getItems().remove(activeItem);
        activeItem = null;
        cleanUI();
    }

    public void setUpTargeting() {
        if(!activeItem.isOnlyUsableInBattle()) {
            InternalState.i().setTargetMode(true);
            boolean partyTargetingMode = activeItem.getTargetType() == ActionTargetType.PARTY_SINGLE || activeItem.getTargetType() == ActionTargetType.PARTY_DEAD;
            if(partyTargetingMode) {
                InternalState.i().setPartyTargeting(partyTargetingMode);
                InternalState.i().setDeadTargetting(activeItem.getTargetType() == ActionTargetType.PARTY_DEAD);

            }
        }
    }

    public void executeItemAction() {
        Deck deck = new Deck(false);
        ActionEffectResolver actionResolver = new ActionEffectResolver();
        Action action = deck.createItemAction(activeItem);
        EntityInfo target;
        PartyList partyList = InternalState.i().partyList();
        EntityInfo source = partyList.getParty().get(0);
        ActionResult result = null;
        if(action.getTargetType() == ActionTargetType.PARTY_SINGLE || action.getTargetType() == ActionTargetType.PARTY_DEAD) {
            target = partyList.getParty().get(BattleState.i().getTargetId());
            result = actionResolver.resolveAction(action, target, source);
        }
        else if(action.getTargetType() == ActionTargetType.PARTY_ALL) {
            for (int i = 0; i < partyList.getParty().size(); i++) {
                EntityInfo e = partyList.getParty().get(i);
                if (e.getHp() > 0) {
                    result = actionResolver.resolveAction(action, e, source);
                }
            }
        }
        if(result != null) {
            actionResultLabel.setText(result.getText());
        }
        inventoryItemList.getItems().remove(activeItem);
        activeItem = null;
        cleanUI();
    }

    @FXML
    public void useItem() {
        setUpTargeting();
    }

}
