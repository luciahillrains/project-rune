package com.lucylhill.projectrune.controllers.dungeon;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.controllers.misc.InterstitialViewController;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.dungeons.Dungeon;
import com.lucylhill.projectrune.objects.dungeons.DungeonEvent;
import com.lucylhill.projectrune.objects.dungeons.DungeonEventSnippet;
import com.lucylhill.projectrune.objects.dungeons.DungeonEventType;
import com.lucylhill.projectrune.objects.items.Item;
import com.lucylhill.projectrune.objects.misc.Choice;
import com.lucylhill.projectrune.objects.misc.ResultOpCode;
import com.lucylhill.projectrune.objects.system.SystemProperties;
import com.lucylhill.projectrune.services.*;
import com.lucylhill.projectrune.objects.characters.CharacterInfo;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import java.io.IOException;
import java.net.URL;
import java.util.*;

public class DungeonEventPaneController implements Initializable {
    @FXML
    VBox parent;

    @FXML
    HBox eventImageBox;

    @FXML
    VBox choiceParent;

    @FXML
    RadioButton choice1;

    @FXML
    RadioButton choice2;

    @FXML
    RadioButton choice3;

    @FXML
    RadioButton choice4;

    @FXML
    Label eventLabel;

    @FXML
    Button continueButton;

    int maxNumPartyCharacters;

    DungeonEvent event;
    private BooleanProperty isVisible = new SimpleBooleanProperty(false);
    private BooleanProperty hasChoices = new SimpleBooleanProperty(false);
    RadioButton[] choiceButtons;
    BooleanProperty isMapShowing = new SimpleBooleanProperty(false);
    ToggleGroup toggles = new ToggleGroup();
    PartyList partyList;
    boolean choiceEventWasFinished = false;
    SceneSwitcher sceneSwitcher;
    CheckValidator checkValidator;
    ItemService itemService = new ItemService();
    DungeonViewController dungeonViewController;
    InterstitialViewController interstitialViewController;
    boolean squareIsUnlocked = false;
    List<Integer> randomPartyPositions = new ArrayList<>();
    public void setEvent(DungeonEvent event, BooleanProperty isMapShowing) {
        this.event = event;
        this.isMapShowing = isMapShowing;
        this.isVisible.set(true);
        initializeUI();
    }

    public void setEvent(DungeonEvent event) {
        this.event = event;
        this.isVisible.set(true);
        initializeUI();
    }

    public void setDungeonViewController(DungeonViewController controller) {
        this.dungeonViewController = controller;
        this.interstitialViewController = null;
    }
    public void setIsVisible(boolean isVisible) {
        this.isVisible.set(isVisible);
    }

    public void initializeUI() {
        randomPartyPositions = new ArrayList<>();
        //eventImageBox.setBackground(BackgroundImageFactory.createEventImage("/art/logo.png"));
        if(event.getPicture() != null && !event.getPicture().isEmpty()) {
            eventImageBox.setBackground(BackgroundImageFactory.createEventImage("/art/events/"+event.getPicture()));
        }
        eventLabel.setText(getEventText(event.getMessage()));
        if(event.getChoices() != null && event.getChoices().size() > 0) {
            hasChoices.set(true);
            toggles.selectToggle(null);
            for(int i = 0; i < choiceButtons.length; i++) {
                RadioButton button = choiceButtons[i];
                if(i < event.getChoices().size()) {
                    button.setVisible(true);
                    button.setText(getEventText(event.getChoices().get(i).getChoice()));
                    button.setUserData(i);
                } else {
                    button.setVisible(false);
                }
            }
        }
        else {
            hasChoices.set(false);
        }
        if(event.getType() == DungeonEventType.HEALING) {
            partyList.fullHeal();
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        maxNumPartyCharacters = RuneConfiguration.i().systemProperties().getMaxNumOfCharactersInParty();
        partyList = InternalState.i().partyList();
        parent.visibleProperty().bindBidirectional(isVisible);
        parent.managedProperty().bindBidirectional(isVisible);
        choiceParent.visibleProperty().bindBidirectional(hasChoices);
        checkValidator = new CheckValidator();
        choiceButtons = new RadioButton[]{choice1, choice2, choice3, choice4};
        int i = 0;
        for(RadioButton rb : choiceButtons) {
            rb.setUserData(i);
            rb.setToggleGroup(toggles);
            i++;
        }
    }

    @FXML
    public void continueWithGame() throws IOException {
        squareIsUnlocked = true;
        if(InternalState.i().partyList().isEntirePartyDead()) {
            SceneSwitcher.switchScreens(continueButton, SceneSwitcher.GAME_OVER_SCREEN);
        }
        if(event.getType() == DungeonEventType.ORANGE) {
            if(InternalState.i().checkpoint() != null) {
                InternalState.i().checkpoint().setDirty(true);
            }
            InternalState.i().records().incrementDungeonRuns();
            if(InternalState.i().partyList().getParty().size() == 1) {
                InternalState.i().records().incrementedSoloRuns();
            }
            SceneSwitcher.switchScreens(continueButton, SceneSwitcher.CREDITS_SCREEN);
        }
        if(event.getChoices() != null && event.getChoices().size() > 0 && !choiceEventWasFinished) {
            if(toggles.getSelectedToggle() == null) {
                Alert chooseSomethingAlert = new Alert(Alert.AlertType.INFORMATION);
                chooseSomethingAlert.setContentText("Please choose an option to continue.");
                chooseSomethingAlert.show();
                return;
            }
            Integer choice = (Integer)toggles.getSelectedToggle().getUserData();
            Choice c = event.getChoices().get(choice);
            hasChoices.set(false);
            if(c.getChecks() != null && c.getChecks().size() > 0) {
                boolean checked = checkValidator.evaluateChecks(c.getChecks());
                if(checked) {
                    eventLabel.setText(getEventText(c.getResultChecked()));
                    evaluateResultCodes(c.getResultCodesChecked());
                }  else {
                    eventLabel.setText(getEventText(c.getResultNotChecked()));
                    evaluateResultCodes(c.getResultCodes());
                }
            } else {
                eventLabel.setText(getEventText(c.getResultNotChecked()));

            }
            choiceEventWasFinished = true;
            return;
        }
        if(event.getChain() != null) {
            setEvent(event.getChain(), isMapShowing);
        } else {
            isVisible.set(false);
            if(isMapShowing != null) {
                isMapShowing.set(true);
            }
            choiceEventWasFinished = false;
            evaluateResultCodes(event.getResultCodes());
            if(event.getItemId() != null && !event.getItemId().equals("")) {
                Item foundItem = RuneConfiguration.i().getItemFromId(event.getItemId());
                if(foundItem != null) {
                    InternalState.i().records().incrementItemsCollected();
                    itemService.addItem(foundItem);
                }
            }
        }

    }

    public void evaluateResultCodes(List<ResultOpCode> resultCodes) throws IOException {
        if(resultCodes == null || resultCodes.size() == 0) {
            squareIsUnlocked = false;
            return;
        }
        for(ResultOpCode code : resultCodes) {
            if(code.getStatus().equals("TRAVEL")) {
                if(code.getComponent().contains("Screen:")) {
                    String screenUri = code.getComponent().split(":")[1];
                    SceneSwitcher.switchScreens(eventLabel, screenUri);
                } else if(code.getComponent().contains("Dungeon:")) {
                    String dungeonName = code.getComponent().split(":")[1];
                    InternalState.i().setCurrentDungeon(dungeonName);
                    Optional<Dungeon> dungeon = RuneConfiguration.i().dungeonList().getDungeonById(dungeonName);
                    if(dungeon.isPresent()) {
                        DungeonGenerator.i().setDungeon(dungeon.get());
                        SceneSwitcher.switchScreens(continueButton, SceneSwitcher.DUNGEON_SCREEN);
                    } else {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setContentText("Was not able to find dungeon to go to!");
                        alert.show();
                    }

                }
            }
            if(code.getStatus().equals("UNLOCK")) {
                if(code.getComponent().contains("Switch:")) {
                    squareIsUnlocked = InternalState.i().switches().contains(code.getComponent().split(":")[1]);
                } else {
                    Item item = RuneConfiguration.i().getItemFromId(code.getComponent());
                    if(InternalState.i().inventory().getItems().contains(item)) {
                        squareIsUnlocked = true;
                    }
                }

            }
            if(code.getStatus().equals("SWITCH")) {
                InternalState.i().switches().add(code.getComponent());
            }
            if(code.getStatus().equals("HP")) {
                for(CharacterInfo c : InternalState.i().partyList().getParty()) {
                    if(c.getHp() > 0) {
                        c.setHp(c.getHp() + Integer.parseInt(code.getComponent()));
                        if(c.getHp() > c.getMaxHp()) {
                            c.setHp(c.getMaxHp());
                        }
                    }
                }
            }
            if(code.getStatus().equals("LIFE")) {
                for(CharacterInfo c: InternalState.i().partyList().getParty()) {
                    if(c.getHp() <= 0) {
                        c.setHp(10);
                    }
                }
            }
            if(code.getStatus().equals("ITEM")) {
                Item foundItem = RuneConfiguration.i().getItemFromId(code.getComponent());
                InternalState.i().records().incrementItemsCollected();
                if(foundItem.isRune()) {
                    InternalState.i().records().incrementRunesCollected();
                }
                if(foundItem.getId().equals("RareMark")) {
                    InternalState.i().records().setRareMarkFound(true);
                }
                itemService.addItem(foundItem);
            }
            if(code.getStatus().equals("STATUS")) {
                StatusEffect effect = RuneConfiguration.i().ailmentList().getStatusEffect(code.getComponent());
                if(effect != null) {
                    for(CharacterInfo c : InternalState.i().partyList().getParty()) {
                        c.getStatusEffects().addEffect(effect);
                    }
                }
            }
            if(code.getStatus().equals("BATTLE")) {
                Timer timer = new Timer(false);
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        Platform.runLater(() -> {
                            DungeonEvent event = new DungeonEvent();
                            event.setType(DungeonEventType.BATTLE);
                            event.setEnemies(List.of(code.getComponent().split(",")));
                            if(interstitialViewController == null) {
                                dungeonViewController.startBattle(event);
                            } else {
                                interstitialViewController.startBattle(event);
                            }
                            this.cancel();
                        });
                    }
                }, 2000);

            }
            if(code.getStatus().equals("GOLD")) {
                InternalState.i().changeGold(Integer.parseInt(code.getComponent()));
            }
        }
    }

    public String getEventText(String eventText) {
        String newEvent = eventText;
        Random random = new Random();
        for(DungeonEventSnippet snip : event.getSnippets()) {
            if(eventText.contains("{"+snip.getId()+"}")) {
                int partyNum = random.nextInt(0, partyList.getParty().size());
                while(randomPartyPositions.contains(partyNum)) {
                    partyNum = random.nextInt(0, partyList.getParty().size());
                }
                CharacterInfo c = partyList.getParty().get(partyNum);
                String replaceText = snip.getResponses().getOrDefault(c.getName(), snip.getResponses().get("default"));
                newEvent = newEvent.replace("{"+snip.getId()+"}", replaceText);
            }
        }
        return newEvent;
    }


    public BooleanProperty getVisible() {
        return isVisible;
    }

    public boolean isSquareIsUnlocked() {
        return squareIsUnlocked;
    }

    public void setSquareIsUnlocked(boolean squareIsUnlocked) {
        this.squareIsUnlocked = squareIsUnlocked;
    }

    public void setInterstitialViewController(InterstitialViewController interstitialViewController) {
        this.dungeonViewController = null;
        this.interstitialViewController = interstitialViewController;
    }
}
