package com.lucylhill.projectrune.controllers.dungeon;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.controllers.battle.BattlePaneViewController;
import com.lucylhill.projectrune.objects.battle.BattleState;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.misc.UITools;
import com.lucylhill.projectrune.objects.passives.PassiveAction;
import com.lucylhill.projectrune.objects.passives.PassiveData;
import com.lucylhill.projectrune.services.BattleMessenger;
import com.lucylhill.projectrune.services.DebugLogger;
import com.lucylhill.projectrune.services.InternalState;
import com.lucylhill.projectrune.services.PartyList;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import com.lucylhill.projectrune.objects.characters.CharacterInfo;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

public class PartyBarViewController implements Initializable {
    @FXML
    Label leaderNameLabel;

    @FXML
    Label leaderHPLabel;

    @FXML
    Label leaderTraitLabel;

    @FXML
    Label leaderClassLabel;
    
    @FXML
    Label leaderClassAlignmentLabel;
    
    @FXML
    Label leaderAlignmentLabel;

    @FXML
    HBox leaderPassiveDisplay;

    @FXML
    Label partyMember1NameLabel;

    @FXML
    Label partyMember1HPLabel;

    @FXML
    Label partyMember1TraitLabel;

    @FXML
    Label partyMember1ClassLabel;

    @FXML
    HBox partyMember1PassiveDisplay;

    @FXML
    Label partyMember1ClassAlignmentLabel;

    @FXML
    Label partyMember1AlignmentLabel;


    @FXML
    Label partyMember2NameLabel;

    @FXML
    Label partyMember2HPLabel;

    @FXML
    Label partyMember2TraitLabel;

    @FXML
    Label partyMember2ClassLabel;

    @FXML
    HBox partyMember2PassiveDisplay;

    @FXML
    Label partyMember2ClassAlignmentLabel;

    @FXML
    Label partyMember2AlignmentLabel;


    @FXML
    Label partyMember3NameLabel;

    @FXML
    Label partyMember3HPLabel;

    @FXML
    Label partyMember3TraitLabel;

    @FXML
    Label partyMember3ClassLabel;

    @FXML
    HBox partyMember3PassiveDisplay;

    @FXML
    Label partyMember3ClassAlignmentLabel;

    @FXML
    Label partyMember3AlignmentLabel;

    @FXML
    VBox leaderParentBox;

    @FXML
    VBox partyMember1ParentBox;

    @FXML
    VBox partyMember2ParentBox;

    @FXML
    VBox partyMember3ParentBox;

    @FXML
    Label leaderStatusLabel;

    @FXML
    Label partyMember1StatusLabel;

    @FXML
    Label partyMember2StatusLabel;

    @FXML
    Label partyMember3StatusLabel;

    @FXML
    HBox parent;

    PartyList partyList;
    List<Label> nameLabels;
    List<Label> hpLabels;
    List<Label> traitLabels;
    List<Label> classLabels;
    List<Label> classAlignmentLabels;
    List<Label> alignmentLabels;
    List<Label> statusLabels;
    List<HBox> passiveDisplays;
    List<VBox> parentBoxes;

    Timer timer = new Timer(true);

    BattlePaneViewController battlePaneViewController;
    ItemMenuPaneController itemMenuPaneController;
    BooleanProperty isBarShowing = new SimpleBooleanProperty(true);

    DebugLogger logger = new DebugLogger(this.getClass());
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        renderUI();
        scheduleTask();
    }

    public void renderUI() {
        parent.visibleProperty().bindBidirectional(isBarShowing);
        parent.managedProperty().bindBidirectional(isBarShowing);
        partyList = InternalState.i().partyList();
        nameLabels = List.of(leaderNameLabel, partyMember1NameLabel, partyMember2NameLabel, partyMember3NameLabel);
        hpLabels = List.of(leaderHPLabel, partyMember1HPLabel, partyMember2HPLabel, partyMember3HPLabel);
        traitLabels = List.of(leaderTraitLabel, partyMember1TraitLabel, partyMember2TraitLabel, partyMember3TraitLabel);
        classLabels = List.of(leaderClassLabel, partyMember1ClassLabel, partyMember2ClassLabel, partyMember3ClassLabel);
        classAlignmentLabels = List.of(leaderClassAlignmentLabel, partyMember1ClassAlignmentLabel, partyMember2ClassAlignmentLabel, partyMember3ClassAlignmentLabel);
        alignmentLabels = List.of(leaderAlignmentLabel, partyMember1AlignmentLabel, partyMember2AlignmentLabel, partyMember3AlignmentLabel);
        passiveDisplays = List.of(leaderPassiveDisplay, partyMember1PassiveDisplay, partyMember2PassiveDisplay, partyMember3PassiveDisplay);
        parentBoxes = List.of(leaderParentBox, partyMember1ParentBox, partyMember2ParentBox, partyMember3ParentBox);
        statusLabels = List.of(leaderStatusLabel, partyMember1StatusLabel, partyMember2StatusLabel, partyMember3StatusLabel);
        for(int i = 0; i < partyList.getParty().size(); i++) {
            CharacterInfo c = partyList.getParty().get(i);
            nameLabels.get(i).setText(c.getName());
            traitLabels.get(i).setText(c.getTrait().getName());
            traitLabels.get(i).setTooltip(new Tooltip(c.getTrait().getDescription()));
            classLabels.get(i).setText(c.getClassInfo().getName());
            classLabels.get(i).setTooltip(new Tooltip(c.getClassInfo().getUIDescription()));
            UITools.setAlignmentText(classAlignmentLabels.get(i), c.getClassInfo().getClassAspect());
            UITools.setAlignmentText(alignmentLabels.get(i), c.getClassInfo().getAspect());
            classAlignmentLabels.get(i).setTooltip( new Tooltip(c.getClassInfo().getClassAspect().name()));
            alignmentLabels.get(i).setTooltip( new Tooltip(c.getClassInfo().getAspect().name()));
            passiveDisplays.get(i).getChildren().clear();
            for(PassiveData d : c.getClassInfo().getPassives()) {
                PassiveAction p = RuneConfiguration.i().passiveList().getPassiveWithId(d.getId());
                if(p.getLevel() <= InternalState.i().level().getLevel()) {
                    Label pLabel = new Label();
                    pLabel.setText(p.getIcon());
                    pLabel.getStyleClass().add("icons");
                    pLabel.getStyleClass().add("name");
                    pLabel.setTooltip(new Tooltip(p.getName() + ": "+p.getDescription()));
                    passiveDisplays.get(i).getChildren().add(pLabel);
                }
            }
            int finalI = i;
            parentBoxes.get(i).addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
                if(InternalState.i().targetMode()) {
                    if((c.getHp() > 0 && !InternalState.i().isDeadTargetting()) || (c.getHp() <= 0 && InternalState.i().isDeadTargetting())) {
                        BattleState.i().setTargetId(finalI);
                        BattleMessenger.i().setMessage(c.getName(), finalI, false);
                        if(battlePaneViewController.isVisible()) {
                            battlePaneViewController.executeAction();
                            battlePaneViewController.runEvent();
                        } else {
                            itemMenuPaneController.executeItemAction();
                        }

                        InternalState.i().setTargetMode(false);
                        InternalState.i().setPartyTargeting(false);
                        turnOffTargetingBorders();
                        event.consume();
                    }
                }
            });
        }
    }

    public void turnOffTargetingBorders() {
        for(VBox parentBox : parentBoxes) {
            parentBox.setStyle("-fx-border: none");
        }
    }
    public void scheduleTask() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    if(InternalState.i().isPartyTargeting()) {
                        for(int i = 0; i < parentBoxes.size(); i++) {
                            if((partyList.getParty().get(i).getHp() <= 0 && InternalState.i().isDeadTargetting()) || (partyList.getParty().get(i).getHp() > 0) && !InternalState.i().isDeadTargetting()) {
                                parentBoxes.get(i).setStyle("-fx-border-color: cyan");
                            }
                        }
                    }
                    renderDeltaList();
                    showPartyEffects();
                });
            }
        }, 0, 1000);
    }
    public void renderDeltaList() {
        for(int i = 0; i < partyList.getParty().size(); i++) {
            CharacterInfo chr = partyList.getParty().get(i);
            double maxHp = chr.getMaxHp();
            for(StatusEffect effect : chr.getStatusEffects().getEffects()) {
                if(effect.getStat() == Stat.MAX_HP) {
                    maxHp = maxHp + effect.getChange();
                }
            }
            hpLabels.get(i).setText(chr.getHp() + "/" + (int)maxHp);
            if(chr.getHp() > maxHp) {
                chr.setHp(chr.getMaxHp());
            }
            if(chr.getHp() <= 0) {
                chr.setHp(0);
                hpLabels.get(i).setText("INCAPACITATED");
            }
        }
    }

    public void showPartyEffects() {
        for(int i = 0; i < partyList.getParty().size(); i++) {
            StringBuilder str = new StringBuilder();
            CharacterInfo ch = partyList.getParty().get(i);
            for(StatusEffect se : ch.getStatusEffects().getEffects()) {
                logger.log(se.toString());
                if(se.getIsNamedAilment()) {
                    str.append(se.getIcon());
                } else {
                    if(se.getStat() == Stat.ATTACK) {
                        str.append("A");
                    }
                    if(se.getStat() == Stat.DEFENSE) {
                        str.append("D");
                    }
                    if(se.getStat() == Stat.MAX_HP) {
                        str.append("HP");
                    }
                    if(se.getStat() == Stat.AGILITY) {
                        str.append("Ag");
                    }
                    if(se.getStat() == Stat.INTELLIGENCE) {
                        str.append("I");
                    }
                    if(se.getStat() == Stat.HP) {
                        str.append("♥");
                    }
                    if(se.getStat() == Stat.BASE_DAMAGE) {
                        str.append("#");
                    }
                    if(se.getStat() == Stat.HIT_RATE) {
                        str.append("%");
                    }
                    if(se.getName().equals("Imbued")) {
                        str.append("Imb");
                    }
                    if(se.getChange() > 0) {
                        str.append("↑");
                    } else if(se.getChange() < 0){
                        str.append("↓");
                    }
                }
            }
            if(!str.toString().isBlank()) {
                statusLabels.get(i).setText(str.toString());
            }
        }
    }

    public void refresh() {
        for(CharacterInfo c: InternalState.i().partyList().getParty()) {
            c.setHp(c.getMaxHp());
        }
        renderDeltaList();
    }

    public void setBattlePaneViewController(BattlePaneViewController battlePaneViewController) {
        this.battlePaneViewController = battlePaneViewController;
    }

    public void setItemMenuPaneController(ItemMenuPaneController itemMenuPaneController) {
        this.itemMenuPaneController = itemMenuPaneController;
    }

    public void setVisibility(boolean visible) {
        isBarShowing.set(visible);
    }

    public BooleanProperty getVisibility() {
        return isBarShowing;
    }
}
