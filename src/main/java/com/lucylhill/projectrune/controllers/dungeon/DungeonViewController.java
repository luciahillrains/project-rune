package com.lucylhill.projectrune.controllers.dungeon;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.controllers.battle.BattlePaneViewController;
import com.lucylhill.projectrune.objects.characters.CharacterInfo;
import com.lucylhill.projectrune.objects.dungeons.*;
import com.lucylhill.projectrune.services.*;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.net.URL;
import java.util.*;

import static com.lucylhill.projectrune.objects.dungeons.DungeonMap.MAP_COORD_MAX;

public class DungeonViewController implements Initializable {
    GeneratedDungeon generatedDungeon;
    DungeonGenerator dungeonGenerator;

    @FXML
    PartyBarViewController partyBarViewController;

    @FXML
    PartyViewPaneController partyViewPaneController;

    @FXML
    DungeonEventPaneController dungeonEventPaneController;

    @FXML
    BattlePaneViewController battlePaneViewController;

    @FXML
    ItemMenuPaneController itemPaneViewController;

    @FXML
    EquipMenuPaneController equipPaneViewController;

    @FXML
    GridPane dungeonButtonContainer;

    @FXML
    Label rollLabel;

    @FXML
    Label dungeonNameLabel;

    @FXML
    HBox menuButtonsParent;

    @FXML
    StackPane parent;

    @FXML
    Button debugEscapeButton;

    static Coordinate partyLocation = new Coordinate();
    int[] rolls = new int[]{1, 1, 1, 1, 1, 1, 1, 2, 2, 3};
    int roll = 1;
    Random random = new Random();
    Button[][] buttonArray = new Button[MAP_COORD_MAX + 1][MAP_COORD_MAX + 1];
    BooleanProperty isMapShowing = new SimpleBooleanProperty(true);
    DebugLogger logger = new DebugLogger(this.getClass());

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        dungeonGenerator = DungeonGenerator.i();
        dungeonButtonContainer.visibleProperty().bindBidirectional(isMapShowing);
        dungeonButtonContainer.managedProperty().bindBidirectional(isMapShowing);
        menuButtonsParent.visibleProperty().bindBidirectional(isMapShowing);
        generatedDungeon = dungeonGenerator.generateDungeon(generatedDungeon);
        if(generatedDungeon.getBackground() != null) {
            parent.setBackground(BackgroundImageFactory.createSceneBackground("/art/backgrounds/"+generatedDungeon.getBackground()+"-1.png"));
        } else {
            parent.setBackground(BackgroundImageFactory.createSceneBackground("/art/backgrounds/trainingGrounds-1.png"));
        }
        dungeonNameLabel.setText(generatedDungeon.getName());
        dungeonEventPaneController.setIsVisible(false);
        generateMapUI(generatedDungeon.getMap());
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText("Quick Tutorial: Make your way to the square at the top of the map.  Clickable squares are highlighted & have a cyan border.");
        alert.show();
        enableButton(partyLocation.x + 1, partyLocation.y);
        battlePaneViewController.getCheckpointProperty().addListener((observable, oldVal, newVal) -> {
            if(newVal.equals("reset")) {
                logger.log("rehydrating from checkpoint!");
                rehydrateFromCheckpoint();
                enableButtons();
                battlePaneViewController.getCheckpointProperty().set("");
            }
        });
        partyBarViewController.setBattlePaneViewController(battlePaneViewController);
        partyBarViewController.setItemMenuPaneController(itemPaneViewController);
        dungeonEventPaneController.setDungeonViewController(this);
        debugEscapeButton.setVisible(RuneConfiguration.i().systemProperties().isDebug());
        RuneRadio.i().play("/music/"+generatedDungeon.getMusic()+".mp3");
    }

    public void generateMapUI(DungeonMap map) {
        for(int i = 0; i <= MAP_COORD_MAX; i++) {
            for(int j = 0; j <= MAP_COORD_MAX; j++) {
                Button button = new Button();
                button.setPrefHeight(83.0);
                button.setPrefWidth(212.0);
                if(map.getMap().get(i).containsKey(j)) {
                    DungeonSquare square = map.getMap().get(i).get(j);
                    button.setBackground(getBackgroundForEventType(square.getType()));
                    if(square.getType() == DungeonEventType.NEGATIVE ||
                            square.getType() == DungeonEventType.CHECKPOINT ||
                            square.getType() == DungeonEventType.STORY ||
                            square.getType() == DungeonEventType.HEALING) {
                        button.setTextFill(Color.WHITE);

                    }
                    button.setDisable(true);
                    if(square.getType() == DungeonEventType.ORANGE && i == 0) {
                        button.setDisable(false);
                        partyLocation.x = 0;
                        partyLocation.y = j;
                    } else {
                        int finalI = i;
                        int finalJ = j;
                        button.setOnAction(e -> {
                            button.setText("O");
                            if(square.getType() != DungeonEventType.NO_EVENT && !square.isVisited()) {
                                DungeonEvent event = generatedDungeon.getEvents().getRandomDungeonEventOfType(square.getType());
                                if(RuneConfiguration.i().systemProperties().isDebug() && InternalState.i().configuration().getDebugConfig().isForceChoiceEvent()) {
                                    event = generatedDungeon.getEvents().getChoiceEvent();
                                }
                                if(square.getType() == DungeonEventType.CHECKPOINT) {
                                    InternalState.i().setCheckpoint(partyLocation, generatedDungeon);
                                }
                                if(square.getType() == DungeonEventType.BATTLE) {
                                    startBattle(event);
                                } else {
                                    isMapShowing.set(false);
                                    dungeonEventPaneController.setEvent(event, isMapShowing);
                                    battlePaneViewController.getVisible().set(false);
                                }

                            }
                            if(square.isLocked() && dungeonEventPaneController.isSquareIsUnlocked()) {
                                square.setVisited(true);
                            } else {
                                square.setVisited(!square.isLocked());
                            }
                            partyLocation.x = finalI;
                            partyLocation.y = finalJ;
                            enableButtons();
                        });
                    }

                    //TODO: set event.
                } else {
                    button.setBackground(BackgroundImageFactory.createImageBackground("/art/tiles/disabled.png"));
                    button.setDisable(true);
                }
                dungeonButtonContainer.add(button,MAP_COORD_MAX - j, MAP_COORD_MAX- i);
                buttonArray[i][j] = button;

            }
        }
    }

    public void startBattle(DungeonEvent event) {
        isMapShowing.set(false);
        battlePaneViewController.setEvent(event, generatedDungeon, isMapShowing);
        dungeonEventPaneController.getVisible().set(false);
    }


    public Background getBackgroundForEventType(DungeonEventType eventType) {
        Background image;
        switch(eventType) {
            case ORANGE -> image = BackgroundImageFactory.createImageBackground("/art/tiles/start.png");
            case ITEM -> image = BackgroundImageFactory.createImageBackground("/art/tiles/item.png");
            case STORY -> image = BackgroundImageFactory.createImageBackground("/art/tiles/story.png");
            case BATTLE -> image = BackgroundImageFactory.createImageBackground("/art/tiles/battle2.png");
            case HEALING -> image = BackgroundImageFactory.createImageBackground("/art/tiles/healing.png");
            case NEGATIVE -> image = BackgroundImageFactory.createImageBackground("/art/tiles/negative.png");
            case NO_EVENT -> image = BackgroundImageFactory.createImageBackground("/art/tiles/noevent.png");
            case POSITIVE -> image = BackgroundImageFactory.createImageBackground("/art/tiles/positive.png");
            case CHECKPOINT -> image = BackgroundImageFactory.createImageBackground("/art/tiles/camp.png");
            default -> throw new IllegalStateException("Unexpected value: " + eventType);
        }
        return image;
    }

    public void enableButtons() {
        disableButtons();
        Button currentButton = buttonArray[partyLocation.x][partyLocation.y];
        currentButton.setText("O");
        currentButton.setStyle("-fx-font-weight: bold; -fx-font-size: 36px; -fx-text-fill: red");
        enableButton(partyLocation.x + 1, partyLocation.y);
        enableButton(partyLocation.x + 1, partyLocation.y - 1);
        enableButton(partyLocation.x + 1, partyLocation.y + 1);
        enableButton(partyLocation.x - 1, partyLocation.y);
        enableButton(partyLocation.x - 1, partyLocation.y + 1);
        enableButton(partyLocation.x - 1, partyLocation.y - 1);
        enableButton(partyLocation.x, partyLocation.y - 1);
        enableButton(partyLocation.x, partyLocation.y + 1);
    }

    public void disableButtons() {
        for(Button[] bts : buttonArray) {
            for(Button bt : bts) {
                bt.setDisable(true);
                bt.setStyle("-fx-font-weight: bold; -fx-font-size: 36px; -fx-text-fill: white");
                if(bt.getText().equals("O")) {
                    bt.setText("X");
                }
            }
        }
    }

    public void enableButton(int x, int y) {
        if(x < 0) {
            x = 0;
        }
        if(y < 0) {
            y = 0;
        }
        if(x > MAP_COORD_MAX) {
            x = MAP_COORD_MAX;
        }
        if(y > MAP_COORD_MAX) {
            y = MAP_COORD_MAX;
        }

        DungeonSquare square = generatedDungeon.getMap().getMap().get(x).get(y);
        if(square != null) {
            Button button = buttonArray[x][y];
            button.setDisable(false);
            button.setStyle(" -fx-font-weight: bold; -fx-font-size: 36px; -fx-text-fill: white");
        }
    }

    @FXML
    public void rollForMovement() {
        roll = rolls[random.nextInt(rolls.length)];
        rollLabel.setText("Roll: "+roll);
        enableButtons();
    }

    @FXML
    public void showPartyStatusPane() {
        isMapShowing.set(false);
        //partyBarViewController.setVisibility(false);
        partyViewPaneController.show(isMapShowing, partyBarViewController.getVisibility());
    }

    @FXML
    public void showItemPane() {
        isMapShowing.set(false);
        itemPaneViewController.show(isMapShowing);
    }

    @FXML
    public void showEquipPane() {
        isMapShowing.set(false);
        partyBarViewController.setVisibility(false);
        equipPaneViewController.show(isMapShowing, partyBarViewController.getVisibility());
    }

    @FXML
    public void returnToStart() throws IOException {
        SceneSwitcher.switchScreens(debugEscapeButton, SceneSwitcher.START_SCREEN);
    }




    public void rehydrateFromCheckpoint() {
        Checkpoint cp = InternalState.i().checkpoint();
        if(!cp.isDirty()) {
            partyLocation = cp.getCoordinate();
            partyBarViewController.refresh();
        }
    }

}
