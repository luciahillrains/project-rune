package com.lucylhill.projectrune.controllers.dungeon;

import com.lucylhill.projectrune.objects.characters.CharacterInfo;
import com.lucylhill.projectrune.objects.items.Equipment;
import com.lucylhill.projectrune.objects.items.EquipmentFlag;
import com.lucylhill.projectrune.objects.items.Item;
import com.lucylhill.projectrune.services.InternalState;
import com.lucylhill.projectrune.services.ItemService;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class EquipMenuPaneController implements Initializable {
    @FXML
    ListView<Item> inventoryItemList;

    @FXML
    Label itemNameLabel;

    @FXML
    Label itemTypeLabel;

    @FXML
    Label itemDescriptionLabel;

    @FXML
    VBox parent;

    @FXML
    VBox actionParent;

    @FXML
    Button equipItemButton;

    @FXML
    Label oldDescriptionLabel;

    @FXML
    Label newDescriptionLabel;

    @FXML
    Label leaderNameLabel;

    @FXML
    Label leaderWeaponLabel;

    @FXML
    Label leaderBodyLabel;

    @FXML
    Label leaderRelic1Label;

    @FXML
    Label leaderRelic2Label;

    @FXML
    Label leaderRelic3Label;

    @FXML
    Label partyMember1NameLabel;

    @FXML
    Label partyMember1WeaponLabel;

    @FXML
    Label partyMember1BodyLabel;

    @FXML
    Label partyMember1Relic1Label;

    @FXML
    Label partyMember1Relic2Label;

    @FXML
    Label partyMember1Relic3Label;

    @FXML
    Label partyMember2NameLabel;

    @FXML
    Label partyMember2WeaponLabel;

    @FXML
    Label partyMember2BodyLabel;

    @FXML
    Label partyMember2Relic1Label;

    @FXML
    Label partyMember2Relic2Label;

    @FXML
    Label partyMember2Relic3Label;

    @FXML
    Label partyMember3NameLabel;

    @FXML
    Label partyMember3WeaponLabel;

    @FXML
    Label partyMember3BodyLabel;

    @FXML
    Label partyMember3Relic1Label;

    @FXML
    Label partyMember3Relic2Label;

    @FXML
    Label partyMember3Relic3Label;
    private BooleanProperty isShowing = new SimpleBooleanProperty(false);
    private BooleanProperty isMapShowing = new SimpleBooleanProperty(false);
    private BooleanProperty isBarShowing = new SimpleBooleanProperty(false);
    private List<Label> partyMemberNameLabels = new ArrayList<>();
    private List<Label> partyMemberWeaponLabels = new ArrayList<>();
    private List<Label> partyMemberBodyLabels = new ArrayList<>();
    private List<Label> partyMemberRelic1Labels = new ArrayList<>();
    private List<Label> partyMemberRelic2Labels = new ArrayList<>();
    private List<Label> partyMemberRelic3Labels = new ArrayList<>();

    int currentSpot = 0;
    EquipmentFlag currentFlag = EquipmentFlag.WEAPON;
    int currentRelicSlot = -1;
    
    ItemService itemService = new ItemService();
    Equipment activeItem;
    List<CharacterInfo> party;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        parent.visibleProperty().bindBidirectional(isShowing);
        parent.managedProperty().bindBidirectional(isShowing);
        initalizeUI();
    }

    public void initalizeUI() {
        partyMemberNameLabels = List.of(leaderNameLabel, partyMember1NameLabel, partyMember2NameLabel, partyMember3NameLabel);
        partyMemberWeaponLabels = List.of(leaderWeaponLabel, partyMember1WeaponLabel, partyMember2WeaponLabel, partyMember3WeaponLabel);
        partyMemberBodyLabels = List.of(leaderBodyLabel, partyMember1BodyLabel, partyMember2BodyLabel, partyMember3BodyLabel);
        partyMemberRelic1Labels = List.of(leaderRelic1Label, partyMember1Relic1Label, partyMember2Relic1Label, partyMember3Relic1Label);
        partyMemberRelic2Labels = List.of(leaderRelic2Label, partyMember1Relic2Label, partyMember2Relic2Label, partyMember3Relic2Label);
        partyMemberRelic3Labels = List.of(leaderRelic3Label, partyMember1Relic3Label, partyMember2Relic3Label, partyMember3Relic3Label);
        party = InternalState.i().partyList().getParty();
    }
    public void show(BooleanProperty isMapShowing, BooleanProperty isBarShowing) {
        isShowing.set(true);
        this.isMapShowing = isMapShowing;
        this.isBarShowing = isBarShowing;
        renderUI();
        renderEquipmentLabels();
    }

    public void renderEquipmentLabels() {
        for(int i = 0; i < party.size(); i++) {
            CharacterInfo character = party.get(i);
            partyMemberNameLabels.get(i).setText(character.getName());
            Equipment weapon = character.getEquipment().getWeapon();
            String weaponName = weapon == null ? "Unarmed" : weapon.getName();
            partyMemberWeaponLabels.get(i).setText(weaponName);
            Equipment body = character.getEquipment().getBody();
            String bodyName = body == null ? "Smallclothes" : body.getName();
            partyMemberBodyLabels.get(i).setText(bodyName);
            if(character.getEquipment().getRelics().size() > 0) {
                Equipment relic1 = character.getEquipment().getRelics().get(0);
                String relic1Name = relic1 == null ? "None" : relic1.getName();
                partyMemberRelic1Labels.get(i).setText(relic1Name);
            } else {
                partyMemberRelic1Labels.get(i).setText("None");
            }
            if(character.getEquipment().getRelics().size() > 1 ) {
                Equipment relic2 = character.getEquipment().getRelics().get(1);
                String relic2Name = relic2 == null ? "None" : relic2.getName();
                partyMemberRelic2Labels.get(i).setText(relic2Name);
            } else {
                partyMemberRelic2Labels.get(i).setText("None");
            }
            if(character.getEquipment().getRelics().size() > 2) {
                Equipment relic3 = character.getEquipment().getRelics().get(2);
                String relic3Name = relic3 == null ? "None" : relic3.getName();
                partyMemberRelic3Labels.get(i).setText(relic3Name);
            } else {
                partyMemberRelic2Labels.get(i).setText("None");
            }

        }
    }

    public void renderUI() {
        ObservableList<Item> items = FXCollections.observableArrayList(new ArrayList<>());
        inventoryItemList.setItems(items);
        inventoryItemList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            activeItem = (Equipment)newValue;
            if(activeItem != null) {
                itemNameLabel.setText(activeItem.getName());
                itemDescriptionLabel.setText(activeItem.getDescription());
                String type = activeItem.getFlag().name().charAt(0) + activeItem.getFlag().name().toLowerCase(Locale.ROOT).substring(1);
                String typeLabel = "Type: "+type;
                Equipment e = party.get(currentSpot).getEquipment().getEquipment(currentFlag, currentRelicSlot);
                if(e != null) {
                    oldDescriptionLabel.setText(e.getDescription());
                } else {
                    oldDescriptionLabel.setText("");
                }
                newDescriptionLabel.setText(activeItem.getDescription());
                itemTypeLabel.setText(typeLabel);
                actionParent.setVisible(true);
                equipItemButton.setDisable(false);
            } else {
                cleanUI();
            }

        });
    }
    public void cleanUI() {
        itemNameLabel.setText("");
        itemDescriptionLabel.setText("");
        itemTypeLabel.setText("");
        actionParent.setVisible(false);
        equipItemButton.setDisable(false);
        inventoryItemList.setItems(FXCollections.emptyObservableList());
        oldDescriptionLabel.setText("");
        newDescriptionLabel.setText("");
        renderEquipmentLabels();
    }
    @FXML
    public void goBack() {
        isShowing.set(false);
        isMapShowing.set(true);
        isBarShowing.set(true);
    }

    public void showType(EquipmentFlag type) {
        List<Item> items = InternalState.i().inventory().getItems()
                .stream().filter(i -> i instanceof Equipment && ((Equipment) i).getFlag() == type)
                .collect(Collectors.toList());
        inventoryItemList.setItems(FXCollections.observableArrayList(items));
    }

    @FXML
    public void equipItem() {
        CharacterInfo c = party.get(currentSpot);
        Equipment old;
        if(currentFlag == EquipmentFlag.WEAPON) {
            old = c.getEquipment().equipWeapon(activeItem);
        } else if(currentFlag == EquipmentFlag.BODY) {
            old = c.getEquipment().equipBody(activeItem);
        } else {
            old  = c.getEquipment().equipRelic(activeItem, currentRelicSlot);
        }
        itemService.discardItem(activeItem.getId());
        InternalState.i().records().incrementItemsCollected();
        itemService.addItem(old);
        cleanUI();

    }

    private void setEquipmentUI(int slot, EquipmentFlag flag) {
        currentSpot = slot;
        currentFlag = flag;
        showType(flag);
    }

    @FXML
    private void changeLeaderWeaponEquipment() {
        setEquipmentUI(0, EquipmentFlag.WEAPON);
    }

    @FXML
    private void changeLeaderBodyEquipment() {
        setEquipmentUI(0, EquipmentFlag.BODY);
    }

    @FXML
    private void changeLeaderRelic1Equipment() {
        setEquipmentUI(0, EquipmentFlag.RELIC);
        currentRelicSlot = 0;
    }

    @FXML
    private void changeLeaderRelic2Equipment() {
        setEquipmentUI(0, EquipmentFlag.RELIC);
        currentRelicSlot = 1;
    }
    @FXML
    private void changeLeaderRelic3Equipment() {
        setEquipmentUI(0, EquipmentFlag.RELIC);
        currentRelicSlot = 2;
    }

    @FXML
    private void changePartyMember1WeaponEquipment() {
        setEquipmentUI(1, EquipmentFlag.WEAPON);
    }

    @FXML
    private void changePartyMember1BodyEquipment() {
        setEquipmentUI(1, EquipmentFlag.BODY);
    }

    @FXML
    private void changePartyMember1Relic1Equipment() {
        setEquipmentUI(1, EquipmentFlag.RELIC);
        currentRelicSlot = 0;
    }

    @FXML
    private void changePartyMember1Relic2Equipment() {
        setEquipmentUI(1, EquipmentFlag.RELIC);
        currentRelicSlot = 1;
    }
    @FXML
    private void changePartyMember1Relic3Equipment() {
        setEquipmentUI(1, EquipmentFlag.RELIC);
        currentRelicSlot = 2;
    }

    @FXML
    private void changePartyMember2WeaponEquipment() {
        setEquipmentUI(2, EquipmentFlag.WEAPON);
    }

    @FXML
    private void changePartyMember2BodyEquipment() {
        setEquipmentUI(2, EquipmentFlag.BODY);
    }

    @FXML
    private void changePartyMember2Relic1Equipment() {
        setEquipmentUI(2, EquipmentFlag.RELIC);
        currentRelicSlot = 0;
    }

    @FXML
    private void changePartyMember2Relic2Equipment() {
        setEquipmentUI(2, EquipmentFlag.RELIC);
        currentRelicSlot = 1;
    }
    @FXML
    private void changePartyMember2Relic3Equipment() {
        setEquipmentUI(2, EquipmentFlag.RELIC);
        currentRelicSlot = 2;
    }

    @FXML
    private void changePartyMember3WeaponEquipment() {
        setEquipmentUI(0, EquipmentFlag.WEAPON);
    }

    @FXML
    private void changePartyMember3BodyEquipment() {
        setEquipmentUI(0, EquipmentFlag.BODY);
    }

    @FXML
    private void changePartyMember3Relic1Equipment() {
        setEquipmentUI(0, EquipmentFlag.RELIC);
        currentRelicSlot = 0;
    }

    @FXML
    private void changePartyMember3Relic2Equipment() {
        setEquipmentUI(0, EquipmentFlag.RELIC);
        currentRelicSlot = 1;
    }
    @FXML
    private void changePartyMember3Relic3Equipment() {
        setEquipmentUI(0, EquipmentFlag.RELIC);
        currentRelicSlot = 2;
    }

}
