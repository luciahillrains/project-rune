package com.lucylhill.projectrune.controllers.battle;

import com.lucylhill.projectrune.controllers.battle.BattlePaneViewController;
import com.lucylhill.projectrune.objects.battle.actions.Action;
import com.lucylhill.projectrune.objects.battle.actions.Deck;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.characters.CharacterInfo;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.objects.misc.UITools;
import com.lucylhill.projectrune.services.BattleMessenger;
import com.lucylhill.projectrune.services.InternalState;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class HandBattleViewController implements Initializable {
    Deck actionDeck;

    @FXML
    Button action1Button;

    @FXML
    Button action2Button;

    @FXML
    Button action3Button;

    @FXML
    Button action4Button;

    @FXML
    Button action5Button;

    @FXML
    Button reserveActionButton;

    @FXML
    Label actionAlignmentLabel;

    @FXML
    Label actionClassName;

    @FXML
    Label actionDescription;

    @FXML
    Label actionAPLabel;

    @FXML
    Label apAmountLabel;

    @FXML
    Button playActionButton;

    @FXML
    Button reserveThisActionButton;

    @FXML
    Button runAwayButton;

    @FXML
    Button endTurnButton;

    Button[] actionButtons;
    List<Action> hand;
    List<Action> discard = new ArrayList<>();
    Action activeAction;
    Action reservedAction;
    double apAmount = 5;
    int actionButtonIndex = -1;
    boolean actionReserved = false;
    boolean turnOver = true;
    boolean disablePlayButton = true;
    boolean actionPlayed = false;
    boolean isRunningAway = false;
    BattlePaneViewController parent;
    List<String> charsToDisable = InternalState.i().partyList().getCharactersWhoAreDisabledOrAuto();
    List<Aspects> aspectsToDisable = InternalState.i().getListOfAspectsDisabled();
    List<String> actionsToDisable = InternalState.i().getListOfDisabledActions();
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        actionButtons = new Button[]{action1Button, action2Button, action3Button, action4Button, action5Button};
        apAmountLabel.setText("AP: "+ (int)apAmount);
    }

    public void setDeck(Deck d) {
        actionDeck = d;
        InternalState.i().setDeck(actionDeck);
        drawHand();
    }

    public void setParent(BattlePaneViewController parent) {
        this.parent = parent;
    }

    public void drawHand() {
        hand = actionDeck.getHand();
        apAmount = 5;

        if(aspectsToDisable.contains(Aspects.RANDOM)) {
            int idx = aspectsToDisable.indexOf(Aspects.RANDOM);
            aspectsToDisable.set(idx, Aspects.getRandomAspect());
        }
        for(int i = 0; i < actionButtons.length; i++) {
            List<Aspects> changedAspects = InternalState.i().getGlobalStatusEffects().getEffects().stream()
                    .filter(e -> e.getStat() == Stat.ACTION_ALIGNMENT)
                    .map(e -> Aspects.valueOf(e.getInfo())).collect(Collectors.toList());
            Aspects aspectToChangeTo = null;
            if(changedAspects.size() >= 1) {
                aspectToChangeTo = changedAspects.get(0);
            }
            Button button = actionButtons[i];
            button.setDisable(false);
            Action action = hand.get(i);
            if(aspectToChangeTo != null) {
                action.setAlignment(aspectToChangeTo);
            }
            setButtonText(button, action);
            int finalI = i;
            button.setDisable(charsToDisable.contains(action.getCharacterName()) ||
                    aspectsToDisable.contains(action.getAlignment()) ||
                    actionsToDisable.contains(action.getId()));

            button.setOnAction(event -> {
                activeAction = action;
                hydrateText(button, finalI, action);
            });
            reserveActionButton.setOnAction(event -> {
                if(reservedAction != null) {
                    activeAction = reservedAction;
                    hydrateText(reserveActionButton, -1, reservedAction);
                    reserveThisActionButton.setDisable(actionReserved);
                }
            });
        }
    }

    public void hydrateText(Button button, int pos, Action action) {
        EntityInfo ei = InternalState.i().partyList().getParty().get((InternalState.i().partyList().getPartyPosition(action.getCharacterName())));
        actionButtonIndex = pos;
        UITools.setAlignmentText(actionAlignmentLabel, action.getAlignment());
        String actionClassNameText = action.getCharacterName() + " : " + action.getName();
        if(actionClassName.getStyleClass().size() > 2) {
            actionClassName.getStyleClass().remove(2);
        }
        if(actionAPLabel.getStyleClass().size() > 2) {
            actionAPLabel.getStyleClass().remove(2);
        }
        boolean characterIsDisabled = ei.getStatusEffects().hasAilment("Disable") || ei.getHp() <= 0;
        if(characterIsDisabled) {
            actionClassNameText = action.getCharacterName() + "(disabled) : " + action.getName();
        }
        actionDescription.setText(action.getDescription());
        actionAPLabel.setText(action.getApCost()+"");
        actionAPLabel.getStyleClass().add(action.getAlignment().toString().toLowerCase(Locale.ROOT));
        resetButtonBorders();
        button.setTextFill(Color.BLACK);
        button.setStyle("-fx-border-color: cyan; -fx-border-size: 5px;");

        actionClassName.setText(actionClassNameText);
        actionClassName.getStyleClass().add(action.getAlignment().toString().toLowerCase(Locale.ROOT));
        playActionButton.setDisable((disablePlayButton
                || turnOver
                || (getApAmount() - action.getApCost() < 0))
                || parent.actionPlayIsBlocked()
                || characterIsDisabled);
        reserveThisActionButton.setDisable(actionReserved);
    }

    public void disableButtons() {
        for(int i = 0; i < actionButtons.length; i++) {
            Action action = hand.get(i);
            Button button = actionButtons[i];
            List<String> charsToDisable = InternalState.i().partyList().getCharactersWhoAreDisabledOrAuto();
            List<Aspects> aspectsToDisable = InternalState.i().getListOfAspectsDisabled();
            List<String> actionsToDisable = InternalState.i().getListOfDisabledActions();
            if(action == null) {
                button.setDisable(true);
            } else {
                button.setDisable(charsToDisable.contains(action.getCharacterName()) ||
                        aspectsToDisable.contains(action.getAlignment()) ||
                        actionsToDisable.contains(action.getId()));
            }
        }
    }



    public void resetButtonBorders() {
        for(int i = 0; i < actionButtons.length; i++) {
            Button button = actionButtons[i];
            if(hand.get(i) != null) {
                setButtonText(button, hand.get(i));
            }
        }
    }

    public void setButtonText(Button button, Action action) {
        if(action == null) {
            return;
        }
        button.setText(action.getApCost()+"");
        if(action.isMustPlay()) {
            button.setText(button.getText()+"!");
            return;
        }
        if(button.getStyleClass().size() > 2) {
            button.getStyleClass().remove(2);
        }
        button.getStyleClass().add("card-"+action.getAlignment().toString().toLowerCase(Locale.ROOT));
    }

    public void timeForPlayersTurn() {
        turnOver = false;
        disablePlayButton = false;
        endTurnButton.setDisable(false);
        runAwayButton.setDisable(false);
    }

    @FXML
    public void markTurnOver() {
        this.turnOver = true;
        actionDeck.discardHand(hand);
        hand = new ArrayList<>();
        drawHand();
        reserveActionButton.setDisable(false);
        apAmount = 5;
        apAmountLabel.setText("AP: "+ (int)apAmount);
        playActionButton.setDisable(true);
        reserveThisActionButton.setDisable(true);
        runAwayButton.setDisable(true);
        endTurnButton.setDisable(true);
        resolveHPAilments();
        parent.partyTurnOver();
    }

    @FXML
    public void playAction() {
        List<Action> mustPlayInHand = hand.stream().filter(a -> a != null && a.isMustPlay()).collect(Collectors.toList());
        if(mustPlayInHand.size() == 0 || (activeAction.isMustPlay())) {
            this.apAmount = this.apAmount - activeAction.getApCost();
            if(actionButtonIndex > -1) {
                this.hand.set(actionButtonIndex, null);
                actionButtons[actionButtonIndex].setDisable(true);
                actionButtons[actionButtonIndex].setText("");
            } else {
                this.reservedAction = null;
                reserveActionButton.setDisable(true);
                reserveActionButton.setText("");
            }
            actionPlayed = true;
            BattleMessenger.i().setActionName(activeAction.getName());
            BattleMessenger.i().setSourceName(activeAction.getCharacterName());
            updateAPCounter();
            reserveThisActionButton.setDisable(true);
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("You have a card that must be played.  They are marked with a *.");
            alert.show();
        }
        if(activeAction.getClassId().equals("Item")) {
            this.actionDeck.removeCard(activeAction.getId());
        }
        parent.partyPlayAction();
        playActionButton.setDisable(true);
        actionAPLabel.setText("");
        actionClassName.setText("");
        actionAlignmentLabel.setText("");
        actionDescription.setText("");
    }

    @FXML
    public void reserveAction() {
        reservedAction = activeAction;
        actionAPLabel.setText("");
        actionClassName.setText("");
        actionAlignmentLabel.setText("");
        actionDescription.setText("");
        actionReserved = true;
        reserveThisActionButton.setDisable(true);
        playActionButton.setDisable(true);
        actionButtons[actionButtonIndex].setDisable(true);
        actionButtons[actionButtonIndex].setText("");
        setButtonText(reserveActionButton, reservedAction);
    }

    @FXML
    public void attemptToRunAway() {
        parent.partyRunAway();
    }

    public void resolveHPAilments() {
        for(CharacterInfo c : InternalState.i().partyList().getParty()) {
            for(StatusEffect e : c.getStatusEffects().getEffects()) {
                if(e.getStat() == Stat.HP) {
                    c.setHp((int) (c.getHp() + e.getChange()));
                }
            }
        }
    }

    public void updateAPCounter() {
        apAmountLabel.setText("AP: "+ (int)this.apAmount);
    }

    public boolean isTurnOver() {
        return turnOver;
    }

    public void setTurnOver(boolean turnOver) {
        this.turnOver = turnOver;
    }

    public void setPlayButtonDisable(boolean disable) {
        disablePlayButton = disable;
    }

    public double getApAmount() {
        return this.apAmount;
    }

    public void incrementApAmount(double ap) {
        this.apAmount = apAmount + ap;
        this.apAmountLabel.setText("AP: "+ (int)this.apAmount);
    }

    public Action getActiveAction() {
        return activeAction;
    }

    public void setActiveAction(Action activeAction) {
        this.activeAction = activeAction;
    }

    public boolean isActionPlayed() {
        return actionPlayed;
    }

    public void setActionPlayed(boolean actionPlayed) {
        this.actionPlayed = actionPlayed;
    }

    public boolean isRunningAway() {
        return isRunningAway;
    }

    public void setRunningAway(boolean runningAway) {
        isRunningAway = runningAway;
    }

    public void drawCard() {
        for(int i = 0; i < this.hand.size(); i++) {
            Action a = this.hand.get(i);
            if(a == null) {
                Action action = actionDeck.drawCard();
                Button button = actionButtons[i];
                hand.set(i, actionDeck.drawCard());
                actionButtons[i].setDisable(false);
                setButtonText(actionButtons[i], action);
                button.setDisable(charsToDisable.contains(action.getCharacterName()) ||
                        aspectsToDisable.contains(action.getAlignment()) ||
                        actionsToDisable.contains(action.getId()));

                int finalI = i;
                button.setOnAction(event -> {
                    activeAction = action;
                    hydrateText(button, finalI, action);
                });
                break;
            }
        }
    }
}
