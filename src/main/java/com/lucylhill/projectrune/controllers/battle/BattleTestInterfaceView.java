package com.lucylhill.projectrune.controllers.battle;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.controllers.dungeon.PartyBarViewController;
import com.lucylhill.projectrune.objects.dungeons.DungeonEvent;
import com.lucylhill.projectrune.objects.dungeons.GeneratedDungeon;
import com.lucylhill.projectrune.services.BattleTestHarnessService;
import com.lucylhill.projectrune.services.SceneSwitcher;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

public class BattleTestInterfaceView implements Initializable {

    @FXML
    BattlePaneViewController battlePaneController;

    @FXML
    PartyBarViewController partyBarController;

    @FXML
    Button backToDebug;

    BattleTestHarnessService service = new BattleTestHarnessService();
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        service.initialize();
        DungeonEvent event = new DungeonEvent();
        GeneratedDungeon dungeon = new GeneratedDungeon(RuneConfiguration.i().dungeonList().getDungeons().get(0));
        dungeon.setBattleBackgroundType("cavern");
        BooleanProperty map = new SimpleBooleanProperty(false);
        event.setEnemies(Arrays.asList("debug", "debug", "debug"));
        battlePaneController.setEvent(event, dungeon, map);
        partyBarController.renderUI();
        partyBarController.setBattlePaneViewController(battlePaneController);
    }

    @FXML
    public void goBackToOptions() throws IOException {
        SceneSwitcher.switchScreens(backToDebug, SceneSwitcher.CONFIG_SCREEN);
    }
}
