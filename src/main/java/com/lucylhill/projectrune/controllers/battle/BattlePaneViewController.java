package com.lucylhill.projectrune.controllers.battle;

import com.lucylhill.projectrune.objects.battle.BattleEvent;
import com.lucylhill.projectrune.objects.battle.BattleEventFlag;
import com.lucylhill.projectrune.objects.battle.BattleState;
import com.lucylhill.projectrune.objects.battle.actions.*;
import com.lucylhill.projectrune.objects.battle.enemies.BattleEnemy;
import com.lucylhill.projectrune.objects.battle.enemies.Enemy;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.dungeons.DungeonEvent;
import com.lucylhill.projectrune.objects.dungeons.GeneratedDungeon;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.services.*;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class BattlePaneViewController implements Initializable {
    @FXML
    VBox parent;

    @FXML
    HandBattleViewController handPaneController;

    @FXML
    Label battleLogLabel;

    @FXML
    ImageView enemy1Image;

    @FXML
    ImageView enemy2Image;

    @FXML
    ImageView enemy3Image;

    @FXML
    ImageView enemy4Image;

    @FXML
    Region enemy1Region;

    @FXML
    Region enemy2Region;

    @FXML
    Region enemy3Region;

    @FXML
    Region enemy4Region;

    @FXML
    Label enemy1Text;

    @FXML
    Label enemy2Text;

    @FXML
    Label enemy3Text;

    @FXML
    Label enemy4Text;

    @FXML
    Label party1Text;

    @FXML
    Label party2Text;

    @FXML
    Label party3Text;

    @FXML
    Label party4Text;

    @FXML
    ProgressBar enemy1Health;

    @FXML
    ProgressBar enemy2Health;

    @FXML
    ProgressBar enemy3Health;

    @FXML
    ProgressBar enemy4Health;

    @FXML
    StackPane battlePane;

    @FXML
    Label enemy1Ailments;

    @FXML
    Label enemy2Ailments;

    @FXML
    Label enemy3Ailments;

    @FXML
    Label enemy4Ailments;

    private BooleanProperty isVisible = new SimpleBooleanProperty(false);
    private BooleanProperty isMapVisible = new SimpleBooleanProperty(true);
    DungeonEvent dungeonEvent;
    GeneratedDungeon dungeon;
    List<ImageView> enemyImageViews = new ArrayList<>();
    List<Region> enemyRegionViews = new ArrayList<>();
    List<Label> enemyTextLabels = new ArrayList<>();
    List<Label> enemyAilmentLabels = new ArrayList<>();
    List<Label> partyTextLabels = new ArrayList<>();
    List<ProgressBar> enemyHealthBars = new ArrayList<>();
    PartyList partyList;
    boolean partyTargetingMode = true;
    int[] imageOrder = new int[]{1, 2, 0, 3};
    List<FadeTransition> enemyFadeOuts = new ArrayList<>();
    List<FadeTransition> partyFadeOuts = new ArrayList<>();
    List<FadeTransition> enemyImageFadeOuts = new ArrayList<>();
    List<FadeTransition> enemyHitFadeOuts = new ArrayList<>();
    boolean endBattle = false;
    Random random = new Random();
    StringProperty checkpointProperty = new SimpleStringProperty("");
    int logNumber = 1;
    DebugLogger logger = new DebugLogger(this.getClass());
    BattleService service = new BattleService();
    Timer battleTimer = new Timer(true);
    TimerTask task;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        parent.visibleProperty().bindBidirectional(isVisible);
        parent.managedProperty().bindBidirectional(isVisible);
        partyList = InternalState.i().partyList();
        enemyImageViews = List.of(enemy1Image, enemy2Image, enemy3Image, enemy4Image);
        enemyRegionViews = List.of(enemy1Region, enemy2Region, enemy3Region, enemy4Region);
        enemyTextLabels = List.of(enemy1Text, enemy2Text, enemy3Text, enemy4Text);
        partyTextLabels = List.of(party1Text, party2Text, party3Text, party4Text);
        enemyHealthBars = List.of(enemy1Health, enemy2Health, enemy3Health, enemy4Health);
        enemyAilmentLabels = List.of(enemy1Ailments, enemy2Ailments, enemy3Ailments, enemy4Ailments);
        enemyFadeOuts = List.of(new FadeTransition(Duration.millis(3000)),
                                new FadeTransition(Duration.millis(3000)),
                                new FadeTransition(Duration.millis(3000)),
                                new FadeTransition(Duration.millis(3000)));
        partyFadeOuts = List.of(new FadeTransition(Duration.millis(3000)),
                    new FadeTransition(Duration.millis(3000)),
                    new FadeTransition(Duration.millis(3000)),
                    new FadeTransition(Duration.millis(3000)));
        enemyImageFadeOuts = List.of(new FadeTransition(Duration.millis(3000)),
                new FadeTransition(Duration.millis(3000)),
                new FadeTransition(Duration.millis(3000)),
                new FadeTransition(Duration.millis(3000)));
        enemyHitFadeOuts = List.of(new FadeTransition(Duration.millis(300)),
                new FadeTransition(Duration.millis(300)),
                new FadeTransition(Duration.millis(300)),
                new FadeTransition(Duration.millis(300)));

        for(int i = 0; i < enemyFadeOuts.size(); i++) {
            initializeFadeOut(enemyFadeOuts.get(i), true, i, false, false);
        }
        for(int i = 0; i < partyFadeOuts.size(); i++) {
            initializeFadeOut(partyFadeOuts.get(i), false, i, false, false);
        }
        for(int i = 0; i < enemyImageFadeOuts.size(); i++ ) {
            initializeFadeOut(enemyImageFadeOuts.get(i), true, i, false, false);
        }
        for(int i = 0; i < enemyHitFadeOuts.size(); i++) {
            initializeHitFadeOut(enemyHitFadeOuts.get(i), i);
        }

        service.initialize(handPaneController);

        handPaneController.setParent(this);
    }



    public void initializeFadeOut(FadeTransition fadeOut, boolean isEnemyFadeOut, int index, boolean isImage, boolean isParentBox) {
        fadeOut.setFromValue(1.0);
        fadeOut.setToValue(0.0);
        fadeOut.setCycleCount(1);
        fadeOut.setAutoReverse(false);
        Node labelToFade = null;
        if(isEnemyFadeOut && !isImage && !isParentBox) {
            labelToFade = enemyTextLabels.get(imageOrder[index]);
        } else if(!isImage && !isParentBox){
            labelToFade = partyTextLabels.get(index);
        }
        if(isImage && !isParentBox) {
            labelToFade = enemyImageViews.get(imageOrder[index]);
        }
        fadeOut.setNode(labelToFade);
    }

    public void initializeHitFadeOut(FadeTransition fadeOut, int index) {
        fadeOut.setFromValue(1.0);
        fadeOut.setToValue(0.0);
        fadeOut.setCycleCount(4);
        fadeOut.setAutoReverse(true);
        Node enemyImage = enemyImageViews.get(imageOrder[index]);
        fadeOut.setNode(enemyImage);
    }

    public void setEvent(DungeonEvent event, GeneratedDungeon dungeon, BooleanProperty isMapVisible) {
        BattleState.i().getPreviousActions().clear();
        this.isMapVisible = isMapVisible;
        this.dungeonEvent = event;
        this.dungeon = dungeon;
        this.isVisible.set(true);
        service.setEnemies(dungeonEvent.getEnemies());
        boolean isHeavy = service.getBattleEnemies().stream().anyMatch(e -> e.getEnemy().isHeavy());
        boolean isFinal = service.getBattleEnemies().stream().anyMatch(e -> e.getEnemy().isFinalBoss());
        service.onSetEvent(event);
        endBattle = false;
       // handPaneController.drawHand();
        hideEnemyEffects();
        if(event.getEnemies().contains("warmachina")) {
            InternalState.i().records().incrementWarmachinaSeen();
        }
        renderUI();
        if(isFinal) {
            RuneRadio.i().play(RuneRadio.FINAL_BATTLE);
        } else if (isHeavy) {
            RuneRadio.i().play(RuneRadio.BOSS);
        } else {
            RuneRadio.i().play(RuneRadio.BATTLE);
        }
    }

    public void renderUI() {
        if(dungeon == null) {
            battlePane.setBackground(BackgroundImageFactory.createSceneBackground("/art/backgrounds/battle/cavern1.png"));
        } else {
            battlePane.setBackground(BackgroundImageFactory.createSceneBackground("/art/backgrounds/battle/"+dungeon.getBattleBackgroundType()+random.nextInt(1,4)+".png"));
        }
        List<BattleEnemy> battleEnemies = service.getBattleEnemies();
        for(int i = 0; i < battleEnemies.size(); i++) {
            Enemy enemy = battleEnemies.get(i).getEnemy();
            enemyImageViews.get(imageOrder[i]).setImage(new Image(Objects.requireNonNull(BattlePaneViewController.class.getResourceAsStream("/art/enemies/" + enemy.getPicture()))));
            enemyImageViews.get(imageOrder[i]).setVisible(true);
            int finalI = i;
            enemyTextLabels.get(imageOrder[i]).setContentDisplay(ContentDisplay.CENTER);
            enemyHealthBars.get(imageOrder[i]).setProgress(1.0);
            enemyHealthBars.get(imageOrder[i]).setVisible(true);
            enemyRegionViews.get(imageOrder[i]).setVisible(true);
            enemyRegionViews.get(imageOrder[i]).addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
                logger.log("clicky");
                logger.log("clicky "+finalI);
                if(InternalState.i().targetMode() && battleEnemies.size() > finalI) {
                    BattleEnemy be = battleEnemies.get(finalI);
                    BattleState.i().setTargetId(finalI);
                    BattleMessenger.i().setMessage(be.getEnemy().getName(), finalI, true);
                    for(int j = 0; j < enemyRegionViews.size(); j++) {
                        enemyRegionViews.get(imageOrder[j]).setStyle("-fx-border: none");
                    }
                    InternalState.i().setTargetMode(false);
                    InternalState.i().setPartyTargeting(false);
                    executeAction();
                    runEvent();
                }
                event.consume();
            });
        }

        service.onUIRender();
        setupTimer();
        runEvent();
    }

    public void setupTimer() {
        task = new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    if(service.getBattleEventQueue().size() > 0) {
                        runEvent();
                    }
                });

            }
        };
        battleTimer.scheduleAtFixedRate(task, 2000, 3000);
    }

    public void runEvent() {
        Optional<BattleEvent> battleEvent = service.getQueuePoll();
        if(battleEvent.isPresent()) {
            BattleEvent event = battleEvent.get();
            writeLog(event.getText());
            if(event.getResult() != null) {
                logger.log("Evaluating the result");
                displayResult(event.getResult());
                if(event.getResult().isDrawCard()) {
                    handPaneController.drawCard();
                }
            }
            if(event.getFlag() == BattleEventFlag.END_BATTLE) {
                handPaneController.setTurnOver(true);
                service.setBattleEnemies(new ArrayList<>());
                isVisible.set(false);
                isMapVisible.set(true);
                service.clearQueue();
                if(dungeon != null) {
                    RuneRadio.i().play("/music/"+dungeon.getMusic()+".mp3");
                }
                task.cancel();
            }
            if(event.getFlag() == BattleEventFlag.END_BATTLE_LOST) {
                service.setBattleEnemies(new ArrayList<>());
                partyLost();
                task.cancel();
            }
        }
    }

    public void writeLog(String logMessage) {
        battleLogLabel.setText(logNumber+": "+logMessage);
        logNumber++;
    }



    public void targetingMode() {
        InternalState.i().setTargetMode(true);
        InternalState.i().setPartyTargeting(partyTargetingMode);
        if(!partyTargetingMode) {
            for(int i = 0; i < enemyRegionViews.size(); i++) {
                enemyRegionViews.get(imageOrder[i]).setStyle("-fx-border: none");
                if(enemyImageViews.get(imageOrder[i]).getImage() != null) {
                    enemyRegionViews.get(imageOrder[i]).setStyle("-fx-border-color: cyan");
                }
            }
        }
    }






    public void handleAction(Action action) {

        //ad source based on action target type
        //get target based on battle state target idmessage target id
        //put action results on a queue
        boolean needToRunEvent = service.handleAction(action);
        if(needToRunEvent) {
            runEvent();
        }
    }

    public void displayResult(ActionResult result) {
            List<BattleEnemy> battleEnemies = service.getBattleEnemies();
            writeLog(result.getText());
            Label labelToChange;
            int targetPosition = result.getTargetPosition();
            if(result.isEnemy()) {
                labelToChange = enemyTextLabels.get(imageOrder[targetPosition]);
            } else {
                labelToChange = partyTextLabels.get(targetPosition);
            }
            if(result.isActionMissed()) {
                if(result.isEnemy() && targetPosition < battleEnemies.size()) {
                    labelToChange.setStyle("-fx-text-fill: white;-fx-font-size:18px");
                    labelToChange.setText("Miss");
                    enemyFadeOuts.get(targetPosition).playFromStart();
                } else if(!result.isEnemy()) {
                    labelToChange.setStyle("-fx-text-fill: white;-fx-font-size:18px");
                    labelToChange.setText("Miss");
                    partyFadeOuts.get(targetPosition).playFromStart();
                }
            }
            else if(!result.isActionMissed() && (result.getHpChange()  <= -1.0 || result.getHpChange() >= 1.0)) {
                if(result.getHpChange() > 0.0) {
                    labelToChange.setStyle("-fx-text-fill: green;-fx-font-size:48px");
                } else {
                    if(result.isEnemy()) {
                        labelToChange.setStyle("-fx-text-fill: white;-fx-font-size:48px");
                    } else {
                        labelToChange.setStyle("-fx-text-fill: black;-fx-font-size:48px");
                    }
                }
                labelToChange.setText((int)result.getHpChange() + "");
                if(result.isEnemy()) {
                    enemyFadeOuts.get(targetPosition).playFromStart();
                    enemyHitFadeOuts.get(targetPosition).playFromStart();
                    double healthProgress = battleEnemies.get(targetPosition).getHp()/(double)battleEnemies.get(targetPosition).getEnemy().getMaxHp();
                    enemyHealthBars.get(imageOrder[targetPosition]).setProgress(healthProgress);
                    logger.log("health progress: "+ healthProgress);
                } else {
                    partyFadeOuts.get(targetPosition).playFromStart();
                }
            }
            if(result.isEnemy() && battleEnemies.get(targetPosition).getHp() <= 0) {
                enemyImageFadeOuts.get(targetPosition).playFromStart();
                enemyImageViews.get(imageOrder[targetPosition]).setVisible(false);
                enemyRegionViews.get(imageOrder[targetPosition]).setVisible(false);
                enemyHealthBars.get(imageOrder[targetPosition]).setVisible(false);
                enemyAilmentLabels.get(imageOrder[targetPosition]).setText("");
            }

            for(int j = 0; j < enemyRegionViews.size(); j++) {
                enemyRegionViews.get(imageOrder[j]).setStyle("-fx-border: none");
            }
            handPaneController.disableButtons();
            service.checkIfPartyIsDead();
            service.checkIfEnemiesAreDead();
            showEnemyEffects();
    }

    public StringProperty getCheckpointProperty() {
        return checkpointProperty;
    }

    public void setCheckpointProperty(String checkpointProperty) {
        this.checkpointProperty.set(checkpointProperty);
    }

    public void partyRunAway() {
        boolean success = service.onPartyRunAway();
        if(success) {
            runEvent();
        }
    }

    public void partyPlayAction() {
        if(handPaneController.getActiveAction().getTargetType() == ActionTargetType.SINGLE ||
                handPaneController.getActiveAction().getTargetType() == ActionTargetType.PARTY_DEAD ||
                handPaneController.getActiveAction().getTargetType() == ActionTargetType.PARTY_SINGLE) {
            logger.log("Targeting!");
            partyTargetingMode = handPaneController.getActiveAction().getTargetType() == ActionTargetType.PARTY_SINGLE || handPaneController.getActiveAction().getTargetType() == ActionTargetType.PARTY_DEAD;
            InternalState.i().setDeadTargetting(handPaneController.getActiveAction().getTargetType() == ActionTargetType.PARTY_DEAD);
            targetingMode();
        } else {
            logger.log("Action!");
            executeAction();
        }
    }

    public void partyTurnOver() {
        service.onPartyTurnOver();
        runEvent();
    }

    public void partyLost() {
        try {
            if (InternalState.i().configuration().getDifficultyConfig().isPermadeath()) {
                SceneSwitcher.switchScreens(parent, SceneSwitcher.GAME_OVER_SCREEN);
            } else {
                if (InternalState.i().configuration().getDifficultyConfig().isResumeFromCheckpoint()) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    if (InternalState.i().checkpoint() != null && !InternalState.i().checkpoint().isDirty()) {
                        alert.setContentText("Due to your difficulty settings, you will be sent to your last checkpoint.");
                        alert.show();
                        isVisible.set(false);
                        isMapVisible.set(true);
                        checkpointProperty.set("reset");
                    }
                } else {
                    SceneSwitcher.switchScreens(parent, SceneSwitcher.HUB_WORLD_SCREEN);
                }
            }
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("E003: error happening switching scenes (gameover)");
            alert.show();
            logger.log("Something wrong happened during going to gameover...");
        }
    }



    public void executeAction() {
        handleAction(handPaneController.getActiveAction());
    }

    public boolean actionPlayIsBlocked() {
        return service.playIsBlocked();
    }


    public boolean isVisible() {
        return isVisible.get();
    }

    public void hideEnemyEffects() {
        for(int i = 0; i < service.getBattleEnemies().size(); i++) {
            enemyAilmentLabels.get(imageOrder[i]).setText("");
        }
    }

    public void showEnemyEffects() {
            for(int i = 0; i < service.getBattleEnemies().size(); i++) {
                StringBuilder str = new StringBuilder();
                EntityInfo ch = service.getBattleEnemies().get(i);
                for(StatusEffect se : ch.getStatusEffects().getEffects()) {
                    if(se.getIsNamedAilment()) {
                        str.append(se.getIcon());
                    } else {
                        if(se.getStat() == Stat.ATTACK) {
                            str.append("A");
                        }
                        if(se.getStat() == Stat.DEFENSE) {
                            str.append("D");
                        }
                        if(se.getStat() == Stat.MAX_HP) {
                            str.append("HP");
                        }
                        if(se.getStat() == Stat.AGILITY) {
                            str.append("Ag");
                        }
                        if(se.getStat() == Stat.INTELLIGENCE) {
                            str.append("I");
                        }
                        if(se.getStat() == Stat.HP) {
                            str.append("♥");
                        }
                        if(se.getStat() == Stat.BASE_DAMAGE) {
                            str.append("#");
                        }
                        if(se.getStat() == Stat.HIT_RATE) {
                            str.append("%");
                        }
                        if(se.getChange() > 0) {
                            str.append("↑");
                        } else {
                            str.append("↓");
                        }
                    }
                }
                if(!str.toString().isBlank()) {
                    enemyAilmentLabels.get(imageOrder[i]).setText(str.toString());
                }
            }
        }

        public BooleanProperty getVisible() {
            return isVisible;
        }


}
