package com.lucylhill.projectrune.controllers.misc;

import com.lucylhill.projectrune.controllers.battle.BattlePaneViewController;
import com.lucylhill.projectrune.controllers.dungeon.*;
import com.lucylhill.projectrune.objects.dungeons.*;
import com.lucylhill.projectrune.objects.quests.InterstitialEvent;
import com.lucylhill.projectrune.services.*;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;

import java.net.URL;
import java.util.ResourceBundle;

public class InterstitialViewController implements Initializable {

    @FXML
    PartyBarViewController partyBarViewController;

    @FXML
    DungeonEventPaneController dungeonEventPaneController;

    @FXML
    BattlePaneViewController battlePaneViewController;

    @FXML
    Label dungeonNameLabel;

    @FXML
    StackPane parent;

    BooleanProperty isMapShowing = new SimpleBooleanProperty(true);
    InterstitialEvent mainEvent;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        dungeonEventPaneController.setIsVisible(true);
        partyBarViewController.setBattlePaneViewController(battlePaneViewController);
        setEvent(InternalState.i().getCurrentInterstitial());
    }

    public void setEvent(InterstitialEvent event) {
        mainEvent = event;
        if(event == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Interstitial not found.");
            alert.show();
            return;
        }
        dungeonNameLabel.setText(event.getName());
        dungeonEventPaneController.setIsVisible(true);
        dungeonEventPaneController.setInterstitialViewController(this);
        if(event.getBackground() != null) {
            parent.setBackground(BackgroundImageFactory.createSceneBackground("/art/backgrounds/"+event.getBackground()));
        } else {
            parent.setBackground(BackgroundImageFactory.createSceneBackground("/art/backgrounds/trainingGrounds-1.png"));
        }
        dungeonEventPaneController.setEvent(event.getEvent());
        RuneRadio.i().play("/music/"+event.getMusic()+".mp3");
    }

    public void startBattle(DungeonEvent event) {
        GeneratedDungeon dungeon = new GeneratedDungeon();
        dungeon.setMusic(mainEvent.getMusic());
        dungeon.setBattleBackgroundType("cavern");
        battlePaneViewController.setEvent(event, dungeon, isMapShowing);
        dungeonEventPaneController.getVisible().set(false);
    }





}
