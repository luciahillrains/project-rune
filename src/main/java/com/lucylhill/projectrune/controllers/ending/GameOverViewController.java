package com.lucylhill.projectrune.controllers.ending;

import com.lucylhill.projectrune.services.BackgroundImageFactory;
import com.lucylhill.projectrune.services.InternalState;
import com.lucylhill.projectrune.services.RuneRadio;
import com.lucylhill.projectrune.services.SceneSwitcher;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class GameOverViewController implements Initializable {
    @FXML
    Button backToCheckpointButton;

    @FXML
    StackPane parent;

    @FXML
    VBox paneParent;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if(!InternalState.i().configuration().getDifficultyConfig().isResumeFromCheckpoint()) {
            backToCheckpointButton.setVisible(false);
        }
        parent.setBackground(BackgroundImageFactory.createSceneBackground("/art/backgrounds/red-background.png"));
        RuneRadio.i().play(RuneRadio.OASIS);
    }

    @FXML
    public void goToTitle() throws IOException {
        SceneSwitcher.switchScreens(backToCheckpointButton, SceneSwitcher.START_SCREEN);
    }

    @FXML
    public void quitGame() {
        Stage stage = (Stage) backToCheckpointButton.getScene().getWindow();
        stage.close();
        Platform.exit();
        System.exit(0);
    }
}
