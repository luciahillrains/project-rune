package com.lucylhill.projectrune.controllers.ending;

import com.lucylhill.projectrune.services.BackgroundImageFactory;
import com.lucylhill.projectrune.services.RuneRadio;
import com.lucylhill.projectrune.services.SceneSwitcher;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ComingSoonScreenController implements Initializable {

    @FXML
    Button titleButton;

    @FXML
    StackPane parent;

    @FXML
    VBox paneParent;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        parent.setBackground(BackgroundImageFactory.createSceneBackground("/art/backgrounds/red-background.png"));
        RuneRadio.i().play(RuneRadio.OASIS);
    }

    @FXML
    public void goBacktoTitleScreen() throws IOException {
        SceneSwitcher.switchScreens(titleButton, SceneSwitcher.START_SCREEN);
    }
}
