package com.lucylhill.projectrune.controllers.start;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.achievements.Achievement;
import com.lucylhill.projectrune.services.BackgroundImageFactory;
import com.lucylhill.projectrune.services.InternalState;
import com.lucylhill.projectrune.services.SceneSwitcher;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

public class AchievementViewController implements Initializable {
    @FXML
    StackPane parent;

    @FXML
    VBox frameParent;

    @FXML
    ListView<Achievement> achievementList;

    @FXML
    Label achievementNameLabel;

    @FXML
    Label achievementDescriptionLabel;

    @FXML
    Label achievementCompletionLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        parent.setBackground(BackgroundImageFactory.createSceneBackground("/art/backgrounds/bedroom-2.png"));
//        frameParent.setBackground(BackgroundImageFactory.createFrameBackground());
        renderUI();
    }

    public void renderUI() {
        buildListView();
    }

    public void buildListView() {
        ObservableList<Achievement> completedAchievements = FXCollections.observableArrayList(InternalState.i().achievements().getAchievements());
        for(Achievement a : RuneConfiguration.i().achievements().getAchievements()) {
            if(!completedAchievements.contains(a)) {
                completedAchievements.add(a);
            }
        }
        achievementList.setItems(completedAchievements);
        achievementList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            achievementNameLabel.setText(newValue.toString());
            achievementDescriptionLabel.setText(newValue.getDescription());
            achievementCompletionLabel.getStyleClass().clear();
            if(newValue.toString().contains("!!!")) {
                achievementCompletionLabel.getStyleClass().add("completed");
                achievementCompletionLabel.setText("Completed!!!");
            } else {
                achievementCompletionLabel.getStyleClass().add("incomplete");
                achievementCompletionLabel.setText("Incomplete");
            }
        });
    }

    @FXML
    public void backToStart() throws IOException {
        SceneSwitcher.switchScreens(achievementCompletionLabel, SceneSwitcher.START_SCREEN);
    }

}
