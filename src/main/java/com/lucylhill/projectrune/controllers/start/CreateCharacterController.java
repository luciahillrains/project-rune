package com.lucylhill.projectrune.controllers.start;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.characters.CharacterType;
import com.lucylhill.projectrune.objects.classes.ClassInfo;
import com.lucylhill.projectrune.objects.classes.ClassList;
import com.lucylhill.projectrune.objects.quests.InterstitialEvent;
import com.lucylhill.projectrune.objects.passives.PassiveAction;
import com.lucylhill.projectrune.objects.passives.PassiveActionList;
import com.lucylhill.projectrune.objects.passives.PassiveData;
import com.lucylhill.projectrune.objects.traits.Trait;
import com.lucylhill.projectrune.objects.traits.TraitList;
import com.lucylhill.projectrune.objects.traits.TraitResolver;
import com.lucylhill.projectrune.services.*;
import com.lucylhill.projectrune.objects.characters.CharacterInfo;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.*;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class CreateCharacterController implements Initializable {


    @FXML
    TextField nameField;

    @FXML
    Button backToStartButton;

    @FXML
    ChoiceBox<String> classPicker;

    @FXML
    Label classInfoLabel;

    @FXML
    ChoiceBox<String> traitPicker;

    @FXML
    Label traitInfoLabel;

    @FXML
    ChoiceBox<String> genderPicker;

    @FXML
    ChoiceBox<String> racePicker;

    @FXML
    StackPane parent;

    @FXML
    VBox frameParent;

    int randomTraitNum;

    List<String> races;

    List<String> genders;

    ClassList classList;
    PartyList partyList;
    TraitList traitList;

    DebugLogger logger = new DebugLogger(this.getClass());


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        classList = RuneConfiguration.i().classList();
        traitList = RuneConfiguration.i().traitList();
        randomTraitNum = RuneConfiguration.i().systemProperties().getRandomNumOfTraits();
        races = RuneConfiguration.i().systemProperties().getRaces();
        genders = RuneConfiguration.i().systemProperties().getGenders();
        partyList = InternalState.i().partyList();
        partyList.setParty(new ArrayList<>());
        List<ClassInfo> enabledClasses = this.classList.getEnabledClasses();
        List<Trait> randomTraits = this.traitList.getRandomTraits(randomTraitNum);
        enabledClasses.forEach(cls -> classPicker.getItems().add(cls.getName()));
        randomTraits.forEach(t -> traitPicker.getItems().add(t.getName()));
        genders.forEach(g -> genderPicker.getItems().add(g));
        races.forEach(r -> racePicker.getItems().add(r));
        RuneRadio.i().play(RuneRadio.TITLE);
        parent.setBackground(BackgroundImageFactory.createSceneBackground("/art/backgrounds/bedroom-2.png"));
    }

    @FXML
    public void goBackToStart() throws IOException {
        SceneSwitcher.switchScreens(backToStartButton, SceneSwitcher.START_SCREEN);
    }

    @FXML
    public void populateClassDescriptionLabel() {
        String className = classPicker.getValue();
        Optional<ClassInfo> info = this.classList.getClassByName(className);
        if(info.isEmpty()) {
            classInfoLabel.setText("ERR!  Class not found.");
        } else {
            classInfoLabel.setText(info.get().getUIDescription());
        }
    }

    @FXML
    public void populateTraitDescriptionLabel() {
        String traitName = traitPicker.getValue();
        Optional<Trait> t = this.traitList.getTraitByName(traitName);
        if(t.isEmpty()) {
            traitInfoLabel.setText("ERR!  Trait not found.");
        } else {
            traitInfoLabel.setText(t.get().getDescription());
        }
    }

    @FXML
    public void createCharacterAndMoveToHub() throws IOException{
        TraitResolver traitResolver = new TraitResolver();
        Alert alert = new Alert(Alert.AlertType.NONE);
        if(fieldsAreEmpty()) {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Please fill in all fields.");
            alert.show();
            return;
        }
        if(nameIsForbidden()) {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Please choose another name.");
            alert.show();
            return;
        }

        alert.setAlertType(Alert.AlertType.CONFIRMATION);
        alert.setContentText("Are you sure you want to create this character?");
        Optional<ButtonType> res = alert.showAndWait();
        if(res.isPresent() && res.get() == ButtonType.OK) {
            Optional<Trait> trait = traitList.getTraitByName(traitPicker.getValue());
            Optional<ClassInfo> clazz = classList.getClassByName(classPicker.getValue());
            CharacterInfo leader = new CharacterInfo();
            leader.setCharacterName(nameField.getText())
                    .setType(CharacterType.LEADER)
                    .setGender(genderPicker.getValue())
                    .setRace(racePicker.getValue())
                    .setTrait(trait.orElseGet(() -> traitList.getTraits().get(0)))
                    .setClassInfo(clazz.orElseGet(() -> classList.getClasses().get(0)));
            traitResolver.resolveTrait(leader);
            if(leader.getAlignment() == null) {
                leader.setAlignment(leader.getClassInfo().getAspect());
                leader.setClassAlignment(leader.getClassInfo().getClassAspect());
            }
            List<PassiveData> passives = clazz.orElseGet(() -> classList.getClasses().get(0)).getPassives();
            List<PassiveAction> passiveList = new ArrayList<>();
            for(PassiveData p : passives) {
                passiveList.add(RuneConfiguration.i().passiveList().getPassiveWithId(p.getId()));
            }
            leader.setPassives(new PassiveActionList());
            leader.getPassives().setPassives(passiveList);
            partyList.getParty().add(leader);
            logger.log("Created leader"+leader);
            Optional<InterstitialEvent> event = RuneConfiguration.i().interstitials().getEventById("story");
            InternalState.i().setCurrentInterstitial(event.orElse(null));
            SceneSwitcher.switchScreens(backToStartButton, SceneSwitcher.INTERSTITIAL_SCREEN);
        }

    }

    public boolean fieldsAreEmpty() {
        return nameField.getText().isBlank() || traitPicker.getValue() == null || classPicker.getValue() == null
                || racePicker.getValue() == null || genderPicker.getValue() == null;
    }

    public boolean nameIsForbidden() {
        return RuneConfiguration.i().partyMembers().getPartyMembers().stream().anyMatch(c -> c.getName().equals(nameField.getText()));
    }
}
