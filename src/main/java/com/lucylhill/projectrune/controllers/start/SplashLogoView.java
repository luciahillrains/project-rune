package com.lucylhill.projectrune.controllers.start;

import com.lucylhill.projectrune.services.BackgroundImageFactory;
import com.lucylhill.projectrune.services.SceneSwitcher;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

public class SplashLogoView implements Initializable {
    @FXML
    AnchorPane parent;

    @FXML
    Pane splashLogoImage;

    Timer timer;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        timer = new Timer(true);
        splashLogoImage.setBackground(BackgroundImageFactory.createImageBackground("/art/splash-logo.png"));
        onInit();
    }

    public void onInit() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    TimerTask task = this;
                    try {
                        SceneSwitcher.switchScreens(splashLogoImage, SceneSwitcher.START_SCREEN);
                        this.cancel();
                    } catch (IOException e) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setContentText("E003: error switching scenes (start)");
                        alert.show();
                        e.printStackTrace();
                    }
                });
            }
        }, 1000);
    }
}
