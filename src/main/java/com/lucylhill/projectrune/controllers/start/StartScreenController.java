package com.lucylhill.projectrune.controllers.start;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.characters.CharacterInfo;
import com.lucylhill.projectrune.objects.characters.CharacterType;
import com.lucylhill.projectrune.objects.configuration.Configuration;
import com.lucylhill.projectrune.objects.dungeons.Dungeon;
import com.lucylhill.projectrune.objects.dungeons.DungeonList;
import com.lucylhill.projectrune.objects.entities.PartyMember;
import com.lucylhill.projectrune.services.*;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;


public class StartScreenController implements Initializable {

    @FXML
    Button startGameButton;

    @FXML
    Button quickStartButton;

    @FXML
    Button exitGameButton;
    @FXML
    Label buildText;

    @FXML
    StackPane parent;

    String buildInfo;

    @FXML
    ImageView logo;
    PartyList partyList;
    DungeonList dungeonList;
    DungeonGenerator dungeonGenerator;

    @Override
    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        loadConfig();
        partyList = InternalState.i().partyList();
        dungeonGenerator = DungeonGenerator.i();
        buildInfo = RuneConfiguration.i().systemProperties().getBuildInfo();
        dungeonList = RuneConfiguration.i().dungeonList();
        buildText.setText("Developed by Lucia Hill. "+buildInfo);
        buildText.setTextFill(Color.WHITE);
        quickStartButton.setTooltip(new Tooltip("A debug feature.  Creates a 4-player party randomly and puts you in the dungeon."));
        Image image = new Image(StartScreenController.class.getResourceAsStream("/art/logo.png"));
        logo.setImage(image);
        parent.setBackground(BackgroundImageFactory.createSceneBackground("/art/backgrounds/red-background.png"));
        RuneRadio.i().play(RuneRadio.TITLE);
    }

    public void loadConfig() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String contents = Files.readString(Paths.get("config.json"));
            Configuration configuration = mapper.readValue(contents, Configuration.class);
            InternalState.i().setConfiguration(configuration);
        }
        catch(IOException e) {
            System.out.println("something happened, like there is no config file");
        }

    }

    @FXML
    private void closeWindow() {
        Stage stage = (Stage) exitGameButton.getScene().getWindow();
        stage.close();
        Platform.exit();
        System.exit(0);
    }

    @FXML
    private void goToCharacterCreation() throws IOException {
        SceneSwitcher.switchScreens(startGameButton, SceneSwitcher.CHARACTER_CREATOR_SCREEN);
    }

    @FXML
    private void goToConfiguration()  throws IOException {
        SceneSwitcher.switchScreens(startGameButton, SceneSwitcher.CONFIG_SCREEN);
    }

    @FXML
    private void goToAchievement() throws IOException {
        SceneSwitcher.switchScreens(startGameButton, SceneSwitcher.ACHIEVEMENT_SCREEN);
    }

    @FXML
    private void quickStart() throws IOException {
        List<PartyMember> partyMembers = RuneConfiguration.i().partyMembers().getPartyMembers().subList(0, 4);
        List<CharacterInfo> characterInfos = new ArrayList<>();
        for(PartyMember p : partyMembers) {
            characterInfos.add(RuneConfiguration.i().partyMembers().getCharacterInfo(p.getName()));
        }
        characterInfos.get(0).setType(CharacterType.LEADER);

        partyList.setParty(characterInfos);
        Dungeon dungeon = dungeonList.getDungeons().get(0);
        dungeonGenerator.setDungeon(dungeon);
        SceneSwitcher.switchScreens(quickStartButton, SceneSwitcher.DUNGEON_SCREEN);
    }

}