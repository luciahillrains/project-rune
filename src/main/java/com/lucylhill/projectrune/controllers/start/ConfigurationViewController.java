package com.lucylhill.projectrune.controllers.start;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.battle.BattleState;
import com.lucylhill.projectrune.objects.battle.actions.Action;
import com.lucylhill.projectrune.objects.battle.actions.ActionEffectResolver;
import com.lucylhill.projectrune.objects.battle.actions.ActionResult;
import com.lucylhill.projectrune.objects.characters.CharacterInfo;
import com.lucylhill.projectrune.objects.characters.CharacterType;
import com.lucylhill.projectrune.objects.classes.ClassInfo;
import com.lucylhill.projectrune.objects.configuration.DebugConfig;
import com.lucylhill.projectrune.objects.configuration.DifficultyConfig;
import com.lucylhill.projectrune.objects.configuration.SoundConfig;
import com.lucylhill.projectrune.objects.dungeons.Dungeon;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.services.*;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public class ConfigurationViewController implements Initializable {

    @FXML
    ChoiceBox<String> difficultyPicker;

    @FXML
    ChoiceBox<Double> partyStrengthPicker;

    @FXML
    ChoiceBox<Double> enemyStrengthPicker;

    @FXML
    ChoiceBox<Double> goldCostModifierChoicebox;

    @FXML
    ChoiceBox<Integer> turnLimitPicker;

    @FXML
    CheckBox permadeathCheckbox;

    @FXML
    CheckBox permakickCheckbox;

    @FXML
    CheckBox noBattlesCheckbox;

    @FXML
    CheckBox resumeFromCheckpointCheckbox;

    @FXML
    CheckBox forceChoiceEventCheckbox;

    @FXML
    CheckBox uberStrongEnemyCheckbox;

    @FXML
    TextField forcedTraitFlag;

    @FXML
    TextField levelField;

    @FXML
    VBox difficultyPane;

    @FXML
    VBox debugPane;

    @FXML
    Button debugButton;

    @FXML
    StackPane parent;

    @FXML
    VBox frameParent;

    @FXML
    ChoiceBox<String> sceneSelector;

    @FXML
    ChoiceBox<String> testActionSelector;

    @FXML
    ChoiceBox<String> previousActionSelector;

    @FXML
    Button ymlFileLoadButton;

    @FXML
    Button fillInventoryButton;

    @FXML
    VBox soundPane;

    @FXML
    ChoiceBox<String> soundTestPicker;

    @FXML
    Slider volumeSlider;

    @FXML
    CheckBox muteMusicCheckbox;

    @FXML
    CheckBox muteSoundEffectsCheckbox;

    String[] difficultySettings = new String[]{"Easier", "Easy", "Standard", "Difficult", "Harder", "Nightmare", "Custom"};
    double[] strengthSettings = new double[]{0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2};
    double[] modifierSettings = new double[]{0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2};
    int[] turnlimitSettings  = new int[]{0, 90, 180, 365, 512};
    BooleanProperty isDifficultySettingsVisible = new SimpleBooleanProperty(false);
    BooleanProperty isDebugSettingsVisible = new SimpleBooleanProperty(false);
    BooleanProperty isSoundSettingsVisible = new SimpleBooleanProperty(false);
    DebugLogger logger = new DebugLogger(this.getClass());
    DebugService debugService = new DebugService();
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        parent.setBackground(BackgroundImageFactory.createSceneBackground("/art/backgrounds/bedroom-2.png"));
        permadeathCheckbox.setTooltip(new Tooltip(RuneConfiguration.i().systemProperties().getPermadeathDescription()));
        permakickCheckbox.setTooltip(new Tooltip(RuneConfiguration.i().systemProperties().getPermakickDescription()));
        noBattlesCheckbox.setTooltip(new Tooltip(RuneConfiguration.i().systemProperties().getNoBattlesDescription()));
        turnLimitPicker.setTooltip(new Tooltip(RuneConfiguration.i().systemProperties().getTurnLimitDescription()));
        resumeFromCheckpointCheckbox.setTooltip(new Tooltip(RuneConfiguration.i().systemProperties().getResumeFromCheckpointDescription()));
        Arrays.stream(difficultySettings).forEach(df -> difficultyPicker.getItems().add(df));
        Arrays.stream(strengthSettings).forEach(d -> {
            partyStrengthPicker.getItems().add(d);
            enemyStrengthPicker.getItems().add(d);
        });
        Arrays.stream(modifierSettings).forEach(m -> goldCostModifierChoicebox.getItems().add(m));
        Arrays.stream(turnlimitSettings).forEach(t -> turnLimitPicker.getItems().add(t));
        difficultyPicker.setValue("Standard");
        partyStrengthPicker.setValue(1.0);
        enemyStrengthPicker.setValue(1.0);
        goldCostModifierChoicebox.setValue(1.0);
        turnLimitPicker.setValue(365);
        debugButton.setVisible(RuneConfiguration.i().systemProperties().isDebug());
        bindVisibilityProperties();
        addScenesToPicker();
        addSoundsToSoundTest();
        previousActionSelector.getItems().add("None");
        RuneConfiguration.i().actions().getActions().stream().map(Action::getId).forEach(a -> testActionSelector.getItems().add(a));
        RuneConfiguration.i().actions().getActions().stream().map(Action::getId).forEach(a -> previousActionSelector.getItems().add(a));
        levelField.setText("99");
        loadConfiguration();
    }

    private void loadConfiguration() {
        DifficultyConfig diff = InternalState.i().configuration().getDifficultyConfig();
        SoundConfig sound = InternalState.i().configuration().getSoundConfig();

        permadeathCheckbox.setSelected(diff.isPermadeath());
        permakickCheckbox.setSelected(diff.isPermakick());
        noBattlesCheckbox.setSelected(diff.isNoBattles());
        resumeFromCheckpointCheckbox.setSelected(diff.isResumeFromCheckpoint());
        partyStrengthPicker.setValue(diff.getPartyStrength());
        enemyStrengthPicker.setValue(diff.getEnemyStrength());
        goldCostModifierChoicebox.setValue(diff.getGoldCostModifier());
        turnLimitPicker.setValue(diff.getTurnLimit());

        volumeSlider.adjustValue(sound.getVolume());
        muteMusicCheckbox.setSelected(sound.isMuteMusic());
        muteSoundEffectsCheckbox.setSelected(sound.isMuteSoundEffects());
    }

    private void addScenesToPicker() {
        sceneSelector.getItems().add("GameOver");
        sceneSelector.getItems().add("Hubtown");
        sceneSelector.getItems().add("Tavern");
        sceneSelector.getItems().add("Credits");
        sceneSelector.getItems().add("Market");
        sceneSelector.getItems().add("Guild");
        sceneSelector.getItems().add("Home");
        sceneSelector.getItems().add("Travel");
    }

    private void addSoundsToSoundTest() {
        soundTestPicker.getItems().add("Battle");
        soundTestPicker.getItems().add("Boss");
        soundTestPicker.getItems().add("Final");
        soundTestPicker.getItems().add("Title");
        soundTestPicker.getItems().add("Create");
        soundTestPicker.getItems().add("Kastelstad");
        soundTestPicker.getItems().add("Calm");
        soundTestPicker.getItems().add("Ground");
        soundTestPicker.getItems().add("Cellars");
        soundTestPicker.getItems().add("Tombs");
        soundTestPicker.getItems().add("Chapel");
        soundTestPicker.getItems().add("Tower");
        soundTestPicker.getItems().add("Gardens");
        soundTestPicker.getItems().add("Library");
        soundTestPicker.getItems().add("Babel");
        soundTestPicker.getItems().add("Tartarus");
    }

    private void bindVisibilityProperties() {
        difficultyPane.visibleProperty().bindBidirectional(isDifficultySettingsVisible);
        difficultyPane.managedProperty().bindBidirectional(isDifficultySettingsVisible);
        debugPane.visibleProperty().bindBidirectional(isDebugSettingsVisible);
        debugPane.managedProperty().bindBidirectional(isDebugSettingsVisible);
        soundPane.visibleProperty().bindBidirectional(isSoundSettingsVisible);
        soundPane.managedProperty().bindBidirectional(isSoundSettingsVisible);
    }

    public void writeConfigFile() {
        try {
            FileWriter writer = new FileWriter("config.json");
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(InternalState.i().configuration());
            writer.write(json);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("There was an error saving config file.");
            alert.show();
            System.out.println(e.getMessage());
        }
    }

    @FXML
    public void showDifficultyPane() {
        isDifficultySettingsVisible.set(true);
        isDebugSettingsVisible.set(false);
        isSoundSettingsVisible.set(false);
    }

    @FXML
    public void showDebugPane() {
        isDebugSettingsVisible.set(true);
        isDifficultySettingsVisible.set(false);
        isSoundSettingsVisible.set(false);
    }

    @FXML
    public void showSoundPane() {
        isSoundSettingsVisible.set(true);
        isDifficultySettingsVisible.set(false);
        isDebugSettingsVisible.set(false);
    }

    @FXML
    public void changeDifficultySettingsToTemplate() {
        String difficultyValue = difficultyPicker.getValue();
        boolean permadeath = true;
        boolean permakick = true;
        double partyStrength = 1.0;
        double enemyStrength = 1.0;
        double goldCostModifier = 1.0;
        int turnLimit = 365;
        switch (difficultyValue) {
            case "Easier" -> {
                permadeath = false;
                permakick = false;
                turnLimit = 0;
                partyStrength = 1.6;
            }
            case "Easy" -> {
                permadeath = false;
                partyStrength = 1.3;
            }
            case "Difficult" -> {
                enemyStrength = 1.3;
                goldCostModifier = 1.5;
            }
            case "Harder" -> {
                enemyStrength = 1.6;
                turnLimit = 180;
                goldCostModifier = 2.0;
            }
            case "Nightmare" -> {
                partyStrength = 0.6;
                enemyStrength = 2.0;
                goldCostModifier = 2.0;
                turnLimit = 90;
            }
        }

        permadeathCheckbox.setSelected(permadeath);
        permakickCheckbox.setSelected(permakick);
        partyStrengthPicker.setValue(partyStrength);
        enemyStrengthPicker.setValue(enemyStrength);
        goldCostModifierChoicebox.setValue(goldCostModifier);
        turnLimitPicker.setValue(turnLimit);
    }

    @FXML
    public void saveAndExit() throws IOException{
        DifficultyConfig diff = InternalState.i().configuration().getDifficultyConfig();
        DebugConfig debug = InternalState.i().configuration().getDebugConfig();
        SoundConfig sound = InternalState.i().configuration().getSoundConfig();
        diff.setPermadeath(permadeathCheckbox.isSelected());
        diff.setPermakick(permakickCheckbox.isSelected());
        diff.setNoBattles(noBattlesCheckbox.isSelected());
        diff.setResumeFromCheckpoint(resumeFromCheckpointCheckbox.isSelected());
        diff.setPartyStrength(partyStrengthPicker.getValue());
        diff.setEnemyStrength(enemyStrengthPicker.getValue());
        diff.setGoldCostModifier(goldCostModifierChoicebox.getValue());
        diff.setTurnLimit(turnLimitPicker.getValue());

        debug.setForceChoiceEvent(forceChoiceEventCheckbox.isSelected());
        debug.setForcedTrait(forcedTraitFlag.getText());
        debug.setUberStrongEnemies(uberStrongEnemyCheckbox.isSelected());

        sound.setVolume(volumeSlider.getValue());
        sound.setMuteMusic(muteMusicCheckbox.isSelected());
        sound.setMuteSoundEffects(muteSoundEffectsCheckbox.isSelected());
        if(muteMusicCheckbox.isSelected()) {
            RuneRadio.i().stop();
        }
        InternalState.i().level().setLevel(Integer.parseInt(levelField.getText()));
        writeConfigFile();
        exit();
    }

    @FXML
    public void exit() throws IOException {
        SceneSwitcher.switchScreens(debugButton, SceneSwitcher.START_SCREEN);
    }

    @FXML
    public void goToBattleTest() throws IOException {
        DebugConfig debugConfig = InternalState.i().configuration().getDebugConfig();
        debugConfig.setBattleTestMode(true);
        debugConfig.setTestAction(testActionSelector.getValue());
        debugConfig.setPreviousAction(previousActionSelector.getValue());
        SceneSwitcher.switchScreens(debugButton, SceneSwitcher.TEST_BATTLE_SCREEN);
    }

    @FXML
    public void doLibertarianTest() {
        if(RuneConfiguration.i().actions().getActionFromList(testActionSelector.getValue()).isPresent()) {
            Action action = RuneConfiguration.i().actions().getActionFromList(testActionSelector.getValue()).get();
            if(previousActionSelector.getValue() != null && RuneConfiguration.i().actions().getActionFromList(previousActionSelector.getValue()).isPresent()) {
                Action prevAction = RuneConfiguration.i().actions().getActionFromList(previousActionSelector.getValue()).get();
                BattleState.i().getPreviousActions().add(prevAction);
            } else {
                if(previousActionSelector.getValue() != null) {
                    logger.log("PREVIOU ACTION "+ previousActionSelector.getValue()+" not found");
                }
            }
            EntityInfo target = new EntityInfo();
            target.setName("Target");
            EntityInfo source = new EntityInfo();
            source.setName("Source");
            source.setAttack(2);
            source.setIntelligence(2);
            source.setAgility(20);
            source.getPassives().getPassives().add(RuneConfiguration.i().passiveList().getPassiveWithId("EnhancedPlunder"));
            source.getPassives().getPassives().add(RuneConfiguration.i().passiveList().getPassiveWithId("Belligerent"));
            target.setCommonItemId("Item:HealthTonic");
            target.setRareItemId("Item:RefreshingDrink");
            target.setMorphItemId("Item:Bandage");
            target.setMaxHp(100);
            target.setHp(2);
            ActionEffectResolver resolver = new ActionEffectResolver();
            for(ClassInfo c : RuneConfiguration.i().classList().getClasses()) {
                if(c.getActions().contains(action.getId()) && action.getClassId() == null) {
                    action.setClassId(c.getName());
                }
            }
            ActionResult result = resolver.resolveAction(action, target, source);
            logger.log(result.toString());
        } else {
            logger.log("ACTION "+testActionSelector.getValue()+" not found");
        }


    }

    @FXML
    public void goToScene() throws IOException {
        CharacterInfo characterInfo = new CharacterInfo();
        characterInfo.setName("Test");
        characterInfo.setClassInfo(RuneConfiguration.i().classList().getClasses().get(0));
        characterInfo.setTrait(RuneConfiguration.i().traitList().getTraits().get(0));
        characterInfo.setGender("Not Disclosed");
        characterInfo.setRace("Not Disclosed");
        characterInfo.setType(CharacterType.LEADER);
        InternalState.i().partyList().addMemberToParty(characterInfo);
        switch (sceneSelector.getValue()) {
            case "GameOver" -> SceneSwitcher.switchScreens(debugButton, SceneSwitcher.GAME_OVER_SCREEN);
            case "Hubtown" -> SceneSwitcher.switchScreens(debugButton, SceneSwitcher.HUB_WORLD_SCREEN);
            case "Tavern" -> SceneSwitcher.switchScreens(debugButton, SceneSwitcher.TAVERN_SCREEN);
            case "Credits" -> SceneSwitcher.switchScreens(debugButton, SceneSwitcher.CREDITS_SCREEN);
            case "Market" -> SceneSwitcher.switchScreens(debugButton, SceneSwitcher.MARKET_SCREEN);
            case "Guild" -> SceneSwitcher.switchScreens(debugButton, SceneSwitcher.RECRUITING_SCREEN);
            case "Home" -> SceneSwitcher.switchScreens(debugButton, SceneSwitcher.YOUR_ROOM_SCREEN);
            case "Travel" -> SceneSwitcher.switchScreens(debugButton, SceneSwitcher.TRAVEL_SCREEN);
        }
    }

    @FXML
    public void loadYMLFiles() {
        RuneConfiguration.i().actions();
        RuneConfiguration.i().systemProperties();
        RuneConfiguration.i().classList();
        RuneConfiguration.i().ailmentList();
        RuneConfiguration.i().aspectTable();
        RuneConfiguration.i().conversationList();
        RuneConfiguration.i().dungeonList();
        RuneConfiguration.i().enemies();
        RuneConfiguration.i().npcs();
        RuneConfiguration.i().passiveList();
        RuneConfiguration.i().traitList();
        List<Dungeon> x = RuneConfiguration.i().dungeonList().getDungeons();
        RuneConfiguration.i().items();
        RuneConfiguration.i().equipment();
        RuneConfiguration.i().relics();
        RuneConfiguration.i().shops();
        RuneConfiguration.i().achievements();
        RuneConfiguration.i().partyMembers();
        RuneConfiguration.i().questList();
        RuneConfiguration.i().interstitials();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText("Files successfully loaded");
        alert.show();
    }

    @FXML
    public void fillInventory() {
        debugService.fillInventory();
    }

    @FXML
    public void addAchievements() { debugService.addAchievements(); }

    @FXML
    public void maxOutLevel() {
        InternalState.i().level().setLevel(99);
    }

    @FXML
    public void playSound() {
        switch(soundTestPicker.getValue()) {
            case "Battle" -> RuneRadio.i().play(RuneRadio.BATTLE);
            case "Boss" -> RuneRadio.i().play(RuneRadio.BOSS);
            case "Final" -> RuneRadio.i().play(RuneRadio.FINAL_BATTLE);
            case "Title" -> RuneRadio.i().play(RuneRadio.TITLE);
            case "Create" -> RuneRadio.i().play(RuneRadio.LOADING);
            case "Kastelstad" -> RuneRadio.i().play(RuneRadio.HUBTOWN);
            case "Calm" -> RuneRadio.i().play(RuneRadio.OASIS);
            case "Ground" -> RuneRadio.i().play(RuneRadio.TRAINING_GROUND);
            case "Cellars" -> RuneRadio.i().play(RuneRadio.ALBION_DUNGEON);
            case "Tombs" -> RuneRadio.i().play(RuneRadio.ROYAL_TOMBS);
            case "Chapel" -> RuneRadio.i().play(RuneRadio.DARK_CHAPEL);
            case "Tower" -> RuneRadio.i().play(RuneRadio.CROOKED_TOWER);
            case "Gardens" -> RuneRadio.i().play(RuneRadio.SKY_GARDENS);
            case "Library" -> RuneRadio.i().play(RuneRadio.RUINED_LIBRARY);
            case "Babel" -> RuneRadio.i().play(RuneRadio.TOWER_OF_BABEL);
            case "Tartarus" -> RuneRadio.i().play(RuneRadio.TARTARUS);
        }
    }
}
