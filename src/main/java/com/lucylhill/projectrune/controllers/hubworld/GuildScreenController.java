package com.lucylhill.projectrune.controllers.hubworld;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.characters.CharacterInfo;
import com.lucylhill.projectrune.objects.characters.CharacterType;
import com.lucylhill.projectrune.objects.entities.PartyMember;
import com.lucylhill.projectrune.services.*;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class GuildScreenController implements Initializable {

    @FXML
    Button leaveButton;

    @FXML
    Button leaderButton;
    @FXML
    Button partyMember1Button;
    @FXML
    Button partyMember2Button;
    @FXML
    Button partyMember3Button;
    @FXML
    Button removePartyMemberButton;

    @FXML
    Label recruitCharNameLabel;

    @FXML
    Label recruitCharTraitLabel;

    @FXML
    Label recruitCharClassLabel;

    @FXML
    Label recruitCharDescriptionLabel;

    @FXML
    Label partyMemberNameLabel;
    @FXML
    Label partyMemberTraitLabel;
    @FXML
    Label partyMemberClassLabel;

    @FXML
    Label partyMemberDescriptionLabel;

    @FXML
    StackPane parent;

    @FXML
    ListView<CharacterInfo> recruitingList;

    @FXML
    HBox frameParent;
    @FXML
    Button recruitCharacterButton;

    PartyList partyList;
    List<CharacterInfo> recruitables = new ArrayList<>();
    List<Button> partyButtonList;
    int memberSelected = -1;
    int partyMemberSelected = -1;
    CharacterInfo activeRecruitable;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.parent.setBackground(BackgroundImageFactory.createSceneBackground("/art/backgrounds/apothecary-2.png"));
        this.partyList = InternalState.i().partyList();
        generateRecruitables();
        partyButtonList = Arrays.asList(leaderButton, partyMember1Button, partyMember2Button, partyMember3Button);
        populatePartyMemberButtons();

        recruitingList.setItems(FXCollections.observableArrayList(recruitables));
        recruitingList.getSelectionModel().selectedItemProperty().addListener(((observable, oldValue, newValue) -> {
            activeRecruitable = newValue;
            if(activeRecruitable != null) {
                populateRecruitLabels();
            } else {
                clearRecruitLabels();
            }
        }));
    }

    @FXML
    public void goToHubWorld() throws IOException {
        SceneSwitcher.switchScreens(leaveButton, SceneSwitcher.HUB_WORLD_SCREEN);
    }

    public void generateRecruitables() {
        List<String> partyMembersToRecruit = new ArrayList<>();
        for(PartyMember p : RuneConfiguration.i().partyMembers().getPartyMembers()) {
            if(p.getChapterUnlocked() <= InternalState.i().getCurrentChapter()) {
                partyMembersToRecruit.add(p.getName());
            }
        }
        for(String name : partyMembersToRecruit) {
            CharacterInfo c = RuneConfiguration.i().partyMembers().getCharacterInfo(name);
            if(c != null && !InternalState.i().partyList().getParty().contains(c)) {
                recruitables.add(c);
            }
        }


    }


    @FXML
    public void populateLeaderLabels() {
        populatePartyLabels(0);
    }

    @FXML
    public void populatePartyMember1Labels() {
        populatePartyLabels(1);
    }

    @FXML
    public void populatePartyMember2Labels() {
        populatePartyLabels(2);
    }

    @FXML
    public void populatePartyMember3Labels() {
        populatePartyLabels(3);
    }

    @FXML
    public void recruitCharacter() {
        boolean addResult = partyList.addMemberToParty(activeRecruitable);
        if(!addResult) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Unable to recruit party member, your party is already full!  Please remove a member.");
            alert.show();
        } else {
            int newPartyMemberIndex = partyList.getParty().size() - 1;
            partyButtonList.get(newPartyMemberIndex).setText(activeRecruitable.getRecruitedName());
            partyButtonList.get(newPartyMemberIndex).setDisable(false);

            recruitables.remove(activeRecruitable);
            recruitingList.setItems(FXCollections.observableArrayList(recruitables));
            clearRecruitLabels();
        }
    }

    @FXML
    public void removePartyMember() {
        if(partyMemberSelected > 0) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setContentText("Are you sure you want to remove this party member?");
            Optional<ButtonType> res = alert.showAndWait();
            if(res.isPresent() && res.get() == ButtonType.OK) {
                partyList.getParty().remove(partyMemberSelected);
                partyButtonList.get(partyMemberSelected).setText("Free");
                partyButtonList.get(partyMemberSelected).setDisable(true);
                clearPartyMemberLabels();
                populatePartyMemberButtons();
            }
        }
    }

    public void populateRecruitLabels() {
        CharacterInfo c = activeRecruitable;
        recruitCharNameLabel.setText(c.getRecruitedName());
        recruitCharNameLabel.setTooltip(new Tooltip(c.getName() + " " + c.getGender() + " " + c.getRace()));
        recruitCharTraitLabel.setText(c.getTrait().getName());
        recruitCharTraitLabel.setTooltip(new Tooltip(c.getTrait().getDescription()));
        recruitCharClassLabel.setText(c.getClassInfo().getName());
        recruitCharClassLabel.setTooltip(new Tooltip(c.getClassInfo().getUIDescription()));
        recruitCharDescriptionLabel.setText(RuneConfiguration.i().partyMembers().getPartyMember(c.getName()).getDescription());
        recruitCharacterButton.setDisable(false);
    }

    public void populatePartyLabels(int index) {
        CharacterInfo c = partyList.getParty().get(index);
        partyMemberNameLabel.setText(c.getRecruitedName());
        partyMemberNameLabel.setTooltip(new Tooltip(c.getName() + " " + c.getGender() + " " + c.getRace()));
        partyMemberTraitLabel.setText(c.getTrait().getName());
        partyMemberTraitLabel.setTooltip(new Tooltip(c.getTrait().getDescription()));
        partyMemberClassLabel.setText(c.getClassInfo().getName());
        partyMemberClassLabel.setTooltip(new Tooltip(c.getClassInfo().getUIDescription()));
        if(c.getType() != CharacterType.LEADER) {
            partyMemberDescriptionLabel.setText(RuneConfiguration.i().partyMembers().getPartyMember(c.getName()).getDescription());
        }
        partyMemberSelected = index;
        removePartyMemberButton.setDisable(false);
        if(index < 1) {
            removePartyMemberButton.setDisable(true);
        }
    }

    public void clearPartyMemberLabels() {
        partyMemberNameLabel.setText("");
        partyMemberNameLabel.setTooltip(null);
        partyMemberTraitLabel.setText("");
        partyMemberTraitLabel.setTooltip(null);
        partyMemberClassLabel.setText("");
        partyMemberClassLabel.setTooltip(null);
        removePartyMemberButton.setDisable(true);
        memberSelected = -1;
    }


    public void clearRecruitLabels() {
        recruitCharNameLabel.setText("");
        recruitCharNameLabel.setTooltip(null);
        recruitCharTraitLabel.setText("");
        recruitCharTraitLabel.setTooltip(null);
        recruitCharClassLabel.setText("");
        recruitCharClassLabel.setTooltip(null);
        recruitCharacterButton.setDisable(true);
        memberSelected = -1;
    }

    public void populatePartyMemberButtons() {
        for(int i = 0; i < partyButtonList.size(); i++) {
            Button b = partyButtonList.get(i);
            if(partyList.getParty().size() >= i+1) {
                CharacterInfo c = partyList.getParty().get(i);
                b.setText(c.getRecruitedName());
                b.setDisable(false);
            } else {
                b.setText("Free");
                b.setDisable(true);
            }
        }
    }
}
