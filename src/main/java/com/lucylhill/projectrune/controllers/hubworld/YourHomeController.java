package com.lucylhill.projectrune.controllers.hubworld;

import com.lucylhill.projectrune.objects.battle.actions.Action;
import com.lucylhill.projectrune.objects.battle.actions.Deck;
import com.lucylhill.projectrune.objects.battle.actions.DeckManagementResult;
import com.lucylhill.projectrune.objects.items.Item;
import com.lucylhill.projectrune.services.*;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.InterfaceAddress;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static com.lucylhill.projectrune.services.DeckManagementService.DUPLICATE_MAX;

public class YourHomeController implements Initializable {
    @FXML
    StackPane parent;

    @FXML
    HBox frameParent;

    @FXML
    VBox deckManagementPane;

    @FXML
    VBox restPane;

    @FXML
    ListView<Action> actionList;

    @FXML
    Label actionNameLabel;

    @FXML
    Label actionDescriptionLabel;

    @FXML
    Label rarityLabel;

    @FXML
    Label duplicateInfoLabel;

    @FXML
    Label removeInfoLabel;

    @FXML
    Label disableInfoLabel;

    @FXML
    Label goldLabel;

    @FXML
    Button duplicateCardButton;

    @FXML
    Button removeDuplicateCardButton;

    @FXML
    Button disableCardButton;

    @FXML
    VBox itemStoragePane;

    @FXML
    ListView<Item> inventoryItemList;

    @FXML
    ListView<Item> storageItemList;

    @FXML
    Label currentGoldLabel;

    @FXML
    Label storedGoldLabel;

    @FXML
    Button itemManagementActionButton;

    @FXML
    Label itemNameLabel;

    @FXML
    Label itemDescriptionLabel;

    Action activeAction;
    Item activeItem;
    Deck deck = new Deck(false);
    BooleanProperty isDeckManagementVisible = new SimpleBooleanProperty(false);
    BooleanProperty isRestPaneVisible = new SimpleBooleanProperty(false);
    BooleanProperty isItemManagementVisible = new SimpleBooleanProperty(false);
    DeckManagementService deckService = new DeckManagementService();
    int goldCostForAction = 0;
    boolean storeMode;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        deckManagementPane.visibleProperty().bindBidirectional(isDeckManagementVisible);
        deckManagementPane.managedProperty().bindBidirectional(isDeckManagementVisible);
        restPane.visibleProperty().bindBidirectional(isRestPaneVisible);
        restPane.managedProperty().bind(isRestPaneVisible);
        itemStoragePane.visibleProperty().bindBidirectional(isItemManagementVisible);
        itemStoragePane.managedProperty().bindBidirectional(isItemManagementVisible);
        parent.setBackground(BackgroundImageFactory.createSceneBackground("/art/backgrounds/bedroom-3.png"));
        ObservableList<Action> listOActions = FXCollections.observableArrayList(deck.getDeck().stream().filter(a -> !a.isItemAction()).collect(Collectors.toList()));
        actionList.setItems(listOActions);
        goldLabel.setText("Gold: "+InternalState.i().gold() + "g");
        actionList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            activeAction = newValue;
            int amount = Deck.getQuantities().get(activeAction.getId());
            goldCostForAction  = activeAction.getApCost() + (10 - activeAction.getRarity());
            actionNameLabel.setText(activeAction.getName());
            actionDescriptionLabel.setText("("+activeAction.getAlignment().name()+")"+" "+activeAction.getDescription());
            rarityLabel.setText("R:"+activeAction.getRarity()+" Q:"+amount);
            int dupNum = amount - activeAction.getRarity();
            setDeckManagementButtonStates(amount, dupNum);
        });
    }

    @FXML
    public void displayDeckManagementPane() {
        isDeckManagementVisible.set(true);
        isRestPaneVisible.set(false);
        isItemManagementVisible.set(false);
    }

    public boolean buyAction() {
        if(InternalState.i().gold() > goldCostForAction) {
            InternalState.i().changeGold(-1*goldCostForAction);
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("You do not have enough gold for this action.");
            alert.show();
            return false;
        }
        return true;
    }

    @FXML
    public void duplicateCard() {
        boolean successful = buyAction();
        if(successful) {
            DeckManagementResult result = deckService.duplicateAction(activeAction);
            if(result.isSuccess()) {
                rarityLabel.setText("R: "+ activeAction.getRarity() + " Q:"+result.getNewQuantity());
                setDeckManagementButtonStates(result.getOriginalQuantity(), result.getNumberOfDuplicates() + 1);
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("You have reached the max for duplicates.");
                alert.show();
            }
        }

    }

    @FXML
    public void removeDuplicate() {
        boolean successful = buyAction();
        if(successful) {
            DeckManagementResult result = deckService.removeDuplicate(activeAction);
            if(result.isSuccess()) {
                rarityLabel.setText("R: "+ activeAction.getRarity() + " Q:"+result.getNewQuantity());
                setDeckManagementButtonStates(result.getOriginalQuantity(), result.getNumberOfDuplicates() - 1);
            }else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("You have no more duplicates.");
                alert.show();
            }
        }
    }

    @FXML
    public void disableAction() {
        boolean successful = buyAction();
        if(successful) {
            if(!deckService.disableAction(activeAction)) {
                rarityLabel.setText("R: "+ activeAction.getRarity() + " Q:"+activeAction.getRarity());
                setDeckManagementButtonStates(activeAction.getRarity(), 0);
            } else {
                rarityLabel.setText("R: 0 Q: 0");
                setDeckManagementButtonStates(0,0);
            }
        }

    }

    @FXML
    public void refresh() {
        InternalState.i().partyList().fullHeal();
        isDeckManagementVisible.set(false);
        isRestPaneVisible.set(true);
        isItemManagementVisible.set(false);
    }

    public void setDeckManagementButtonStates(int amount, int dupNum) {
        disableCardButton.setDisable(false);
        if(amount == 0) {
            duplicateInfoLabel.setText("**DISABLED**");
            removeInfoLabel.setText("**DISABLED**");
            disableCardButton.setText("Re-enable");
            duplicateCardButton.setDisable(true);
            removeDuplicateCardButton.setDisable(true);
        } else {
            duplicateCardButton.setDisable(false);
            removeDuplicateCardButton.setDisable(false);
            disableCardButton.setText("Disable");
            duplicateInfoLabel.setText("(" + dupNum + "/" + DUPLICATE_MAX + ") "+goldCostForAction+" gold");
            if (dupNum == 0) {
                removeDuplicateCardButton.setDisable(true);
                removeInfoLabel.setText("0 left "+goldCostForAction+" gold");
            } else {
                removeDuplicateCardButton.setDisable(false);
                removeInfoLabel.setText(dupNum + " left "+goldCostForAction+" gold");
            }
        }
        goldLabel.setText(InternalState.i().gold() + " g");

   }

   @FXML
   public void showItemManagementPane() {
        isDeckManagementVisible.set(false);
        isRestPaneVisible.set(false);
        isItemManagementVisible.set(true);
        hydrateGoldLabels();
        ObservableList<Item> inventoryItems = FXCollections.observableArrayList(InternalState.i().inventory().getItems());
        inventoryItemList.setItems(inventoryItems);
        inventoryItemList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            activeItem = newValue;
            itemManagementActionButton.setText("STORE ->");
            hydrateItemLabels();
            itemManagementActionButton.setDisable(false);
            storeMode = true;
        });
        ObservableList<Item> storedItems = FXCollections.observableArrayList(InternalState.i().storedItems().getItems());
        storageItemList.setItems(storedItems);
        storageItemList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            activeItem = newValue;
            itemManagementActionButton.setText("<- TAKE");
            itemManagementActionButton.setDisable(false);
            hydrateItemLabels();
            storeMode = false;
        });
   }

   public void hydrateItemLabels() {
        if(activeItem != null) {
            itemNameLabel.setText(activeItem.getName());
            itemDescriptionLabel.setText(activeItem.getDescription());
        } else {
            clearItemLabels();
        }

   }

   public void clearItemLabels() {
        itemNameLabel.setText("");
        itemDescriptionLabel.setText("");
       itemManagementActionButton.setDisable(true);
   }

   public void hydrateGoldLabels() {
       currentGoldLabel.setText("Current Gold:"+ InternalState.i().gold());
       storedGoldLabel.setText("Stored Gold:"+InternalState.i().storedGold());
   }

   @FXML
   public void itemManagementAction() {
       ItemService itemService = new ItemService();
        if(activeItem != null) {
            if(storeMode) {
                InternalState.i().storedItems().getItems().add(activeItem);
                itemService.discardItem(activeItem.getId());
            } else {
                itemService.addItem(activeItem);
                InternalState.i().storedItems().getItems().remove(activeItem);
            }
            clearItemLabels();
            activeItem = null;
            ObservableList<Item> inventoryItems = FXCollections.observableArrayList(InternalState.i().inventory().getItems());
            inventoryItemList.setItems(inventoryItems);
            ObservableList<Item> storedItems = FXCollections.observableArrayList(InternalState.i().storedItems().getItems());
            storageItemList.setItems(storedItems);
        }
   }

   @FXML
   public void storeGold() {
        int gold = InternalState.i().gold();
        InternalState.i().changeGold(gold * -1);
        InternalState.i().storeGold(gold);
        hydrateGoldLabels();
   }

   @FXML
   public void takeGold() {
        int gold = InternalState.i().storedGold();
        InternalState.i().changeGold(gold);
        InternalState.i().storeGold(gold * -1);
        hydrateGoldLabels();
   }

    @FXML
    public void goToHubWorld() throws IOException {
        SceneSwitcher.switchScreens(disableCardButton, SceneSwitcher.HUB_WORLD_SCREEN);
    }

}
