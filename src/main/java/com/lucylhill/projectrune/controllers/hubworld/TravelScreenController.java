package com.lucylhill.projectrune.controllers.hubworld;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.dungeons.Dungeon;
import com.lucylhill.projectrune.objects.dungeons.DungeonList;
import com.lucylhill.projectrune.services.BackgroundImageFactory;
import com.lucylhill.projectrune.services.DungeonGenerator;
import com.lucylhill.projectrune.services.InternalState;
import com.lucylhill.projectrune.services.SceneSwitcher;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class TravelScreenController implements Initializable {
    DungeonGenerator dungeonGenerator;
    DungeonList dungeonList;

    @FXML
    Button trainingGroundTravelButton;

    @FXML
    Button guidestoneTravelButton;

    @FXML
    Button royalTombsTravelButton;

    @FXML
    Button devilChapelTravelButton;

    @FXML
    Button crookedTowerTravelButton;

    @FXML
    Button skyGardensTravelButton;

    @FXML
    Button ruinedLibraryTravelButton;

    @FXML
    Button tartarusTravelButton;

    @FXML
    Button babelTravelButton;

    @FXML
    Button provingGroundTravelButton;

    @FXML
    Button backToTownTravelButton;

    @FXML
    Label placeNameLabel;

    @FXML
    Pane placePictureLabel;

    @FXML
    StackPane parent;

    @FXML
    HBox frameParent;

    @FXML
    Label placeDescriptionLabel;

    @FXML
    Button goToDungeonButton;

    String currentDungeon = "";

    public static final int TRAINING_GROUND_INDEX = 0;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        dungeonList = RuneConfiguration.i().dungeonList();
        trainingGroundTravelButton.setText(dungeonList.getDungeons().get(TRAINING_GROUND_INDEX).getName());
        parent.setBackground(BackgroundImageFactory.createSceneBackground("/art/backgrounds/cave-1.png"));
        dungeonGenerator = DungeonGenerator.i();
    }

    @FXML
    public void backToTown() throws IOException {
        SceneSwitcher.switchScreens(backToTownTravelButton, SceneSwitcher.HUB_WORLD_SCREEN);
    }

    @FXML
    public void displayTrainingGroundLabels() {
        placeNameLabel.setText(dungeonList.getDungeons().get(TRAINING_GROUND_INDEX).getName());
        placePictureLabel.setBackground(BackgroundImageFactory.createTravelImageBackground("/art/places/trainingGround.png"));
        placeDescriptionLabel.setText(dungeonList.getDungeons().get(TRAINING_GROUND_INDEX).getDescription());
        goToDungeonButton.setDisable(false);
        currentDungeon = "trainingGrounds";
    }

    @FXML
    public void goToDungeon() throws IOException {
        InternalState.i().setCurrentDungeon(currentDungeon);
        if(currentDungeon.equals("trainingGrounds")) {
            goToTrainingGrounds();
        }
    }

    @FXML
    public void goToTrainingGrounds() throws IOException {
        Dungeon dungeon = dungeonList.getDungeons().get(TRAINING_GROUND_INDEX);
        dungeonGenerator.setDungeon(dungeon);
        SceneSwitcher.switchScreens(trainingGroundTravelButton, SceneSwitcher.DUNGEON_SCREEN);
    }

}
