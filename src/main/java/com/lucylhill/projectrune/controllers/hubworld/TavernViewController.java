package com.lucylhill.projectrune.controllers.hubworld;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.items.Item;
import com.lucylhill.projectrune.objects.tavern.Conversation;
import com.lucylhill.projectrune.objects.tavern.ConversationResponse;
import com.lucylhill.projectrune.objects.tavern.TavernNPC;
import com.lucylhill.projectrune.objects.tavern.TavernNPCRender;
import com.lucylhill.projectrune.services.BackgroundImageFactory;
import com.lucylhill.projectrune.services.InternalState;
import com.lucylhill.projectrune.services.ItemService;
import com.lucylhill.projectrune.services.SceneSwitcher;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class TavernViewController implements Initializable {
    @FXML
    StackPane parent;

    @FXML
    HBox frameParent;

    @FXML
    VBox peopleParentBox;

    @FXML
    ImageView faceViewImage;

    @FXML
    Label personNameLabel;

    @FXML
    Label personDescriptionLabel;

    @FXML
    Label dialogTextLabel;

    @FXML
    VBox dialogChoiceParent;

    @FXML
    VBox marketPane;

    @FXML
    VBox conversationPane;

    @FXML
    ListView<Item> itemList;

    @FXML
    Label itemNameLabel;

    @FXML
    Label itemDescriptionLabel;

    @FXML
    Label itemCostLabel;

    @FXML
    Label itemQuantityLabel;

    @FXML
    Label itemTypeLabel;

    @FXML
    Label goldLabel;

    @FXML
    Button actionButton;


    List<TavernNPC> applicableNPCs = new ArrayList<>();
    List<TavernNPCRender> renderedNPCs = new ArrayList<>();
    BooleanProperty showConversationPane = new SimpleBooleanProperty(false);
    BooleanProperty showMarketPane = new SimpleBooleanProperty(false);
    Item activeItem;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        applicableNPCs = RuneConfiguration.i().npcs().getNPCsForChapter();
        parent.setBackground(BackgroundImageFactory.createSceneBackground("/art/backgrounds/bedroom-2.png"));
        conversationPane.visibleProperty().bindBidirectional(showConversationPane);
        conversationPane.managedProperty().bindBidirectional(showConversationPane);
        marketPane.visibleProperty().bindBidirectional(showMarketPane);
        marketPane.managedProperty().bindBidirectional(showMarketPane);
        renderUI();
    }

    public void getRenderedNPCs() {
        int level = InternalState.i().level().getLevel();
        int chapter = InternalState.i().level().getChapter();
        for(TavernNPC npc : applicableNPCs) {
            Conversation chosenConv = null;
            for(String convId : npc.getConversationIds()) {
                Conversation conv = RuneConfiguration.i().conversationList().getConversationById(convId);
                if((conv.getUnlock().getChapter() == chapter || conv.getUnlock().getChapter() == -1) && conv.getUnlock().getLevelMin() <= level) {
                    chosenConv = conv;
                }
                if(chosenConv != null) {
                    TavernNPCRender render = new TavernNPCRender();
                    render.setName(npc.getName());
                    render.setDescription(npc.getDescription());
                    render.setConversation(chosenConv);
                    renderedNPCs.add(render);
                }
            }
        }
    }

    public void renderUI() {
        getRenderedNPCs();
        for(TavernNPCRender npcR : renderedNPCs) {
            Button button = new Button();
            button.setMinWidth(448.0);
            button.setPrefWidth(448.0);
            button.setMaxWidth(448.0);
            button.getStyleClass().add("pane-switcher");
            button.setText(npcR.getName());
            button.setOnAction(e -> {
                showConversationPane();
                personNameLabel.setText(npcR.getName());
                personDescriptionLabel.setText(npcR.getDescription());
                dialogTextLabel.setText(npcR.getConversation().getDialog());
                renderChoices(npcR.getConversation().getChoices());
            });
            peopleParentBox.getChildren().add(button);
        }
    }

    public void renderChoices(List<ConversationResponse> responses) {
        dialogChoiceParent.getChildren().clear();
        for(ConversationResponse r : responses) {
            Button button = new Button();
            button.setMinWidth(773.0);
            button.setMaxWidth(773.0);
            button.setPrefWidth(773.0);
            button.setText(r.getDialog());
            button.getStyleClass().add("view-switcher-2");
            button.setOnAction(e -> {
                dialogTextLabel.setText(r.getResponse());
                renderChoices(r.getChoices());
            });
            dialogChoiceParent.getChildren().add(button);
        }

    }

    public void showConversationPane() {
        showConversationPane.set(true);
        showMarketPane.set(false);
    }

    public void showMarketPane() {
        showMarketPane.set(true);
        showConversationPane.set(false);
    }

    public List<Item> getWares(List<String> itemIds) {
        return RuneConfiguration.i().getItemsFromIds(itemIds);
    }


    @FXML
    public void showTavernMarket() {
        showMarketPane();
        int chapter = InternalState.i().getCurrentChapter();
        List<String> ids = RuneConfiguration.i().shops().getTavern().get(0);
        for(int i = 1; i <= chapter; i++) {
            ids.addAll(RuneConfiguration.i().shops().getTavern().get(i));
        }
        List<Item> items = getWares(ids);
        itemList.setItems(FXCollections.observableArrayList(items));
        itemList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            activeItem = newValue;
            if(activeItem != null) {
                goldLabel.setText("Gold: "+InternalState.i().gold()+" g");
                int amount = (int) InternalState.i().inventory().getItems().stream().filter(i -> i.getId().equals(activeItem.getId())).count();
                int cost = activeItem.getCost();
                itemNameLabel.setText(activeItem.getName());
                itemDescriptionLabel.setText(activeItem.getDescription());
                itemTypeLabel.setText("Type: Food");
                itemCostLabel.setText("Cost: "+cost+" g");
                itemQuantityLabel.setText("Held: "+amount);
                actionButton.setDisable(InternalState.i().gold() < cost);
            } else {
                itemNameLabel.setText("");
                itemDescriptionLabel.setText("");
                itemTypeLabel.setText("");
                itemCostLabel.setText("");
                itemQuantityLabel.setText("");
            }

        });
    }

    @FXML
    public void goToHubWorld() throws IOException {
        SceneSwitcher.switchScreens(dialogChoiceParent, SceneSwitcher.HUB_WORLD_SCREEN);
    }

    @FXML
    public void doAction() {
        ItemService itemService = new ItemService();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        int cost = activeItem.getCost();
        alert.setContentText("Buy "+activeItem.getName()+" for "+ cost+" gold?");
        Optional<ButtonType> result = alert.showAndWait();
        if(result.isPresent() && result.get() == ButtonType.OK) {
            InternalState.i().changeGold(cost * -1);
            itemService.addItem(activeItem);
            int amount = (int) InternalState.i().inventory().getItems().stream().filter(i -> i.getId().equals(activeItem.getId())).count();
            itemQuantityLabel.setText("Held: " + amount);
        }
        goldLabel.setText("Gold: "+InternalState.i().gold() + " g");
    }
}
