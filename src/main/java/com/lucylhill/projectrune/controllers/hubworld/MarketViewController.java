package com.lucylhill.projectrune.controllers.hubworld;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.characters.CharacterInfo;
import com.lucylhill.projectrune.objects.items.Equipment;
import com.lucylhill.projectrune.objects.items.EquipmentFlag;
import com.lucylhill.projectrune.objects.items.Item;
import com.lucylhill.projectrune.services.BackgroundImageFactory;
import com.lucylhill.projectrune.services.InternalState;
import com.lucylhill.projectrune.services.ItemService;
import com.lucylhill.projectrune.services.SceneSwitcher;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.ResourceBundle;

public class MarketViewController implements Initializable {
    @FXML
    Button smithyButton;

    @FXML
    StackPane parent;

    @FXML
    HBox frameParent;

    @FXML
    VBox marketPane;

    @FXML
    ListView<Item> itemList;

    @FXML
    Label itemNameLabel;

    @FXML
    Label itemDescriptionLabel;

    @FXML
    Label itemCostLabel;

    @FXML
    Label itemQuantityLabel;

    @FXML
    Label itemTypeLabel;

    @FXML
    Label goldLabel;

    @FXML
    Label merchantNameText;

    @FXML
    Button actionButton;

    boolean sellingMode = false;
    Item activeItem;
    int chapter;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Random random = new Random();
        chapter = InternalState.i().getCurrentChapter();
        parent.setBackground(BackgroundImageFactory.createSceneBackground("/art/backgrounds/town-2.png"));
        goldLabel.setText("Gold: "+InternalState.i().gold()+" g");
        ObservableList<Item> items = FXCollections.observableArrayList();
        itemList.setItems(items);
        itemList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            activeItem = newValue;
            if(activeItem != null) {
                actionButton.setVisible(true);
                goldLabel.setText("Gold: "+InternalState.i().gold()+" g");
                int amount = (int) InternalState.i().inventory().getItems().stream().filter(i -> i.getId().equals(activeItem.getId())).count();
                int charisma = InternalState.i().partyList().getParty().stream().map(CharacterInfo::getCharisma).reduce(0, Integer::sum);
                if(charisma == 0) {
                    charisma = 1;
                }
                int hit = random.nextInt(0, 100) + charisma;
                double percentIncrease = 0;
                if(InternalState.i().partyList().partyHasTrait("Goblin Bartering")) {
                    hit = hit + 100;
                }
                double modifier = 0;
                if(InternalState.i().partyList().partyHasTrait("Celebrity Status")) {
                    modifier = 0.1;
                }
                if(hit > 800) {
                    percentIncrease = 0.3 + modifier;
                } else if (hit > 500) {
                    percentIncrease = 0.15 + modifier;
                } else if (hit > 300) {
                    percentIncrease = 0.05 + modifier;
                }
                int cost = activeItem.getCost();
                cost = cost - ((int)(cost * percentIncrease));
                if(sellingMode) {
                    cost = activeItem.getCost() / 2;
                    cost = cost + ((int)(cost * percentIncrease));
                }

                itemNameLabel.setText(activeItem.getName());
                itemDescriptionLabel.setText(activeItem.getDescription());
                itemTypeLabel.setText("Type: "+getActiveItemType());
                itemCostLabel.setText("Cost: "+cost+" g");
                if(sellingMode) {
                    itemCostLabel.setText("Earn: "+cost+" g");
                }
                itemQuantityLabel.setText("Held: "+amount);
                actionButton.setDisable(!sellingMode &&
                        InternalState.i().gold() < cost);
            } else {
                itemNameLabel.setText("");
                itemDescriptionLabel.setText("");
                itemTypeLabel.setText("");
                itemCostLabel.setText("");
                itemQuantityLabel.setText("");
            }

        });
    }

    public String getActiveItemType() {
        String typeLabel;
        if (activeItem.isComponent()) {
            typeLabel = "Component/Sellable";
        }
        else if (activeItem.isOnlyUsableInBattle()) {
            typeLabel = "Battle";
        } else {
            if (activeItem instanceof Equipment a) {
                typeLabel = "Equipment";
                if(a.getFlag() == EquipmentFlag.RELIC) {
                    typeLabel = "Relic";
                }
            } else {
                typeLabel = "Field";
            }
        }
        return typeLabel;
    }

    public void buyMode() {
        sellingMode = false;
        actionButton.setVisible(false);
        actionButton.setText("BUY");
    }

    @FXML
    public void goToHubWorld() throws IOException {
        SceneSwitcher.switchScreens(smithyButton, SceneSwitcher.HUB_WORLD_SCREEN);
    }

    public List<Item> getWares(List<String> itemIds) {
        return RuneConfiguration.i().getItemsFromIds(itemIds);
    }

    @FXML
    public void getSmithyWares() {
        buyMode();
        merchantNameText.setText("Roy Jarvis, Blacksmith");
        List<String> ids = RuneConfiguration.i().shops().getBlacksmith().get(0);
        for(int i = 1; i <= chapter; i++) {
            ids.addAll(RuneConfiguration.i().shops().getBlacksmith().get(i));
        }
        List<Item> items = getWares(ids);
        itemList.setItems(FXCollections.observableArrayList(items));
    }

    @FXML
    public void getWeaverWares() {
        buyMode();
        merchantNameText.setText("Charlotte Dolina, Weaver");
        List<String> ids = RuneConfiguration.i().shops().getWeaver().get(0);
        for(int i = 1; i <= chapter; i++) {
            ids.addAll(RuneConfiguration.i().shops().getWeaver().get(i));
        }
        List<Item> items = getWares(ids);
        itemList.setItems(FXCollections.observableArrayList(items));
    }

    @FXML
    public void getItemWares() {
        buyMode();
        merchantNameText.setText("Bosco & Bosco, Item Sellers");
        List<String> ids = RuneConfiguration.i().shops().getItems().get(0);
        for(int i = 1; i <= chapter; i++) {
            ids.addAll(RuneConfiguration.i().shops().getItems().get(i));
        }
        List<Item> items = getWares(ids);
        itemList.setItems(FXCollections.observableArrayList(items));
    }

    @FXML
    public void getRareWares() {
        buyMode();
        merchantNameText.setText("Creepy Old Man Selling Rare Items");
        List<String> ids = RuneConfiguration.i().shops().getRareItems().get(0);
        for(int i = 1; i <= chapter; i++) {
            ids.addAll(RuneConfiguration.i().shops().getRareItems().get(i));
        }
        List<Item> items = getWares(ids);
        itemList.setItems(FXCollections.observableArrayList(items));
    }

    @FXML
    public void getPawn() {
        merchantNameText.setText("Richard Haroldson, Pawn Broker");
        ObservableList<Item> items = FXCollections.observableArrayList(InternalState.i().inventory().getItems());
        itemList.setItems(items);
        actionButton.setVisible(true);
        actionButton.setText("SELL");
        sellingMode = true;
    }

    @FXML
    public void doAction() {
        ItemService itemService = new ItemService();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        if(sellingMode) {
            int goldValue = activeItem.getCost() / 2;
            alert.setContentText("Sell "+activeItem.getName()+" for "+ goldValue+" gold?");
            Optional<ButtonType> result = alert.showAndWait();
            if(result.isPresent() && result.get() == ButtonType.OK) {
                InternalState.i().changeGold(goldValue);
                itemService.discardItem(activeItem.getId());
                itemList.getItems().remove(activeItem);
                activeItem = null;
                itemNameLabel.setText("");
                itemDescriptionLabel.setText("");
                itemTypeLabel.setText("");
                itemCostLabel.setText("");
                itemQuantityLabel.setText("");
            }

        } else {
            int cost = activeItem.getCost();
            alert.setContentText("Buy "+activeItem.getName()+" for "+ cost+" gold?");
            Optional<ButtonType> result = alert.showAndWait();
            if(result.isPresent() && result.get() == ButtonType.OK) {
                InternalState.i().changeGold(cost * -1);
                itemService.addItem(activeItem);
                int amount = (int) InternalState.i().inventory().getItems().stream().filter(i -> i.getId().equals(activeItem.getId())).count();
                itemQuantityLabel.setText("Held: "+amount);
                actionButton.setDisable(!sellingMode &&
                        InternalState.i().gold() < cost);
            }
        }
        goldLabel.setText("Gold: "+InternalState.i().gold() + " g");
    }

}
