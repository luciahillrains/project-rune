package com.lucylhill.projectrune.controllers.hubworld;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.services.BackgroundImageFactory;
import com.lucylhill.projectrune.services.RuneRadio;
import com.lucylhill.projectrune.services.SceneSwitcher;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class HubWorldController implements Initializable {

    @FXML
    Button guildButton;

    @FXML
    Button tavernButton;

    @FXML
    Button wellButton;

    @FXML
    Button fortuneButton;

    @FXML
    Button marketsButton;

    @FXML
    Button homeButton;

    @FXML
    Button innButton;

    @FXML
    Label buttonDescriptionLabel;

    @FXML
    Label townNameLabel;

    @FXML
    StackPane parent;

    @FXML
    HBox frameParent;

    @FXML
    Pane imagePane;

    @FXML
    Button visitButton;

    String townName;

    String activeView = "";

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        townName = RuneConfiguration.i().systemProperties().getHubWorldName();
        townNameLabel.setText(townName);
        parent.setBackground(BackgroundImageFactory.createSceneBackground("/art/backgrounds/town-2.png"));
        RuneRadio.i().play(RuneRadio.HUBTOWN);
    }

    @FXML
    public void goToRecruitingScreen()  {
        changeLabelToGuildButtonDescription();
        activeView = SceneSwitcher.RECRUITING_SCREEN;
        visitButton.setVisible(true);

    }

    @FXML
    public void goToTravelScreen() {
        changeLabelToTravelButtonDescription();
        activeView = SceneSwitcher.TRAVEL_SCREEN;
        visitButton.setVisible(true);
    }

    @FXML
    public void goToTavernScreen() {
        changeLabelToTavernButtonDescription();
        activeView = SceneSwitcher.TAVERN_SCREEN;
        visitButton.setVisible(true);
    }

    @FXML
    public void goToYourRoom() {
        changeLabelToHomeButtonDescription();
        activeView = SceneSwitcher.YOUR_ROOM_SCREEN;
        visitButton.setVisible(true);
    }

    @FXML
    public void goToMarketScreen() {
        changeLabelToMarketsButtonDescription();
        activeView = SceneSwitcher.MARKET_SCREEN;
        visitButton.setVisible(true);
    }

    @FXML
    public void changeLabelToGuildButtonDescription() {
        buttonDescriptionLabel.setText("The guild is where you can recruit members and change your party.");
        imagePane.setBackground(BackgroundImageFactory.createHubtownImageBackground("/art/backgrounds/guild.png"));
    }

    @FXML
    public void changeLabelToTavernButtonDescription() {
        buttonDescriptionLabel.setText("The tavern is where you can catch up on gossip and progress through quests.");
        imagePane.setBackground(BackgroundImageFactory.createHubtownImageBackground("/art/backgrounds/tavern.png"));

    }

    @FXML
    public void changeLabelToWellButtonDescription() {
        buttonDescriptionLabel.setText("The well is where you can pay some gold for a chance of getting the character of your dreams the next day at the guild.");
    }

    @FXML
    public void changeLabelToFortuneTellerButtonDescription() {
        buttonDescriptionLabel.setText("The fortune teller is where you can pay to get clues on where to go next.");
    }

    @FXML
    public void changeLabelToMarketsButtonDescription() {
        buttonDescriptionLabel.setText("The markets is where you can buy equipment, treasures and items.");
        imagePane.setBackground(BackgroundImageFactory.createHubtownImageBackground("/art/backgrounds/market.png"));
    }

    @FXML
    public void changeLabelToHomeButtonDescription() {
        buttonDescriptionLabel.setText("Your home is where you can stash away items, treasures and change your deck.");
        imagePane.setBackground(BackgroundImageFactory.createHubtownImageBackground("/art/backgrounds/bedroom.png"));

    }

    @FXML
    public void changeLabelToApothecaryButtonDescription() {
        buttonDescriptionLabel.setText("The Apothecary is where you can go to change your party member's traits.");
    }

    @FXML
    public void changeLabelToTravelButtonDescription() {
        buttonDescriptionLabel.setText("Go outside of the village and venture into one of the dungeons you've unlocked.");
        imagePane.setBackground(BackgroundImageFactory.createHubtownImageBackground("/art/backgrounds/map.png"));
    }

    @FXML
    public void changeLabelToLibraryDescription() {
        buttonDescriptionLabel.setText("The Library is where you can go to change your party member's class.");
        imagePane.setBackground(BackgroundImageFactory.createHubtownImageBackground("/art/backgrounds/library.png"));
    }

    @FXML
    public void changeLabelToInnButtonDescription() {
        buttonDescriptionLabel.setText("The inn is where you can heal and progress for the night.");
    }

    @FXML
    public void visit() throws IOException{
        SceneSwitcher.switchScreens(visitButton, activeView);
    }
}
