package com.lucylhill.projectrune.services;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.battle.status.AilmentFlag;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.characters.CharacterInfo;
import com.lucylhill.projectrune.objects.characters.Stat;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

public class PartyList {
    List<CharacterInfo> party = new ArrayList<>();
    List<CharacterInfo> reserves = new ArrayList<>();

    int maxNumOfCharacters;

    public PartyList() {
        maxNumOfCharacters = RuneConfiguration.i().systemProperties().getMaxNumOfCharactersInParty();
    }

    public Optional<CharacterInfo> getCharacterWithClassOrTraitFlag(String flag) {
        Optional<CharacterInfo> found = Optional.empty();
        List<CharacterInfo> foundList = party.stream().filter(ch -> characterHasClassFlag(ch, flag)).collect(Collectors.toList());
        if(foundList.size() >= 1) {
            found = Optional.of(foundList.get(0));
        }
        return found;
    }

    public boolean partyHasCharacterWithClassOrTraitFlag(String flag) {
        Optional<CharacterInfo> ch = getCharacterWithClassOrTraitFlag(flag);
        return ch.isPresent();
    }

    public List<String> getCharactersWhoAreDisabledOrAuto() {
        List<String> names = new ArrayList<>();
        for(CharacterInfo c: party) {
            for(StatusEffect e: c.getStatusEffects().getEffects()) {
                if(e.getAilmentFlag() == AilmentFlag.AUTO ||
                    e.getAilmentFlag() == AilmentFlag.AUTO_HIT_ALL ||
                        e.getAilmentFlag() == AilmentFlag.DISABLE) {
                    names.add(c.getName());
                }
            }
        }
        return names;
    }

    public List<CharacterInfo> getCharactersWhoAreAuto() {
        List<CharacterInfo> chars = new ArrayList<>();
        for(CharacterInfo c: party) {
            for(StatusEffect e: c.getStatusEffects().getEffects()) {
                if(e.getAilmentFlag() == AilmentFlag.AUTO ||
                        e.getAilmentFlag() == AilmentFlag.AUTO_HIT_ALL) {
                    chars.add(c);
                }
            }
        }
        return chars;
    }

    public boolean addMemberToParty(CharacterInfo c) {
        if(party.size() > maxNumOfCharacters) {
            return false;
        }
        party.add(c);
        return true;
    }

    public boolean characterHasClassFlag(CharacterInfo c, String flag) {
        return c.getClassInfo().getFlag().equals(flag);
    }

    public List<CharacterInfo> getParty() {
        return party;
    }

    public void setParty(List<CharacterInfo> party) {
        this.party = party;
    }

    public boolean isEntirePartyDead() {
        return party.stream().noneMatch(ch -> ch.getHp() > 0);
    }

    public void fullHeal() {
        party.forEach(ch -> ch.setHp(ch.getMaxHp()));
    }

    public List<CharacterInfo> getReserves() {
        return reserves;
    }

    public void setReserves(List<CharacterInfo> reserves) {
        this.reserves = reserves;
    }

    public List<CharacterInfo> getValidTargets() {
        return party.stream().filter(c -> c.getHp() > 0).collect(Collectors.toList());
    }

    public int getPartyPosition(String name) {
        int partyPos = -1;
        for(int i = 0; i < party.size(); i++) {
            if(party.get(i).getName().equals(name)) {
                partyPos = i;
                break;
            }
        }
        return partyPos;
    }

    public boolean partyHasTrait(String traitName) {
        return party.stream().anyMatch(c -> c.getTrait().getName().equals(traitName));
    }

    public void executeLevelUpBonuses() {
        Random random = new Random();
        List<Stat> stats = List.of(Stat.ATTACK, Stat.INTELLIGENCE, Stat.DEFENSE, Stat.CHARISMA, Stat.AGILITY);
        for(CharacterInfo c : party) {
            for(Stat s : stats) {
                int delta = random.nextInt(0, 2);
                if(c.getClassInfo().getStatPriority().contains(s) || c.getTrait().getName().equals("Youth")) {
                    delta = random.nextInt(1, 4);
                }
                if(c.getTrait().getName().equals("TRAIT_NOT_FOUND")) {
                    delta = random.nextInt(-1, -20);
                }
                if(s == Stat.AGILITY || s == Stat.CHARISMA) {
                    delta = delta * 2;
                }
                if(c.getTrait().getName().equals("Versatile")) {
                    if(s == Stat.ATTACK) {
                        c.addAttack(delta);
                        c.addIntelligence(delta);
                    }
                    if(s == Stat.INTELLIGENCE) {
                        continue;
                    }
                }
                switch(s) {
                    case ATTACK -> c.addAttack(delta);
                    case DEFENSE -> c.addDefense(delta);
                    case INTELLIGENCE -> c.addIntelligence(delta);
                    case AGILITY -> c.addAgility(delta);
                    case CHARISMA -> c.addCharisma(delta);
                }
            }
            if(InternalState.i().level().getLevel() % 5 == 0 && getPartyPosition("Mendas") > -1) {
                c.addIntelligence(3);
            }
        }

    }
}
