package com.lucylhill.projectrune.services;

import com.lucylhill.projectrune.objects.characters.CharacterInfo;
import com.lucylhill.projectrune.objects.misc.Check;
import com.lucylhill.projectrune.objects.misc.Operator;

import java.util.List;
import java.util.stream.Collectors;

public class CheckValidator {

    PartyList partyList;

    public CheckValidator() {
        this.partyList = InternalState.i().partyList();
    }

    public boolean evaluateChecks(List<Check> checks) {
        return checks.stream().anyMatch(this::evaluateCheck);
    }

    public boolean evaluateCheck(Check check) {
        CharacterInfo character = partyList.getParty().get(0);
        if(check.getTrait().equals("Class")) {
            if(check.getOperator() == Operator.EQUALS) {
                return character.getClassInfo().getFlag().equals(check.getTarget()) || character.getClassInfo().getFlag().equals("X");
            } else if (check.getOperator() == Operator.NOT_EQUALS) {
                return !character.getClassInfo().getFlag().equals(check.getTarget());
            }
        }
        if(check.getTrait().equals("Agility")) {
            if(check.getOperator() == Operator.GREATER_THAN) {
                return character.getAgility() > Integer.parseInt(check.getTarget());
            }
        }
        if(check.getTrait().equals("Intelligence")) {
            if(check.getOperator() == Operator.GREATER_THAN) {
                return character.getIntelligence() > Integer.parseInt(check.getTarget());
            }
        }
        return false;
    }
}
