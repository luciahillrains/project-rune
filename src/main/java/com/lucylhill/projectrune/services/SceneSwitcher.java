package com.lucylhill.projectrune.services;

import com.lucylhill.projectrune.RuneApplication;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;


import java.io.IOException;

public class SceneSwitcher {
    public static final String RECRUITING_SCREEN = "guild-screen-view.fxml";
    public static final String HUB_WORLD_SCREEN = "hub-world-view.fxml";
    public static final String CHARACTER_CREATOR_SCREEN = "create-character-view.fxml";
    public static final String START_SCREEN = "hello-view.fxml";
    public static final String TRAVEL_SCREEN = "travel-screen-view.fxml";
    public static final String DUNGEON_SCREEN = "dungeon-view.fxml";
    public  static final String CONFIG_SCREEN = "configuration-view.fxml";
    public static final String CREDITS_SCREEN = "coming-soon-screen.fxml";
    public static final String GAME_OVER_SCREEN = "game-over-view.fxml";
    public static final String TAVERN_SCREEN = "tavern-view.fxml";
    public static final String MARKET_SCREEN = "market-screen.fxml";
    public static final String ACHIEVEMENT_SCREEN = "achievement-view.fxml";
    public static final String TEST_BATTLE_SCREEN = "battle-test-interface-view.fxml";
    public static final String YOUR_ROOM_SCREEN = "your-home-screen.fxml";

    public static final String INTERSTITIAL_SCREEN = "interstitial-view.fxml";


    public static void switchScreens(Node button, String screen) throws IOException {
        Stage stage = (Stage) button.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(RuneApplication.class.getResource(screen));
        Parent rootNode = loader.load();
        Scene scene = new Scene(rootNode, RuneApplication.WINDOW_WIDTH + 20, RuneApplication.WINDOW_HEIGHT);
        scene.getStylesheets().add("style.css");
        stage.setScene(scene);
    }

}
