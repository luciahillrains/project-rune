package com.lucylhill.projectrune.services;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.controllers.battle.HandBattleViewController;
import com.lucylhill.projectrune.objects.battle.BattleEvent;
import com.lucylhill.projectrune.objects.battle.BattleEventFlag;
import com.lucylhill.projectrune.objects.battle.BattleState;
import com.lucylhill.projectrune.objects.battle.actions.*;
import com.lucylhill.projectrune.objects.battle.enemies.BattleEnemy;
import com.lucylhill.projectrune.objects.battle.enemies.Enemy;
import com.lucylhill.projectrune.objects.battle.status.AilmentFlag;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.battle.status.StatusEffectList;
import com.lucylhill.projectrune.objects.characters.CharacterInfo;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.dungeons.DungeonEvent;
import com.lucylhill.projectrune.objects.entities.EntityInfo;
import com.lucylhill.projectrune.objects.items.Item;
import com.lucylhill.projectrune.objects.passives.PassiveFlag;
import java.util.*;
import java.util.stream.Collectors;

public class BattleService {
    PartyList partyList;
    HandBattleViewController handController;
    Deque<BattleEvent> battleEventQueue = new ArrayDeque<>();
    List<BattleEnemy> battleEnemies = new ArrayList<>();
    Deck actionDeck;
    DebugLogger logger = new DebugLogger(this.getClass());
    boolean partyTurn = false;
    int enemyTurnEnemyIdx = 0;
    Random random = new Random();
    DungeonEvent event;
    ActionEffectResolver actionResolver = new ActionEffectResolver();
    public void initialize(HandBattleViewController handController) {
        this.handController = handController;
        partyList = InternalState.i().partyList;
        partyTurn = decideIfPlayerGoesFirst();
        for(CharacterInfo c : partyList.getParty()) {
            c.executePassives(PassiveFlag.BATTLE);
        }
        int baptistePosition = InternalState.i().partyList().getPartyPosition("Baptiste");
        int faeriePosition = InternalState.i().partyList().getPartyPosition("Trina");
        if( baptistePosition > -1 && InternalState.i().partyList().partyHasTrait("Vanity")) {
            CharacterInfo c = InternalState.i().partyList().getParty().get(baptistePosition);
            c.addAdditionalDefense(5);
            c.addAdditionalAttack(5);
            c.addAdditionalIntelligence(5);
        }
        if(faeriePosition > -1 && InternalState.i().partyList().partyHasTrait("Faerie Power")) {
            double increase = random.nextDouble(0.0, 0.07);
            CharacterInfo c = InternalState.i().partyList().getParty().get(faeriePosition);
            c.getAdditionalDefenses().put(Aspects.FIRE, c.getAdditionalDefenses().get(Aspects.FIRE) - increase);
            c.getAdditionalDefenses().put(Aspects.WATER, c.getAdditionalDefenses().get(Aspects.WATER) - increase);
            c.getAdditionalDefenses().put(Aspects.ICE, c.getAdditionalDefenses().get(Aspects.ICE) - increase);
            c.getAdditionalDefenses().put(Aspects.AIR, c.getAdditionalDefenses().get(Aspects.AIR) - increase);
            c.getAdditionalDefenses().put(Aspects.EARTH, c.getAdditionalDefenses().get(Aspects.EARTH) - increase);
            c.getAdditionalDefenses().put(Aspects.DARK, c.getAdditionalDefenses().get(Aspects.DARK) - increase);
            c.getAdditionalDefenses().put(Aspects.HOLY, c.getAdditionalDefenses().get(Aspects.HOLY) - increase);
            c.getAdditionalDefenses().put(Aspects.PHYSICAL, c.getAdditionalDefenses().get(Aspects.PHYSICAL) - increase);
            c.getAdditionalDefenses().put(Aspects.OMNI, c.getAdditionalDefenses().get(Aspects.OMNI) - increase);
            c.getAdditionalDefenses().put(Aspects.NONE, c.getAdditionalDefenses().get(Aspects.NONE) - increase);
        }
    }

    public void setEnemies(List<String> enemyIds) {
        for(String e : enemyIds) {
            battleEnemies.add(new BattleEnemy(RuneConfiguration.i().enemies().getEnemyById(e)));
        }
    }

    public boolean decideIfPlayerGoesFirst() {
        if(InternalState.i().configuration().getDebugConfig().isBattleTestMode()) {
            return true;
        }
        Random random = new Random();
        int enemyRange = 50;
        int partyRange = partyList.getParty().stream().mapToInt(CharacterInfo::getAgility).sum();
        if(partyRange < 1) {
            partyRange = 1;
        }
        partyRange = partyRange * 3;
        int target =  random.nextInt(enemyRange + partyRange);
        return target < partyRange;

    }

    public void onUIRender() {
        String logMessage = "The party encountered " + battleEnemies.get(0).getEnemy().getName();
        if(battleEnemies.size() > 1) {
            logMessage = logMessage + " and cohorts!";
        }
        battleEventQueue.add(new BattleEvent(logMessage));
        handController.setTurnOver(!partyTurn);
        if(partyTurn) {
            playerTurn();
        } else {
            enemyTurn();
        }
    }

    public Optional<BattleEvent> getQueuePoll() {
        logger.log("Queue contents:"+battleEventQueue.size());
        Optional<BattleEvent> optEvent = Optional.empty();
        if(!battleEventQueue.isEmpty()) {
            BattleEvent event = battleEventQueue.poll();
            optEvent = Optional.of(event);
            logger.log("Battle Event: "+event);
        }
        if(!partyTurn) {
            logger.log("Enemy turn!");
            enemyTurn();
        }
        return optEvent;
    }

    public Deque<BattleEvent> getBattleEventQueue() {
        return battleEventQueue;
    }

    public void clearQueue() {
        battleEventQueue.clear();
    }

    public void onSetEvent(DungeonEvent event) {
        actionDeck = new Deck(true);
        this.event = event;
        if(InternalState.i().configuration().getDebugConfig().isBattleTestMode()) {
            actionDeck.debugDeck();
        }
        handController.setDeck(actionDeck);
        if(InternalState.i().configuration().getDebugConfig().isUberStrongEnemies()) {
            for(BattleEnemy battleEnemy : battleEnemies) {
                battleEnemy.setHp(10000);
                battleEnemy.getEnemy().setAttack(999);
                battleEnemy.getEnemy().setDefense(999);
            }
        }
    }

    public boolean playerTurn() {
        for(CharacterInfo c : partyList.getParty()) {
            c.executePassives(PassiveFlag.TURN);
        }
        handController.timeForPlayersTurn();
        battleEventQueue.add(new BattleEvent("Your party's turn to attack!"));
        if(areAllEnemiesDead()) {
            battleEventQueue.add(new BattleEvent("Congrats!  You've won!"));
            logger.log("ALL ENEMIES ARE DEAD");
            handController.setTurnOver(true);
        }
        return areAllEnemiesDead() || partyList.isEntirePartyDead();

    }

    public boolean enemyTurn() {
        if(enemyTurnEnemyIdx >= battleEnemies.size()) {
            enemyTurnEnemyIdx = 0;
            handController.setTurnOver(false);
            handController.setPlayButtonDisable(false);
            partyTurn = true;
            playerTurn();
            return false;
        }
        handController.setTurnOver(true);
        Random random = new Random();
        if(battleEnemies.get(enemyTurnEnemyIdx).getHp() <= 0) {
            enemyTurnEnemyIdx++;
            return false;
        }
        BattleEnemy enemy = battleEnemies.get(enemyTurnEnemyIdx);
        int weight = random.nextInt(enemy.getCurrentWeightAmount());
        String actionId = enemy.getActionForWeight(weight);
        Optional<Action> candidate = RuneConfiguration.i().actions().getActionFromList(actionId);
        Action action;
        if(candidate.isEmpty()) {
            logger.log("ERR!!! Action not found "+ actionId);
            return false;
        }
        action = candidate.get();
        action.setCharacterName("e:" + enemyTurnEnemyIdx);
        if(action.getTargetType() == ActionTargetType.PARTY_SINGLE) {
            String enmityKey = enemy.getEnemy().getName() + enemy.getEnemy().hashCode();
            if(BattleState.i().getEnmityMap().containsKey(enmityKey)) {
                String charName = BattleState.i().getEnmityMap().get(enmityKey);
                int pos = partyList.getPartyPosition(charName);
                if(partyList.getParty().get(pos).getHp() <= 0) {
                    pos = getRandomPartyMember();
                }
                BattleState.i().setTargetId(pos);
            } else {
                int targetPos = getRandomPartyMember();
                BattleState.i().setTargetId(targetPos);
            }
        }
        if(action.getTargetType() == ActionTargetType.SINGLE) {
            int targetPos = getRandomEnemy();
            BattleState.i().setTargetId(targetPos);
        }
        handleAction(action);
        enemyTurnEnemyIdx++;

        return false;
    }

    public boolean handleAction(Action action) {
        handController.setActionPlayed(false);
        EntityInfo source;
        String charName = action.getCharacterName();
        EntityInfo target = null;
        if(charName.startsWith("e:")) {
            int enemyIdx = Integer.parseInt(charName.split(":")[1]);
            source = battleEnemies.get(enemyIdx);
        } else {
            source = partyList.getParty().get(partyList.getPartyPosition(charName));
        }
        if(source.getPassives().hasPassive("AidMastery")) {
            for(int i = 0; i < partyList.getParty().size(); i++) {
                EntityInfo e = partyList.getParty().get(i);
                if (e.getHp() > 0) {
                    Action aidAction = RuneConfiguration.i().actions().getActionFromList("Aid").orElse(new Action());
                    ActionResult result = actionResolver.resolveAction(action, e, source);
                    result.setTargetPosition(i);
                    battleEventQueue.add(new BattleEvent(result));
                    return true;
                }
            }
        }
        //SINGLE is single enemy.
        if(action.getTargetType() == ActionTargetType.SINGLE) {
            target = battleEnemies.get(BattleState.i().getTargetId());
        }
        if(action.getTargetType() == ActionTargetType.PARTY_SINGLE || action.getTargetType() == ActionTargetType.PARTY_DEAD) {
            target = partyList.getParty().get(BattleState.i().getTargetId());
        }
        if(action.getTargetType() == ActionTargetType.SELF) {
            target = source;
        }
        if(action.getTargetType() == ActionTargetType.PARTY_SINGLE ||
                action.getTargetType() == ActionTargetType.PARTY_DEAD ||
                action.getTargetType() == ActionTargetType.SINGLE) {
            ActionResult result = actionResolver.resolveAction(action, target, source);
            result.setTargetPosition(BattleState.i().getTargetId());
            result.setEnemy(action.getTargetType() == ActionTargetType.SINGLE);
            battleEventQueue.add(new BattleEvent(result));
        } else if(action.getTargetType() == ActionTargetType.SELF) {
            ActionResult result = actionResolver.resolveAction(action, target, source);
            int targetPosition;
            if(action.getCharacterName().startsWith("e")) {
                targetPosition = Integer.parseInt(action.getCharacterName().split(":")[1]);
                result.setEnemy(true);
            } else {
                targetPosition = partyList.getPartyPosition(action.getCharacterName());
            }
            result.setTargetPosition(targetPosition);
            battleEventQueue.add(new BattleEvent(result));
        } else if(action.getTargetType() == ActionTargetType.ALL) {
            for(int i = 0; i < battleEnemies.size(); i++) {
                EntityInfo e = battleEnemies.get(i);
                if(e.getHp() > 0) {
                    ActionResult result = actionResolver.resolveAction(action, e, source);
                    result.setTargetPosition(i);
                    result.setEnemy(true);
                    battleEventQueue.add(new BattleEvent(result));
                }
            }
            return true;
        } else if(action.getTargetType() == ActionTargetType.PARTY_ALL) {
            for(int i = 0; i < partyList.getParty().size(); i++) {
                EntityInfo e = partyList.getParty().get(i);
                if(e.getHp() > 0) {
                    ActionResult result = actionResolver.resolveAction(action, e, source);
                    result.setTargetPosition(i);
                    battleEventQueue.add(new BattleEvent(result));
                }
            }
            return true;
        } else if(action.getTargetType() == ActionTargetType.RANDOM1 ||
                action.getTargetType() == ActionTargetType.RANDOM2 ||
                action.getTargetType() == ActionTargetType.RANDOM3) {
            int numTimes = switch(action.getTargetType()) {
                case RANDOM1 -> 1;
                case RANDOM2 -> 2;
                case RANDOM3 -> 3;
                default -> 0;
            };
            for(int i = 0; i < numTimes; i++) {
                int targetId = getRandomEnemy();
                target = battleEnemies.get(targetId);
                ActionResult result = actionResolver.resolveAction(action, target, source);
                result.setTargetPosition(targetId);
                result.setEnemy(true);
                battleEventQueue.add(new BattleEvent(result));
            }
            return true;
        }
        else if(action.getTargetType() == ActionTargetType.EVERYONE) {
            for(int i = 0; i < battleEnemies.size(); i++) {
                EntityInfo e = battleEnemies.get(i);
                ActionResult result = actionResolver.resolveAction(action, e, source);
                result.setTargetPosition(i);
                result.setEnemy(true);
                battleEventQueue.add(new BattleEvent(result));
            }
            for(int i = 0; i < partyList.getParty().size(); i++) {
                EntityInfo e = partyList.getParty().get(i);
                ActionResult result = actionResolver.resolveAction(action, e, source);
                result.setTargetPosition(i);
                battleEventQueue.add(new BattleEvent(result));
            }
            return true;
        }
        return false;
    }

    public void onPartyTurnOver() {
        handleAutoCharacters();
        handleAutoEnemyCharacters();
        partyTurn = false;
        enemyTurn();
    }

    public int[] getCharacterEscapeNumbers() {
        int[] numbers = new int[partyList.getParty().size()];
        int runNum;
        for(int i = 0; i < partyList.getParty().size(); i++) {
            CharacterInfo character = partyList.getParty().get(i);
            runNum = random.nextInt(25);
            numbers[i] = runNum;
        }

        return numbers;
    }

    public int getRunAwayTarget() {
        int num = 0;
        int lvl = InternalState.i().level().getLevel();
        if(lvl < 20) {
            num = 50;
        }
        if(lvl >= 20 && lvl < 50) {
            num = 75;
        }
        if(lvl >= 50) {
            num = 100;
        }
        return num;
    }

    public boolean onPartyRunAway() {
        int[] numbers = getCharacterEscapeNumbers();
        int total = Arrays.stream(numbers).sum();
        int target = random.nextInt(getRunAwayTarget());
        boolean success = total > target;
        handController.setRunningAway(false);
        if(success) {
            partyList.getParty().forEach(EntityInfo::clearAdditionalStats);
            partyList.getParty().stream().map(EntityInfo::getStatusEffects).forEach(StatusEffectList::clearStatChanges);

            battleEventQueue.addFirst(new BattleEvent("The party ran away!"));
            BattleEvent event = new BattleEvent("The party ran away!");
            event.setFlag(BattleEventFlag.END_BATTLE);
            battleEventQueue.addFirst(event);
        } else {
            battleEventQueue.addFirst(new BattleEvent("The party couldn't run away!"));
        }
        return success;
    }

    public boolean checkIfPartyIsDead() {
        boolean isDead = partyList.isEntirePartyDead();
        if(isDead) {
            BattleEvent ending = new BattleEvent("The party has been defeated.");
            ending.setFlag(BattleEventFlag.END_BATTLE_LOST);
            battleEventQueue.add(ending);
            partyList.getParty().forEach(EntityInfo::clearAdditionalStats);
            partyList.getParty().stream().map(EntityInfo::getStatusEffects).forEach(StatusEffectList::clearStatChanges);
            InternalState.i().records().incrementTimesDied();
        }
        return isDead;
    }


    public boolean areAllEnemiesDead() {
        List<BattleEnemy> livingEnemies =battleEnemies.stream().filter(e -> e.getHp() > 0).collect(Collectors.toList());
        return livingEnemies.size() == 0;
    }

    public int getRandomPartyMember() {
        int targetPos = random.nextInt(partyList.getParty().size());
        while(partyList.getParty().get(targetPos).getHp() <= 0) {
            targetPos = random.nextInt(partyList.getParty().size());
        }
        return targetPos;
    }

    public int getRandomEnemy() {
        int targetPos = random.nextInt(battleEnemies.size());
        while(battleEnemies.get(targetPos).getHp() <= 0) {
            targetPos++;
            if(targetPos >= battleEnemies.size()) {
                targetPos = 0;
            }
        }
        return targetPos;
    }

    public void checkIfEnemiesAreDead() {
        if(areAllEnemiesDead()) {
            if(event.getEnemies().contains("warmachina")) {
                InternalState.i().records().incrementWarmachinaDefeated();
            }
            battleEventQueue.add(new BattleEvent("Congrats!  Your party won the battle."));
            displayRewards();
            BattleEvent event = new BattleEvent("");
            partyList.getParty().forEach(EntityInfo::clearAdditionalStats);
            partyList.getParty().stream().map(EntityInfo::getStatusEffects).forEach(StatusEffectList::clearStatChanges);
            event.setFlag(BattleEventFlag.END_BATTLE);
            battleEventQueue.add(event);
            InternalState.i().records().incrementEnemiesDefeated();
            logger.log("ALL ENEMIES ARE DEAD");
        }
    }

    public void displayRewards() {
        //decide what rewards party gets.
        int exp = battleEnemies.stream().map(BattleEnemy::getEnemy).map(Enemy::getExp).reduce(0, Integer::sum);
        int gold = battleEnemies.stream().map(BattleEnemy::getEnemy).map(Enemy::getGold).reduce(0, Integer::sum);
        if(InternalState.i().partyList().partyHasTrait("Shake Out")) {
            gold = gold + random.nextInt(0, 10);
        }
        int item1Threshold = 50;
        int item2Threshold = 90;
        int itemHit = random.nextInt(100);
        String itemString = "!";
        Item item = null;
        if(itemHit > item1Threshold && itemHit < item2Threshold) {
            item = RuneConfiguration.i().getItemFromId(event.getCommonItem());
        } else if(itemHit > item2Threshold) {
            item = RuneConfiguration.i().getItemFromId(event.getRareItem());
        }
        if(item != null) {
            itemString = "! Received "+item.getName()+"!";
            InternalState.i().records().incrementItemsCollected();
            InternalState.i().inventory().getItems().add(item);
        }
        boolean levelGained = InternalState.i().level().incrementExperience(exp);
        InternalState.i().changeGold(gold);
        String raw = "Received "+exp+" EXP and "+gold+" gold"+itemString;
        battleEventQueue.add(new BattleEvent(raw));

        if(levelGained) {
            battleEventQueue.add(new BattleEvent("Congrats!  You've gained a level."));
            InternalState.i().partyList().executeLevelUpBonuses();
        }
    }


    public void handleAutoCharacters() {
        Random random = new Random();
        List<CharacterInfo> autos = partyList.getCharactersWhoAreAuto();
        for(CharacterInfo auto : autos) {
            List<Action> subset = actionDeck.getDeck().stream().filter(a -> a.getCharacterName().equals(auto.getName())).collect(Collectors.toList());
            Action act = subset.get(random.nextInt(subset.size()));
            if(auto.getStatusEffects().hasFlag(AilmentFlag.AUTO)) {
                BattleState.i().setTargetId(random.nextInt(battleEnemies.size()));
                handleAction(act);
            }
            if(auto.getStatusEffects().hasFlag(AilmentFlag.AUTO_HIT_ALL)) {
                int pos = random.nextInt(battleEnemies.size() + partyList.getParty().size());
                int reverse = random.nextInt(100);
                if(reverse < 50) {
                    reverseTargetType(act);
                }
                if(pos < battleEnemies.size()) {
                    BattleState.i().setTargetId(pos);
                } else {
                    BattleState.i().setTargetId(pos - battleEnemies.size());
                }
                handleAction(act);
           }
        }
    }

    public void handleAutoEnemyCharacters() {
        Random random = new Random();
        for(int i = 0; i < battleEnemies.size(); i++) {
            BattleEnemy e = battleEnemies.get(i);
            if(isAutoEnemy(e)) {
                List<EnemyAction> eas = e.getValidEnemyActions();
                EnemyAction eAction = eas.get(random.nextInt(eas.size()));
                Action act = RuneConfiguration.i().actions().getActionFromList(eAction.getActionId()).orElse(new Action());
                if(e.getStatusEffects().hasFlag(AilmentFlag.AUTO)) {
                    BattleState.i().setTargetId(random.nextInt(battleEnemies.size()));
                    act.setCharacterName("e:"+i);
                    handleAction(act);
                }
                if(e.getStatusEffects().hasFlag(AilmentFlag.AUTO_HIT_ALL)) {
                    int pos = random.nextInt(battleEnemies.size() + partyList.getParty().size());
                    int reverse = random.nextInt(100);
                    act.setCharacterName("e:"+i);
                    if(reverse < 50) {
                        reverseTargetType(act);
                    }
                    if(pos < battleEnemies.size()) {
                        BattleState.i().setTargetId(pos);
                    } else {
                        BattleState.i().setTargetId(pos - battleEnemies.size());
                    }
                    handleAction(act);
                }
            }
        }
    }

    public void reverseTargetType(Action act) {
        if(act.getTargetType() == ActionTargetType.ALL) {
            act.setTargetType(ActionTargetType.PARTY_ALL);
        }
        if(act.getTargetType() == ActionTargetType.PARTY_ALL) {
            act.setTargetType(ActionTargetType.ALL);
        }
        if(act.getTargetType() == ActionTargetType.SINGLE) {
            act.setTargetType(ActionTargetType.PARTY_SINGLE);
        }
        if(act.getTargetType() == ActionTargetType.PARTY_SINGLE) {
            act.setTargetType(ActionTargetType.SINGLE);
        }
    }

    private boolean isAutoEnemy(BattleEnemy enemy) {
        for(StatusEffect e : enemy.getStatusEffects().getEffects()) {
            if(e.getAilmentFlag() == AilmentFlag.AUTO || e.getAilmentFlag() == AilmentFlag.AUTO_HIT_ALL) {
                return true;
            }
        }
        return false;
    }

    public boolean playIsBlocked() {
        return battleEventQueue.size() > 0;
    }

    public List<BattleEnemy> getBattleEnemies() {
        return battleEnemies;
    }

    public void setBattleEnemies(List<BattleEnemy> battleEnemies) {
        this.battleEnemies = battleEnemies;
    }
}
