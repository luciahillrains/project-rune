package com.lucylhill.projectrune.services;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.layout.*;

import java.io.InputStream;

public class BackgroundImageFactory {

    public static Background createSceneBackground(String imageResource) {
        return getBackground(imageResource, (int) BackgroundSize.AUTO, (int) BackgroundSize.AUTO, false, true);
    }

    public static Background createImageBackground(String imageResource) {
        return getBackground(imageResource,(int) BackgroundSize.AUTO, (int) BackgroundSize.AUTO, true, false);
    }

    public static Background createEventImage(String imageResource) {
        return getBackground(imageResource, 700, 834, false, false);
    }


    public static Background createHubtownImageBackground(String imageResource) {
        return getBackground(imageResource, 1266, 613, false, false);

    }

    public static Background createTravelImageBackground(String imageResource) {
        return getBackground(imageResource, 1392, 580, false, false);
    }


    public static Background getBackground(String url, int width, int height, boolean contain, boolean cover) {
        InputStream stream = BackgroundImageFactory.class.getResourceAsStream(url);
        if(stream != null) {
            BackgroundImage bImg = new BackgroundImage(new Image(stream), BackgroundRepeat.NO_REPEAT,
                    BackgroundRepeat.NO_REPEAT,
                    BackgroundPosition.CENTER,
                    new BackgroundSize(width, height, false, false, contain, cover));
            return new Background(bImg);
        } else {
            Platform.runLater(() -> {
                Alert error = new Alert(Alert.AlertType.ERROR);
                error.setContentText("getBackground specified URL not found.");
                error.show();
            });
        }
        return null;
    }
}
