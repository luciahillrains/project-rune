package com.lucylhill.projectrune.services;

import com.lucylhill.projectrune.objects.battle.actions.Action;
import com.lucylhill.projectrune.objects.battle.actions.Deck;
import com.lucylhill.projectrune.objects.battle.actions.DeckManagementResult;

public class DeckManagementService {
    public static final int DUPLICATE_MAX = 3;

    public DeckManagementResult duplicateAction(Action activeAction) {
        DeckManagementResult result = new DeckManagementResult();
        Integer originalQuantity = Deck.getQuantities().get(activeAction.getId());
        result.setOriginalQuantity(originalQuantity);
        int numOfDuplicates = originalQuantity - activeAction.getRarity();
        result.setNumberOfDuplicates(numOfDuplicates);
        if(numOfDuplicates < DUPLICATE_MAX) {
            int newQuantity = originalQuantity + 1;
            Deck.getQuantities().put(activeAction.getId(), newQuantity);
            result.setNewQuantity(newQuantity);
            result.setSuccess(true);
        }
        return result;
    }

    public DeckManagementResult removeDuplicate(Action activeAction) {
        DeckManagementResult result = new DeckManagementResult();
        Integer originalQuantity = Deck.getQuantities().get(activeAction.getId());
        result.setOriginalQuantity(originalQuantity);
        int numOfDuplicates = originalQuantity - activeAction.getRarity();
        result.setNumberOfDuplicates(numOfDuplicates);
        if(numOfDuplicates > 0) {
            int newQuantity = originalQuantity - 1;
            result.setNewQuantity(numOfDuplicates);
            Deck.getQuantities().put(activeAction.getId(), newQuantity);
            result.setSuccess(true);
        }
        return result;
    }

    public boolean disableAction(Action activeAction) {
        Integer originalQuantity = Deck.getQuantities().get(activeAction.getId());
        if (originalQuantity == 0) {
            Deck.getQuantities().put(activeAction.getId(), activeAction.getRarity());
            return false;
        } else {
            Deck.getQuantities().put(activeAction.getId(), 0);
            return true;
        }
    }
}
