package com.lucylhill.projectrune.services;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.battle.enemies.Enemy;
import com.lucylhill.projectrune.objects.dungeons.Dungeon;
import com.lucylhill.projectrune.objects.dungeons.DungeonEventList;
import com.lucylhill.projectrune.objects.dungeons.GeneratedDungeon;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.InputStream;
import java.util.List;

public class DungeonGenerator {
    Dungeon dungeon;
    static DungeonGenerator instance;

    public static DungeonGenerator i() {
        if(instance == null) {
            instance = new DungeonGenerator();
        }
        return instance;
    }

    public GeneratedDungeon generateDungeon(GeneratedDungeon oldGeneratedDungeon) {
        GeneratedDungeon generatedDungeon = new GeneratedDungeon(dungeon);
        if(oldGeneratedDungeon == null ) {
            generatedDungeon.setName(dungeon.getName() + " F1");
        } else {
            generatedDungeon.setCurrentFloor(oldGeneratedDungeon.getCurrentFloor() + 1);
            generatedDungeon.setName(dungeon.getName() + " F" + generatedDungeon.getCurrentFloor());
        }
        generatedDungeon.setEvents(loadDungeonEvents());
        List<Enemy> dungeonEnemies = RuneConfiguration.i().enemies().getEnemiesForDungeon(dungeon.getId());
        generatedDungeon.setEnemies(dungeonEnemies);
        generatedDungeon.setBattleBackgroundType(dungeon.getBattleBackgroundType());
        generatedDungeon.setBackground(dungeon.getBackground());
        generatedDungeon.setMusic(dungeon.getMusic());
        return generatedDungeon;
    }

    public DungeonEventList loadDungeonEvents() {
            InputStream inputStream = RuneConfiguration.class.getResourceAsStream("/properties/dungeons/"+dungeon.getId()+"-events.yml");
            Yaml yaml = new Yaml(new Constructor(DungeonEventList.class));
            return yaml.load(inputStream);
    }

    public void setDungeon(Dungeon dungeon) {
        this.dungeon = dungeon;
    }
}
