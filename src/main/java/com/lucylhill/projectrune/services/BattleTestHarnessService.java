package com.lucylhill.projectrune.services;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.battle.BattleState;
import com.lucylhill.projectrune.objects.battle.actions.Action;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.characters.CharacterInfo;
import com.lucylhill.projectrune.objects.characters.CharacterType;

import java.util.List;
import java.util.Optional;

public class BattleTestHarnessService {

    public void initialize() {
        InternalState.i().partyList().getParty().clear();
        for(int i = 0; i < 4; i++) {
            CharacterInfo c = RuneConfiguration.i().partyMembers().getCharacterInfo(RuneConfiguration.i().partyMembers().getPartyMembers().get(i).getName());
            if(i == 0) {
                c.setType(CharacterType.LEADER);
            }
            if(i == 1) {
                List<StatusEffect> statusEffects = c.getStatusEffects().getEffects();
//                StatusEffect defenseDebuff = new StatusEffect();
//                defenseDebuff.setName("DEBUG DEFENSE DEBUFF");
//                defenseDebuff.setStat(Stat.DEFENSE);
//                defenseDebuff.setTurns(99);
//                StatusEffect offenseDebuff = new StatusEffect();
//                offenseDebuff.setName("DEBUG ATTACK DEBUFF");
//                offenseDebuff.setStat(Stat.ATTACK);
//                offenseDebuff.setTurns(99);
//                statusEffects.add(defenseDebuff);
//                statusEffects.add(offenseDebuff);
                c.setHp(3);
            }
            if(i == 2) {
                List<StatusEffect> statusEffects = c.getStatusEffects().getEffects();
                StatusEffect poison = RuneConfiguration.i().ailmentList().getStatusEffect("Poison");
                StatusEffect blind = RuneConfiguration.i().ailmentList().getStatusEffect("Blind");
                statusEffects.add(poison);
                statusEffects.add(blind);
                c.setHp(10);
            }
            if(i == 3) {
                c.setHp(0);
            }
            InternalState.i().partyList().getParty().add(c);
        }
        if(InternalState.i().configuration().getDebugConfig().getPreviousAction() != null && !InternalState.i().configuration().getDebugConfig().getPreviousAction().equals("None")) {
            String actionId = InternalState.i().configuration().getDebugConfig().getPreviousAction();
            Optional<Action> possibleAction = RuneConfiguration.i().actions().getActionFromList(actionId);
            BattleState.i().getPreviousActions().add(possibleAction.orElse(new Action()));
        }
    }
}
