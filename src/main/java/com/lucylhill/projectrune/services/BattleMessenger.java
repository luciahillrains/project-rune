package com.lucylhill.projectrune.services;

public class BattleMessenger {
    static BattleMessenger instance;
    String targetPartyName;
    String sourceName;
    String actionName;
    int partyPosition;
    boolean isTargetEnemy;
    boolean newInfo = false;

    DebugLogger logger = new DebugLogger(this.getClass());

    public static BattleMessenger i() {
        if(instance == null) {
            instance = new BattleMessenger();
        }
        return instance;
    }

    public void setMessage(String targetPartyName, int partyPosition, boolean isEnemy) {
        newInfo = true;
        logger.log(this.targetPartyName);
        this.targetPartyName = targetPartyName;
        this.partyPosition = partyPosition;
        this.isTargetEnemy = isEnemy;
    }

    public String getTargetPartyName() {
        return targetPartyName;
    }

    public void setTargetPartyName(String targetPartyName) {
        this.targetPartyName = targetPartyName;
    }

    public int getPartyPosition() {
        return partyPosition;
    }

    public void setPartyPosition(int partyPosition) {
        this.partyPosition = partyPosition;
    }

    public boolean isTargetEnemy() {
        return isTargetEnemy;
    }

    public void setTargetEnemy(boolean targetEnemy) {
        isTargetEnemy = targetEnemy;
    }

    public boolean isNewInfo() {
        return newInfo;
    }

    public void setNewInfo(boolean newInfo) {
        this.newInfo = newInfo;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }
}
