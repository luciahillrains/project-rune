package com.lucylhill.projectrune.services;

import com.lucylhill.projectrune.objects.achievements.AchievementList;
import com.lucylhill.projectrune.objects.achievements.Records;
import com.lucylhill.projectrune.objects.battle.actions.Deck;
import com.lucylhill.projectrune.objects.battle.status.StatusEffect;
import com.lucylhill.projectrune.objects.battle.status.StatusEffectList;
import com.lucylhill.projectrune.objects.characters.Stat;
import com.lucylhill.projectrune.objects.classes.Aspects;
import com.lucylhill.projectrune.objects.configuration.Configuration;
import com.lucylhill.projectrune.objects.dungeons.Checkpoint;
import com.lucylhill.projectrune.objects.dungeons.Coordinate;
import com.lucylhill.projectrune.objects.dungeons.GeneratedDungeon;
import com.lucylhill.projectrune.objects.items.Item;
import com.lucylhill.projectrune.objects.items.ItemList;
import com.lucylhill.projectrune.objects.quests.InterstitialEvent;
import com.lucylhill.projectrune.objects.misc.Level;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class InternalState {
    PartyList partyList = new PartyList();
    Configuration configuration = new Configuration();
    Level level = new Level();
    Aspects lastUsedAspect = Aspects.NONE;
    StatusEffectList globalStatusEffects = new StatusEffectList();
    Checkpoint checkpoint;
    ItemList inventory = new ItemList();
    ItemList storedItems = new ItemList();
    Records records = new Records();
    Deck deck;
    int gold;
    int storedGold;
    AchievementList achievements = new AchievementList();
    boolean battleTargetingMode = false;
    boolean partyTargeting = false;
    boolean deadTargetting = false;
    String currentDungeon = "";
    List<String> switches = new ArrayList<>();
    InterstitialEvent currentInterstitial;

    static InternalState instance;

    public static InternalState i() {
        if (instance == null) {
            instance = new InternalState();
        }
        return instance;
    }

    public PartyList partyList() {
        return partyList;
    }

    public Configuration configuration() {
        return configuration;
    }

    public Level level() { return level; }

    public boolean targetMode() { return battleTargetingMode; }

    public void setTargetMode(boolean t) { this.battleTargetingMode = t; }

    public boolean isPartyTargeting() {
        return partyTargeting;
    }

    public void setPartyTargeting(boolean partyTargeting) {
        this.partyTargeting = partyTargeting;
    }

    public Aspects getLastUsedAspect() {
        return lastUsedAspect;
    }

    public void setLastUsedAspect(Aspects lastUsedAspect) {
        this.lastUsedAspect = lastUsedAspect;
    }

    public boolean isDeadTargetting() {
        return deadTargetting;
    }

    public void setDeadTargetting(boolean deadTargetting) {
        this.deadTargetting = deadTargetting;
    }

    public List<Aspects> getListOfAspectsDisabled() {
        return globalStatusEffects.getEffects().stream()
                .filter(e -> e.getStat() == Stat.ALIGNMENT && e.getChange() < 0)
                .map(e -> Aspects.valueOf(e.getName()))
                .collect(Collectors.toList());
    }

    public List<String> getListOfDisabledActions() {
        return globalStatusEffects.getEffects().stream()
                .filter(e -> e.getStat() == Stat.DISABLE_ACTION)
                .map(StatusEffect::getName)
                .collect(Collectors.toList());
    }

    public StatusEffectList getGlobalStatusEffects() {
        return globalStatusEffects;
    }


    public void setGlobalStatusEffects(StatusEffectList globalStatusEffects) {
        this.globalStatusEffects = globalStatusEffects;
    }

    public void setCheckpoint(Coordinate coordinate, GeneratedDungeon generatedDungeon) {
        checkpoint = new Checkpoint(coordinate);
    }

    public Checkpoint checkpoint() {
        return checkpoint;
    }

    public int gold() { return gold; }

    public void changeGold(int delta) {
        gold = gold + delta;
    }

    public ItemList inventory() { return inventory; }

    public Deck deck() {
        return deck;
    }

    public void setDeck(Deck d) {
        deck = d;
    }

    public int getCurrentChapter() {
        return (int) inventory().getItems().stream().filter(Item::isRune).count();
    }

    public ItemList storedItems() { return storedItems; }

    public int storedGold() { return storedGold; }

    public void storeGold(int delta) {
        storedGold = storedGold + delta;
    }

    public Records records() { return records; }

    public AchievementList achievements() { return achievements; }

    public String getCurrentDungeon() {
        return currentDungeon;
    }

    public void setCurrentDungeon(String currentDungeon) {
        this.currentDungeon = currentDungeon;
    }

    public List<String> switches() {
        return switches;
    }

    public void setConfiguration(Configuration c) {
        configuration = c;
    }

    public InterstitialEvent getCurrentInterstitial() {
        return currentInterstitial;
    }

    public void setCurrentInterstitial(InterstitialEvent currentInterstitial) {
        this.currentInterstitial = currentInterstitial;
    }
}
