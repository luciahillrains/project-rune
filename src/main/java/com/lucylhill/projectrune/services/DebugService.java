package com.lucylhill.projectrune.services;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import com.lucylhill.projectrune.objects.achievements.Achievement;
import com.lucylhill.projectrune.objects.items.ItemList;
import javafx.scene.Node;
import javafx.scene.control.Alert;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class DebugService {

    public void fillInventory() {
        ItemList items = RuneConfiguration.i().items();
        ItemList equipment = RuneConfiguration.i().equipment();
        ItemList relics = RuneConfiguration.i().relics();

        //TEMPORARY MEASURE
        //don't add runes to inventory
        InternalState.i().inventory().getItems().addAll(items.getItems().stream().filter(i -> !i.isRune()).collect(Collectors.toList()));
        InternalState.i().inventory().getItems().addAll(equipment.getItems());
        InternalState.i().inventory().getItems().addAll(relics.getItems());

        InternalState.i().changeGold(10000);
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText("Inventory is filled.");
        alert.show();
    }

    public void returnToMainScreen(Node node) throws IOException {
        SceneSwitcher.switchScreens(node, SceneSwitcher.START_SCREEN);
    }

    public void addAchievements() {
        List<Achievement> subset = RuneConfiguration.i().achievements().getAchievements().subList(0,4);
        InternalState.i().achievements().getAchievements().addAll(subset);
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText("Achievements were added.");
        alert.show();
    }
}
