package com.lucylhill.projectrune.services;

import com.lucylhill.projectrune.objects.items.Equipment;
import com.lucylhill.projectrune.objects.items.Item;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ItemService {

    public void discardItem(String itemId) {
        List<Item> inventory = InternalState.i().inventory().getItems();
        Item toRemove = null;
        for(Item i : inventory) {
            if(i.getId().equals(itemId)) {
                toRemove = i;
                break;
            }
        }
        if(toRemove != null) {
            InternalState.i().inventory().getItems().remove(toRemove);
        }
    }

    public void addItem(Item toAdd) {
        InternalState.i().inventory().getItems().add(toAdd);
    }
}
