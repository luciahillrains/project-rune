package com.lucylhill.projectrune.services;

import com.lucylhill.projectrune.configuration.RuneConfiguration;

public class DebugLogger {
    String classString;

    public DebugLogger(Class clazz) {
        classString = clazz.getName();
    }
    public void log(String log) {
        if(RuneConfiguration.i().systemProperties().isDebug()) {
            System.out.println(classString + ":"+log);
        }
    }
}
