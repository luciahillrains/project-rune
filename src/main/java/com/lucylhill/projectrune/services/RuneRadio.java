package com.lucylhill.projectrune.services;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import java.io.File;
import java.net.URL;

public class RuneRadio {

    private static RuneRadio instance;

    MediaPlayer mediaPlayer;
    String currentMedia = "";

    public static String ALBION_DUNGEON = "/music/albionguidestonecellars.mp3";
    public static String BATTLE = "/music/battle.mp3";
    public static String BOSS = "/music/boss.mp3";
    public static String CROOKED_TOWER = "/music/crookedtower.mp3";
    public static String DARK_CHAPEL = "/music/darkchapel.mp3";
    public static String TRAINING_GROUND = "/music/deepblue.mp3";
    public static String FINAL_BATTLE = "/music/finalbattles.mp3";
    public static String HUBTOWN = "/music/hubtown.mp3";
    public static String LOADING = "/music/loading.mp3";
    public static String TITLE = "/music/lullingoftheleaves.mp3";
    public static String ROYAL_TOMBS ="/music/royaltombs.mp3";
    public static String RUINED_LIBRARY = "/music/ruinedlibrary.mp3";
    public static String OASIS = "/music/sadresolution.mp3";
    public static String SKY_GARDENS = "/music/skygardens.mp3";
    public static String TARTARUS = "/music/tartarus.mp3";
    public static String TOWER_OF_BABEL = "/music/towerofbabel.mp3";

    private RuneRadio() {}

    public static RuneRadio i() {
        if(instance == null) {
            instance = new RuneRadio();
        }
        return instance;
    }


    public void play(String media) {
        if(InternalState.i().configuration.getSoundConfig().isMuteMusic()) {
            return;
        }
        if(!media.equals(currentMedia)) {
            currentMedia = media;
            if(mediaPlayer != null) {
                mediaPlayer.stop();
            }
            URL resource = RuneRadio.class.getResource(media);
            if(resource != null) {
                Media mediaFile = new Media(resource.toString());
                mediaPlayer = new MediaPlayer(mediaFile);
                mediaPlayer.setOnEndOfMedia(() -> mediaPlayer.seek(Duration.ZERO));
                mediaPlayer.setVolume(InternalState.i().configuration().getSoundConfig().getVolume()/100);
                mediaPlayer.play();
            } else {
                Platform.runLater(() -> {
                    Alert mediaNotFound = new Alert(Alert.AlertType.ERROR);
                    mediaNotFound.setContentText("Media file not found.");
                    mediaNotFound.show();
                });
            }
        }
    }

    public void stop() {
        currentMedia = "";
        mediaPlayer.stop();
    }




}
