package com.lucylhill.projectrune.configuration;

import com.lucylhill.projectrune.objects.classes.ClassList;
import com.lucylhill.projectrune.objects.traits.TraitList;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RuneConfigurationTest {
    RuneConfiguration config = new RuneConfiguration();

    @Test
    public void testLoadingClassList() {
        ClassList cl = config.classList();
        assertEquals(6, cl.getClasses().size());
    }

    @Test
    public void testLoadingTraitList() {
        TraitList tl = config.traitList();
        assertEquals(53, tl.getTraits().size());
    }
}