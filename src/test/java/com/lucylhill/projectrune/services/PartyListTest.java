package com.lucylhill.projectrune.services;

import com.lucylhill.projectrune.objects.characters.CharacterType;
import com.lucylhill.projectrune.objects.classes.ClassInfo;
import com.lucylhill.projectrune.objects.system.SystemProperties;
import com.lucylhill.projectrune.objects.traits.Trait;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import com.lucylhill.projectrune.objects.characters.CharacterInfo;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class PartyListTest {
    SystemProperties props = new SystemProperties();
    PartyList partyList;

    @BeforeAll
    public void before() {
        props.setMaxNumOfCharactersInParty(4);
        partyList = new PartyList();
    }
    @Test
    public void partyHasCharacterWithFlagReturnsFalseOrTrueAccordingly() {
        buildPartyOfX(1);
        assertTrue(partyList.partyHasCharacterWithClassOrTraitFlag("TEST_TRAIT"));
        assertTrue(partyList.partyHasCharacterWithClassOrTraitFlag("TEST"));
        assertFalse(partyList.partyHasCharacterWithClassOrTraitFlag("FLAG"));
    }

    @Test
    public void getCharacterWithFlagReturnsOptionalEmptyOrNotAccordingly() {
        buildPartyOfX(2);
        Optional<CharacterInfo> ch = partyList.getCharacterWithClassOrTraitFlag("TEST_TRAIT");
        Optional<CharacterInfo> ch2 = partyList.getCharacterWithClassOrTraitFlag("TEST");
        Optional<CharacterInfo> ch3 = partyList.getCharacterWithClassOrTraitFlag("DUMMY_TRAIT");
        Optional<CharacterInfo> ch4 = partyList.getCharacterWithClassOrTraitFlag("DUMMY");
        Optional<CharacterInfo> ch5 = partyList.getCharacterWithClassOrTraitFlag("SOMETHINGELSE");

        assertEquals("Char0", ch.get().getName());
        assertEquals("Char0", ch2.get().getName());
        assertEquals("Char1", ch3.get().getName());
        assertEquals("Char1", ch4.get().getName());
        assertTrue(ch5.isEmpty());
    }
    
    @Test
    public void testAddPartyLimits() {
        CharacterInfo c = new CharacterInfo();
        buildPartyOfX(1);
        assertTrue(partyList.addMemberToParty(c));
        assertTrue(partyList.addMemberToParty(c));
        assertTrue(partyList.addMemberToParty(c));
        assertEquals(4, partyList.getParty().size());
        assertFalse(partyList.addMemberToParty(c));
        assertEquals(4, partyList.getParty().size());
    }

    public void buildPartyOfX(int num) {
        this.partyList.setParty(new ArrayList<>());
        ClassInfo cla1 = new ClassInfo();
        ClassInfo cla2 = new ClassInfo();
        Trait t1 = new Trait();
        Trait t2 = new Trait();
        t1.setName("Dummy Trait");
        //t1.setFlag("DUMMY_TRAIT");
        t2.setName("Test Trait");
        //t2.setFlag("TEST_TRAIT");
        cla1.setName("Dummy Class");
        cla1.setFlag("DUMMY");
        cla2.setName("Test Class");
        cla2.setFlag("TEST");

        for(int i = 0; i < num; i++) {
            Trait t = i % 2 != 0 ? t1 : t2;
            ClassInfo c = i %2  != 0 ? cla1 : cla2;

            CharacterInfo ch =new CharacterInfo()
                    .setCharacterName("Char"+i)
                    .setType(i == 0 ? CharacterType.LEADER : CharacterType.MEMBER)
                    .setClassInfo(c)
                    .setTrait(t);
            partyList.getParty().add(ch);
        }
    }
}