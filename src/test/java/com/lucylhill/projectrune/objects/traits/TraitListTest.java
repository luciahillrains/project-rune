package com.lucylhill.projectrune.objects.traits;

import com.lucylhill.projectrune.configuration.RuneConfiguration;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class TraitListTest {
    TraitList traitList = new TraitList();

    @Test
    public void getRandomTraitsReturnsNumberOfTraitsThatEqualParameter() {
        RuneConfiguration runeConfiguration = new RuneConfiguration();
        traitList = runeConfiguration.traitList();
        List<Trait> randomList = traitList.getRandomTraits(6);
        assertEquals(6, randomList.size());
    }

    @Test
    public void getTraitByNameReturnsTraitByName() {
        traitList.setTraits(new ArrayList<>());
        Trait t1 = new Trait();
        t1.setName("Dummy");
        Trait t2 = new Trait();
        t2.setName("Test");
        traitList.getTraits().add(t1);
        traitList.getTraits().add(t2);
        traitList.getTraits().add(t1);

        Optional<Trait> actual = traitList.getTraitByName("Test");
        assertEquals("Test", actual.get().getName());
    }

    @Test
    public void getTraitByNameReturnsEmptyOptionalIfNotFound() {
        traitList.setTraits(new ArrayList<>());
        Optional<Trait> actual = traitList.getTraitByName("Test");
        assertTrue(actual.isEmpty());
    }

    @Test
    public void getTraitByFlagReturnsTraitByFlag() {
        traitList.setTraits(new ArrayList<>());
        Trait t1 = new Trait();
        Trait t2 = new Trait();
        traitList.getTraits().add(t1);
        traitList.getTraits().add(t1);
        traitList.getTraits().add(t2);
        traitList.getTraits().add(t1);

        assertTrue(true);
    }

    @Test
    public void getTraitByFlagReturnsEmptyOptionalIfNotFound() {
        traitList.setTraits(new ArrayList<>());
        assertTrue(traitList.getTraits().isEmpty());
    }

}