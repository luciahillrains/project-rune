package com.lucylhill.projectrune.objects.classes;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ClassListTest {
    ClassList classList = new ClassList();

    @Test
    public void getBasicClassesOnlyReturnsBasicClasses() {
        classList.setClasses(new ArrayList<>());
        ClassInfo basic = new ClassInfo();
        basic.setJobEligibility(ClassType.BASIC);
        ClassInfo level = new ClassInfo();
        level.setJobEligibility(ClassType.LEVEL);
        ClassInfo job = new ClassInfo();
        job.setJobEligibility(ClassType.BOOK);
        classList.getClasses().add(basic);
        classList.getClasses().add(basic);
        classList.getClasses().add(level);
        classList.getClasses().add(job);

        assertEquals(2, classList.getBasicClasses().size());
    }

    @Test
    public void getClassByNameGetsClassByNameIfThere() {
        classList.setClasses(new ArrayList<>());
        ClassInfo c1 = new ClassInfo();
        c1.setName("Test");
        ClassInfo c2 = new ClassInfo();
        c2.setName("Dummy");
        classList.getClasses().add(c1);
        classList.getClasses().add(c2);
        Optional<ClassInfo> actual = classList.getClassByName("Dummy");
        assertTrue(actual.isPresent());
        assertEquals("Dummy", actual.get().getName());
    }

    @Test
    public void getClassByNameReturnsEmptyOptionalifNotThere() {
        classList.setClasses(new ArrayList<>());
        Optional<ClassInfo> actual = classList.getClassByName("Test");
        assertTrue(actual.isEmpty());
    }

    @Test
    public void getRandomClassReturnsAValidRandomClass() {
        classList.setClasses(new ArrayList<>());
        classList.getClasses().add(new ClassInfo());
        ClassInfo actual = classList.getRandomClass();
        assertNotNull(actual);
    }
}