# All Below the Tower of Babel
(Gaia Gaiden Volume 1)


[ This project is dead and is now uploaded for benefits of my resume. ]

Build: Alpha 4

A rogue-lite party-based dungeon exploration card battler with text adventure elements that take place in an alternate fantasy version of Europe, North Africa and the Middle East.

## Plot

Your family lived in Gallia Celtica and your mother used to read you stories from the Holy Book, Tome of the Runes.
The Tome talked about the Master Runes, six Runes that would grant the owner power, provided you make your way to the
top of the infernal Tower of Babel.

Unfortunately, when you were 9, a civil war broke out in Gaul, and your parents died.  You and your siblings were
seperated, and you made your way to the town of Kastelstad, a small city ruled by the Elder Ephesus.  You did small jobs there
until you were of age to become a mercenary.

Sick of the constant war, you decide to see if these Runes were for real.


## Planned Features
- A rogue-like card battler dungeon exploration game that takes place in an alternate history
- Over 200 character traits
- Over 70 classes that play differently from each other
- Save & Pick Up Anywhere
- Localization
- Be able to play on most systems
- Enthralling & immersive story telling.
- Achievements
- A leveling system, so unlike many roguelikes, you never lose your story progress on death.
- PVP

## Roadmap

We are currently planning the alpha phase.

- Alpha 1
  - create a leader
  - recruit a party
  - explore a very basic dungeon
- **Alpha 2**
  - battles
  - class passives, class actions
  - leveling is implemented and in the background.
  - talking to NPCs in the tavern
  - difficulty configuration
  - change deck
- Alpha 3
    - earn gold and exp from events and battles
    - buy items from markets and merchants in dungeon
    - get treasure and runes in events and chests
    - leveling is visible, but no level restrictions
    - achievements
    - framework in place for localization
    - party morale framework
- Alpha 4
  - all hub town actions implemented
  - unlock classes and perks thrugh level
  - implement tutorial/first story beats
  - implement class books
  - turn limit is visible, inn takes one night, traveling takes X turns.
  - saving/loading
  - music
  - modern UI



Alpha 4 is the planned last alpha.  After the alpha phase, comes the beta phase, which will be iterative, frequent builds based on story and dungeon implmentation.
## Changes
### Alpha 4
- major changes
  - new and beautiful UI
- minor changes
  - when using the scene picker in the debug menu, it'll create a runeseeker for you
  - added Travel, Guild & Your Home to the scene picker.
  - added hit rate to party screen.
  - have party bar appear when you have the party view up.
- fixes
  - fixed issue where party members race/gender do not appear on buttons in the guild screen
  - fix issue where showing Item menu before Deck menu in "Your Home" would not hide the Item menu.
  - you can get a gameover from the dungeon itself, as a result of events.
  - fixed the hit algorithm to be a bit more fair
  - fix cavern3 background from never appearing
### Alpha 3
- major changes
  - this game is now known as "Runeseeker".  In official circumstances, it might be referred to it's full title "Gaia Gaiden Chapter 1: Runeseeker".  For short, and for most purposes, it'll be known as "Runeseeker".
  - default resolution is 1920x1080
    - when using full screen, you'll get a border around the game.
  - overhauled how the game handles actions.
    - this allows us to write little independent modules for each action, passive, trait, and status ailment, meaning they can be very creative or advanced in what they do.
    - as a result, existing action, changes, passives and traits have been overhauled and re-examined. Many changes to this were made, that are not listed here.
    - there have been battle system tweaks that allow this overhaul to happen.
  - battle system overhaul
    - there might be some small bugs related to this
  - added the Jester, Mystic Knight, Monk, Chirugeon & Astrologist classes
    - fun fact: the Jester class was supposed to be in the last release, but removed last minute due to design issues in regards to the skills.  The skills were more bombastic in nature, which involved changing ailments, and even changing actual action effects permamently.
    - The Jester class was also one big Kefka from Final Fantasy VI reference and that was removed because Kefka wasn't a jester but a very powerful magitek knight.
  - traits are overhauled
    - traits have been redesigned to fit within certain niches, with range of traits within each niche
    - as a result, there are 114 new traits
    - traits no longer have any effect during dungeon events except for charisma checks
    - a separate system called "Player Skills" will be designed and developed at a later date for various different checks.
  - added consumable items
    - you can use items on the field through the item menu, or in battle (they will be added to your deck).
    - you will be able to take/store items in Your Home.
  - added equipment
    - weapons will add attack, intelligence, agility
    - body equipment will add defense and maybe intelligence
  - added relics
    - special accessories that tend to do "bigger things" then just equipment alone
    - you can equip up to 3 on a member
  - added an item menu and an equip menu in the dungeon to manage these.
  - items that steal or morph enemies have been implemented.
  - get exp, gold and items from battles
  - level is set to 1 at the start of battle.  There is a new debug option to increase level to 99.
  - added a marketplace
    - crafting and smithing on demand will be added in the next release.
  - you can buy items from the tavern
  - you can store items and gold at your house.
  - removed random characters, now the characters you get have been pre-designed with their own backstories and personality.
  - dungeon events can have checks based on stats
- minor changes
  - aspect vulnerabilities have changed slightly, now the type of action is dependent.  Furthermore, a mechanic similar to Pokemon's "STAB Damage" has been added, where an action that shares an alignment with its user will do slightly more damage.
  - ui tweaks to font size & image size across the board.  This combined with the new resolution is known as "Dragon UI", a step up in terms of the game UI.
  - action, passives and traits are rewritten to use potency when talking about damage. 
  - Gladiator's Heavy Strike now increases enmity.
  - added heavy status to some enemies, meaning certain actions & status ailments won't work on them
  - a lot of tech debt repaid.  most things should be not seen, but there might be bugs and issues from a couple of items. 
  - added tile art for the dungeon map
  - the travel to screen has been changed to be more obvious to use - click the dungeon, and click the go to dungeon button to actually go to the dungeon (as opposed to hovering and clicking).
  - reorganized battle screen, especially how text and health appears over each enemy.
  - changed some terms
    - amplification is now elemental offense
    - deamplification is now elemental defense
      - no idea why it took me so long to realize those terms make more sense
  - changed Witch's "Witch's Mix" to be a  self-buff
  - changed Smithy's "Synthesis" to be "Morph".  Now, the plan is to do synthesis in town or at camp (with a full-level smithy)
  - changed coming soon screen's credits
  - gold and exp are displayed on the UI.
  - added new fonts in some locations
  - events can no be locked, and events can also provide switches (untested)
  - redesigned the existing dungeon events.
- fixes
  - configuration screen doesn't default to debug options
  - guild screen has a background
  - fix issue where game crashes when you leave the dungeon
    - the fun part of being an alpha build...
  - party bar disappears when viewing the party status screen and vice versa
  - not necessarily a fix, more like a design change, but battles are not fought again after stepping over the space.
  - before, elemental offense and defense were not calculated correctly.  this was changed so that elemental offense & defense are calculated like any other stat.
  - fixed not clearing enemy and enemy stats after battle
  - fixed typos regarding the Idealism trait
  - reworded the dungeon tutorial text
  - possibly fixed an issue where game crashes when doing an event or battle after hitting two double spaces.
  - possibly fixed weirdness when running away
  - fixed enemies attacking dead members.
  - fixed timing issues with battle screen
  - fixed party result labels not lining up with their boxes.
  - added a debug escape button to return to the start.
  - if an error occurs, you'll see an error message.  (Note: not all errors will have this and some will still crash the game.)
  - leave house button in "your house" will take you back to the hub world.
  - status labels on the party bar are now the same size.
  - fixed issue where player won't get their first turn in.
  - Intelligence buff/debuff reflected on the party bar.
  - made it so that there's always a result if an action missed.
  - fixed Heal's description to fit it's result.
  - changed party bar status labels to accurately reflect results.
  - clear enemy status labels if they die.
  -fix issue where battle crashes after an auto enemy attacks.
  -clear out text labels in the hand component after play.
  -changed how auto characters get actions.
  - possibly fixed a rare issue where an action gets null and get pushed into the hand component.
  - added background for the travel screen.
### Alpha 2
- major changes
  - implemented difficulty configuration
  - debug mode
  - added these classes: Shaman, Smithy, Witch, Bard, Musketeer Archer & Gladiator
    - because of how leveling is implemented currently, you will see this classes in the character editor for now.
  - added these traits: Firespirited, Icespirited, Waterspirited, Airspirited, Earthspirited, Lightspirited, Darkspirited, Heart of Gold, Friendly, Persuasive, Hyped Up, Strongminded, Weakminded, Village Drunk, Village Idiot, Ambitious, Weakling, Nerdy
  - added a leveling system!
    - leveling will determine what classes are available to you at character creation/character generation, as well as items you can buy in the market and what passives/actions your party members have.
    - for now, it's set at lvl 99.
    - you will not gain exp from battle.
  - added a basic concept of enemy enmity.  Currently, if you use a skill that effects an enemy's enmity towards the party member who used the skill, they'll be targeted.  This will be expanded in a future update.
  - a more in-depth version of party status is available from dungeon screen.
  - battles, actions and passives are implemented!
    - a big update to this is planned for Alpha 3, which will allow more flexibility to what actions do.  As a result, in Alpha 3, existing and new actions will be re-examined and changed.
  - added the tavern to the hubworld
    - the tavern is a place to go to talk to people and get information about the world you live in, as well as your next goal.  You can also buy alcohol here (in a future update).
    - Conversations are given to the player depending on level and progress in the game.
    - currently, you can only talk to test NPC and the bartender, but more will be added later.
  - added "Your Home" to the hub world
    - this is where you can go right now to manage your deck.  You can duplicate an action (up to 3 times), remove duplicates & disable actions completely.
    - in the future, you can store items/gold/relics/runes and take them as needed.
- minor changes
  - added new art
  - added art to the coming soon/credits.
  - added symbols to the dungeon squares.
  - added beast races as playable races.
  - more developer control when it comes to creating dungeons.
  - added a space on the dungeon map where no events happen.
  - made your "view" in the dungeon bigger
  - you can't revisit squares you already visited.
  - Socially anxious trait involves *ALL* dungeon events.
  - characters have a charisma, agility & intelligence trait now.
  - "Traveler" trait is now "Escape Artist" trait, and removed the "dungeon roll" part of the description.
  - added more names to generation 
  - alignments are on the party bar now
  - removed Deck button from the dungeon bar.
  - dungeon events are displayed differently for better art aspect
- fixes:
  - fixed issue where some trait texts are cut off
  - quick start will always remove a pre-existing party.
  - fixed bug where you try to continue without selecting a choice in some dungeon events, causing an error to occur.
  - fixed issue where sometimes the right dungeon square in map won't enable
  - fixed issue where some dungeon squares won't enable.
  - Fixed a typo in "Lawful" description
  - Fixed a typo in "Nihilist" description
  - Fixed a typo in "Carnivore" and "Vegan" description
  - Reworded the "Egalitarian" trait description
  - Fixed a typo in the "Rogue" class description
  - changed the text of the dungeon tutorial to fit with how dungeon exploration is today.

### Alpha 1
Our first released build!
- 53 traits
- 6 classes
- Form a party
- Be able to explore the Ruined Castle/Alpha Dungeon, which shows how dungeon exploration happens.
- No battling or items
- See planned buildings in the hub world

This is an early build, and it's definitely a work in progress.

## Sub Build Notes
- 7.22 featured a different battle system, that involved clicking through individual events.  This was awkward and unwiedly, so it was changed back to automatic timers.

## Personal History
1. This is a combination of a bunch of unfinished projects
   1. The presentation and character creation comes from Doors, an unfinished text RPG.
   2. The presentation of the dungeons come from Portals of Peril, an unreleased card game I worked on with Brian Colon, as well as a Dice RPG I finished but never released.
   3. The hub world presentation comes from an unfinished RPG I worked on.
   4. The class system comes from an RPG concept that was designed but not finished.
2. Originally 12 dungeons were created, each with their own runes.  This was changed to 6 "story" dungeons.  The other runes are not cut and will be used in an endgame dungeon.
3. Class Changes:
   1. Shaman was originally designed to be just a basic magic class without any ailment-causing.  It's main mechanic was that all its actions would use what AP was left over, and that determined the power of their cards.  This version of Shaman was also Dark Vile.
   2. Witch turned into more of a ailment class as well.
4. Originally Alpha 2 was going to introduce Jester, Chirugeon, Astrologist, Mystic Knight & Monk, but you'll see those in Alpha 3.
5. Fighter was changed the most from design to implementation. The training perk was a gauranteed Attack buff, and then Internal Release would have a base damage of the current attack minus 5, then reset the attack for the fighter.  This was too complicated for the current action system, so it was simplified.
6. The Shaman action "En Vitra" was more complex in it's design, where cast it once, it turns the target into a zombie, cast it again and it removes the zombie and recovers them to 50%.  Now it fully recovers a fallen ally, but it turns them into a zombie.  There are ways to remove zombie status planned.
7. Another class that was changed a lot was Witch.  Originally designed as a DPS Support with a penchant for adding damage over time, there was a need for healers in this next set of classes, so it became a regen-based healing with a rare, hard hitting single target action.  Only skills that weren't changed were Magic Touch and Witch's Mix.

##Things to look forward to
1. Items, Relics, Treasure and Runes
   1. These will be implemented in next Alpha
      1. Items are consumeables.  In battle, they'll appear in your hand (one action per item), or you can use them outside of battle.
      2. Relics provide some sort passive ability across the entire party.
      3. Runes are super powerful abilities that'll be added to your deck.  They're passives, but have to be activated to use.  (Example: increase all HP of your party to 100, etc.)
      4. Treasures are fun things modeled after real world artifacts that can be sold for both gold & exp.
2. Modern UI
   1. the modern UI is a new UI developed by the folks at Novy Unlimited.  The goal of it is to simply make the game present and feel build.
   2. The plan is to start merging Modern UI at the end of Alpha 3/Alpha 4
3. Morale & Reputation
   1. In Alpha 3, your party will have morale.  When your party has low morale, the chances of individuals leaving is greater.
   2. Your Morale also plays into your reputation.  If your party morale is low, so is your own reputation, making it harder to recruit people on your quest.  And the people who may join you...you may not want.
4. Story
   1. After Alpha 4, the plan on the game is entirely on the story.  
   2. The plan is to have several releases which will be official Beta releases, that'll feature part of the story, fixes, etc.
   3. These Betas will start at .1 and go to .99.
   4. Final release will have all seven chapters of content and a clear end, and will be 1.0
   5. Part of the first beta (.10) will be a tutorial section.
5. Post Game & PVP
   1. Two major things are planned after the final release.
      1. The first is the Tower of Tartarus, an underground tower leading you to the depths of hell.  This presents a post game challenge, as it's planned to be very difficult without mastering traits/classes/runes.
         1. Tower of Tartarus will feature 30 floors, 6 new runes that you'll have to find, and even more treasure to acquire.
         2. Treasure from the postgame can be taken to new runs in the main game.
      2. The second major addition will be a simple, asynchronous PVP mode, called the Proving Grounds.
         1. Choose your party, and send them off against player's party.  See the battle in real time.
            1. Currently, the plan is that the battles happen without your control.  This may change in the future.  As a result, it's all how well your class/trait synergy and deck is.
            2. There will be rewards and various tournaments that will happen.
            3. You will be able to do the Proving Ground without any other players being online.
      
